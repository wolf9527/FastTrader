#include "stdafx.h"
#include "BreakNKStrategy.h"
#include <boost/make_shared.hpp>
#include <boost/dll/alias.hpp>
#include <boost/dll.hpp>
#include "../Log/logging.h"
#include <boost/date_time/gregorian/gregorian.hpp>
#include "../FacilityBaseLib/Container.h"

BreakNKStrategy::BreakNKStrategy(const std::string& sid)
	:IStrategy(sid)
{
	LOGINIT("BreakNKStrategy");
}


BreakNKStrategy::~BreakNKStrategy()
{
}

void BreakNKStrategy::Initialize()
{
	std::map<std::string, int>& InstrumentHandsMap = GetInstruments();
	for (auto iIter = InstrumentHandsMap.begin(); iIter != InstrumentHandsMap.end(); ++iIter)
	{
		BreakNK_Params& params = m_ParamsMap[iIter->first];
		params.InstrumentID = iIter->first;
		params.pInstrument = GetDataStoreFace()->PrepareData(iIter->first, InstrumentData::dataK,
			params.nKtype, params.nHistoryKCount);
		KdataContainer& kdata = params.pInstrument->GetKData(params.nKtype);

		params.pKLine = boost::make_shared<CKLine>(&kdata);

		InstrumentInfo info;
		InstrumentContainer::get_instance().GetInstrumentInfo(iIter->first.c_str(), &info);

		params.PriceTick=info.PriceTick;
	}
}

void BreakNKStrategy::OnTick(boost::shared_ptr<Tick> tick)
{
	auto paramIter = m_ParamsMap.find(tick->InstrumentID());
	if (paramIter==m_ParamsMap.end())
	{
		return;
	}
	BreakNK_Params& params = paramIter->second;

	//首先判断一下,是否到了平仓时间;
	boost::posix_time::ptime tickTime = boost::posix_time::from_time_t(tick->UpdateTime);
	if (tickTime.time_of_day()>=params.DayClosePositionTime)
	{
		if (tickTime.time_of_day()>=params.NightClosePositionTime)
		{
			//夜盘平仓;
			CloseAllPositions();
			return;
		}
		if (tickTime.time_of_day()<boost::posix_time::time_duration(14,59,59))
		{
			//日盘平仓;
			CloseAllPositions();
			return;
		}
	}

	if (params.openOrder && params.openOrder->OrderStatus == OST_AllTraded)
	{
		if (!params.openInputOrderAction)
		{
			params.openInputOrderAction = SafeCancelOrder(params.openOrder, params.openInputOrder);
		}
	}

	if (params.closeOrder && params.closeOrder->OrderStatus == OST_AllTraded)
	{
		if (!params.closeInputOrderAction)
		{
			params.closeInputOrderAction = SafeCancelOrder(params.closeOrder, params.closeInputOrder);
		}
	}

	int signedPosition = GetSignedPosition(params.InstrumentID);
	if (signedPosition>0)
	{
		//持有多仓;
		double PositionCost = 0;
		if (GetPositionCost(params.InstrumentID,PositionCost))
		{
			bool bNeedSell = false;
			double profit = tick->LastPrice - PositionCost;
			int profitJumps = profit / params.PriceTick;
			if (profitJumps>params.nMaxProfitJumps)
			{
				//更新当前最大盈利点;
				params.nMaxProfitJumps = profitJumps;
			}
			if (profitJumps > 0)
			{
				//判断要不要进行移动止盈;
				bool bMoreBaseProfit = profitJumps > params.nStopProfitJumps; //盈利点大于移动止盈基点;
				bool bMoveStopProfit =
					params.nMaxProfitJumps - profitJumps > params.nMoveProfitJumps;//盈利的回撤已经超过移动止盈点;
				if (bMoreBaseProfit && bMoveStopProfit)
				{
					bNeedSell = true;
				}
			}
			else if (profitJumps<0)
			{
				if (std::abs(profitJumps)>=params.nStopLossJumps)
				{
					//止损;
					bNeedSell = true;
				}
			}

			//时间止盈;
			if (params.openTrade && profitJumps>0 && profitJumps<params.nMaxProfitJumps)
			{
				//成交时间;
				boost::posix_time::ptime ptTrade(
					boost::gregorian::date_from_iso_string(params.openTrade->TradeDate),
					boost::posix_time::duration_from_string(params.openTrade->TradeTime)
					);
				if( (tickTime - ptTrade) > boost::posix_time::seconds(params.nStopProfitSeconds))
				{
					bNeedSell = true;
				}
			}
			//止盈平仓;
			if (bNeedSell && !params.closeInputOrder)
			{
				params.closeInputOrder =
					OrderInsert(params.InstrumentID, params.nMaxPositions,
					tick->BidPrice[0], OF_CloseToday, D_Sell, GetMainTrader());
			}
		}
	}
	else if (signedPosition<0)
	{
		//持有空仓;
		//持有多仓;
		double PositionCost = 0;
		if (GetPositionCost(params.InstrumentID, PositionCost))
		{
			bool bNeedBuy = false;
			double profit = PositionCost - tick->LastPrice;
			int profitJumps = profit / params.PriceTick;
			if (profitJumps > params.nMaxProfitJumps)
			{
				//更新当前最大盈利点;
				params.nMaxProfitJumps = profitJumps;
			}
			if (profitJumps > 0)
			{
				//判断要不要进行移动止盈;
				bool bMoreBaseProfit = profitJumps > params.nStopProfitJumps; //盈利点大于移动止盈基点;
				bool bMoveStopProfit =
					params.nMaxProfitJumps - profitJumps > params.nMoveProfitJumps;//盈利的回撤已经超过移动止盈点;
				if (bMoreBaseProfit && bMoveStopProfit)
				{
					bNeedBuy = true;
				}
			}
			else if (profitJumps < 0)
			{
				if (std::abs(profitJumps) >= params.nStopLossJumps)
				{
					//止损;
					bNeedBuy = true;
				}
			}

			//时间止盈;
			if (params.openTrade && profitJumps>0 && profitJumps < params.nMaxProfitJumps)
			{
				//成交时间;
				boost::posix_time::ptime ptTrade(
					boost::gregorian::date_from_iso_string(params.openTrade->TradeDate),
					boost::posix_time::duration_from_string(params.openTrade->TradeTime)
					);
				if ((tickTime - ptTrade) > boost::posix_time::seconds(params.nStopProfitSeconds))
				{
					bNeedBuy = true;
				}
			}
			//止盈平仓;
			if (bNeedBuy && !params.closeInputOrder)
			{
				params.closeInputOrder =
					OrderInsert(params.InstrumentID, params.nMaxPositions,
					tick->BidPrice[0], OF_CloseToday, D_Buy, GetMainTrader());
			}
		}
	}
	else
	{
		//开仓逻辑;
		int nIndex = params.pKLine->get_kdata()->size() - 1;
		if (nIndex>1 && nIndex <= params.nHistoryKCount)
		{
			return;
		}
		double dMax=0, dMin=0;
		if (!params.pKLine->min_max_info(nIndex - 1 - params.nHistoryKCount, nIndex - 1, &dMin, &dMax))
		{
			LOGDEBUG("KLine get mini max error!!");
			return;
		}
		//K线的的振幅区间;
		int nRangeJumps = (dMax - dMin) / params.PriceTick;
		if (nRangeJumps<params.nMinMaxRange)
		{
			//在参数的震荡区间之内;
			//描述一段突破;
			if (tick->LastPrice >(dMax + params.nRangeJumps))
			{
				//价格突破高点;
				Buy(params.InstrumentID, params.nMaxPositions, tick->AskPrice[0],nIndex);
			}
			else if (tick->LastPrice <(dMin - params.nRangeJumps))
			{
				//价格突破低点;
				Sell(params.InstrumentID, params.nMaxPositions, tick->BidPrice[0], nIndex);
			}
			else
			{
				LOGDEBUG("突破的力度不够,或者没有突破{} {} {}",tick->LastPrice,dMin,dMax);
			}
		}
		else
		{
			LOGDEBUG("从{} {} 不是震荡区间 dMin={} dMax={}", 
				nIndex - 1 - params.nHistoryKCount, nIndex - 1, dMin, dMax);
		}
	}
}

void BreakNKStrategy::OnOrder(boost::shared_ptr<Order> order)
{
	if (order->ErrorID!=0)
	{
		LOGDEBUG("报单回报出错 {}",order->ErrorMsg);
		return;
	}
	auto pmIter = m_ParamsMap.find(order->InstrumentID);
	if (pmIter==m_ParamsMap.end())
	{
		LOGDEBUG("合约出错 {},非本策略报单", order->InstrumentID);
		return ;
	}
	auto& params = pmIter->second;
	if (order->OrderStatus == OST_Canceled)
	{
		if (params.openInputOrder && order->OrderRef == params.openInputOrder->OrderRef)
		{
			params.openInputOrder = nullptr;
			params.openInputOrderAction = nullptr;
			if (order->Direction ==  D_Buy)
			{
				Buy(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->AskPrice[0], params.pKLine->get_kdata()->size());
			}
			else
			{
				
				Sell(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->BidPrice[0], params.pKLine->get_kdata()->size());
			}
		}
		else if (params.closeInputOrder && order->OrderRef == params.closeInputOrder->OrderRef)
		{
			params.closeInputOrderAction = nullptr;
			params.closeInputOrder = nullptr;
			if (order->Direction == D_Buy)
			{
				Buy(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->AskPrice[0], params.pKLine->get_kdata()->size());
			}
			else
			{
				Sell(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->BidPrice[0], params.pKLine->get_kdata()->size());
			}
		}
		else
		{
			LOGDEBUG("OrderRef出错 {},非本策略报单", order->OrderRef);
		}
	}
	else
	{
		if (order->CombOffsetFlag[0]==OF_Open)
		{
			params.openOrder = order;
		}
		else
		{
			params.closeOrder = order;
		}
	}
}

void BreakNKStrategy::OnTrade(boost::shared_ptr<Trade> trade)
{
	auto pmIter = m_ParamsMap.find(trade->InstrumentID);
	if (pmIter==m_ParamsMap.end())
	{
		return;
	}
	if (trade->OffsetFlag == OF_Open)
	{
		pmIter->second.openTrade = trade;
	}
	else
	{
		auto& params = pmIter->second;
		params.openInputOrder = nullptr;
		params.closeInputOrder = nullptr;
		params.openInputOrderAction = nullptr;
		params.closeInputOrderAction = nullptr;
		params.openTrade = nullptr;
	}
}



void BreakNKStrategy::Finalize()
{
	
}

bool BreakNKStrategy::Buy(const std::string& InstrumentID, int Volume, double Price, int nKIndex)
{
	BreakNK_Params& params = m_ParamsMap[InstrumentID];
	boost::shared_ptr<InvestorPosition> pPosition = GetPosition(InstrumentID);

	if (!pPosition || pPosition->PosiDirection ==  PD_Long)
	{
		if (!params.openInputOrder)
		{
			//开仓;
			params.nSignalIndex = nKIndex;
			params.openInputOrder = OrderInsert(InstrumentID, Volume, Price, OF_Open, D_Buy, GetMainTrader());
			return true;
		}
	}
	else
	{
		if (pPosition || pPosition->PosiDirection ==  PD_Short)
		{
			//平仓;
			if (!params.closeInputOrder)
			{
				params.nSignalIndex = nKIndex;
				params.closeInputOrder = ClosePosition(*pPosition);
				return true;
			}
		}
	}
	return false;
}

bool BreakNKStrategy::Sell(const std::string& InstrumentID, int Volume, double Price, int nKIndex)
{
	BreakNK_Params& params = m_ParamsMap[InstrumentID];
	boost::shared_ptr<InvestorPosition> pPosition = GetPosition(InstrumentID);

	if (!pPosition || pPosition->PosiDirection == PD_Short)
	{
		if (!params.openInputOrder)
		{
			//开仓;
			params.nSignalIndex = nKIndex;
			params.openInputOrder = OrderInsert(InstrumentID, Volume, Price, OF_Open, D_Sell, GetMainTrader());
			return true;
		}
	}
	else
	{
		if (pPosition || pPosition->PosiDirection == PD_Long)
		{
			//平仓;
			if (!params.closeInputOrder)
			{
				params.nSignalIndex = nKIndex;
				params.closeInputOrder = ClosePosition(*pPosition);
				return true;
			}
		}
	}
	return false;
}


boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid)
{
	return boost::make_shared<BreakNKStrategy>(sid);
}




boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid);
BOOST_DLL_ALIAS(CreateStrategy, create_strategy)