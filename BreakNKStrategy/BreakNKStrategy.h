#pragma once

#include "../SimpleStrategyLib/IStrategy.h"

#include "../TechLib/MA.h"
#include "../FacilityBaseLib/InstrumentData.h"
#include "../FacilityBaseLib/Express.h"

#include <boost/date_time/posix_time/ptime.hpp>
#include "../TechLib/KLine.h"

struct BreakNK_Params
{
	boost::shared_ptr<InputOrder> openInputOrder;
	boost::shared_ptr<InputOrder> closeInputOrder;

	boost::shared_ptr<InputOrderAction> openInputOrderAction;

	boost::shared_ptr<InputOrderAction> closeInputOrderAction;

	boost::shared_ptr<Order> openOrder;
	boost::shared_ptr<Order> closeOrder;

	boost::shared_ptr<Trade> openTrade;

	boost::shared_ptr<CKLine> pKLine;

	std::string InstrumentID;

	double PriceTick;

	int nRangeJumps;//向上或者向下突破nRangeJumps跳,作为突破标志;

	int nStopProfitJumps;//移动止盈起始点;

	int nMaxProfitJumps;//历史最大盈利点;

	int nMoveProfitJumps;//移动止盈点;

	int nStopLossJumps;//固定止损;

	//定时平仓时间点;
	boost::posix_time::time_duration DayClosePositionTime;
	boost::posix_time::time_duration NightClosePositionTime;

	//时间止盈;
	int nStopProfitSeconds;

	//合约的数据;
	boost::shared_ptr<InstrumentData> pInstrument;

	//要加载多长的历史数据;
	int nHistoryKCount;
	//振荡区间的范围;
	int nMinMaxRange;

	int nMaxPositions;//最大持仓数量;

	int nSignalIndex;

	int nKtype;

	BreakNK_Params()
	{
		nSignalIndex = -1;
		nMaxPositions = 1;

		nKtype = ktypeMin;

		nHistoryKCount = 30;

		nMinMaxRange = 5;


		nRangeJumps=2;//向上或者向下突破nRangeJumps跳,作为突破标志;

		nStopProfitJumps=2;//移动止盈起始点;

		nMaxProfitJumps = 0;//历史最大盈利点;

		nMoveProfitJumps=2;//移动止盈点;

		nStopLossJumps=4;//固定止损;

		nStopProfitSeconds = 180;//持续180秒后止盈;

		DayClosePositionTime = boost::posix_time::duration_from_string("14:59:00");
		DayClosePositionTime = boost::posix_time::duration_from_string("23:29:00");
	}
};

class BreakNKStrategy :
	public IStrategy
{
public:
	explicit BreakNKStrategy(const std::string& sid);
	virtual ~BreakNKStrategy();

	virtual void OnTick(boost::shared_ptr<Tick> tick) ;

	virtual void OnOrder(boost::shared_ptr<Order> order) ;

	virtual void OnTrade(boost::shared_ptr<Trade> trade) ;

	virtual void Initialize() ;

	virtual void Finalize() ;
public:
	bool Buy(const std::string& InstrumentID,int Volume,double Price,int nKIndex);
	bool Sell(const std::string& InstrumentID, int Volume, double Price, int nKIndex);
protected:
	std::map<std::string, BreakNK_Params> m_ParamsMap;
};

