TEMPLATE = lib
QT -= core gui GL
TARGET = sqlite3

CONFIG(debug,debug|release){
    DEFINES +=_DEBUG
    DESTDIR  =../build/v120/debug/
    LIBS += -L../build/v120/debug/
} else {
    DESTDIR = ../build/v120/release/
    LIBS += -L../build/v120/release/
}
macx{
    QMAKE_MAC_SDK = macosx10.11
}
include(sqlite3.pri)
