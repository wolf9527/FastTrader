#ifndef CONFIGLIB_H
#define CONFIGLIB_H


#ifndef Q_DECL_EXPORT
#  if defined(_WIN32) || defined(WIN32) || defined(Q_CC_RVCT)
#    define Q_DECL_EXPORT __declspec(dllexport)
#  elif defined(QT_VISIBILITY_AVAILABLE)
#    define Q_DECL_EXPORT __attribute__((visibility("default")))
#  endif
#  ifndef Q_DECL_EXPORT
#    define Q_DECL_EXPORT
#  endif
#endif
#ifndef Q_DECL_IMPORT
#  if defined(_WIN32) || defined(Q_CC_NOKIAX86) || defined(Q_CC_RVCT)
#    define Q_DECL_IMPORT __declspec(dllimport)
#  else
#    define Q_DECL_IMPORT
#  endif
#endif

#if defined(CONFIGLIB_EXPORTS)
#  define CONFIGLIB_API Q_DECL_EXPORT
#else
#  define CONFIGLIB_API Q_DECL_IMPORT
#endif
#endif // CONFIGLIB_H
