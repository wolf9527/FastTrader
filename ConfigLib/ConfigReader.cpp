
#include "ConfigReader.h"



ConfigReader::ConfigReader(void)
	:document(NULL),root(NULL)
{

}


ConfigReader::~ConfigReader(void)
{
	Unload();
}

//要传递全路径;
bool ConfigReader::Load( const string& config_file/*="brokers.xml"*/,bool bForceLoad )
{
	//创建一个XML的文档对象。
	if (bForceLoad)
	{
		Unload();
	}
	if (document!=NULL)
	{
		return true;
	}
	document = new TiXmlDocument(config_file.c_str());
	if (NULL==document)
	{
		return false;
	}
	if (!document->LoadFile())
	{
		return false;
	}
	
	//获得根元素，即Persons。
	root = document->RootElement();
	if (NULL==root)
	{
		return false;
	}
	return true;
}

std::string ConfigReader::GetDefault()
{
	return "";
}

bool ConfigReader::Unload()
{
	if (NULL!=document)
	{
		delete document;
		document=NULL;
	}
	root = NULL;
	return true;
}

std::string ConfigReader::GetFileName() const
{
	if (document)
	{
		return document->Value();
	}
	return "";
}




