CONFIG -= qt
TARGET = ConfigLib
TEMPLATE = lib
INCLUDEPATH += ../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += ../
DEFINES *= CONFIGLIB_EXPORTS
linux-g++|macx-g++{
    QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
    QMAKE_CFLAGS = -arch x86_64 -lpthread
    INCLUDEPATH += /home/rmb338/boost_1_64_0
    LIBS += -L/home/rmb338/boost_1_64_0/stage/lib
}

win32{
    DEFINES += WIN32_LEAN_AND_MEAN _CRT_SECURE_NO_WARNINGS
    INCLUDEPATH += F:/boost_1_63_0
    LIBS += -LF:/boost_1_63_0/lib32-msvc-12.0
}

CONFIG(debug, debug|release) {
        LIBS += -L../build/v120/debug  -ltinyxml
        DESTDIR = ../build/v120/debug
} else {
        LIBS += -L../build/v120/release  -ltinyxml
        DESTDIR = ../build/v120/release
}
HEADERS += \
    XmlParserBase.h \
    ViewConfig.h \
    configlib.h \
    AllConfig.h \
    tinyxml/tinyxml.h \
    tinyxml/tinystr.h \
    BrokerConfigReader.h \
    DataStoreConfigReader.h \
    UserConfigReader.h \
    ConfigReader.h \
    FieldInterceptor.h

SOURCES += \
    XmlParserBase.cpp \
    ViewConfig.cpp \
    configlib.cpp \
    AllConfig.cpp \
    BrokerConfigReader.cpp \
    DataStoreConfigReader.cpp \
    UserConfigReader.cpp \
    ConfigReader.cpp \
    FieldInterceptor.cpp
