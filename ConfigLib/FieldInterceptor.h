﻿#ifndef _FIELD_INTERCEPTOR_H_
#define _FIELD_INTERCEPTOR_H_
#include <PlatformStruct.h>
#include "Order.h"
#include "InvestorPosition.h"
#include "Trade.h"
#include "TradingAccount.h"
#include "ParkOrder.h"
#include "configlib.h"

#include <string>
#include <map>
using namespace std;

enum SLH_CTP
{
	//从这里开始，是合约列表
	SLH_ExchangeInstID        =97,
	SLH_ExchangeName          =98,
	SLH_ExchangeProperty      =99,
	SLH_ProductID             =100,
	SLH_ProductClass          =101,
	SLH_DeliveryYear          =102,   //交割年份
	SLH_DeliveryMonth         =103,   //交割月
	SLH_MaxMarketOrderVolume  =104,   //市价单最大下单量
	SLH_MinMarketOrderVolume  =105,   //市价单最小下单量
	SLH_MaxLimitOrderVolume   =106,
	SLH_MinLimitOrderVolume   =107,
	SLH_VolumeMultiple        =108,
	SLH_PriceTick             =109,
	SLH_CreateDate            =110,
	SLH_OpenDate              =111,
	SLH_ExpireDate            =112,
	SLH_StartDelivDate        =113,
	SLH_EndDelivDate          =114,
	SLH_InstLifePhase         =115,
	SLH_IsTrading             =116,
	SLH_PositionType          =117,
	SLH_PositionDateType      =118,
	SLH_LongMarginRatio       =119,
	SLH_ShortMarginRatio      =120,//空头保证金率
	//SLH_LASTPRICE             =121,
	SLH_SettlementPrice       =122,

	SLH_UserID                =200,
	SLH_LoginTime             =201,
	SLH_BrokerID              =202,
	SLH_InvestorID			  =203,

	SLH_SystemName            =204,
	SLH_FrontID               =205,
	SLH_MaxOrderRef           =207,
	SLH_SHFETime              =208,
	SLH_DCETime               =209,
	SLH_CZCETime              =210,
	SLH_FFEXTime              =211,

	SLH_PreMortgage           =301,
	SLH_PreCredit             =302,
	SLH_PreDeposit            =303,
	SLH_PreBalance            =304,
	SLH_PreMargin             =305,
	SLH_InterestBase          =306,
	SLH_Interest              =307,
	SLH_Deposit               =308,
	SLH_Withdraw               =309,
	SLH_FrozenMargin          =310,
	SLH_FrozenCash            =311,
	SLH_FrozenCommission      =312,
	SLH_CurrMargin            =313,
	SLH_CashIn                =314,
	SLH_Commission            =315,
	SLH_CloseProfit           =316,
	SLH_PositionProfit        =317,
	SLH_Balance               =318,
	SLH_Available             =319,
	SLH_WithdrawQuota         =320,
	SLH_Reserve               =321,
	SLH_SettlementID          =322,
	SLH_Credit                =323,
	SLH_Mortgage              =324,
	SLH_ExchangeMargin        =325,
	SLH_DeliveryMargin        =326,
	SLH_ExchangeDeliveryMargin=327,
	SLH_PositionDate		  =328,

	SLH_PosiDirection         =401,
	SLH_HedgeFlag             =402,
	SLH_YdPosition            =403,
	SLH_Position              =404,
	SLH_LongFrozen            =405,
	SLH_ShortFrozen           =406,
	SLH_LongFrozenAmount      =407,
	SLH_ShortFrozenAmount     =408,
	SLH_OpenVolume            =409,
	SLH_CloseVolume           =410,
	SLH_OpenAmount            =411,
	SLH_CloseAmount           =412,
	SLH_PositionCost          =413,
	SLH_UseMargin             =415,
	SLH_PreSettlementPrice    =423,
	SLH_OpenCost              =425,
	SLH_CombPosition          =427,
	SLH_CombLongFrozen        =428,
	SLH_CombShortFrozen       =429,
	SLH_CloseProfitByDate     =430,
	SLH_CloseProfitByTrade    =431,
	SLH_TodayPosition         =432,
	SLH_MarginRateByMoney     =433,
	SLH_MarginRateByVolume    =434,

	SLH_OrderPriceType        =499,
	SLH_Direction			  =500,
	SLH_CombOffsetFlag_0      =501,
	SLH_CombOffsetFlag_1      =502,
	SLH_CombOffsetFlag_2      =503,
	SLH_CombOffsetFlag_3      =504,
	SLH_CombOffsetFlag_4      =505,
	SLH_CombHedgeFlag_0       =506,
	SLH_CombHedgeFlag_1       =507,
	SLH_CombHedgeFlag_2       =508,
	SLH_CombHedgeFlag_3       =509,
	SLH_CombHedgeFlag_4       =510,
	SLH_LimitPrice			  =511,
	SLH_VolumeTotalOriginal   =512,
	SLH_TimeCondition         =513,
	SLH_GTDDate				  =514,
	SLH_VolumeCondition	      =515,
	SLH_MinVolume             =516,
	SLH_ContingentCondition   =517,
	SLH_StopPrice			  =518,
	SLH_ForceCloseReason	  =519,
	SLH_IsAutoSuspend		  =520,
	SLH_BusinessUnit		  =521,
	SLH_UserForceClose		  =523,

	SLH_RequestID			  =524,

	SLH_PlateSerial           =599,
	SLH_TradeDate			  =600,
	SLH_TradeTime			  =601,
	SLH_TradeCode			  =602,
	SLH_SessionID			  =603,
	SLH_BankID				  =604,
	SLH_BankBranchID		  =605,
	SLH_BankAccType			  =606,
	SLH_BankAccount			  =607,
	SLH_BankSerial			  =608,
	SLH_BrokerBranchID		  =609,
	SLH_FutureAccType		  =610,
	SLH_AccountID			  =611,
	SLH_FutureSerial		  =612,
	SLH_IdCardType			  =613,
	SLH_IdentifiedCardNo	  =614,
	SLH_CurrencyID			  =615,
	SLH_TradeAmount			  =616,
	SLH_CustFee			      =617,
	SLH_BrokerFee			  =618,

	SLH_AvailabilityFlag	  =619,
	SLH_OperatorCode		  =620,
	SLH_BankNewAccount		  =621,
	SLH_ErrorID				  =622,
	SLH_ErrorMsg			  =623,

	SLH_OrderRef			  =700,
	SLH_TradeID				  =701,
	SLH_OrderSysID			  =702,
	SLH_ParticipantID		  =703,
	SLH_ClientID			  =704,
	SLH_TradingRole		      =705,
	SLH_OffsetFlag            =706,
	SLH_Price				    =709,
	SLH_Volume					=710,
	SLH_TradeType				=711,
	SLH_PriceSource				=712,
	SLH_TraderID				=713,
	SLH_OrderLocalID			=714,
	SLH_ClearingPartID			=715,
	SLH_SequenceNo				=716,
	SLH_BrokerOrderSeq			=717,
	SLH_InstallID               =800,
	SLH_OrderSubmitStatus		=801,
	SLH_NotifySequence			=802,

	SLH_OrderSource				=803,
	SLH_OrderStatus				=804,
	SLH_OrderType				=805,
	SLH_VolumeTraded			=806,
	SLH_VolumeTotal				=807,
	SLH_InsertDate				=808,
	SLH_InsertTime				=809,
	SLH_ActiveTime				=810,
	SLH_SuspendTime				=811,
	SLH_UpdateTime				=812,
	SLH_CancelTime				=813,
	SLH_ActiveTraderID			=814,
	SLH_UserProductInfo			=815,
	SLH_StatusMsg				=816,
	SLH_ActiveUserID			=817,
	SLH_RelativeOrderSysID		=818,
	SLH_ParkedOrderID			=819,
	SLH_UserType				=820,
	SLH_Status					=821,
	SLH_BankName				=822,
	SLH_TradingDay				=823,
	SLH_CombInstrumentID		=824,
	SLH_PositionProfitByDate	=825,
	SLH_Margin					=826,
	SLH_LegID					=827,
	SLH_TotalAmt				=828,
	SLH_LegMultiple				=829,


	SLH_ZCETotalTradedVolume=830,
	SLH_AcitveUserID=832,

	//静态权益;
	SLH_StaticBalance=833,
	//动态权益;
	SLH_DynamicBalance=834,
	//风险度=持仓保证金/动态权益;
	SLH_RiskRatio=835,
};

//字段解释器;
class CONFIGLIB_API FieldInterceptor
{
public:
	FieldInterceptor(void);
	virtual ~FieldInterceptor(void);
public:
	virtual string  get_disp_str(int id,const string& field_type_name="");
	virtual string  get_disp_str(const BASEDATA& rsp,int id);
	virtual string  get_disp_str(const MINUTE& rsp,int id);
	virtual string  get_disp_str(const Tick& rsp,int id);
// 	virtual string  get_disp_str(const TradeUser& rsp,int id);
// 	virtual string  get_disp_str(const InvestorPosition& ta,int id);
// 	virtual string  get_disp_str(const TradingAccount& ta,int id);
// 	virtual string  get_disp_str(const Trade& ta,int id);
// 	virtual string  get_disp_str(const ParkedOrder& ta,int id);
//	virtual string  get_disp_str(const OrderField& rsp,int id);
	static map<char,string> PriceTypeMap;
	static map<char,string> ProductTypeMap;
	static map<char,string> OrderStatusTypeMap;
	static map<char,string> OffsetFlagTypeMap;
	static map<char,string> ContingentConditionTypeMap;

	static map<char,string> get_price_type_map();
	static string get_price_type(char type_id);
	static string get_product_type(char type_id);
	static string get_order_status_type(char type_id);
	
	static string get_offset_flag_type(char type_id);
	static string get_contingent_cond_type(char type_id);
	static string get_hedge_flag_type(char type_id);
	static string get_direction_type(char type_id);
	static string get_volume_cond_type(char type_id);
	static string get_time_cond_type(char type_id);
	static string get_force_close_reason(char type_id);
	static string get_position_type(char type_id);
	static string get_order_submit_status_type(char type_id);
	static string get_inst_life_phase_type(char type_id);
	static string get_order_type(char type_id);
	static string get_exchange_property(char type_id);
	static string get_position_date_type(char type_id);
	static string get_disconnect_reason(int reason);
	static string get_price_source_type(char price_src);
	static string get_trade_type(char trade_type);
	static string get_trade_role_type(char trade_role);
	static string get_instrument_status(char status);
	static string get_park_order_status(char status);
	static std::string get_position_direction_type(char posiDirection);
	static std::string get_event_type(int eventId);
};

CONFIGLIB_API std::string  GetVariantDispString(const InputOrder& rsp,int id);
CONFIGLIB_API std::string  GetVariantDispString(const BASEDATA& rsp,int id);
//string  GetVariantDispString(const TransferSerial& rsp,int id);
struct UserInfo;
CONFIGLIB_API std::string  GetVariantDispString(const UserInfo& rsp,int id);
CONFIGLIB_API std::string  GetVariantDispString(const InvestorPosition& ta,int id);
CONFIGLIB_API std::string  GetVariantDispString(const InvestorPositionDetail& ta,int id);
CONFIGLIB_API std::string  GetVariantDispString(const InvestorPositionCombineDetail& ta,int id);
CONFIGLIB_API std::string  GetVariantDispString(const TradingAccount& ta,int id);
CONFIGLIB_API std::string  GetVariantDispString(const Trade& ta,int id);
//string  GetVariantDispString(const ContractBank& ta,int id);
CONFIGLIB_API std::string  GetVariantDispString(const ParkedOrder& ta, int id);
CONFIGLIB_API std::string  GetVariantDispString(const MINUTE& rsp,int id);
CONFIGLIB_API std::string  GetVariantDispString(const Tick& rsp,int id);
//string  GetVariantDispString(const OrderField& rsp,int id);

#endif

