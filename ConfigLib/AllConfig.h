﻿#ifndef _POSITION_CONFIG_H_
#define _POSITION_CONFIG_H_
#include "ViewConfig.h"
#include <PlatformStruct.h>
#include <string>
using namespace std ;
//#include "../ServiceLib/Broker.h"
#include "../TechLib/Tech.h"
#include "../FacilityBaseLib/Express.h"
#include "../FacilityBaseLib/ExchangeContainer.h"
#include "ServerInfo.h"

//数据源配置项;
struct host_info
{
	string name;
	string type;
	string host;
	int port;
	string username;
	string password;
};

const std::string m_ListCtrlColumns="m_ListCtrlColumns";



struct tagListConfig
{
	std::string m_strTitle;
	vector<ColumnInfo> m_vecColumnItems;//列头;

};
//持仓;
struct tagPositionListConfig:public tagListConfig
{
	tagPositionListConfig(){ m_strTitle = "持仓"; m_nPositionType = 0; }
	int m_nPositionType;
};

//委托;
struct tagDelegateListConfig:public tagListConfig
{
	tagDelegateListConfig(){m_strTitle="委托";}
};
//预埋;
struct tagEmbeddedListConfig:public tagListConfig
{
	tagEmbeddedListConfig(){m_strTitle="预埋";}
};
//经纪公司;
struct tagBrokerListConfig
{
	std::vector<ServerInfo> m_Broker;
};
//用户信息;
struct tagUsersListConfig:public tagListConfig
{
	tagUsersListConfig(){m_strTitle="用户";}
};
struct tagOrderListConfig:public tagListConfig
{
	tagOrderListConfig(){m_strTitle="下单";}
};

struct tagUserFundsListConfig:public tagListConfig
{
	tagUserFundsListConfig(){m_strTitle="资金";}
};

struct tagMakeOrderBoardConfig:public tagListConfig
{
	tagMakeOrderBoardConfig(){m_strTitle="标准下单板";}
	bool m_bKeepLots;//保留手数;
	bool m_bKeepInstrument;//保留合约;
};

struct tagTradeHistoryListConfig:public tagListConfig
{
	tagTradeHistoryListConfig(){m_strTitle="历史交易记录";}
};

struct tagBank2FutureConfig:public tagListConfig
{
	tagBank2FutureConfig(){m_strTitle="银期转帐";}
	int transfer_type;
};

struct UserAccountConfig
{
	std::string InvestorID;
	std::string Password;
	std::string BrokerID;
	bool auto_login;
	bool save_password;
	int sid;
	bool operator()(const UserAccountConfig& _Left, const UserAccountConfig& _Right) const
	{	// apply operator< to operands
		return (_Left.sid < _Right.sid);
	}
	bool operator<(const UserAccountConfig& _Right) const
	{	// apply operator< to operands
		return (this->sid < _Right.sid);
	}
};

struct tagLoginedUserConfig
{
	tagLoginedUserConfig()
	{
		m_strTitle="HistoryUsers";
	}
	std::string m_strTitle;
	std::string lastInvestorID;
	std::map<std::string,UserAccountConfig> m_mapUsers;
};

//主界面的配置;
struct tagMainUiConfig:public tagListConfig
{
	bool bRestoreSession;
};

struct tagInstrumentListConfig:public tagListConfig
{
	tagInstrumentListConfig(){m_strTitle="行情";}
	int m_nGridLines;
	int m_bEnableSort;
	int m_bEnableTitleTip;
	int m_bEnableTabKey;
	int m_bEnableDragDrop;
	int m_bEnableSelection;
	int m_bEnableEdit;
	long m_lBkColor; //背景色
	long m_lTextColor;//文本色
	long m_lRiseColor;//上涨色
	long m_lFallColor;//下跌色
	long m_lPlaneColor;//平盘色
	long m_lDJColor;//时间坐标文本颜色
	long m_lStatusBorderColor;
	long m_lStatusTextColor;
	long m_lStatusSelectedColor;
	long m_lTextBkColor;
	std::string m_strSortAsc;//升序排列
	std::string m_strSortDesc;//降序排列
	std::string m_strTabTitle;//窗口标题
	int m_nColSort;   //被排序的那一列
	int m_bSortAscend;//排序标记
	int m_nWorkTabHeight;//下面的高度
	work_tab_item m_ActiveTab;//活动页;
	PaneConfig m_paneConfig;
};

struct tagBrokerServerConfig
{
	ServerInfo broker;//经纪公司;
	std::string network_type; //tcp or udp;
	std::string proxy_type;
	std::map<std::string, host_info> m_mapProxyConfig;//代理;
	//bool operator()(const tagBrokerServerConfig& b)
	//{
	//	return broker.operator ()(b.broker);
	//}
	//bool operator ==(const tagBrokerServerConfig& b)
	//{
	//	return broker.operator()(b.broker);
	//}
};

struct tagBrokersListConfig:public tagListConfig
{
	std::string last_broker_id;
	std::vector<tagBrokerServerConfig> m_vecBrokers;
};
struct tagDataSourceConfig:public tagListConfig
{
	std::string default_username;
	std::map<std::string,host_info> m_mapUser2Proxy;//代理;
	std::string file_store_path;//文件路径;
	std::map<std::string,std::map<std::string,host_info> > m_mapUser2Database;//数据库;
};
struct tagWebInfoConfig:tagListConfig
{
	std::vector<std::string> m_vecUrlList;
};

struct tagClientConfig:public tagListConfig
{
	bool m_bSingleton;//是否只允许一个实例运行
	int m_nMultiUsers;//支持同时登录的用户数
	bool m_bAutoRun;//开机自动运行
	int  m_nCheckVersion;//多长时间检查一次新版本,以秒为单位
	int m_nCacheDays;//缓存多少天的数据
	int m_nRunTimes;//运行次数
	time_t m_tmLastCheckVersion;//上一次检查新版本的时间
	std::string m_strVersionServerUrl;//版本服务器IP地址
	std::string m_strVersionFilePath;//版本文件路径
	std::string m_strCheckSum;//校验和
	std::string m_strClientAppName;//本程序的名称
	std::string m_strTraderPath;//交易软件的路径
	std::string m_strConfigFileVersion;//配置文件版本号
	std::vector<std::string> m_vecDomains;//板块列表
	std::vector<std::string> m_vecGroups;//分组列表
};
//图形的配置属性
struct tagRTGraphConfig
{
	int m_nIndex;//合约索引号
	long m_lAxisColor;//坐标线颜色
	long m_lTimeColor;//横坐标颜色
	long m_lValueColor;//纵坐 标值的颜色
	map<int,long> m_ilLine2Color;//技术线坐标颜色,int表示技术指标的含义,long表示颜色值
};
//对话框的配置属性
struct tagRealTimeConfig:public tagListConfig
{
	int m_nMultiInstrumentCount;//显示多幅分时图
	int m_nTabWidth;
	std::vector<work_tab_item> m_vecTabItems;//状态栏的文字
	std::map<int,tagRTGraphConfig> m_igInstrument2Graph;//每个图的配置属性
	PaneConfig m_paneConfig;
};


//对话框的配置属性;
struct tagTickGraphConfig:public tagListConfig
{
	int m_nMultiInstrumentCount;//显示多幅分时图;
	int m_nTabWidth;
	std::vector<work_tab_item> m_vecTabItems;//状态栏的文字;
	std::map<int,tagRTGraphConfig> m_igInstrument2Graph;//每个图的配置属性;
	PaneConfig m_paneConfig;
};

struct tagKLineGraphConfig:public tagListConfig
{
	int m_nCurKLineMode;//K线形状;
	int	 m_nCurKType;//类型;
	int  m_nReportWhat;//报表类型;
	int m_nTabHeight;
	int m_nTabWidth;
	long m_lBkColor; //背景色;
	long m_lTextColor;//文本色;
	long m_lRiseColor;//上涨色;
	long m_lFallColor;//下跌色;
	long m_lPlaneColor;//平盘色;
	long m_lDJColor;//时间坐标文本颜色;
	long m_lStatusBorderColor;  //统计面板的状态栏边框颜色;
	long m_lStatusTextColor;    //统计面板的文本颜色;
	long m_lStatusSelectedColor;//统计面板的选中项颜色;
	long m_lNewLineColor;
	long m_lFallEntityColor;
	//LOGFONT m_fontTextFont;//字体
	std::vector<work_tab_item> m_vecTabItems;
	std::vector<int> m_vecTechLines;//技术指标线;
	PaneConfig m_paneConfig;
};
struct tagMultiSortConfig:public tagListConfig
{
	long m_lBkColor; //背景色;
	long m_lTextColor;//文本色;
	long m_lRiseColor;//上涨色;
	long m_lFallColor;//下跌色;
	long m_lPlaneColor;//平盘色;
	long m_lDJColor;//时间坐标文本颜色;
	long m_lStatusBorderColor;  //统计面板的状态栏边框颜色;
	long m_lStatusTextColor;    //统计面板的文本颜色;
	long m_lStatusSelectedColor;//统计面板的选中项颜色;
	//LOGFONT m_fontTextFont;//字体;
	std::vector<work_tab_item> m_vecTabItems;
	long m_lSortClass;
	PaneConfig m_paneConfig;
	int m_nSortItemCount;//每种排名项的个数;
	bool m_bAsc;//是升序还是降序;
};
struct tagGroupConfig:public tagListConfig
{
	std::vector<std::string> m_vecDomains;
	std::vector<std::string> m_vecGroups;
};

//策略运行时参数;
struct tagStrategyParams
{
	std::string name;
	std::string investor_id;//投资者ID;
	std::vector<std::string> sub_investor_id;//子帐户;
	std::string beginRunTime;//开始运行时间;
	std::vector<std::string> instruments;//合约;需要订阅行情的合约(可以写品种);
	std::size_t runDuration;//运行周期;//比如，每天，每周,每小时 等这个值是秒;
};
struct tagStrategyConfig:public tagListConfig
{
	std::string m_strLastStrategyDirectory;//上一次打开过的策略目录;
	std::string m_strStrategyFileExt;//策略文件扩展名;
	std::vector<std::string> m_vecStrategyFileItems;
	PaneConfig m_paneConfig;
	std::vector<tagStrategyParams> m_vecStrategyParams;
};


struct tagMultiPaneConfig:public tagListConfig
{
	// 显示边框
	int IsShowBorder;
	// 鼠标指针
	int IsCustomCursor;
	// 客户区边框
	int IsShowClientEdge;
	// 是否激活分割条
	int IsSplitterInActive;
	int IsShowImage;
	int IsHideSingleTab;
	int IsRemoveTabEnable;
	int IsDragTabEnable;
	int IsWatchActivityCtrl;
	int IsShowSelectedFont;
	int IsShowTabCloseButton;
	int IsShowTabMenuButton;
	int IsShowTabScrollButton;
	int IsShowCtrlBorder;

	int TabLayout;
	int TabBehavior;
	int SplitterDraggingMode;
	int EnableDockPos;
	int Style;

	int TabMinWidth;
	int TabMinHeight;
	int selectStyle;
	std::map<int,std::string> m_mapStyles;
};

enum enum_str_id
{
	slh_strs_begin=1000,
	nodata=1001,
	strategy_logicand=1002,
	strategy_logicor=1003,
	strategy_sdonce=1004,
	strategy_sdtwice=1005,
	strategy_sdthird=1006,
	strategy_sdforth=1007,
	strategy_sdfifth=1008,
	strategy_sdsixth=1009,
	strategy_sdseventh=1010,
	strategy_sdeighth=1011,
	strategy_sdninth=1012,
	strategy_sdtenth=1013,
	strategy_noselected=1014,
	strategy_noselectedtech=1015,
	strategy_optype_buy=1016,
	strategy_optype_sell=1017,
	strategy_optype_add=1018,
	strategy_optype_remove=1019,
	strategy_optype_addcash=1020,
	strategy_optype_removecash=1021,
	strategy_errfile=1022,
	strategy_errfilever=1023,
	multisort_aumount=1024,
	multisort_bsratiodesc=slh_strs_begin+25,
	multisort_bsratioasc=slh_strs_begin+26,
	multisort_volratio=slh_strs_begin+27,
	multisort_fall=slh_strs_begin+28,
	multisort_diff=slh_strs_begin+29,
	multisort_rise=slh_strs_begin+30,
	multisort_risemin5=slh_strs_begin+31,
	multisort_fallmin5=slh_strs_begin+32,
	bsratio=slh_strs_begin+33,
	bsdiff=slh_strs_begin+34,
	sell5=slh_strs_begin+35,
	sell4=slh_strs_begin+36,
	sell3=slh_strs_begin+37,
	sell2=slh_strs_begin+38,
	sell1=slh_strs_begin+39,
	buy1=slh_strs_begin+40,
	buy2=slh_strs_begin+41,
	buy3=slh_strs_begin+42,
	buy4=slh_strs_begin+43,
	buy5=slh_strs_begin+44,
	price_now=slh_strs_begin+45,
	price_ave=slh_strs_begin+46,
	price_diff=slh_strs_begin+47,
	price_open=slh_strs_begin+48,
	price_diff_percent=slh_strs_begin+49,
	price_high=slh_strs_begin+50,
	price_low=slh_strs_begin+51,
	vol_sum=slh_strs_begin+52,
	vol_now=slh_strs_begin+53,
	vol_ratio=slh_strs_begin+54,
	vol_outer=slh_strs_begin+55,
	vol_inner=slh_strs_begin+56,
	advance=slh_strs_begin+57,
	decline=slh_strs_begin+58,
	sort_asc=slh_strs_begin+59,
	sort_desc=slh_strs_begin+60,
	quote=slh_strs_begin+61,
	price=slh_strs_begin+62, //价
	minute=slh_strs_begin+63,
	buysell=slh_strs_begin+64,
	value=slh_strs_begin+65,
	bigtrade=slh_strs_begin+66,
	project=slh_strs_begin+67,
	primary=slh_strs_begin+68,
	mediate=slh_strs_begin+69,
	might=slh_strs_begin+70,
	pressure=slh_strs_begin+71,
	up_hold=slh_strs_begin+72,
	price_detial=slh_strs_begin+73,
	realtime_graph=slh_strs_begin+74,
	position_diff=slh_strs_begin+75,
	str_time=slh_strs_begin+76,   //"时间"
	str_price=slh_strs_begin+77, //"价格"
	str_volume=slh_strs_begin+78, //"现手"
	str_property=slh_strs_begin+79, //"性质"
	str_time_unit=slh_strs_begin+100, //"时间(单位)"
	str_time_month=slh_strs_begin+101,//时间(月)"
	str_time_day=slh_strs_begin+102,//"时间(日)"
	str_time_hour = slh_strs_begin+103,//时间(时)"
	str_time_minute = slh_strs_begin+104,//时间(分)"
	str_day=slh_strs_begin+105,//"日"
	str_tick_graph = slh_strs_begin + 106,
	
};
struct SLH_CTP_DATA
{
	int id;
	char ename[SLMAX_SLHTEXTLEN+1];
	char name[SLMAX_SLHTEXTLEN+1];

	int weight;
	long bkColor;
	long planeColor;
	long raiseColor;
	long fallColor;
	SLH_CTP_DATA(int id,const char* ename,const char* name,int weight=100,long bkColor=0,long planeColor=255)
	{
		this->id=id;
		strcpy(this->name,name);
		strcpy(this->ename,ename);
		this->weight=weight;
		this->bkColor=bkColor;
		this->planeColor=planeColor;
	}
	bool operator()(const SLH_CTP_DATA& _Left, const SLH_CTP_DATA& _Right) const
	{	// apply operator< to operands
		return (_Left.id < _Right.id);
	}
};

class CONFIGLIB_API config_manager
{
	static config_manager* g_Instance;
	
public:
	std::string get_filename();
	static config_manager* get_instance();
	//如果想将配置文件保存到不同的地方，保存之前调用该函数即可;
    void SetConfigParser(TinyXmlParser* parser){}
	bool load_config(tagInstrumentListConfig* config,const string& config_file);
	bool load_config(tagUsersListConfig* config,const string& config_file);
	bool load_config(tagTradeHistoryListConfig* config,const string& config_file);
	bool load_config(tagUserFundsListConfig* config,const string& config_file);
	bool load_config(tagOrderListConfig* config,const string& config_file);
	bool load_config(tagPositionListConfig* config,const string& config_file);

	bool load_q7_config(const std::string& config_file,tagBrokerListConfig& brokerList);

	bool load_config(tagBank2FutureConfig* config,const string& config_file);
	bool load_config(tagBrokerListConfig* config,const string& config_file);
	bool load_config(tagBrokersListConfig* config,const string& config_file);
	bool load_config(tagMainUiConfig* config,const string& config_file);
	bool load_config(tagDelegateListConfig* config,const string& config_file);
	bool load_config(tagEmbeddedListConfig* config,const string& config_file);
	bool load_config(tagLoginedUserConfig* config,const string& config_file);
	bool load_config(tagDataSourceConfig* config,const string& config_file);
	bool load_config(PaneConfig* config,const string& config_file);
	bool load_config(tagWebInfoConfig* config,const string& config_file);
	bool load_config(tagRealTimeConfig* config,const string& config_file);
	bool load_config(tagKLineGraphConfig* config,const string& config_file);
	bool load_config(tagMultiSortConfig* config,const string& config_file);
	bool load_config(tagGroupConfig* config,const string& config_file);
	bool load_config(tagStrategyConfig* config,const string& config_file);
	bool load_config(tagMultiPaneConfig* config,const string& config_file);
protected:
	bool load_config_extra(tagStrategyConfig* config,const string& root_node_name="StrategyList",const string& config_file="config.xml");
	bool load_config_extra(tagInstrumentListConfig* config,string root_node_name="InstrumentList",string config_file="config.xml");
	bool load_config_extra(tagRealTimeConfig* config,string root_node_name="RealTimeGraph",string config_file="config.xml");
	bool load_config_extra(tagKLineGraphConfig* config,string root_node_name="KLineGraph",string config_file="config.xml");
	bool load_config_extra(tagMultiSortConfig* config,string root_node_name="MultiSortGraph",const string& config_file="config.xml");
	bool save_config_extra(tagInstrumentListConfig* config,const string& root_node_name="InstrumentList",string config_file="config.xml");
public:
	bool save_config(PaneConfig& config,const string& config_file="config.xml");
	bool save_config(const string& config_file,tagUsersListConfig* config);
	bool save_config(const string& config_file,tagInstrumentListConfig* config);
	bool save_config(const string& config_file,tagUserFundsListConfig* config);
	bool save_config(const string& config_file,tagTradeHistoryListConfig* config);
	bool save_config(const string& config_file,tagOrderListConfig* config);
	bool save_config(const string& config_file,tagPositionListConfig* config);
	bool save_config(const string& config_file,tagBank2FutureConfig* config);
	bool save_config(const string& config_file,tagBrokerListConfig* config);
	bool save_config(const string& config_file,tagBrokersListConfig* config);
	bool save_config(const string& config_file,tagMainUiConfig* config);
	bool save_config(const string& config_file,tagDelegateListConfig* config);
	bool save_config(const string& config_file,tagEmbeddedListConfig* config);
	bool save_config(const string& config_file,tagLoginedUserConfig* config);
	bool save_config(tagDataSourceConfig* config);
	bool save_config(tagMultiPaneConfig* config);
	string get_name_by_id(int slh_idx,bool bEname=false);
	void set_name_by_id(int slh_idx,const string name,bool bEname=false);
	//读取所有的字符串配置;
	bool load_strings_config(const string& config_file="config.xml");
	bool save_string_config(const string& list_name,map<int,SLH_CTP_DATA>& str_map,const string& config_file="config.xml");
	bool save_strings_config(const string& config_file="config.xml");
protected:
	bool save_list_column(const string& config_file,const string& list_name,vector<ColumnInfo>& m_vecColumnItems);
	bool load_list_column(const string& config_file,const string& list_name,vector<ColumnInfo>& m_vecColumnItems);
	bool load_default_column(const string& list_name,vector<ColumnInfo>& m_vecColumnItems);
	
public:

	static map<int,SLH_CTP_DATA> m_mapInstrumentFields;
	static map<int,SLH_CTP_DATA> m_mapBank2FutureFields;
	static map<int,SLH_CTP_DATA> m_mapUserinfoFields;
	static map<int,SLH_CTP_DATA> m_mapTradelistFields;
	static map<int,SLH_CTP_DATA> m_mapOrderlistFields;
	static map<int,SLH_CTP_DATA> m_mapPositionsFields;
	static map<int,SLH_CTP_DATA> m_mapPositionsDetailFields;
	static map<int,SLH_CTP_DATA> m_mapCombPositionsDetailFields;
	static map<int,SLH_CTP_DATA> m_mapUserfundsFields;
	static map<int,SLH_CTP_DATA> m_mapParkedOrderFields;
	static map<int,SLH_CTP_DATA> m_mapOtherSlhFields;
	static map<string,map<int,SLH_CTP_DATA> > m_listname2listfield;

	static map<int,PaneFont> m_defalut_id2font;
	static map<int,PaneColor> m_default_id2color;
protected:
	bool load_string_config(const string& list_name,map<int,SLH_CTP_DATA>& str_map,const string& config_file="config.xml");
	bool load_default_string(const string& list_name,map<int,SLH_CTP_DATA>& str_map);

public:
	//默认配色方案;
	void set_default_color(tagTickGraphConfig& config);
	void set_default_color(tagKLineGraphConfig& config);
	void set_default_color(tagRealTimeConfig& config);
	void set_default_color(tagMultiSortConfig& config);
protected:
	tagTickGraphConfig m_tick_graph_config;
};

#endif

