﻿#include "AllConfig.h"
//#include "../BasicLib/basiclib.h"
//#include "../ctp_platform/ctp_platform_server.h"
#include "FieldInterceptor.h"


map<int,SLH_CTP_DATA> config_manager::m_mapBank2FutureFields;

map<int,SLH_CTP_DATA> config_manager::m_mapInstrumentFields;

map<int,SLH_CTP_DATA> config_manager::m_mapUserinfoFields;


map<int,SLH_CTP_DATA> config_manager::m_mapPositionsFields;


map<int,SLH_CTP_DATA> config_manager::m_mapUserfundsFields;


map<int,SLH_CTP_DATA> config_manager::m_mapTradelistFields;



map<int,SLH_CTP_DATA> config_manager::m_mapOrderlistFields;


map<string,map<int,SLH_CTP_DATA> > config_manager::m_listname2listfield;


map<int,SLH_CTP_DATA> config_manager::m_mapParkedOrderFields;


map<int,SLH_CTP_DATA> config_manager::m_mapOtherSlhFields;

map<int,SLH_CTP_DATA> config_manager::m_mapPositionsDetailFields;
map<int,SLH_CTP_DATA> config_manager::m_mapCombPositionsDetailFields;


map<int,PaneFont> config_manager::m_defalut_id2font;

map<int,PaneColor> config_manager::m_default_id2color;

config_manager* config_manager::g_Instance=NULL;

bool config_manager::save_config(const string& config_file,tagBrokersListConfig* config)
{
	return true;
	//if (NULL==config)
	//{
	//	return false;
	//}
	//if(!AfxGetXmlParser().load(config_file))
	//{
	//	return false;
	//}
	//if (config->m_strTitle.empty())
	//{
	//	config->m_strTitle="brokers";
	//}
	//TiXmlElement* element=AfxGetXmlParser().GetElementByTagName(config->m_strTitle);
	//if (NULL==element)
	//{
	//	element=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),config->m_strTitle,
	//		map<string,string>(),"");
	//}

	//vector<TiXmlElement*> brokers= AfxGetXmlParser().GetSubElements(element);
	//for (size_t i=0;i<brokers.size();i++)
	//{
	//	tagBrokerServerConfig broker_server;
	//	server_info b;
	//	b.server_id=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerID"));
	//	vector<tagBrokerServerConfig>::iterator find_iter=
	//		std::find(config->m_vecBrokers.begin(),config->m_vecBrokers.end(),broker_server);
	//	if (find_iter!=config->m_vecBrokers.end())
	//	{
	//		broker_server=*find_iter;
	//		b=broker_server.broker;
	//		config->m_vecBrokers.erase(find_iter);
	//	}
	//	AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerName"),b.BrokerName);
	//	AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerEName"),b.BrokerEName);
	//	
	//	AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"network"),broker_server.network_type);
	//	TiXmlElement* proxies_elem=AfxGetXmlParser().GetSubElementByTagName(brokers[i],"Proxies");
	//	if (NULL==proxies_elem)
	//	{
	//		proxies_elem=AfxGetXmlParser().CreateElementWithAttributes(brokers[i],"Proxies",
	//			map<string,string>(),"");
	//	}
	//	AfxGetXmlParser().SetAttributeValue(proxies_elem,"default",broker_server.proxy_type);
	//	map<string,host_info>::iterator proxy_host_iter=broker_server.m_mapProxyConfig.begin();
	//	AfxGetXmlParser().RemoveAllChildNodes(proxies_elem);
	//	for (;proxy_host_iter!=broker_server.m_mapProxyConfig.end();++proxy_host_iter)
	//	{
	//		host_info proxy_config=proxy_host_iter->second;
	//		TiXmlElement* proxy_elem=AfxGetXmlParser().CreateElementWithAttributes(proxies_elem,"item",
	//			map<string,string>(),"");
	//		AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxy_elem,"name"),proxy_config.name);
	//		AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxy_elem,"type"),proxy_config.type);
	//		AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxy_elem,"host"),proxy_config.host);
	//		AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxy_elem,"port"),proxy_config.port);
	//		AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxy_elem,"username"),proxy_config.username);
	//		AfxGetXmlParser().SetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxy_elem,"password"),proxy_config.password);
	//		broker_server.m_mapProxyConfig.insert(make_pair(proxy_config.type,proxy_config));
	//	}
	//	vector<TiXmlElement*> servers=AfxGetXmlParser().GetSubElements(
	//		AfxGetXmlParser().GetSubElementByTagName(brokers[i],"Servers"));

	//	for (size_t j=0;j<servers.size();j++)
	//	{
	//		ServerInfo server;
	//		server.name=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Name"));
	//		server.protocol=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Protocol"));
	//		vector<TiXmlElement*> markets=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(servers[j],"MarketData"));
	//		for (size_t m=0;m<markets.size();++m)
	//		{
	//			string market_addr=AfxGetXmlParser().GetInnerText(markets[m]);
	//			server.market_server_front.push_back(market_addr);
	//		}
	//		vector<TiXmlElement*> tradings=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Trading"));
	//		for (size_t n=0;n<tradings.size();++n)
	//		{
	//			string trade_addr=AfxGetXmlParser().GetInnerText(tradings[n]);
	//			server.trade_server_front.push_back(trade_addr);
	//		}
	//		b.server_addrs.push_back(server);
	//	}
	//	broker_server.broker=b;
	//	config->m_vecBrokers.push_back(broker_server);
	//}
}
bool config_manager::load_config(tagBrokersListConfig* config,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	TiXmlElement* element=AfxGetXmlParser().GetElementByTagName("brokers");
	if (NULL==element)
	{
		element=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),"brokers",
			map<string,string>(),"");
	}
	
	vector<TiXmlElement*> brokers= AfxGetXmlParser().GetSubElements(element);
	if (brokers.size()<=0)
	{
		return false;
	}
	//for (size_t i=0;i<brokers.size();i++)
	//{
	//	tagBrokerServerConfig broker_server;
	//	server_info b;
	//	b.server_id=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerID"));
	//	b.BrokerName=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerName"));
	//	b.BrokerEName=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerEName"));
	//	broker_server.network_type=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"network"));
	//	vector<TiXmlElement*> proxies=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"proxies"));
	//	broker_server.proxy_type=AfxGetXmlParser().GetAttributeValue(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"proxies"),"default");
	//	for (size_t m=0;m<proxies.size();++m)
	//	{
	//		host_info proxy_config;
	//		proxy_config.name=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxies[m],"name"));
	//		proxy_config.type=AfxGetXmlParser().GetAttributeValue(proxies[m],"type");
	//		proxy_config.host=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxies[m],"host"));
	//		proxy_config.port=AfxGetXmlParser().GetInnerInt(AfxGetXmlParser().GetSubElementByTagName(proxies[m],"port"));
	//		proxy_config.username=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxies[m],"user"));
	//		proxy_config.password=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxies[m],"pass"));
	//		broker_server.m_mapProxyConfig.insert(make_pair(proxy_config.type,proxy_config));
	//	}
	//	vector<TiXmlElement*> servers=AfxGetXmlParser().GetSubElements(
	//		AfxGetXmlParser().GetSubElementByTagName(brokers[i],"Servers"));

	//	for (size_t j=0;j<servers.size();j++)
	//	{
	//		ServerInfo server;
	//		server.name=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Name"));
	//		server.protocol=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Protocol"));
	//		vector<TiXmlElement*> markets=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(servers[j],"MarketData"));
	//		for (size_t m=0;m<markets.size();++m)
	//		{
	//			string market_addr=AfxGetXmlParser().GetInnerText(markets[m]);
	//			server.market_server_front.push_back(market_addr);
	//		}
	//		vector<TiXmlElement*> tradings=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Trading"));
	//		for (size_t n=0;n<tradings.size();++n)
	//		{
	//			string trade_addr=AfxGetXmlParser().GetInnerText(tradings[n]);
	//			server.trade_server_front.push_back(trade_addr);
	//		}
	//		b.server_addrs.push_back(server);
	//	}
	//	broker_server.broker=b;
	//	config->m_vecBrokers.push_back(broker_server);
	//}
	return true;
}


bool config_manager::load_config(tagBrokerListConfig* config,const std::string& config_file)
{
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	TiXmlElement* element=AfxGetXmlParser().GetElementByTagName("brokers");
	if (NULL==element)
	{
		element=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),"brokers",
			std::map<std::string,std::string>(),"");
	}
	std::vector<TiXmlElement*> brokers= AfxGetXmlParser().GetSubElements(element);
	if (brokers.size()<=0)
	{
		return false;
	}
	//for (size_t i=0;i<brokers.size();i++)
	//{
	//	server_info b;
	//	b.server_id=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerID"));
	//	b.BrokerName=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerName"));
	//	b.BrokerEName=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"BrokerEName"));
	//	b.network=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"network"));

	//	vector<TiXmlElement*> servers=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(brokers[i],"Servers"));
	//	for (size_t j=0;j<servers.size();j++)
	//	{
	//		ServerInfo server;
	//		server.name=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Name"));
	//		server.protocol=AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Protocol"));
	//		vector<TiXmlElement*> markets=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(servers[j],"MarketData"));
	//		for (size_t m=0;m<markets.size();++m)
	//		{
	//			string market_addr=AfxGetXmlParser().GetInnerText(markets[m]);
	//			server.market_server_front.push_back(market_addr);
	//		}
	//		vector<TiXmlElement*> tradings=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(servers[j],"Trading"));
	//		for (size_t n=0;n<tradings.size();++n)
	//		{
	//			string trade_addr=AfxGetXmlParser().GetInnerText(tradings[n]);
	//			server.trade_server_front.push_back(trade_addr);
	//		}
	//		b.server_addrs.push_back(server);
	//	}
	//	TiXmlElement* proxy_root = AfxGetXmlParser().GetSubElementByTagName(brokers[i],"proxies");
	//	if (proxy_root)
	//	{
	//		std::vector<TiXmlElement*> proxies=AfxGetXmlParser().GetSubElements(proxy_root);
	//		std::string default_proxy=AfxGetXmlParser().GetAttributeValue(proxy_root,"default");
	//		for (size_t k=0;i<proxies.size();k++)
	//		{
	//			string host_type=
	//				AfxGetXmlParser().GetAttributeValue(proxies[k],"type");
	//			if (host_type == default_proxy)
	//			{
	//				host_info hinfo;
	//				b.proxy_info.type=host_type;
	//				b.proxy_info.host=
	//					AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxies[k],"host"));
	//				b.proxy_info.username=
	//					AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxies[k],"user"));
	//				b.proxy_info.password=
	//					AfxGetXmlParser().GetInnerText(AfxGetXmlParser().GetSubElementByTagName(proxies[k],"pass"));
	//				b.proxy_info.port=
	//					AfxGetXmlParser().GetInnerInt(AfxGetXmlParser().GetSubElementByTagName(proxies[k],"port"));
	//				break;
	//			}
	//		}
	//	}
	//	config->m_Broker.push_back(b);
	//}
	return true;
}

bool config_manager::load_config(tagInstrumentListConfig* config,const string& config_file )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//读取其他的配置信息
	load_config_extra(config,"InstrumentList");
	//读取列表的配置信息
	return load_list_column(config_file,"ListCtrl列头",config->m_vecColumnItems);
}



config_manager* config_manager::get_instance()
{
	if (NULL==g_Instance)
	{
		g_Instance=new config_manager();
		m_mapInstrumentFields.insert(make_pair(SLH_CODE,SLH_CTP_DATA(SLH_CODE,"InstrumentID","合约ID",100,0)));
		m_mapInstrumentFields.insert(make_pair(SLH_NAME,SLH_CTP_DATA( SLH_NAME,						"InstrumentName",		"合约名",			110, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_EXCHANGEID,SLH_CTP_DATA( SLH_EXCHANGEID,				"ExchangeID",			"交易所ID",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_ExchangeInstID,SLH_CTP_DATA( SLH_ExchangeInstID,			"ExchangeInstID",		"合约在交易所代码", 100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_ExchangeName,SLH_CTP_DATA( SLH_ExchangeName,				"ExchangeName",			"交易所名称",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_ExchangeProperty,SLH_CTP_DATA( SLH_ExchangeProperty,			"ExchangeProperty",		"交易所属性",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_OPEN,SLH_CTP_DATA( SLH_OPEN,						"OpenPrice",			"开盘价",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_CLOSE,SLH_CTP_DATA( SLH_CLOSE,					"ClosePrice",			"收盘价",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_HIGH,SLH_CTP_DATA( SLH_HIGH,						"HighestPrice",			"最高价",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_LOW,SLH_CTP_DATA( SLH_LOW,						"LowestPrice",			"最低价",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_AVERAGE,SLH_CTP_DATA( SLH_AVERAGE,					"AveragePrice",			"平均价",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_LASTPRICE,SLH_CTP_DATA( SLH_LASTPRICE,					"LastPrice",			"最新价",			100, 0)));

		m_mapInstrumentFields.insert(make_pair(SLH_ProductID,SLH_CTP_DATA( SLH_ProductID,				"ProductID",			"产品代码",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_ProductClass,SLH_CTP_DATA( SLH_ProductClass,				"ProductClass",			"产品类型",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_DeliveryYear,SLH_CTP_DATA( SLH_DeliveryYear,				"DeliveryYear",			"交割年份",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_DeliveryMonth,SLH_CTP_DATA( SLH_DeliveryMonth	,			"DeliveryMonth",		"交割月份",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_MaxMarketOrderVolume,SLH_CTP_DATA( SLH_MaxMarketOrderVolume,		"MaxMarketOrderVolume",	"市价单最大下单量",	100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_MinMarketOrderVolume,SLH_CTP_DATA( SLH_MinMarketOrderVolume,		"MinMarketOrderVolume",	"市价单最小下单量",	100, 0)));

		m_mapInstrumentFields.insert(make_pair(SLH_MaxLimitOrderVolume,SLH_CTP_DATA( SLH_MaxLimitOrderVolume,		"MaxLimitOrderVolume",	"限价单最大下单量",	100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_MinLimitOrderVolume,SLH_CTP_DATA( SLH_MinLimitOrderVolume,		"MinLimitOrderVolume",	"限价单最小下单量",	100, 0)));

		m_mapInstrumentFields.insert(make_pair(SLH_VolumeMultiple,SLH_CTP_DATA( SLH_VolumeMultiple,			"VolumeMultiple",		"合约数量乘数",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_PriceTick,SLH_CTP_DATA( SLH_PriceTick		,			"PriceTick",			"最小变动价位",		100, 0)));

		m_mapInstrumentFields.insert(make_pair(SLH_CreateDate,SLH_CTP_DATA( SLH_CreateDate	,			"CreateDate",			"创建日",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_OpenDate,SLH_CTP_DATA( SLH_OpenDate		,			"OpenDate",				"上市日",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_ExpireDate,SLH_CTP_DATA( SLH_ExpireDate	,			"ExpireDate",			"到期日",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_StartDelivDate,SLH_CTP_DATA( SLH_StartDelivDate,			"StartDelivDate",		"开始交割日",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_EndDelivDate,SLH_CTP_DATA( SLH_EndDelivDate	,			"EndDelivDate",			"最小变动价位",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_InstLifePhase,SLH_CTP_DATA( SLH_InstLifePhase	,			"InstLifePhase",		"合约生命周期状态",	100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_IsTrading,SLH_CTP_DATA( SLH_IsTrading		,			"IsTrading",			"当前是否交易",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_PositionType,SLH_CTP_DATA( SLH_PositionType	,			"PositionType",			"持仓类型",			100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_PositionDateType,SLH_CTP_DATA( SLH_PositionDateType,			"PositionDateType",		"持仓日期类型",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_LongMarginRatio,SLH_CTP_DATA( SLH_LongMarginRatio	,		"LongMarginRatio",		"多头保证金率",		100, 0)));
		m_mapInstrumentFields.insert(make_pair(SLH_ShortMarginRatio,SLH_CTP_DATA( SLH_ShortMarginRatio,			"ShortMarginRatio",		"空头保证金率",		100, 0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_PlateSerial,SLH_CTP_DATA(SLH_PlateSerial,"PlateSerial","平台流水号",100,0)));
		m_mapBank2FutureFields.insert(make_pair( 
			SLH_TradeDate,SLH_CTP_DATA(SLH_TradeDate,"TradeDate","交易发起方日期",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_TradeTime,SLH_CTP_DATA(SLH_TradeTime,"TradeTime","交易时间",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_TradeCode,SLH_CTP_DATA(SLH_TradeCode,"TradeCode","交易代码",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_SessionID,SLH_CTP_DATA(SLH_SessionID,"SessionID","会话编号",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BankID,SLH_CTP_DATA(SLH_BankID,"BankID","银行编码",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BankBranchID,SLH_CTP_DATA(SLH_BankBranchID,"BankBranchID","银行分支机构编码",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BankAccType,SLH_CTP_DATA(SLH_BankAccType,"BankAccType","银行帐号类型",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BankAccount,SLH_CTP_DATA(SLH_BankAccount,"BankAccount","银行帐号",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BankSerial,SLH_CTP_DATA(SLH_BankSerial,"BankSerial","银行流水号",100,0)));


		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BrokerBranchID,SLH_CTP_DATA(SLH_BrokerBranchID,"BrokerBranchID","期商分支机构代码",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_FutureAccType,SLH_CTP_DATA(SLH_FutureAccType,"FutureAccType","期货公司帐号类型",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_AccountID,SLH_CTP_DATA(SLH_AccountID,"AccountID","投资者帐号",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_FutureSerial,SLH_CTP_DATA(SLH_FutureSerial,"FutureSerial","期货公司流水号",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_IdCardType,SLH_CTP_DATA(SLH_IdCardType,"IdCardType","证件类型",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_IdentifiedCardNo,SLH_CTP_DATA(SLH_IdentifiedCardNo,"IdCardType","证件号码",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_CurrencyID,SLH_CTP_DATA(SLH_CurrencyID,"CurrencyID","币种代码",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_TradeAmount,SLH_CTP_DATA(SLH_TradeAmount,"TradeAmount","交易金额",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_CustFee,SLH_CTP_DATA(SLH_CustFee,"CustFee","应收客户费用",100,0)));


		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BrokerFee,SLH_CTP_DATA(SLH_BrokerFee,"BrokerFee","应收期货公司费用",100,0)));


		m_mapBank2FutureFields.insert(make_pair( 
			SLH_AvailabilityFlag,SLH_CTP_DATA(SLH_AvailabilityFlag,"AvailabilityFlag","有效标志",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_OperatorCode,SLH_CTP_DATA(SLH_OperatorCode,"OperatorCode","操作员",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_BankNewAccount,SLH_CTP_DATA(SLH_BankNewAccount,"BankNewAccount","新银行帐号",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_ErrorID,SLH_CTP_DATA(SLH_ErrorID,"ErrorID","错误代码",100,0)));

		m_mapBank2FutureFields.insert(make_pair( 
			SLH_UserID,SLH_CTP_DATA(SLH_UserID,"UserID","用户代码",100,0)));

		m_mapUserinfoFields.insert(make_pair( 
			SLH_UserID,SLH_CTP_DATA(SLH_UserID,"UserID","用户代码",100,0)));

		m_mapUserinfoFields.insert(make_pair( 
			SLH_LoginTime,SLH_CTP_DATA(SLH_LoginTime,"LoginTime","登录成功时间",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_BrokerID,SLH_CTP_DATA(SLH_BrokerID,"BrokerID","经纪公司代码",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_InvestorID,SLH_CTP_DATA(SLH_InvestorID,"InvestorID","投资者ID",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_SystemName,SLH_CTP_DATA(SLH_SystemName,"SystemName","交易系统名称",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_FrontID,SLH_CTP_DATA(SLH_FrontID,"FrontID","前置编号",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_SessionID,SLH_CTP_DATA(SLH_SessionID,"SessionID","会话编号",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_MaxOrderRef,SLH_CTP_DATA(SLH_MaxOrderRef,"MaxOrderRef","最大报单引用",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_SHFETime,SLH_CTP_DATA(SLH_SHFETime,"SHFETime","上期所时间",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_DCETime,SLH_CTP_DATA(SLH_DCETime,"DCETime","大商所时间",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_CZCETime,SLH_CTP_DATA(SLH_CZCETime,"CZCETime","郑商所时间",100,0)));
		m_mapUserinfoFields.insert(make_pair( 
			SLH_FFEXTime,SLH_CTP_DATA(SLH_FFEXTime,"FFEXTime","中金所时间",100,0)));




		m_mapPositionsFields.insert(make_pair( 
			SLH_CODE,SLH_CTP_DATA(SLH_CODE,"InstrumentID","合约代码",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_BrokerID,SLH_CTP_DATA(SLH_BrokerID,"BrokerID","经纪公司代码",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_UserID,SLH_CTP_DATA(SLH_UserID,"UserID","投资者代码",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_PosiDirection,SLH_CTP_DATA(SLH_PosiDirection,"PosiDirection","持仓多空方向",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_HedgeFlag,SLH_CTP_DATA(SLH_HedgeFlag,"HedgeFlag","投机套保标志",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_PositionDate,SLH_CTP_DATA(SLH_PositionDate,"PositionDate","持仓日期",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_YdPosition,SLH_CTP_DATA(SLH_YdPosition,"YdPosition","上日持仓",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_Position,SLH_CTP_DATA(SLH_Position,"Position","持仓",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_LongFrozen,SLH_CTP_DATA(SLH_LongFrozen,"LongFrozen","多头冻结",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_ShortFrozen,SLH_CTP_DATA(SLH_ShortFrozen,"ShortFrozen","空头冻结",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_LongFrozenAmount,SLH_CTP_DATA(SLH_LongFrozenAmount,"LongFrozenAmount","多头冻结金额",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_ShortFrozenAmount,SLH_CTP_DATA(SLH_ShortFrozenAmount,"ShortFrozenAmount","空头冻结金额",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_OpenVolume,SLH_CTP_DATA(SLH_OpenVolume,"OpenVolume","开仓量",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CloseVolume,SLH_CTP_DATA(SLH_CloseVolume,"CloseVolume","平仓量",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_OpenAmount,SLH_CTP_DATA(SLH_OpenAmount,"OpenAmount","开仓金额",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CloseAmount,SLH_CTP_DATA(SLH_CloseAmount,"CloseAmount","平仓金额",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_PositionCost,SLH_CTP_DATA(SLH_PositionCost,"PositionCost","持仓成本",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_PreMargin,SLH_CTP_DATA(SLH_PreMargin,"PreMargin","上次占用的保证金",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_UseMargin,SLH_CTP_DATA(SLH_UseMargin,"UseMargin","占用的保证金",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_FrozenMargin,SLH_CTP_DATA(SLH_FrozenMargin,"FrozenMargin","冻结的保证金",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_FrozenCash,SLH_CTP_DATA(SLH_FrozenCash,"FrozenCash","冻结的资金",100,0)));

		m_mapPositionsFields.insert(make_pair( 
			SLH_FrozenCommission,SLH_CTP_DATA(SLH_FrozenCommission,"FrozenCommission","冻结的手续费",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CashIn,SLH_CTP_DATA(SLH_CashIn,"CashIn","资金差额",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_Commission,SLH_CTP_DATA(SLH_Commission,"Commission","手续费",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CloseProfit,SLH_CTP_DATA(SLH_CloseProfit,"CloseProfit","平仓盈亏",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_PositionProfit,SLH_CTP_DATA(SLH_PositionProfit,"PositionProfit","持仓盈亏",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_PreSettlementPrice,SLH_CTP_DATA(SLH_PreSettlementPrice,"PreSettlementPrice","昨结算",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_SettlementPrice,SLH_CTP_DATA(SLH_SettlementPrice,"SettlementPrice","本次结算价",100,0)));
		// 		m_mapPositionsFields.insert(make_pair( 
		// 			SLH_TradingDay,SLH_CTP_DATA(SLH_TradingDay,"TradingDay","冻结的资金",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_OpenCost,SLH_CTP_DATA(SLH_OpenCost,"OpenCost","开仓成本",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_ExchangeMargin,SLH_CTP_DATA(SLH_ExchangeMargin,"ExchangeMargin","交易所保证金",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CombPosition,SLH_CTP_DATA(SLH_CombPosition,"CombPosition","组合成交形成的持仓",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CombLongFrozen,SLH_CTP_DATA(SLH_CombLongFrozen,"CombLongFrozen","组合多头冻结",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CloseProfitByDate,SLH_CTP_DATA(SLH_CloseProfitByDate,"CloseProfitByDate","逐日盯市平仓盈亏",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_CloseProfitByTrade,SLH_CTP_DATA(SLH_CloseProfitByTrade,"CloseProfitByTrade","逐笔对冲平仓盈亏",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_TodayPosition,SLH_CTP_DATA(SLH_TodayPosition,"TodayPosition","今持仓",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_MarginRateByMoney,SLH_CTP_DATA(SLH_MarginRateByMoney,"MarginRateByMoney","保证金率",100,0)));
		m_mapPositionsFields.insert(make_pair( 
			SLH_MarginRateByVolume,SLH_CTP_DATA(SLH_MarginRateByVolume,"MarginRateByVolume","保证金率(按手数)",100,0)));

		//持仓明细
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CODE,SLH_CTP_DATA(SLH_CODE,"InstrumentID","合约代码",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_BrokerID,SLH_CTP_DATA(SLH_BrokerID,"BrokerID","经纪公司代码",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_UserID,SLH_CTP_DATA(SLH_UserID,"UserID","投资者代码",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_PosiDirection,SLH_CTP_DATA(SLH_PosiDirection,"PosiDirection","持仓多空方向",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_HedgeFlag,SLH_CTP_DATA(SLH_HedgeFlag,"HedgeFlag","投机套保标志",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_PositionDate,SLH_CTP_DATA(SLH_PositionDate,"PositionDate","持仓日期",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_YdPosition,SLH_CTP_DATA(SLH_YdPosition,"YdPosition","上日持仓",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_Position,SLH_CTP_DATA(SLH_Position,"Position","今持仓",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_LongFrozen,SLH_CTP_DATA(SLH_LongFrozen,"LongFrozen","多头冻结",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_ShortFrozen,SLH_CTP_DATA(SLH_ShortFrozen,"ShortFrozen","空头冻结",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_LongFrozenAmount,SLH_CTP_DATA(SLH_LongFrozenAmount,"LongFrozenAmount","多头冻结金额",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_ShortFrozenAmount,SLH_CTP_DATA(SLH_ShortFrozenAmount,"ShortFrozenAmount","空头冻结金额",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_OpenVolume,SLH_CTP_DATA(SLH_OpenVolume,"OpenVolume","开仓量",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CloseVolume,SLH_CTP_DATA(SLH_CloseVolume,"CloseVolume","平仓量",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_OpenAmount,SLH_CTP_DATA(SLH_OpenAmount,"OpenAmount","开仓金额",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CloseAmount,SLH_CTP_DATA(SLH_CloseAmount,"CloseAmount","平仓金额",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_PositionCost,SLH_CTP_DATA(SLH_PositionCost,"PositionCost","持仓成本",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_PreMargin,SLH_CTP_DATA(SLH_PreMargin,"PreMargin","上次占用的保证金",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_UseMargin,SLH_CTP_DATA(SLH_UseMargin,"UseMargin","占用的保证金",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_FrozenMargin,SLH_CTP_DATA(SLH_FrozenMargin,"FrozenMargin","冻结的保证金",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_FrozenCash,SLH_CTP_DATA(SLH_FrozenCash,"FrozenCash","冻结的资金",100,0)));

		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_FrozenCommission,SLH_CTP_DATA(SLH_FrozenCommission,"FrozenCommission","冻结的手续费",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CashIn,SLH_CTP_DATA(SLH_CashIn,"CashIn","资金差额",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_Commission,SLH_CTP_DATA(SLH_Commission,"Commission","手续费",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CloseProfit,SLH_CTP_DATA(SLH_CloseProfit,"CloseProfit","平仓盈亏",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_PositionProfit,SLH_CTP_DATA(SLH_PositionProfit,"PositionProfit","持仓盈亏",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_PreSettlementPrice,SLH_CTP_DATA(SLH_PreSettlementPrice,"PreSettlementPrice","昨结",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_SettlementPrice,SLH_CTP_DATA(SLH_SettlementPrice,"SettlementPrice","本次结算价",100,0)));
		// 		m_mapPositionsFields.insert(make_pair( 
		// 			SLH_TradingDay,SLH_CTP_DATA(SLH_TradingDay,"TradingDay","冻结的资金",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_OpenCost,SLH_CTP_DATA(SLH_OpenCost,"OpenCost","开仓成本",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_ExchangeMargin,SLH_CTP_DATA(SLH_ExchangeMargin,"ExchangeMargin","交易所保证金",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CombPosition,SLH_CTP_DATA(SLH_CombPosition,"CombPosition","组合成交形成的持仓",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CombLongFrozen,SLH_CTP_DATA(SLH_CombLongFrozen,"CombLongFrozen","组合多头冻结",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CloseProfitByDate,SLH_CTP_DATA(SLH_CloseProfitByDate,"CloseProfitByDate","逐日盯市平仓盈亏",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_CloseProfitByTrade,SLH_CTP_DATA(SLH_CloseProfitByTrade,"CloseProfitByTrade","逐笔对冲平仓盈亏",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_TodayPosition,SLH_CTP_DATA(SLH_TodayPosition,"TodayPosition","今日持仓",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_MarginRateByMoney,SLH_CTP_DATA(SLH_MarginRateByMoney,"MarginRateByMoney","保证金率",100,0)));
		m_mapPositionsDetailFields.insert(make_pair( 
			SLH_MarginRateByVolume,SLH_CTP_DATA(SLH_MarginRateByVolume,"MarginRateByVolume","保证金率(按手数)",100,0)));

		//组合持仓明细
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CODE,SLH_CTP_DATA(SLH_CODE,"InstrumentID","合约代码",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_BrokerID,SLH_CTP_DATA(SLH_BrokerID,"BrokerID","经纪公司代码",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_UserID,SLH_CTP_DATA(SLH_UserID,"UserID","投资者代码",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_PosiDirection,SLH_CTP_DATA(SLH_PosiDirection,"PosiDirection","持仓多空方向",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_HedgeFlag,SLH_CTP_DATA(SLH_HedgeFlag,"HedgeFlag","投机套保标志",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_PositionDate,SLH_CTP_DATA(SLH_PositionDate,"PositionDate","持仓日期",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_YdPosition,SLH_CTP_DATA(SLH_YdPosition,"YdPosition","上日持仓",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_Position,SLH_CTP_DATA(SLH_Position,"Position","持仓",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_LongFrozen,SLH_CTP_DATA(SLH_LongFrozen,"LongFrozen","多头冻结",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_ShortFrozen,SLH_CTP_DATA(SLH_ShortFrozen,"ShortFrozen","空头冻结",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_LongFrozenAmount,SLH_CTP_DATA(SLH_LongFrozenAmount,"LongFrozenAmount","多头冻结金额",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_ShortFrozenAmount,SLH_CTP_DATA(SLH_ShortFrozenAmount,"ShortFrozenAmount","空头冻结金额",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_OpenVolume,SLH_CTP_DATA(SLH_OpenVolume,"OpenVolume","开仓量",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CloseVolume,SLH_CTP_DATA(SLH_CloseVolume,"CloseVolume","平仓量",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_OpenAmount,SLH_CTP_DATA(SLH_OpenAmount,"OpenAmount","开仓金额",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CloseAmount,SLH_CTP_DATA(SLH_CloseAmount,"CloseAmount","平仓金额",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_PositionCost,SLH_CTP_DATA(SLH_PositionCost,"PositionCost","持仓成本",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_PreMargin,SLH_CTP_DATA(SLH_PreMargin,"PreMargin","上次占用的保证金",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_UseMargin,SLH_CTP_DATA(SLH_UseMargin,"UseMargin","占用的保证金",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_FrozenMargin,SLH_CTP_DATA(SLH_FrozenMargin,"FrozenMargin","冻结的保证金",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_FrozenCash,SLH_CTP_DATA(SLH_FrozenCash,"FrozenCash","冻结的资金",100,0)));

		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_FrozenCommission,SLH_CTP_DATA(SLH_FrozenCommission,"FrozenCommission","冻结的手续费",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CashIn,SLH_CTP_DATA(SLH_CashIn,"CashIn","资金差额",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_Commission,SLH_CTP_DATA(SLH_Commission,"Commission","手续费",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CloseProfit,SLH_CTP_DATA(SLH_CloseProfit,"CloseProfit","平仓盈亏",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_PositionProfit,SLH_CTP_DATA(SLH_PositionProfit,"PositionProfit","持仓盈亏",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_PreSettlementPrice,SLH_CTP_DATA(SLH_PreSettlementPrice,"PreSettlementPrice","昨结算",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_SettlementPrice,SLH_CTP_DATA(SLH_SettlementPrice,"SettlementPrice","本次结算价",100,0)));
		// 		m_mapPositionsFields.insert(make_pair( 
		// 			SLH_TradingDay,SLH_CTP_DATA(SLH_TradingDay,"TradingDay","冻结的资金",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_OpenCost,SLH_CTP_DATA(SLH_OpenCost,"OpenCost","开仓成本",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_ExchangeMargin,SLH_CTP_DATA(SLH_ExchangeMargin,"ExchangeMargin","交易所保证金",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CombPosition,SLH_CTP_DATA(SLH_CombPosition,"CombPosition","组合成交形成的持仓",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CombLongFrozen,SLH_CTP_DATA(SLH_CombLongFrozen,"CombLongFrozen","组合多头冻结",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CloseProfitByDate,SLH_CTP_DATA(SLH_CloseProfitByDate,"CloseProfitByDate","逐日盯市平仓盈亏",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_CloseProfitByTrade,SLH_CTP_DATA(SLH_CloseProfitByTrade,"CloseProfitByTrade","逐笔对冲平仓盈亏",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_TodayPosition,SLH_CTP_DATA(SLH_TodayPosition,"TodayPosition","持仓",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_MarginRateByMoney,SLH_CTP_DATA(SLH_MarginRateByMoney,"MarginRateByMoney","保证金率",100,0)));
		m_mapCombPositionsDetailFields.insert(make_pair( 
			SLH_MarginRateByVolume,SLH_CTP_DATA(SLH_MarginRateByVolume,"MarginRateByVolume","保证金率(按手数)",100,0)));

		//用户资金


		m_mapUserfundsFields.insert(make_pair( 
			SLH_MarginRateByVolume,SLH_CTP_DATA(SLH_MarginRateByVolume,"MarginRateByVolume","保证金率(按手数)",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_MarginRateByVolume,SLH_CTP_DATA(SLH_MarginRateByVolume,"MarginRateByVolume","保证金率(按手数)",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_MarginRateByVolume,SLH_CTP_DATA(SLH_MarginRateByVolume,"MarginRateByVolume","保证金率(按手数)",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_BrokerID,SLH_CTP_DATA(SLH_BrokerID,"BrokerID","经纪公司代码",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_UserID,SLH_CTP_DATA(SLH_UserID,"UserID","投资者帐号",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_PreMortgage,SLH_CTP_DATA(SLH_PreMortgage,"PreMortgage","上次质押金额",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_PreCredit,SLH_CTP_DATA(SLH_PreCredit,"PreCredit","上次信用额度",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_PreDeposit,SLH_CTP_DATA(SLH_PreDeposit,"PreDeposit","上次存款额",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_PreBalance,SLH_CTP_DATA(SLH_PreBalance,"PreBalance","上次结算准备金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_PreMargin,SLH_CTP_DATA(SLH_PreMargin,"PreMargin","上次占用的保证金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_InterestBase,SLH_CTP_DATA(SLH_InterestBase,"InterestBase","利息基数",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Interest,SLH_CTP_DATA(SLH_Interest,"Interest","利息收入",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Deposit,SLH_CTP_DATA(SLH_Deposit,"Deposit","入金金额",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Withdraw,SLH_CTP_DATA(SLH_Withdraw,"Withdraw","出金金额",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_FrozenMargin,SLH_CTP_DATA(SLH_FrozenMargin,"FrozenMargin","冻结的保证金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_FrozenCash,SLH_CTP_DATA(SLH_FrozenCash,"FrozenCash","冻结的资金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_FrozenCommission,SLH_CTP_DATA(SLH_FrozenCommission,"FrozenCommission","冻结的手续费",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_CurrMargin,SLH_CTP_DATA(SLH_CurrMargin,"CurrMargin","当前保证金总额",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_CashIn,SLH_CTP_DATA(SLH_CashIn,"CashIn","资金差额",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Commission,SLH_CTP_DATA(SLH_Commission,"Commission","手续费",100,0)));

		m_mapUserfundsFields.insert(make_pair( 
			SLH_CloseProfit,SLH_CTP_DATA(SLH_CloseProfit,"CloseProfit","平仓盈亏",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_PositionProfit,SLH_CTP_DATA(SLH_PositionProfit,"PositionProfit","持仓盈亏",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Balance,SLH_CTP_DATA(SLH_Balance,"Balance","期货结算准备金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Available,SLH_CTP_DATA(SLH_Available,"Available","可用资金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Reserve,SLH_CTP_DATA(SLH_WithdrawQuota,"WithdrawQuota","可取资金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Reserve,SLH_CTP_DATA(SLH_Reserve,"Reserve","基本准备金",100,0)));
		// 		m_mapUserinfoFields.insert(make_pair( 
		// 			SLH_TradingDay,SLH_CTP_DATA(SLH_TradingDay,"TradingDay","交易日",100,0)));

		m_mapUserfundsFields.insert(make_pair( 
			SLH_SettlementID,SLH_CTP_DATA(SLH_SettlementID,"SettlementID","结算编号",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Credit,SLH_CTP_DATA(SLH_Credit,"Credit","信用额度",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_Mortgage,SLH_CTP_DATA(SLH_Mortgage,"Mortgage","质押金额",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_ExchangeMargin,SLH_CTP_DATA(SLH_ExchangeMargin,"ExchangeMargin","交易所保证金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_DeliveryMargin,SLH_CTP_DATA(SLH_DeliveryMargin,"DeliveryMargin","投资者交割保证金",100,0)));
		m_mapUserfundsFields.insert(make_pair( 
			SLH_ExchangeDeliveryMargin,SLH_CTP_DATA(SLH_ExchangeDeliveryMargin,"ExchangeDeliveryMargin","交易所交割保证金",100,0)));
		m_mapTradelistFields.insert(make_pair(
			SLH_EXCHANGEID,SLH_CTP_DATA(SLH_EXCHANGEID,"ExchangeID","交易所",100,0)));
		m_mapTradelistFields.insert(make_pair(
			SLH_TradeTime,SLH_CTP_DATA(SLH_TradeTime,"TradeTime","成交时间",100,0)));
		m_mapTradelistFields.insert(make_pair(
			SLH_PositionType,SLH_CTP_DATA(SLH_PositionType,"PositionType","开平",100,0)));
		m_mapTradelistFields.insert(make_pair(
			SLH_Direction,SLH_CTP_DATA(SLH_Direction,"Direction","买卖",100,0)));
		m_mapTradelistFields.insert(make_pair(
			SLH_CODE,SLH_CTP_DATA(SLH_CODE,"InstrumentID","合约ID",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_OrderRef,SLH_CTP_DATA(SLH_OrderRef,"OrderRef","报单引用",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_TradeID,SLH_CTP_DATA(SLH_TradeID,"TradeID","成交编号",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_OrderSysID,SLH_CTP_DATA(SLH_OrderSysID,"OrderSysID","报单编号",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_ParticipantID,SLH_CTP_DATA(SLH_ParticipantID,"ParticipantID","会员代码",100,0)));

		m_mapTradelistFields.insert(make_pair( 
			SLH_ClientID,SLH_CTP_DATA(SLH_ClientID,"ClientID","客户代码",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_TradingRole,SLH_CTP_DATA(SLH_TradingRole,"TradingRole","交易角色",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_OffsetFlag,SLH_CTP_DATA(SLH_OffsetFlag,"OffsetFlag","开平标志",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_HedgeFlag,SLH_CTP_DATA(SLH_HedgeFlag,"HedgeFlag","投机套保标志",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_Price,SLH_CTP_DATA(SLH_Price,"Price","价格",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_Volume,SLH_CTP_DATA(SLH_Volume,"Volume","数量",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_TradeType,SLH_CTP_DATA(SLH_TradeType,"TradeType","TradeType",100,0)));

		m_mapTradelistFields.insert(make_pair( 
			SLH_PriceSource,SLH_CTP_DATA(SLH_PriceSource,"PriceSource","成交价来源",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_TraderID,SLH_CTP_DATA(SLH_TraderID,"TraderID","交易所交易员代码",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_OrderLocalID,SLH_CTP_DATA(SLH_OrderLocalID,"OrderLocalID","本地报单编号",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_ClearingPartID,SLH_CTP_DATA(SLH_ClearingPartID,"ClearingPartID","结算会员编号",100,0)));
		m_mapTradelistFields.insert(make_pair( 
			SLH_SequenceNo,SLH_CTP_DATA(SLH_SequenceNo,"SequenceNo","序号",100,0)));

		m_mapTradelistFields.insert(make_pair( 
			SLH_BrokerOrderSeq,SLH_CTP_DATA(SLH_BrokerOrderSeq,"BrokerOrderSeq","经纪公司报单编号",100,0)));

		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderRef,SLH_CTP_DATA(SLH_OrderRef,"OrderRef","报单引用",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_BrokerID,SLH_CTP_DATA(SLH_BrokerID,"BrokerID","经纪公司代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_InvestorID,SLH_CTP_DATA(SLH_InvestorID,"InvestorID","投资者代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CODE,SLH_CTP_DATA(SLH_CODE,"InstrumentID","合约代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_UserID,SLH_CTP_DATA(SLH_UserID,"UserID","用户代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderPriceType,SLH_CTP_DATA(SLH_OrderPriceType,"OrderPriceType","报单价格条件",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_Direction,SLH_CTP_DATA(SLH_Direction,"Direction","报单引用",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombOffsetFlag_0,SLH_CTP_DATA(SLH_CombOffsetFlag_0,"CombOffsetFlag_0","组合开平标志0",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombOffsetFlag_1,SLH_CTP_DATA(SLH_CombOffsetFlag_1,"CombOffsetFlag_1","组合开平标志1",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombOffsetFlag_2,SLH_CTP_DATA(SLH_CombOffsetFlag_2,"CombOffsetFlag_2","组合开平标志2",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombOffsetFlag_3,SLH_CTP_DATA(SLH_CombOffsetFlag_3,"CombOffsetFlag_3","组合开平标志3",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombOffsetFlag_4,SLH_CTP_DATA(SLH_CombOffsetFlag_4,"CombOffsetFlag_4","组合开平标志4",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombHedgeFlag_0,SLH_CTP_DATA(SLH_CombHedgeFlag_0,"CombHedgeFlag_0","组合投机套保标志0",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombHedgeFlag_1,SLH_CTP_DATA(SLH_CombHedgeFlag_1,"CombHedgeFlag_1","组合投机套保标志1",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombHedgeFlag_2,SLH_CTP_DATA(SLH_CombHedgeFlag_2,"CombHedgeFlag_2","组合投机套保标志2",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombHedgeFlag_3,SLH_CTP_DATA(SLH_CombHedgeFlag_3,"CombHedgeFlag_3","组合投机套保标志3",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CombHedgeFlag_4,SLH_CTP_DATA(SLH_CombHedgeFlag_4,"CombHedgeFlag_4","组合投机套保标志4",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_LimitPrice,SLH_CTP_DATA(SLH_LimitPrice,"LimitPrice","价格",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_VolumeTotalOriginal,SLH_CTP_DATA(SLH_VolumeTotalOriginal,"VolumeTotalOriginal","数量",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_TimeCondition,SLH_CTP_DATA(SLH_TimeCondition,"TimeCondition","有效期类型",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_GTDDate,SLH_CTP_DATA(SLH_GTDDate,"GTDDate","GTD日期",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_VolumeCondition,SLH_CTP_DATA(SLH_VolumeCondition,"VolumeCondition","成交量类型",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_MinVolume,SLH_CTP_DATA(SLH_MinVolume,"MinVolume","最小成交量",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_ContingentCondition,SLH_CTP_DATA(SLH_ContingentCondition,"ContingentCondition","触发条件",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_StopPrice,SLH_CTP_DATA(SLH_StopPrice,"StopPrice","止损价",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_ForceCloseReason,SLH_CTP_DATA(SLH_ForceCloseReason,"ForceCloseReason","强平原因",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_IsAutoSuspend,SLH_CTP_DATA(SLH_IsAutoSuspend,"IsAutoSuspend","自动挂起标志",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_BusinessUnit,SLH_CTP_DATA(SLH_BusinessUnit,"BusinessUnit","业务单元",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_RequestID,SLH_CTP_DATA(SLH_RequestID,"OrderRef","请求编号",100,0)));

		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderLocalID,SLH_CTP_DATA(SLH_OrderLocalID,"OrderLocalID","本地报单编号",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_EXCHANGEID,SLH_CTP_DATA(SLH_EXCHANGEID,"ExchangeID","交易所代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_ParticipantID,SLH_CTP_DATA(SLH_ParticipantID,"ParticipantID","会员代码",100,0)));

		m_mapOrderlistFields.insert(make_pair( 
			SLH_ClientID,SLH_CTP_DATA(SLH_ClientID,"ClientID","客户代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_ExchangeInstID,SLH_CTP_DATA(SLH_ExchangeInstID,"ExchangeInstID","合约在交易所的代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_TraderID,SLH_CTP_DATA(SLH_TraderID,"TraderID","交易所交易员代码",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_InstallID,SLH_CTP_DATA(SLH_InstallID,"InstallID","安装编号",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderSubmitStatus,SLH_CTP_DATA(SLH_OrderSubmitStatus,"OrderSubmitStatus","报单提交状态",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_NotifySequence,SLH_CTP_DATA(SLH_NotifySequence,"NotifySequence","报单提示序号",100,0)));

		// 		m_mapOrderlistFields.insert(make_pair( 
		// 			SLH_TradingDay,SLH_CTP_DATA(SLH_TradingDay,"TradingDay","交易日",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_SettlementID,SLH_CTP_DATA(SLH_SettlementID,"SettlementID","结算编号",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderSysID,SLH_CTP_DATA(SLH_OrderSysID,"OrderSysID","报单编号",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderSource,SLH_CTP_DATA(SLH_OrderSource,"OrderSource","报单来源",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderStatus,SLH_CTP_DATA(SLH_OrderStatus,"OrderStatus","报单状态",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_OrderType,SLH_CTP_DATA(SLH_OrderType,"OrderType","报单类型",100,0)));

		m_mapOrderlistFields.insert(make_pair( 
			SLH_VolumeTraded,SLH_CTP_DATA(SLH_VolumeTraded,"VolumeTraded","今成交数量",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_VolumeTotal,SLH_CTP_DATA(SLH_VolumeTotal,"VolumeTotal","剩余数量",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_InsertDate,SLH_CTP_DATA(SLH_InsertDate,"InsertDate","报单日期",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_InsertTime,SLH_CTP_DATA(SLH_InsertTime,"InsertTime","委托时间",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_ActiveTime,SLH_CTP_DATA(SLH_ActiveTime,"ActiveTime","激活时间",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_SuspendTime,SLH_CTP_DATA(SLH_SuspendTime,"SuspendTime","挂起时间",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_UpdateTime,SLH_CTP_DATA(SLH_UpdateTime,"UpdateTime","最后修改时间",100,0)));
		m_mapOrderlistFields.insert(make_pair( 
			SLH_CancelTime,SLH_CTP_DATA(SLH_CancelTime,"CancelTime","撤销时间",100,0)));
		
		m_mapOtherSlhFields.insert(make_pair(nodata,SLH_CTP_DATA(nodata,"No Data","没有数据",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_logicand,
			SLH_CTP_DATA(strategy_logicand,"与","strategy_logicand",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_logicor,SLH_CTP_DATA(strategy_logicor,
			"strategy_logicor","或",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdonce,SLH_CTP_DATA(strategy_sdonce,
			"strategy_sdonce","全仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdtwice,SLH_CTP_DATA(strategy_sdtwice,
			"半仓","strategy_sdtwice",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdthird,SLH_CTP_DATA(strategy_sdthird,
			"strategy_sdthird","三分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdforth,SLH_CTP_DATA(strategy_sdforth,
			"strategy_sdforth","四分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdfifth,SLH_CTP_DATA(strategy_sdfifth,
			"strategy_sdfifth","五分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdsixth,SLH_CTP_DATA(strategy_sdsixth,
			"strategy_sdsixth","六分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdseventh,SLH_CTP_DATA(strategy_sdseventh,
			"strategy_sdseventh","七分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdeighth,SLH_CTP_DATA(strategy_sdeighth,
			"strategy_sdeighth","八分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdninth,SLH_CTP_DATA(strategy_sdninth,
			"strategy_sdninth","九分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(strategy_sdtenth,SLH_CTP_DATA(strategy_sdtenth,
			"strategy_sdtenth","十分之一仓",100,0)));
		m_mapOtherSlhFields.insert(make_pair(multisort_bsratioasc,SLH_CTP_DATA(multisort_bsratioasc,
			"MULTISORT_BSRATIOASC","今日委比%前六名",100,0)));
		m_mapOtherSlhFields.insert(make_pair(multisort_volratio,SLH_CTP_DATA(multisort_volratio,
			"MULTISORT_VOLRATIO","今日委比%后六名",100,0)));
		m_mapOtherSlhFields.insert(make_pair(multisort_bsratiodesc,SLH_CTP_DATA(multisort_bsratiodesc,
			"MULTISORT_VOLRATIO","今日量比排名",100,0)));

		m_mapOtherSlhFields.insert(make_pair(multisort_fall,SLH_CTP_DATA(multisort_fall,
			"MULTISORT_FALL","今日跌幅%排名",100,0)));
		m_mapOtherSlhFields.insert(make_pair(multisort_diff,SLH_CTP_DATA(multisort_diff,
			"multisort_diff","今日震幅%排名",100,0)));
		m_mapOtherSlhFields.insert(make_pair(multisort_rise,SLH_CTP_DATA(multisort_rise,
			"multisort_rise","今日涨幅%排名",100,0)));

		m_mapOtherSlhFields.insert(make_pair(multisort_risemin5,SLH_CTP_DATA(multisort_risemin5,
			"multisort_risemin5","5分钟涨幅%排名",100,0)));
		m_mapOtherSlhFields.insert(make_pair(multisort_fallmin5,SLH_CTP_DATA(multisort_fallmin5,
			"multisort_fallmin5","5分钟跌幅%排名",100,0)));
		m_mapOtherSlhFields.insert(make_pair(multisort_aumount,SLH_CTP_DATA(multisort_aumount,
			"multisort_aumount","今日总金额排名(千元)",100,0)));

		m_mapOtherSlhFields.insert(make_pair(strategy_noselected,
			SLH_CTP_DATA(strategy_noselected,"strategy_noselected","没有选中",100,0)));

		m_mapOtherSlhFields.insert(make_pair(realtime_graph,
			SLH_CTP_DATA(realtime_graph,"Realtime Graph","分时行情图",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_tick_graph,
			SLH_CTP_DATA(str_tick_graph,"Tick Graph","闪电趋势图",100,0)));

		m_mapOtherSlhFields.insert(make_pair(bsratio,
			SLH_CTP_DATA(bsratio,"bsratio","委比",100,0)));
		m_mapOtherSlhFields.insert(make_pair(bsdiff,
			SLH_CTP_DATA(bsdiff,"bsdiff","委差",100,0)));
		m_mapOtherSlhFields.insert(make_pair(sell5,
			SLH_CTP_DATA(sell5,"sell5","卖⑤",100,0)));
		m_mapOtherSlhFields.insert(make_pair(sell4,
			SLH_CTP_DATA(sell4,"sell4","卖④",100,0)));
		m_mapOtherSlhFields.insert(make_pair(sell3,
			SLH_CTP_DATA(sell3,"sell3","卖③",100,0)));
		m_mapOtherSlhFields.insert(make_pair(sell2,
			SLH_CTP_DATA(sell2,"sell2","卖②",100,0)));
		m_mapOtherSlhFields.insert(make_pair(sell1,
			SLH_CTP_DATA(sell1,"sell1","卖①",100,0)));

		m_mapOtherSlhFields.insert(make_pair(buy1,
			SLH_CTP_DATA(buy1,"buy1","买①",100,0)));
		m_mapOtherSlhFields.insert(make_pair(buy2,
			SLH_CTP_DATA(buy2,"buy2","买②",100,0)));
		m_mapOtherSlhFields.insert(make_pair(buy3,
			SLH_CTP_DATA(buy3,"buy3","买③",100,0)));
		m_mapOtherSlhFields.insert(make_pair(buy4,
			SLH_CTP_DATA(buy4,"buy4","买④",100,0)));
		m_mapOtherSlhFields.insert(make_pair(buy5,
			SLH_CTP_DATA(buy5,"buy5","买⑤",100,0)));

		m_mapOtherSlhFields.insert(make_pair(price_diff,
			SLH_CTP_DATA(price_diff,"price_diff","涨跌",100,0)));
		m_mapOtherSlhFields.insert(make_pair(price_diff_percent,
			SLH_CTP_DATA(price_diff_percent,"price_diff_percent","振幅",100,0)));
		m_mapOtherSlhFields.insert(make_pair(vol_sum,
			SLH_CTP_DATA(vol_sum,"vol_sum","总手",100,0)));
		m_mapOtherSlhFields.insert(make_pair(vol_now,
			SLH_CTP_DATA(vol_now,"vol_now","现手",100,0)));

		m_mapOtherSlhFields.insert(make_pair(position_diff,
			SLH_CTP_DATA(position_diff,"position_diff","仓差",100,0)));
 		m_mapOtherSlhFields.insert(make_pair(vol_outer,
 			SLH_CTP_DATA(vol_outer,"vol_outer","外盘",100,0)));

		m_mapOtherSlhFields.insert(make_pair(vol_inner,
			SLH_CTP_DATA(vol_inner,"vol_inner","内盘",100,0)));

		m_mapOtherSlhFields.insert(make_pair(str_time,
			SLH_CTP_DATA(str_time,"time","时间",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_price,
			SLH_CTP_DATA(str_price,"time","价格",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_volume,
			SLH_CTP_DATA(str_volume,"volume","现手",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_property,
			SLH_CTP_DATA(str_property,"property","性质",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_time_unit,
			SLH_CTP_DATA(str_time_unit,"unit","单位",100,0)));
		
		m_mapOtherSlhFields.insert(make_pair(str_time_month,
			SLH_CTP_DATA(str_time_month,"month","月",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_time_day,
			SLH_CTP_DATA(str_time_day,"day","日",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_time_hour,
			SLH_CTP_DATA(str_time_hour,"day","时",100,0)));
		m_mapOtherSlhFields.insert(make_pair(str_time_minute,
			SLH_CTP_DATA(str_time_minute,"day","分",100,0)));

		//预埋单字段;
		m_mapParkedOrderFields.insert(make_pair(SLH_BrokerID,
			SLH_CTP_DATA(SLH_BrokerID,"BrokerID","经纪公司代码",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_InvestorID,
			SLH_CTP_DATA(SLH_InvestorID,"InvestorID","投资者代码",100,0)));

		m_mapParkedOrderFields.insert(make_pair(SLH_CODE,
			SLH_CTP_DATA(SLH_CODE,"InstrumentID","合约代码",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_OrderPriceType,
			SLH_CTP_DATA(SLH_OrderPriceType,"OrderPriceType","价格类型",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_Direction,
			SLH_CTP_DATA(SLH_Direction,"Direction","买卖",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_CombOffsetFlag_0,
			SLH_CTP_DATA(SLH_CombOffsetFlag_0,"CombOffsetFlag_0","组合开平标志0",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_LimitPrice,
			SLH_CTP_DATA(SLH_LimitPrice,"LimitPrice","价格",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_VolumeTotalOriginal,
			SLH_CTP_DATA(SLH_VolumeTotalOriginal,"VolumeTotalOriginal","手数",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_EXCHANGEID,
			SLH_CTP_DATA(SLH_EXCHANGEID,"SLH_ExchangeID","交易所ID",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_ParkedOrderID,
			SLH_CTP_DATA(SLH_ParkedOrderID,"ParkedOrderID","预埋单ID",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_Status,
			SLH_CTP_DATA(SLH_Status,"Status","状态",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_VolumeCondition,
			SLH_CTP_DATA(SLH_VolumeCondition,"VolumeCondition","手数类型",100,0)));
		m_mapParkedOrderFields.insert(make_pair(SLH_ContingentCondition,
			SLH_CTP_DATA(SLH_ContingentCondition,"ContingentCondition","触发条件",100,0)));


		m_listname2listfield.insert(make_pair("ListCtrl列头",m_mapInstrumentFields));
		m_listname2listfield.insert(make_pair("ListCtrl银期转帐",m_mapBank2FutureFields));
		m_listname2listfield.insert(make_pair("ListCtrl当前用户",m_mapUserinfoFields));
		m_listname2listfield.insert(make_pair("ListCtrl持仓",m_mapPositionsFields));
		m_listname2listfield.insert(make_pair("ListCtrl持仓明细",m_mapCombPositionsDetailFields));
		m_listname2listfield.insert(make_pair("ListCtrl组合持仓明细",m_mapCombPositionsDetailFields));
		m_listname2listfield.insert(make_pair("资金账户",m_mapUserfundsFields));
		m_listname2listfield.insert(make_pair("ListCtrl所有委托单",m_mapOrderlistFields));
		m_listname2listfield.insert(make_pair("ListCtrl成交记录",m_mapTradelistFields));
		m_listname2listfield.insert(make_pair("ListCtrl预埋单",m_mapParkedOrderFields));
		m_listname2listfield.insert(make_pair("其他",m_mapOtherSlhFields));
		PaneFont p={fontText,"text font","宋体",0,12,0};
		m_defalut_id2font.insert(make_pair((int)fontText,p));
		p.Id=fontWorkTab;
		m_defalut_id2font.insert(make_pair((int)fontWorkTab,p));
		p.Id=fontTitle;
		m_defalut_id2font.insert(make_pair((int)fontTitle,p));
		//m_defalut_id2font.insert(make_pair((int)fontTitle,(struct PaneFont){fontText,"text font","宋体",0,12,0}));
		//map<int,PaneFont* > ss;
		//ss.insert(make_pair((int)fontTitle,&(struct PaneFont){fontText,"text font","宋体",0,12,0}));
		long default_colors[30]={
			BLACK,BLUE,FUCHSIA,GRAY,GREEN,LIME,MAROON,NAVY,OLIVE,PURPLE,
			RED,SILVER,TEAL,WHITE,YELLOW,AZURE,BEIGE,BISQUE,BROWN,CHOCOLATE,
			CORAL,CORNSILK,CRIMSON,CYAN,GAINSBORO,GOLD,GREY,PINK,ORANGE,TOMATO
		};
		PaneColor pC={0,"","",0};
		for (int i=clrBK;i<=clrListHeaderText;++i)
		{
			pC.Id=i;
            //pC.EName=rgb2str(default_colors[i]);
			pC.Color=default_colors[i];
			m_default_id2color.insert(make_pair((int)i,pC));
		}
		//g_Instance->load_strings_config("config.xml");
	}
	return g_Instance;
}

bool config_manager::save_config( const string& config_file,tagInstrumentListConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	bool bSave=save_list_column(config_file,"ListCtrl列头",config->m_vecColumnItems);
	//todo:读取配置文件 m_ListCtrlColumns
	bSave=save_config_extra(config,"InstrumentList",config_file);
	return bSave;
}


bool config_manager::load_config( tagUsersListConfig* config,const string& config_file )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns ListCtrl当前用户
	return load_list_column(config_file,"ListCtrl当前用户",config->m_vecColumnItems);
}


bool config_manager::load_config( tagUserFundsListConfig* config ,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "资金账户"
	return load_list_column(config_file,"资金账户",config->m_vecColumnItems);
}


bool config_manager::load_config( tagTradeHistoryListConfig* config, const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "资金账户"
	return load_list_column(config_file,"ListCtrl成交记录",config->m_vecColumnItems);
}


bool config_manager::load_config( tagOrderListConfig* config ,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "资金账户"
	return load_list_column(config_file,"ListCtrl所有委托单",config->m_vecColumnItems);
}


bool config_manager::load_config( tagPositionListConfig* config ,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	return load_list_column(config_file,config->m_strTitle,config->m_vecColumnItems);
}


bool config_manager::load_config( tagBank2FutureConfig* config ,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "资金账户"
	return load_list_column(config_file,"ListCtrl银期转帐",config->m_vecColumnItems);
}


bool config_manager::load_config( tagMainUiConfig* config ,const string& config_file)
{
	return true;
}

bool config_manager::load_config( tagDelegateListConfig* config ,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "资金账户"
	return load_list_column(config_file,"ListCtrl所有委托单",config->m_vecColumnItems);
}

bool config_manager::load_config( tagEmbeddedListConfig* config ,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "资金账户"
	return load_list_column(config_file,"ListCtrl预埋单",config->m_vecColumnItems);
}


bool config_manager::load_config(tagLoginedUserConfig* config, const string& config_file )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	TiXmlElement* listCtrlColumns=AfxGetXmlParser().GetElementByTagName(config->m_strTitle);
	if (NULL==listCtrlColumns)
	{
		map<string,string> attr_value;
		attr_value.insert(make_pair("lastInvestorID",config->lastInvestorID));
		listCtrlColumns=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_ListCtrlColumns,attr_value,"");
		if (NULL==listCtrlColumns)
		{
			return false;
		}
	}
	else
	{
		config->lastInvestorID=AfxGetXmlParser().GetAttributeValue(listCtrlColumns,"lastInvestorID");
	}
	vector<TiXmlElement*> vec_all_sequence=AfxGetXmlParser().GetSubElements(listCtrlColumns);
	size_t i=0;
	for (;i<vec_all_sequence.size();i++)
	{
		UserAccountConfig uac;
		uac.InvestorID=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"InvestorID");
		uac.BrokerID=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"BrokerID");
		uac.auto_login=(AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"AutoLogin")=="1");
		uac.save_password=(AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"SavePassword")=="1");
		uac.Password=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"Password");
		config->m_mapUsers.insert(make_pair(uac.InvestorID,uac));
	}
	return true;
}


bool config_manager::load_config(tagDataSourceConfig* config,const string& config_file)
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	config->m_mapUser2Database.clear();
	config->m_mapUser2Proxy.clear();
	//读取数据源;
	string data_source_root_node_name="datasources";
	TiXmlElement* datasources_node=AfxGetXmlParser().GetElementByTagName(data_source_root_node_name);
	if (NULL==datasources_node)
	{
		datasources_node=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			data_source_root_node_name,map<string,string>(),"");
		if (NULL==datasources_node)
		{
			return false;
		}
		AfxGetXmlParser().save();
		return true;
	}
	else
	{
		//读取默认结点;
		string dfuser=AfxGetXmlParser().GetAttributeValue(datasources_node,"default");
		if (!dfuser.empty())
		{
			config->default_username=dfuser;
		}
	}
	std::vector<TiXmlElement* > datasrcs=AfxGetXmlParser().GetSubElements(datasources_node);
	for (size_t j=0;j<datasrcs.size();j++)
	{
		TiXmlElement* list_element=datasrcs[j];
		std::vector<TiXmlElement*> list_header_columns=AfxGetXmlParser().GetSubElements(list_element);
		std::string user_name=AfxGetXmlParser().GetAttributeValue(list_element,"key");
		if (list_header_columns.size()<=0)
		{
			//什么也没有,没有用户配置过数据源;
			continue;
		}
		else
		{
			std::map<std::string,host_info> user_host_infos;
			for (size_t j=0;j<list_header_columns.size();j++)
			{
				host_info aHost;
				aHost.name=(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"name"));
				aHost.type=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"type");
				aHost.host=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"host");

				aHost.port=atoi(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"port").c_str());
				aHost.username=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"username");
				aHost.password=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"password");
				user_host_infos.insert(make_pair(aHost.name,aHost));
			}
			config->m_mapUser2Database.insert(make_pair(user_name,user_host_infos));
		}
	}
	return true;
}

bool config_manager::load_config( PaneConfig* config,const string& config_file/*="config.xml"*/ )
{
	//PRTINF("LoadPaneConfig:paneName:%s-->Enter",config.m_PaneEName.c_str());
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	string m_AllPanes="m_AllPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> panes=AfxGetXmlParser().GetSubElements(paneRootNode);
	int pane_idx=-1;
	for (size_t i=0;i<panes.size();++i)
	{
		string paneName=AfxGetXmlParser().GetAttributeValue(panes[i],"EName");
		if (paneName==config->m_PaneEName)
		{
			pane_idx=i;
			break;
		}
	}
	TinyXmlParser::xml_elem_type* onePane=NULL;
	if (-1==pane_idx)
	{
		map<string,string> attr2value;
		attr2value.insert(make_pair("EName",config->m_PaneEName));
		attr2value.insert(make_pair("Name",config->m_PaneEName));
        char id_str[32]={0};
        //itoa(config.m_PaneId,id_str,10);
        sprintf(id_str,"%d",config->m_PaneId);
		attr2value.insert(make_pair("Id",id_str));
		onePane=AfxGetXmlParser().CreateElementWithAttributes(paneRootNode,"Pane",attr2value,"");
		if (NULL==onePane)
		{
			return false;
		}
	}
	else
	{
		onePane=panes[pane_idx];
	}
	config->m_Attr_2_Value.clear();
	config->m_Fonts.clear();
	config->m_Colors.clear();
	config->m_PaneId=atoi(AfxGetXmlParser().GetAttributeValue(onePane,"Id").c_str());
	config->m_PaneEName=AfxGetXmlParser().GetAttributeValue(onePane,"EName");
	config->m_PaneName=AfxGetXmlParser().GetAttributeValue(onePane,"Name");
	vector<TinyXmlParser::xml_elem_type*>  PaneFonts=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(onePane,"m_Fonts"));
	if (PaneFonts.size()<=0)
	{
		config->m_Fonts=m_defalut_id2font;
	}
	else
	{
		PaneFont paneFont;
		for (unsigned int j=0;j<PaneFonts.size();j++)
		{
			paneFont.Id=atoi(AfxGetXmlParser().GetAttributeValue(PaneFonts[j],"Id").c_str());
			paneFont.EName=AfxGetXmlParser().GetAttributeValue(PaneFonts[j],"EName");
			paneFont.Name=AfxGetXmlParser().GetAttributeValue(PaneFonts[j],"Name");
			paneFont.Width=atoi(AfxGetXmlParser().GetAttributeValue(PaneFonts[j],"Width").c_str());
			paneFont.Height=atoi(AfxGetXmlParser().GetAttributeValue(PaneFonts[j],"Height").c_str());
			paneFont.Flags=FONT_WITH_NONE;
			string Flags=AfxGetXmlParser().GetAttributeValue(PaneFonts[j],"Flags");
			bool bBold=false,bItalic=false,bUnderline=false;
			int pos1=Flags.find("bold");
			if (pos1!=string::npos)
			{
				paneFont.Flags|=FONT_WITH_BOLD;
			}
			int pos2=Flags.find("italic");
			if (pos2!=string::npos)
			{
				paneFont.Flags|=FONT_WITH_ITALIC;
			}
			int pos3=Flags.find("underline");
			if (pos3!=string::npos)
			{
				paneFont.Flags|=FONT_WITH_UNDERLINE;
			}
			config->m_Fonts.insert(make_pair(paneFont.Id,paneFont));
		}
	}

	vector<TinyXmlParser::xml_elem_type*>  PaneColors=AfxGetXmlParser().GetSubElements(AfxGetXmlParser().GetSubElementByTagName(onePane,"m_Colors"));
	if (PaneColors.size()<=0)
	{
		config->m_Colors=m_default_id2color;
	}
	else
	{
		PaneColor paneColor;
		for (unsigned int k=0;k<PaneColors.size();k++)
		{
			paneColor.Id=atoi(AfxGetXmlParser().GetAttributeValue(PaneColors[k],"Id").c_str());
			paneColor.EName=AfxGetXmlParser().GetAttributeValue(PaneColors[k],"EName");
			paneColor.Name=AfxGetXmlParser().GetAttributeValue(PaneColors[k],"Name");
            //paneColor.Color=str2rgb(AfxGetXmlParser().GetInnerText(PaneColors[k]).c_str());
			config->m_Colors.insert(make_pair(paneColor.Id,paneColor));
		}
	}

	//PRTINF("LoadPaneConfig:paneName:%s-->Leave",config.m_PaneEName.c_str());
	return true;
}


bool config_manager::load_config( tagWebInfoConfig* config,const string& config_file/*="config.xml"*/ )
{
	return true;
}


bool config_manager::load_config( tagRealTimeConfig* config,const string& config_file/*="config.xml"*/ )
{
	return  load_config_extra(config,"RealtimeGraph",config_file);
}


bool config_manager::load_config( tagMultiSortConfig* config,const string& config_file/*="config.xml"*/ )
{

	return load_config_extra(config,"MultiSortGraph",config_file);
}


bool config_manager::load_config( tagKLineGraphConfig* config,const string& config_file/*="config.xml"*/ )
{
	return load_config_extra(config,"KLineGraph",config_file);
}


bool config_manager::load_config( tagGroupConfig* config,const string& config_file/*="config.xml"*/ )
{
	return true;
}


bool config_manager::load_config( tagStrategyConfig* config,const string& config_file/*="config.xml"*/ )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//加载要运行的策略项;
	std::string strategies_name="strategies";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(strategies_name);
	if (NULL==paneRootNode)
	{
		std::map<std::string,std::string> attr_value;
		attr_value.insert(make_pair("dir","./")); //策略存放目录;
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			strategies_name,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	config->m_strLastStrategyDirectory=AfxGetXmlParser().GetAttributeValue(paneRootNode,"dir");
	std::vector<TinyXmlParser::xml_elem_type*> strategy_nodes=AfxGetXmlParser().GetSubElements(paneRootNode);
	
	for (std::size_t i=0;i<strategy_nodes.size();++i)
	{
		//读取信息;
		tagStrategyParams strategy_params;
		strategy_params.name=AfxGetXmlParser().GetAttributeValue(strategy_nodes[i],"name");
		strategy_params.investor_id=AfxGetXmlParser().GetAttributeValue(strategy_nodes[i],"InvestorID");
		strategy_params.beginRunTime=AfxGetXmlParser().GetAttributeValue(strategy_nodes[i],"beginRunTime");
		strategy_params.runDuration=
			atoi(AfxGetXmlParser().GetAttributeValue(strategy_nodes[i],"runDuration").c_str());
		//获取合约;
		TinyXmlParser::xml_elem_type* instruments_node=AfxGetXmlParser().GetSubElementByTagName(strategy_nodes[i],"instruments");
		if (NULL!=instruments_node)
		{
			std::vector<TinyXmlParser::xml_elem_type*> instrument_nodes=AfxGetXmlParser().GetSubElements(instruments_node);
			for (std::size_t j=0;j<instrument_nodes.size();++j)
			{
				strategy_params.instruments.push_back(AfxGetXmlParser().GetInnerText(instrument_nodes[j]));
			}
		}
		config->m_vecStrategyParams.push_back(strategy_params);
	}
	return true;
	//return load_config_extra(config,"StrategyList",config_file);
}


bool config_manager::load_config( tagMultiPaneConfig* config,const string& config_file/*="config.xml"*/ )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	string m_AllPanes="m_MultiPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=paneRootNode;
	//读取其他属性
	TinyXmlParser::xml_elem_type* styleElems=AfxGetXmlParser().GetElementByTagName("Style");
	if (!styleElems)
	{
		styleElems=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,map<string,string>(),"");
		if (NULL==styleElems)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> items=AfxGetXmlParser().GetSubElements(styleElems);
	for (size_t i=0;i<items.size();i++)
	{
		int id=atoi(AfxGetXmlParser().GetAttributeValue(items[i],"Id").c_str());
		string styleName=AfxGetXmlParser().GetAttributeValue(items[i],"EName");
		config->m_mapStyles.insert(make_pair(id,styleName));
	}
	config->selectStyle=atol(AfxGetXmlParser().GetAttributeValue(styleElems,"selected").c_str());
	string sub_node_name;
	sub_node_name="TabMinWidth";
	TinyXmlParser::xml_elem_type* TabMinWidth=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=TabMinWidth)
	{
		config->TabMinWidth=AfxGetXmlParser().GetInnerInt(TabMinWidth);
		if (config->TabMinWidth<=0)
		{
			config->TabMinWidth=200;
		}
	}
	else
	{
		TabMinWidth=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,
			map<string,string>(),"200");
		config->TabMinWidth=200;
	}
	sub_node_name="TabMinHeight";
	TinyXmlParser::xml_elem_type* TabMinHeight=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=TabMinHeight)
	{
		config->TabMinHeight=AfxGetXmlParser().GetInnerInt(TabMinHeight);
		if (config->TabMinHeight<=0)
		{
			config->TabMinHeight=200;
		}
	}
	else
	{
		TabMinHeight=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"200");
		config->TabMinHeight=200;
	}
	sub_node_name="TabLayout";
	TinyXmlParser::xml_elem_type* TabLayout=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=TabLayout)
	{
		config->TabLayout=AfxGetXmlParser().GetInnerInt(TabLayout);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
	}

	sub_node_name="SplitterDraggingMode";
	TinyXmlParser::xml_elem_type* SplitterDraggingMode=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->SplitterDraggingMode=AfxGetXmlParser().GetInnerInt(SplitterDraggingMode);
	if (NULL!=SplitterDraggingMode)
	{
		config->SplitterDraggingMode=AfxGetXmlParser().GetInnerInt(SplitterDraggingMode);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
	}
	sub_node_name="TabBehavior";
	TinyXmlParser::xml_elem_type* TabBehavior=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->TabBehavior=AfxGetXmlParser().GetInnerInt(TabBehavior);
	if (NULL!=TabBehavior)
	{
		config->TabBehavior=AfxGetXmlParser().GetInnerInt(TabBehavior);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
	}
	sub_node_name="IsShowBorder";
	TinyXmlParser::xml_elem_type* IsShowBorder=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsShowBorder=AfxGetXmlParser().GetInnerInt(IsShowBorder);
	if (NULL!=IsShowBorder)
	{
		config->IsShowBorder=AfxGetXmlParser().GetInnerInt(IsShowBorder);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
	}
	sub_node_name="IsCustomCursor";
	TinyXmlParser::xml_elem_type* IsCustomCursor=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=IsCustomCursor)
	{
		config->IsCustomCursor=AfxGetXmlParser().GetInnerInt(IsCustomCursor);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
	}

	sub_node_name="IsShowClientEdgeBorder";
	TinyXmlParser::xml_elem_type* IsShowClientEdge=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=IsShowClientEdge)
	{
		config->IsShowClientEdge=AfxGetXmlParser().GetInnerInt(IsShowClientEdge);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
	}
	sub_node_name="IsSplliterInActivate";
	TinyXmlParser::xml_elem_type* IsSplliterInActivate=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=IsSplliterInActivate)
	{
		config->IsSplitterInActive=AfxGetXmlParser().GetInnerInt(IsCustomCursor);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
	}
	sub_node_name="IsShowImage";
	TinyXmlParser::xml_elem_type* IsShowImage=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsShowImage=AfxGetXmlParser().GetInnerInt(IsShowImage);


	sub_node_name="IsHideSingleTab";
	TinyXmlParser::xml_elem_type* IsHideSingleTab=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsHideSingleTab=AfxGetXmlParser().GetInnerInt(IsHideSingleTab);


	sub_node_name="IsRemoveTabEnable";
	TinyXmlParser::xml_elem_type* IsRemoveTabEnable=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsRemoveTabEnable=AfxGetXmlParser().GetInnerInt(IsRemoveTabEnable);


	sub_node_name="IsDragTabEnable";
	TinyXmlParser::xml_elem_type* IsDragTabEnable=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsDragTabEnable=AfxGetXmlParser().GetInnerInt(IsDragTabEnable);


	sub_node_name="IsWatchActivityCtrl";
	TinyXmlParser::xml_elem_type* IsWatchActivityCtrl=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsWatchActivityCtrl=AfxGetXmlParser().GetInnerInt(IsWatchActivityCtrl);


	sub_node_name="IsShowSelectedFont";
	TinyXmlParser::xml_elem_type* IsShowSelectedFont=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsShowSelectedFont=AfxGetXmlParser().GetInnerInt(IsShowSelectedFont);

	sub_node_name="IsShowTabCloseButton";
	TinyXmlParser::xml_elem_type* IsShowTabCloseButton=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsShowTabCloseButton=AfxGetXmlParser().GetInnerInt(IsShowTabCloseButton);

	sub_node_name="IsShowTabMenuButton";
	TinyXmlParser::xml_elem_type* IsShowTabMenuButton=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsShowTabMenuButton=AfxGetXmlParser().GetInnerInt(IsShowTabMenuButton);

	sub_node_name="IsShowSelectedFont";
	TinyXmlParser::xml_elem_type* IsShowTabScrollButton=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsShowTabScrollButton=AfxGetXmlParser().GetInnerInt(IsShowTabScrollButton);

	sub_node_name="IsShowSelectedFont";
	TinyXmlParser::xml_elem_type* IsShowCtrlBorder=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->IsShowCtrlBorder=AfxGetXmlParser().GetInnerInt(IsShowCtrlBorder);

	sub_node_name="EnableDockPos";
	TinyXmlParser::xml_elem_type* EnableDockPos=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->EnableDockPos=AfxGetXmlParser().GetInnerInt(EnableDockPos);
	AfxGetXmlParser().save();
	return true;
}

bool config_manager::load_config_extra( tagInstrumentListConfig* config,string root_node_name/*="InstrumentList"*/, string config_file/*="config.xml*/)
{
	//PRTINF("ConfigManager::load_config_extra:InstrumentListConfig-->Enter");
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	string m_AllPanes="m_AllPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> panes=AfxGetXmlParser().GetSubElements(paneRootNode);
	//PRTINF("Find %s in %d Panes",root_node_name.c_str(),panes.size());
	int pane_idx=-1;
	for (size_t i=0;i<panes.size();++i)
	{
		string paneName=AfxGetXmlParser().GetAttributeValue(panes[i],"EName");
		if (paneName==root_node_name)
		{
			pane_idx=i;
			break;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=NULL;
	if (-1==pane_idx)
	{
		map<string,string> attr2value;
		attr2value.insert(make_pair("EName",root_node_name));
		attr2value.insert(make_pair("Name","合约列表"));
		char id_str[32];
        //itoa(config->m_paneConfig.m_PaneId,id_str,10);
        sprintf(id_str,"%d",config->m_paneConfig.m_PaneId);
		attr2value.insert(make_pair("Id",id_str));
		paneNode=AfxGetXmlParser().CreateElementWithAttributes(paneRootNode,"Pane",attr2value,"");
		if (NULL==paneNode)
		{
			return false;
		}

		//PRTINF("Save Pane Node by InstrumentList...");
		AfxGetXmlParser().save();
	}
	else
	{
		paneNode=panes[pane_idx];
	}
	//读取其他属性;
	TinyXmlParser::xml_elem_type* m_ActiveTab = AfxGetXmlParser().GetElementByTagName("m_ActiveTab");
	if (m_ActiveTab)
	{
		work_tab_item& item=config->m_ActiveTab;
		std::string strId = AfxGetXmlParser().GetAttributeValue(m_ActiveTab, "Id");
		item.id=atoi(strId.c_str());
		item.EName = AfxGetXmlParser().GetAttributeValue(m_ActiveTab, "EName");
		item.name = AfxGetXmlParser().GetAttributeValue(m_ActiveTab, "Name");
	}
	std::string sub_node_name;
	sub_node_name="m_bEnableEdit";
	TinyXmlParser::xml_elem_type* m_bEnableEdit=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_bEnableEdit)
	{
		config->m_bEnableEdit=AfxGetXmlParser().GetInnerInt(m_bEnableEdit);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_bEnableEdit=0;
	}
	sub_node_name="m_bEnableSelection";
	TinyXmlParser::xml_elem_type* m_bEnableSelection=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_bEnableSelection)
	{
		config->m_bEnableSelection=AfxGetXmlParser().GetInnerInt(m_bEnableEdit);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_bEnableSelection=0;
	}

	sub_node_name="m_bEnableDragDrop";
	TinyXmlParser::xml_elem_type* m_bEnableDragDrop=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_bEnableDragDrop)
	{
		config->m_bEnableDragDrop=AfxGetXmlParser().GetInnerInt(m_bEnableDragDrop);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_bEnableDragDrop=0;
	}

	sub_node_name="m_bEnableTabKey";
	TinyXmlParser::xml_elem_type* m_bEnableTabKey=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_bEnableTabKey)
	{
		config->m_bEnableTabKey=AfxGetXmlParser().GetInnerInt(m_bEnableTabKey);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_bEnableTabKey=0;
	}

	sub_node_name="m_bEnableTitleTip";
	TinyXmlParser::xml_elem_type* m_bEnableTitleTip=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_bEnableTitleTip)
	{
		config->m_bEnableTitleTip=AfxGetXmlParser().GetInnerInt(m_bEnableTitleTip);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_bEnableTitleTip=0;
	}

	sub_node_name="m_bEnableSort";
	TinyXmlParser::xml_elem_type* m_bEnableSort=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	
	if (NULL!=m_bEnableSort)
	{
		config->m_bEnableSort=AfxGetXmlParser().GetInnerInt(m_bEnableSort);
	} 
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_bEnableSort=0;
	}

	sub_node_name="m_nGridLines";
	TinyXmlParser::xml_elem_type* m_nGridLines=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_nGridLines)
	{
		config->m_nGridLines=AfxGetXmlParser().GetInnerInt(m_nGridLines);
	} 
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_nGridLines=0;
	}
	
	sub_node_name="m_nColSort";
	TinyXmlParser::xml_elem_type* m_nColSort=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_nColSort)
	{
		config->m_nColSort=AfxGetXmlParser().GetInnerInt(m_nColSort);
	} 
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_nColSort=0;
	}
	

	sub_node_name="m_nWorkTabHeight";
	TinyXmlParser::xml_elem_type* m_nWorkTabHeight=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL!=m_nWorkTabHeight)
	{
		config->m_nWorkTabHeight=AfxGetXmlParser().GetInnerInt(m_nWorkTabHeight);
	}
	else
	{
		AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"0");
		config->m_nWorkTabHeight=0;
	}
	if (config->m_paneConfig.m_PaneEName.empty())
	{
		config->m_paneConfig.m_PaneEName=root_node_name;
	}
	if (config->m_paneConfig.m_PaneEName.empty())
	{
		config->m_paneConfig.m_PaneEName="InstrumentList";
	}
	if (config->m_strTabTitle.empty())
	{
		config->m_strTabTitle="合约列表";
	}
	AfxGetXmlParser().save();
	//PRTINF("ConfigManager::load_config_extra:InstrumentListConfig-->Leave");
	return load_config(&(config->m_paneConfig),AfxGetXmlParser().GetXmlFilePath());
}

bool config_manager::load_config_extra( tagRealTimeConfig* config,string root_node_name/*="RealTimeGraph"*/,string config_file/*="config.xml"*/ )
{
	//PRTINF("ConfigManager::load_config_extra-->RealtimeConfig-->Enter");
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	string m_AllPanes="m_AllPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		attr_value.insert(make_pair("Name","分时行情图"));
		attr_value.insert(make_pair("EName",root_node_name));
		char str_pane_id[8];
		sprintf(str_pane_id,"%d",config->m_paneConfig.m_PaneId);
		attr_value.insert(make_pair("Id",str_pane_id));
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
		AfxGetXmlParser().save();
	}
	vector<TinyXmlParser::xml_elem_type*> panes=AfxGetXmlParser().GetSubElements(paneRootNode);
	int pane_idx=-1;
	for (size_t i=0;i<panes.size();++i)
	{
		string paneName=AfxGetXmlParser().GetAttributeValue(panes[i],"EName");
		if (paneName==root_node_name)
		{
			pane_idx=i;
			break;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=NULL;
	if (-1==pane_idx)
	{
		map<string,string> attr2value;
		attr2value.insert(make_pair("EName",root_node_name));
		attr2value.insert(make_pair("Name","分时行情图"));
		char id_str[32];
        //itoa(config->m_paneConfig.m_PaneId,id_str,10);
         sprintf(id_str,"%d",config->m_paneConfig.m_PaneId);
		attr2value.insert(make_pair("Id",id_str));
		paneNode=AfxGetXmlParser().CreateElementWithAttributes(paneRootNode,"Pane",attr2value,"");
		if (NULL==paneNode)
		{
			return false;
		}
		AfxGetXmlParser().save();
	}
	else
	{
		paneNode=panes[pane_idx];
	}
	config->m_strTitle=AfxGetXmlParser().GetAttributeValue(paneNode,"Name");
	TinyXmlParser::xml_elem_type* m_TabItems=AfxGetXmlParser().GetSubElementByTagName(paneNode,"m_TabItems");
	vector<TinyXmlParser::xml_elem_type*> items=AfxGetXmlParser().GetSubElements(m_TabItems);
	for (size_t i=0;i<items.size();i++)
	{
		work_tab_item item;
		string strId=AfxGetXmlParser().GetAttributeValue(items[i],"Id");
		item.id=atoi(strId.c_str());
		item.EName=AfxGetXmlParser().GetAttributeValue(items[i],"EName");
		item.name=AfxGetXmlParser().GetAttributeValue(items[i],"Name");
		config->m_vecTabItems.push_back(item);
	}
	string sub_node_name;
	sub_node_name="m_nMultiInstrumentCount";
	TinyXmlParser::xml_elem_type* m_nMultiInstrumentCount=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->m_nMultiInstrumentCount=AfxGetXmlParser().GetInnerInt(m_nMultiInstrumentCount);
	config->m_paneConfig.m_PaneEName=AfxGetXmlParser().GetAttributeValue(paneNode,"EName");
	config->m_paneConfig.m_PaneId=atoi(AfxGetXmlParser().GetAttributeValue(paneNode,"Id").c_str());
	//PRTINF("ConfigManager::load_config_extra-->RealtimeConfig-->Leave");
	return load_config(&(config->m_paneConfig),config_file);
}

bool config_manager::load_config_extra( tagKLineGraphConfig* config,string root_node_name/*="KLineGraph"*/,string config_file/*="config.xml"*/ )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	string m_AllPanes="m_AllPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		attr_value.insert(make_pair("Name","K线图"));
		attr_value.insert(make_pair("EName",root_node_name));
		char str_pane_id[8];
		sprintf(str_pane_id,"%d",config->m_paneConfig.m_PaneId);
		attr_value.insert(make_pair("Id",str_pane_id));
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> panes=AfxGetXmlParser().GetSubElements(paneRootNode);
	int pane_idx=-1;
	for (size_t i=0;i<panes.size();++i)
	{
		string paneName=AfxGetXmlParser().GetAttributeValue(panes[i],"EName");
		if (paneName==root_node_name)
		{
			pane_idx=i;
			break;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=NULL;
	if (-1==pane_idx)
	{
		map<string,string> attr2value;
		attr2value.insert(make_pair("EName",root_node_name));
		attr2value.insert(make_pair("Name",config->m_paneConfig.m_PaneName));
		char id_str[32];
        //itoa(config->m_paneConfig.m_PaneId,id_str,10);
         sprintf(id_str,"%d",config->m_paneConfig.m_PaneId);
		attr2value.insert(make_pair("Id",id_str));
		paneNode=AfxGetXmlParser().CreateElementWithAttributes(paneRootNode,"Pane",attr2value,"");
		if (NULL==paneNode)
		{
			return false;
		}
	}
	else
	{
		paneNode=panes[pane_idx];
	}
	config->m_strTitle=AfxGetXmlParser().GetAttributeValue(paneNode,"Name");
	TinyXmlParser::xml_elem_type* m_TabItems=AfxGetXmlParser().GetSubElementByTagName(paneNode,"m_TabItems");
	vector<TinyXmlParser::xml_elem_type*> items=AfxGetXmlParser().GetSubElements(m_TabItems);
	for (size_t i=0;i<items.size();i++)
	{
		work_tab_item item;
		string strId=AfxGetXmlParser().GetAttributeValue(items[i],"Id");
		item.id=atoi(strId.c_str());
		item.EName=AfxGetXmlParser().GetAttributeValue(items[i],"EName");
		item.name=AfxGetXmlParser().GetAttributeValue(items[i],"Name");
		config->m_vecTabItems.push_back(item);
	}
	// 	string sub_node_name;
	// 	sub_node_name="m_nMultiInstrumentCount";
	// 	TinyXmlParser::xml_elem_type* m_nMultiInstrumentCount=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);

	config->m_paneConfig.m_PaneEName=AfxGetXmlParser().GetAttributeValue(paneNode,"EName");
	config->m_paneConfig.m_PaneId=atoi(AfxGetXmlParser().GetAttributeValue(paneNode,"Id").c_str());
	return load_config(&(config->m_paneConfig),config_file);
}

bool config_manager::load_config_extra( tagMultiSortConfig* config,string root_node_name/*="MultiSortGraph"*/,const string& config_file/*="config.xml"*/ )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	string m_AllPanes="m_AllPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		attr_value.insert(make_pair("Name","综合排名图"));
		attr_value.insert(make_pair("EName",root_node_name));
		char str_pane_id[8];
		sprintf(str_pane_id,"%d",config->m_paneConfig.m_PaneId);
		attr_value.insert(make_pair("Id",str_pane_id));
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> panes=AfxGetXmlParser().GetSubElements(paneRootNode);
	int pane_idx=-1;
	for (size_t i=0;i<panes.size();++i)
	{
		string paneName=AfxGetXmlParser().GetAttributeValue(panes[i],"EName");
		if (paneName==root_node_name)
		{
			pane_idx=i;
			break;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=NULL;
	if (-1==pane_idx)
	{
		map<string,string> attr2value;
		attr2value.insert(make_pair("EName",root_node_name));
		attr2value.insert(make_pair("Name",config->m_paneConfig.m_PaneName));
		char id_str[32];
        //itoa(config->m_paneConfig.m_PaneId,id_str,10);
         sprintf(id_str,"%d",config->m_paneConfig.m_PaneId);
		attr2value.insert(make_pair("Id",id_str));
		paneNode=AfxGetXmlParser().CreateElementWithAttributes(paneRootNode,"Pane",attr2value,"");
		if (NULL==paneNode)
		{
			return false;
		}
	}
	else
	{
		paneNode=panes[pane_idx];
	}
	config->m_strTitle=AfxGetXmlParser().GetAttributeValue(paneNode,"Name");
	TinyXmlParser::xml_elem_type* m_TabItems=AfxGetXmlParser().GetSubElementByTagName(paneNode,"m_TabItems");
	vector<TinyXmlParser::xml_elem_type*> items=AfxGetXmlParser().GetSubElements(m_TabItems);
	for (size_t i=0;i<items.size();i++)
	{
		work_tab_item item;
		string strId=AfxGetXmlParser().GetAttributeValue(items[i],"Id");
		item.id=atoi(strId.c_str());
		item.EName=AfxGetXmlParser().GetAttributeValue(items[i],"EName");
		item.name=AfxGetXmlParser().GetAttributeValue(items[i],"Name");
		config->m_vecTabItems.push_back(item);
	}
// 	string sub_node_name;
// 	sub_node_name="m_nMultiInstrumentCount";
// 	TinyXmlParser::xml_elem_type* m_nMultiInstrumentCount=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);

	config->m_paneConfig.m_PaneEName=AfxGetXmlParser().GetAttributeValue(paneNode,"EName");
	config->m_paneConfig.m_PaneId=atoi(AfxGetXmlParser().GetAttributeValue(paneNode,"Id").c_str());
	return load_config(&(config->m_paneConfig),config_file);
}

bool config_manager::load_config_extra( tagStrategyConfig* config,const std::string& root_node_name/*="StrategyList"*/,const string& config_file/*="config.xml"*/ )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	std::string m_AllPanes="m_AllPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		std::map<std::string,std::string> attr_value;
		attr_value.insert(make_pair("Name","策略列表"));
		attr_value.insert(make_pair("EName",root_node_name));
		char str_pane_id[8];
		sprintf(str_pane_id,"%d",config->m_paneConfig.m_PaneId);
		attr_value.insert(make_pair("Id",str_pane_id));
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> panes=AfxGetXmlParser().GetSubElements(paneRootNode);
	int pane_idx=-1;
	for (size_t i=0;i<panes.size();++i)
	{
		std::string paneName=AfxGetXmlParser().GetAttributeValue(panes[i],"EName");
		if (paneName==root_node_name)
		{
			pane_idx=i;
			break;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=NULL;
	if (-1==pane_idx)
	{
		std::map<std::string,std::string> attr2value;
		attr2value.insert(make_pair("EName",root_node_name));
		attr2value.insert(make_pair("Name",config->m_paneConfig.m_PaneName));
		char id_str[32];
        //itoa(config->m_paneConfig.m_PaneId,id_str,10);
        sprintf(id_str,"%d",config->m_paneConfig.m_PaneId);
		attr2value.insert(make_pair("Id",id_str));
		paneNode=AfxGetXmlParser().CreateElementWithAttributes(paneRootNode,"Pane",attr2value,"");
		if (NULL==paneNode)
		{
			return false;
		}
	}
	else
	{
		paneNode=panes[pane_idx];
	}
	config->m_strTitle=AfxGetXmlParser().GetAttributeValue(paneNode,"Name");
	TinyXmlParser::xml_elem_type* m_TabItems=AfxGetXmlParser().GetSubElementByTagName(paneNode,"m_StrategyItems");
	vector<TinyXmlParser::xml_elem_type*> items=AfxGetXmlParser().GetSubElements(m_TabItems);
	for (size_t i=0;i<items.size();i++)
	{
 		string strId=AfxGetXmlParser().GetAttributeValue(items[i],"Id");
		config->m_vecStrategyFileItems.push_back(strId);
	}
	string sub_node_name;
	sub_node_name="m_strLastStrategyDirectory";
	TinyXmlParser::xml_elem_type* m_strLastStrategyDirectory=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->m_strLastStrategyDirectory=AfxGetXmlParser().GetInnerText(m_strLastStrategyDirectory);

	//m_strStrategyFileExt
	sub_node_name="m_strStrategyFileExt";
	TinyXmlParser::xml_elem_type* m_strStrategyFileExt=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	config->m_strStrategyFileExt=AfxGetXmlParser().GetInnerText(m_strStrategyFileExt);
	return true;
}

bool config_manager::save_config( const string& config_file,tagUsersListConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "ListCtrl列头"
	bool bSave=save_list_column(config_file,"ListCtrl当前用户",config->m_vecColumnItems);
	AfxGetXmlParser().save();
	return bSave;
}

bool config_manager::save_config( const string& config_file,tagUserFundsListConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns "资金账户"
	bool bSave=save_list_column(config_file,"资金账户",config->m_vecColumnItems);
	AfxGetXmlParser().save();
	return bSave;
}

bool config_manager::save_config( const string& config_file,tagTradeHistoryListConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns ListCtrl成交记录
	bool bSave=save_list_column(config_file,"ListCtrl成交记录",config->m_vecColumnItems);
	AfxGetXmlParser().save();
	return bSave;
}

bool config_manager::save_config( const string& config_file,tagOrderListConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns ListCtrl成交记录
	bool bSave=save_list_column(config_file,"ListCtrl所有委托单",config->m_vecColumnItems);
	AfxGetXmlParser().save();
	return bSave;
}

bool config_manager::save_config( const string& config_file,tagPositionListConfig* config )
{
	//ListCtrl持仓
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns ListCtrl成交记录
	bool bSave=save_list_column(config_file,config->m_strTitle,config->m_vecColumnItems);
	AfxGetXmlParser().save();
	return bSave;
}

bool config_manager::save_config( const string& config_file,tagBank2FutureConfig* config )
{
	//ListCtrl持仓
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns ListCtrl成交记录
	bool bSave=save_list_column(config_file,"ListCtrl银期转帐",config->m_vecColumnItems);
	AfxGetXmlParser().save();
	return bSave;
}

bool config_manager::save_config( const string& config_file,tagBrokerListConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	if (config->m_Broker.size()<=0)
	{
		return true;
	}
	//todo:读取配置文件 m_ListCtrlColumns ListCtrl成交记录
	TiXmlElement* element=AfxGetXmlParser().GetElementByTagName("brokers");
	if (NULL==element)
	{
		element=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),"brokers",
			map<string,string>(),"");
	}
	//vector<TiXmlElement*> brokers= AfxGetXmlParser().GetSubElements(element);
	if (config->m_Broker.size()<=0)
	{
		return true;
	}
	//AfxGetXmlParser().RemoveAllChildNodes(element);
	//for (size_t i=0;i<config->m_Broker.size();i++)
	//{
	//	server_info b=config->m_Broker[i];
	//	map<string,string> broker_attr_value;
	//	broker_attr_value.insert(make_pair("BrokerID",b.server_id));
	//	broker_attr_value.insert(make_pair("BrokerName",b.BrokerName));
	//	broker_attr_value.insert(make_pair("BrokerEName",b.BrokerEName));
	//	TiXmlElement* broker_node=AfxGetXmlParser().CreateElementWithAttributes(element,"broker",broker_attr_value,"");
	//	AfxGetXmlParser().CreateElementWithAttributes(broker_node,"BrokerID",map<string,string>(),b.server_id);
	//	AfxGetXmlParser().CreateElementWithAttributes(broker_node,"BrokerName",map<string,string>(),b.BrokerName);
	//	AfxGetXmlParser().CreateElementWithAttributes(broker_node,"BrokerEName",map<string,string>(),b.BrokerEName);
	//	TiXmlElement* servers_node=AfxGetXmlParser().CreateElementWithAttributes(broker_node,"Servers",map<string,string>(),"");
	//	for (size_t j=0;j<b.server_addrs.size();j++)
	//	{
	//		ServerInfo server=b.server_addrs[j];
	//		map<string,string> server_attr_value;
	//		server_attr_value.insert(make_pair("Name",server.name));
	//		server_attr_value.insert(make_pair("Protocol",server.protocol));
	//		//server_attr_value.insert(make_pair("Market",server.MarketData));
	//		//server_attr_value.insert(make_pair("Name",server.Name));
	//		TiXmlElement* server_node=AfxGetXmlParser().CreateElementWithAttributes(servers_node,"Server",server_attr_value,"");

	//		TiXmlElement* server_name_node=AfxGetXmlParser().CreateElementWithAttributes(servers_node,"Name",map<string,string>(),server.name);
	//		TiXmlElement* server_protocol_node=AfxGetXmlParser().CreateElementWithAttributes(servers_node,
	//			"Protocol",map<string,string>(),server.protocol);
	//		TiXmlElement* market_node=AfxGetXmlParser().CreateElementWithAttributes(servers_node,"Market",map<string,string>(),"");
	//		TiXmlElement* trade_node=AfxGetXmlParser().CreateElementWithAttributes(servers_node,"Trade",map<string,string>(),"");
	//		for (size_t k=0;k<server.trade_server_front.size();++k)
	//		{
	//			AfxGetXmlParser().CreateElementWithAttributes(trade_node,"Item",map<string,string>(),server.trade_server_front[k]);
	//		}
	//		for (size_t k=0;k<server.market_server_front.size();++k)
	//		{
	//			AfxGetXmlParser().CreateElementWithAttributes(market_node,"Item",map<string,string>(),server.market_server_front[k]);
	//		}
	//	}
	//}
	//AfxGetXmlParser().save();
	return true;
}

bool config_manager::save_config( const string& config_file,tagMainUiConfig* config )
{
	return true;
}

bool config_manager::save_config( const string& config_file,tagEmbeddedListConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns ListCtrl成交记录
	bool bSave=save_list_column(config_file,"ListCtrl银期转帐",config->m_vecColumnItems);
	AfxGetXmlParser().save();
	return bSave;
}

bool config_manager::save_config( const string& config_file,tagLoginedUserConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	TiXmlElement* element=AfxGetXmlParser().GetElementByTagName(config->m_strTitle);
	if (NULL==element)
	{
		map<string,string> attr_value;
		attr_value.insert(make_pair("lastInvestorID",config->lastInvestorID));
		element=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),config->m_strTitle,
			attr_value,"");
	}
	else
	{
		AfxGetXmlParser().SetAttributeValue(element,"lastInvestorID",config->lastInvestorID);
	}
	//vector<TiXmlElement*> brokers= AfxGetXmlParser().GetSubElements(element);
	if (config->m_mapUsers.size()<=0)
	{
		return true;
	}
	AfxGetXmlParser().RemoveAllChildNodes(element);
	map<string,UserAccountConfig>::iterator it=config->m_mapUsers.begin();
	for (;it!=config->m_mapUsers.end();++it)
	{
		UserAccountConfig uac=it->second;
		map<string,string> attr_value;
		attr_value.insert(make_pair("BrokerID",uac.BrokerID));
		attr_value.insert(make_pair("InvestorID",uac.InvestorID));
		attr_value.insert(make_pair("Password",uac.Password));
		attr_value.insert(make_pair("AutoLogin",uac.auto_login?"1":"0"));
		attr_value.insert(make_pair("SavePassword",uac.save_password?"1":"0"));
		/*TiXmlElement* broker_node=*/AfxGetXmlParser().CreateElementWithAttributes(element,"Item",attr_value,"");
	}
	AfxGetXmlParser().save();
	return true;
}

bool config_manager::save_config(tagDataSourceConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(get_filename()))
	{
		return false;
	}
	//读取数据源
	string data_source_root_node_name="datasources";
	TiXmlElement* datasources_node=AfxGetXmlParser().GetElementByTagName(data_source_root_node_name);
	TiXmlElement* list_element=NULL;
	if (NULL==datasources_node)
	{
		datasources_node=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			data_source_root_node_name,map<string,string>(),"");
		if (NULL==datasources_node)
		{
			return false;
		}
	}
	else
	{
		//读取用户节点
		vector<TiXmlElement*> vec_all_sequence=AfxGetXmlParser().GetSubElements(datasources_node);
		size_t i=0;
		for (;i<vec_all_sequence.size();i++)
		{
			if (AfxGetXmlParser().HasAttribute(vec_all_sequence[i],"key"))
			{
				string attrValue=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"key");
				map<string,map<string,host_info> >::iterator uit=config->m_mapUser2Database.find(attrValue);
				if (config->m_mapUser2Database.end()!=uit)
				{
					//更新数据
					AfxGetXmlParser().SetAttributeValue(vec_all_sequence[i],"key",uit->first);
				}
				if (attrValue==config->default_username) //这里要取用户名为参数
				{
					list_element=vec_all_sequence[i];
					break;
				}
			}
		}
		//将map中剩下的数据也保存起来;
		map<string,map<string,host_info> >::iterator uit=config->m_mapUser2Database.begin();
		for (;uit!=config->m_mapUser2Database.end();++uit)
		{
			map<string,string> attr2val;
			attr2val.insert(make_pair("key",config->default_username));
			attr2val.insert(make_pair("file_dir",config->default_username));
			list_element=AfxGetXmlParser().CreateElementWithAttributes(datasources_node,"datasource",attr2val,"");
			if (NULL==list_element)
			{
				return false;
			}
		}
	}
	if (NULL==list_element)
	{
		//该结点下子结点为空
		
		config->file_store_path=config->default_username;
		AfxGetXmlParser().save();
		return true;
	}
	//文件存储目录
	

	config->file_store_path=AfxGetXmlParser().GetAttributeValue(list_element,"file_dir");
	vector<TiXmlElement*> list_header_columns=AfxGetXmlParser().GetSubElements(list_element);
	if (list_header_columns.size()<=0)
	{
		//什么也没有,没有用户配置过数据源
		return true;
	}
	else
	{
		map<string,host_info> user_host_infos;
		for (size_t j=0;j<list_header_columns.size();j++)
		{
			host_info aHost;
			aHost.name=(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"name"));
			aHost.type=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"type");
			aHost.host=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"host");

			aHost.port=atoi(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"port").c_str());
			aHost.username=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"username");
			aHost.password=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"password");
			user_host_infos.insert(make_pair(aHost.name,aHost));
		}
		config->m_mapUser2Database.insert(make_pair(config->default_username,user_host_infos));
	}
	return true;
}

bool config_manager::save_config( PaneConfig& config,const string& config_file/*="config.xml"*/ )
{
	return true;
}

bool config_manager::save_config( tagMultiPaneConfig* config )
{
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(get_filename()))
	{
		return false;
	}
	string m_AllPanes="m_MultiPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=paneRootNode;
	//读取其他属性;
	TinyXmlParser::xml_elem_type* styleElems=AfxGetXmlParser().GetElementByTagName("Style");
	if (!styleElems)
	{
		styleElems=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,map<string,string>(),"");
		if (NULL==styleElems)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> items=AfxGetXmlParser().GetSubElements(styleElems);
	for (size_t i=0;i<items.size();i++)
	{
		int id=atoi(AfxGetXmlParser().GetAttributeValue(items[i],"Id").c_str());
		string styleName=AfxGetXmlParser().GetAttributeValue(items[i],"EName");
		config->m_mapStyles.insert(make_pair(id,styleName));
	}
	//窗口使用哪种风格;
	AfxGetXmlParser().SetAttributeValue(styleElems,"selected",config->selectStyle);

	string sub_node_name;
	sub_node_name="TabMinWidth";
	TinyXmlParser::xml_elem_type* TabMinWidth=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (TabMinWidth)
	{
		if (config->TabMinWidth<=0)
		{
			config->TabMinWidth=200;
		}
		AfxGetXmlParser().SetInnerText(TabMinWidth,config->TabMinWidth);
	}
	else
	{
		TabMinWidth=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,
			map<string,string>(),"200");
		config->TabMinWidth=200;
	}
	sub_node_name="TabMinHeight";
	TinyXmlParser::xml_elem_type* TabMinHeight=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (TabMinHeight)
	{
		config->TabMinHeight=AfxGetXmlParser().GetInnerInt(TabMinHeight);
		if (config->TabMinHeight<=0)
		{
			config->TabMinHeight=200;
		}
		AfxGetXmlParser().SetInnerText(TabMinHeight,config->TabMinHeight);
	}
	else
	{
		TabMinHeight=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"200");
		config->TabMinHeight=200;
	}
	sub_node_name="TabLayout";
	TinyXmlParser::xml_elem_type* TabLayout=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!TabLayout)
	{
		TabLayout=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(TabLayout,config->TabLayout);

	sub_node_name="SplitterDraggingMode";
	TinyXmlParser::xml_elem_type* SplitterDraggingMode=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!SplitterDraggingMode)
	{
		SplitterDraggingMode=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(SplitterDraggingMode,config->SplitterDraggingMode);
	sub_node_name="TabBehavior";
	TinyXmlParser::xml_elem_type* TabBehavior=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!TabBehavior)
	{
		TabBehavior=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(TabBehavior,config->TabBehavior);

	sub_node_name="IsShowBorder";
	TinyXmlParser::xml_elem_type* IsShowBorder=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowBorder)
	{
		IsShowBorder=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(TabBehavior,config->IsShowBorder);

	sub_node_name="IsCustomCursor";
	TinyXmlParser::xml_elem_type* IsCustomCursor=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsCustomCursor)
	{
		IsCustomCursor=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsCustomCursor,config->IsCustomCursor);

	sub_node_name="IsShowClientEdgeBorder";
	TinyXmlParser::xml_elem_type* IsShowClientEdge=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowClientEdge)
	{
		IsShowClientEdge=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsShowClientEdge,config->IsShowClientEdge);
	sub_node_name="IsSplliterInActivate";
	TinyXmlParser::xml_elem_type* IsSplliterInActivate=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsSplliterInActivate)
	{
		IsSplliterInActivate=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsSplliterInActivate,config->IsSplitterInActive);

	sub_node_name="IsShowImage";
	TinyXmlParser::xml_elem_type* IsShowImage=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowImage)
	{
		IsShowImage=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsSplliterInActivate,config->IsShowImage);



	sub_node_name="IsHideSingleTab";
	TinyXmlParser::xml_elem_type* IsHideSingleTab=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsHideSingleTab)
	{
		IsHideSingleTab=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsHideSingleTab,config->IsHideSingleTab);

	sub_node_name="IsRemoveTabEnable";
	TinyXmlParser::xml_elem_type* IsRemoveTabEnable=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsRemoveTabEnable)
	{
		IsRemoveTabEnable=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsRemoveTabEnable,config->IsRemoveTabEnable);


	sub_node_name="IsDragTabEnable";
	TinyXmlParser::xml_elem_type* IsDragTabEnable=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsDragTabEnable)
	{
		IsDragTabEnable=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsDragTabEnable,config->IsDragTabEnable);


	sub_node_name="IsWatchActivityCtrl";
	TinyXmlParser::xml_elem_type* IsWatchActivityCtrl=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsWatchActivityCtrl)
	{
		IsWatchActivityCtrl=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsWatchActivityCtrl,config->IsWatchActivityCtrl);



	sub_node_name="IsShowSelectedFont";
	TinyXmlParser::xml_elem_type* IsShowSelectedFont=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowSelectedFont)
	{
		IsShowSelectedFont=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsShowSelectedFont,config->IsShowSelectedFont);


	sub_node_name="IsShowTabCloseButton";
	TinyXmlParser::xml_elem_type* IsShowTabCloseButton=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowTabCloseButton)
	{
		IsShowTabCloseButton=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsShowTabCloseButton,config->IsShowTabCloseButton);

	sub_node_name="IsShowTabMenuButton";
	TinyXmlParser::xml_elem_type* IsShowTabMenuButton=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowTabMenuButton)
	{
		IsShowTabMenuButton=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsShowTabMenuButton,config->IsShowTabMenuButton);

	sub_node_name="IsShowSelectedFont";
	TinyXmlParser::xml_elem_type* IsShowTabScrollButton=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowTabScrollButton)
	{
		IsShowTabScrollButton=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsShowTabScrollButton,config->IsShowTabScrollButton);

	sub_node_name="IsShowSelectedFont";
	TinyXmlParser::xml_elem_type* IsShowCtrlBorder=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!IsShowCtrlBorder)
	{
		IsShowCtrlBorder=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(IsShowCtrlBorder,config->IsShowCtrlBorder);

	sub_node_name="EnableDockPos";
	TinyXmlParser::xml_elem_type* EnableDockPos=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (!EnableDockPos)
	{
		EnableDockPos=AfxGetXmlParser().CreateElementWithAttributes(paneNode,
			sub_node_name,map<string,string>(),"0");
	}
	AfxGetXmlParser().SetInnerText(EnableDockPos,config->EnableDockPos);

	AfxGetXmlParser().save();
	return true;
}

bool config_manager::save_list_column( const string& config_file,const string& list_name,vector<ColumnInfo>& m_vecColumnItems )
{
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	if (m_vecColumnItems.size()<=0)
	{
		return true;
	}
	//todo:读取配置文件 m_ListCtrlColumns
	TiXmlElement* listCtrlColumns=AfxGetXmlParser().GetElementByTagName(m_ListCtrlColumns);
	vector<TiXmlElement*> vec_all_sequence=AfxGetXmlParser().GetSubElements(listCtrlColumns);
	size_t i=0;
	for (;i<vec_all_sequence.size();i++)
	{
		if (AfxGetXmlParser().HasAttribute(vec_all_sequence[i],"key"))
		{
			string attrValue=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"key");
			if (attrValue==list_name)
			{
				break;
			}
		}
	}
	if (i>=vec_all_sequence.size())
	{
		return false;
	}
	if (m_vecColumnItems.size()>0)
	{
		AfxGetXmlParser().RemoveAllChildNodes(vec_all_sequence[i]);
	}
	size_t j=0;
	for (;j<m_vecColumnItems.size();j++)
	{
		ColumnInfo ci=m_vecColumnItems[j];
		map<string,string> attr2val;
		attr2val.insert(make_pair("Visible",ci.isVisible?"1":"0"));
		attr2val.insert(make_pair("EName",ci.m_ColumnEName));
		attr2val.insert(make_pair("Express",ci.m_Express));
		attr2val.insert(make_pair("Name",ci.m_ColumnName));
		char buf[32]={0};
        //itoa(ci.m_ColumnId,buf,10);
        sprintf(buf,"%d",ci.m_ColumnId);
		attr2val.insert(make_pair("Id",buf));
        //itoa(ci.m_ColumnWidth,buf,10);
        sprintf(buf,"%d",ci.m_ColumnWidth);
		attr2val.insert(make_pair("Width",buf));
        //itoa(ci.bkColor,buf,10);
         sprintf(buf,"%d",ci.bkColor);
		attr2val.insert(make_pair("bkColor",buf));
        //itoa(ci.planeColor,buf,10);
         sprintf(buf,"%d",ci.planeColor);
		attr2val.insert(make_pair("planeColor",buf));
        //itoa(ci.raiseColor,buf,10);
        sprintf(buf,"%d",ci.raiseColor);
		attr2val.insert(make_pair("raiseColor",buf));
        //itoa(ci.fallColor,buf,10);
        sprintf(buf,"%d",ci.fallColor);
		attr2val.insert(make_pair("fallColor",buf));
		AfxGetXmlParser().CreateElementWithAttributes(vec_all_sequence[i],"item",attr2val,"");
	}
	AfxGetXmlParser().save();
	return true;
}
bool config_manager::load_default_column( const string& list_name,vector<ColumnInfo>& m_vecColumnItems )
{
	//加载默认数据
	map<string,map<int,SLH_CTP_DATA > >::iterator default_list_column_it=m_listname2listfield.find(list_name);
	if (m_listname2listfield.end()==default_list_column_it)
	{
		return false;
	}

	map<int,SLH_CTP_DATA >::iterator it=
		default_list_column_it->second.begin();
	for (;it!=default_list_column_it->second.end();++it)
	{
		ColumnInfo ci;
		ci.isVisible=true;
		ci.m_ColumnEName=it->second.ename;
		ci.m_ColumnName=it->second.name;
		ci.m_Express=it->second.ename;
		ci.m_ColumnId=it->second.id;
		ci.m_ColumnWidth=it->second.weight;
		ci.bkColor=!(ci.planeColor=ci.raiseColor=ci.fallColor=0);
		m_vecColumnItems.push_back(ci);
	}
	return true;
}

bool config_manager::load_list_column( const string& config_file,const string& list_name,vector<ColumnInfo>& m_vecColumnItems )
{
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns
	TiXmlElement* listCtrlColumns=AfxGetXmlParser().GetElementByTagName(m_ListCtrlColumns);
	TiXmlElement* list_element=NULL;
	if (NULL==listCtrlColumns)
	{
		listCtrlColumns=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_ListCtrlColumns,map<string,string>(),"");
		if (NULL==listCtrlColumns)
		{
			return false;
		}
	}
	else
	{
		vector<TiXmlElement*> vec_all_sequence=AfxGetXmlParser().GetSubElements(listCtrlColumns);
		size_t i=0;
		for (;i<vec_all_sequence.size();i++)
		{
			if (AfxGetXmlParser().HasAttribute(vec_all_sequence[i],"key"))
			{
				string attrValue=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"key");
				if (attrValue==list_name)
				{
					list_element=vec_all_sequence[i];
					break;
				}
			}
		}
	}
	if (NULL==list_element)
	{
		//该结点下子结点为空
		map<string,string> attr2val;
		attr2val.insert(make_pair("key",list_name));
		list_element=AfxGetXmlParser().CreateElementWithAttributes(listCtrlColumns,"sequence",attr2val,"");
		if (NULL==list_element)
		{
			return false;
		}
		AfxGetXmlParser().save();
	}
	vector<TiXmlElement*> list_header_columns=AfxGetXmlParser().GetSubElements(list_element);
	if (list_header_columns.size()<=0)
	{
		load_default_column(list_name,m_vecColumnItems);
		return save_list_column(config_file,list_name,m_vecColumnItems);
	}
	else
	{
		for (size_t j=0;j<list_header_columns.size();j++)
		{
			ColumnInfo ci;
			ci.isVisible=(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Visible")=="1");
			ci.m_ColumnEName=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"EName");
			ci.m_ColumnName=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Name");
			ci.m_Express=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Express");
			ci.m_ColumnId=atoi(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Id").c_str());
			ci.m_ColumnWidth=atoi(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Width").c_str());
			if (ci.m_ColumnWidth<=0)
			{
				ci.m_ColumnWidth=64;
			}
			ci.bkColor=atol(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"bkColor").c_str());
			if (ci.bkColor<0 || ci.bkColor> RGB(255,255,255))
			{
				//默认背景色为黑
				ci.bkColor=RGB(0,0,0);
			}
			ci.planeColor=atol(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"planeColor").c_str());
			if (ci.planeColor<0 || ci.planeColor> RGB(255,255,255))
			{
				//默认背景色为黑
				ci.planeColor=RGB(128,128,128);
			}
			ci.raiseColor=atol(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"raiseColor").c_str());
			if (ci.raiseColor<0 || ci.raiseColor> RGB(255,255,255))
			{
				//默认背景色为黑
				ci.raiseColor=RGB(255,0,0);
			}
			ci.fallColor=atol(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"fallColor").c_str());
			if (ci.fallColor<0 || ci.fallColor> RGB(255,255,255))
			{
				//默认背景色为黑
				ci.fallColor=RGB(0,255,0);
			}
			m_vecColumnItems.push_back(ci);
		}
		return true;
	}
}

std::string config_manager::get_name_by_id( int slh_idx,bool bEname/*=false*/ )
{
	map<string,map<int,SLH_CTP_DATA > >::iterator default_list_column_it=m_listname2listfield.begin();
	while (m_listname2listfield.end()!=default_list_column_it)
	{
		map<int,SLH_CTP_DATA >::iterator it=default_list_column_it->second.find(slh_idx);
		if (default_list_column_it->second.end()!=it)
		{
			if (bEname)
			{
				return it->second.ename;
			}
			return it->second.name;
		}
		++default_list_column_it;
	}
	return "";
}

bool config_manager::load_strings_config( const string& config_file/*="config.xml"*/ )
{
	bool bLoaded=true;

	map<string,map<int,SLH_CTP_DATA > >::iterator default_list_column_it=m_listname2listfield.begin();
	while (m_listname2listfield.end()!=default_list_column_it)
	{
		bool bIsLoad=load_string_config(default_list_column_it->first,default_list_column_it->second);
		if (false==bIsLoad)
		{
			bLoaded=bIsLoad;
		}
		++default_list_column_it;
	}
	return bLoaded;
}

bool config_manager::load_string_config( const string& list_name,map<int,SLH_CTP_DATA>& str_map,const string& config_file/*="config.xml"*/ )
{
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns
	string m_AllPaneStrings="m_AllPaneStrings";
	TiXmlElement* m_AllPaneStringsNode=AfxGetXmlParser().GetElementByTagName(m_AllPaneStrings);
	TiXmlElement* list_element=NULL;
	if (NULL==m_AllPaneStringsNode)
	{
		m_AllPaneStringsNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPaneStrings,map<string,string>(),"");
		if (NULL==m_AllPaneStringsNode)
		{
			return false;
		}
	}
	else
	{
		vector<TiXmlElement*> vec_all_sequence=AfxGetXmlParser().GetSubElements(m_AllPaneStringsNode);
		size_t i=0;
		for (;i<vec_all_sequence.size();i++)
		{
			if (AfxGetXmlParser().HasAttribute(vec_all_sequence[i],"key"))
			{
				string attrValue=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"key");
				if (attrValue==list_name)
				{
					list_element=vec_all_sequence[i];
					break;
				}
			}
		}
	}
	if (NULL==list_element)
	{
		//该结点下子结点为空
		map<string,string> attr2val;
		attr2val.insert(make_pair("key",list_name));
		list_element=AfxGetXmlParser().CreateElementWithAttributes(m_AllPaneStringsNode,"sequence",attr2val,"");
		if (NULL==list_element)
		{
			return false;
		}
		AfxGetXmlParser().save();
		return true;
	}
	vector<TiXmlElement*> list_header_columns=AfxGetXmlParser().GetSubElements(list_element);
	if (list_header_columns.size()<=0)
	{
		load_default_string(list_name,str_map);
		return save_string_config(list_name,str_map);
	}
	else
	{
		for (size_t j=0;j<list_header_columns.size();j++)
		{
			string ename=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"EName");
			string name=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Name");
			//data.ename=AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Express");
			int id=atoi(AfxGetXmlParser().GetAttributeValue(list_header_columns[j],"Id").c_str());
			SLH_CTP_DATA data(id,ename.c_str(),name.c_str());
			str_map.insert(make_pair(data.id,data));
		}
	}
	return true;
}

bool config_manager::load_default_string( const string& list_name,map<int,SLH_CTP_DATA>& str_map )
{
	static bool bLoaded=false;
	if (true==bLoaded)
	{
		map<string,map<int,SLH_CTP_DATA> >::iterator it=m_listname2listfield.find(list_name);
		if (m_listname2listfield.end()==it)
		{
			return false;
		}
		str_map=it->second;
		return true;
	}
	bLoaded=true;
	return bLoaded;
}

void config_manager::set_name_by_id( int slh_idx,const string name,bool bEname/*=false*/ )
{

}

bool config_manager::save_strings_config( const string& config_file/*="config.xml"*/ )
{
	bool bLoaded=true;

	map<string,map<int,SLH_CTP_DATA > >::iterator default_list_column_it=m_listname2listfield.begin();
	while (m_listname2listfield.end()!=default_list_column_it)
	{
		bool bIsLoad=save_string_config(default_list_column_it->first,default_list_column_it->second);
		if (false==bIsLoad)
		{
			bLoaded=bIsLoad;
		}
		++default_list_column_it;
	}
	return bLoaded;
}

bool config_manager::save_string_config( const string& list_name,map<int,SLH_CTP_DATA>& str_map,const string& config_file/*="config.xml"*/ )
{
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	//todo:读取配置文件 m_ListCtrlColumns
	string m_AllPaneStrings="m_AllPaneStrings";
	TiXmlElement* m_AllPaneStringsNode=AfxGetXmlParser().GetElementByTagName(m_AllPaneStrings);
	TiXmlElement* list_element=NULL;
	if (NULL==m_AllPaneStringsNode)
	{
		m_AllPaneStringsNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPaneStrings,map<string,string>(),"");
		if (NULL==m_AllPaneStringsNode)
		{
			return false;
		}
	}
	else
	{
		vector<TiXmlElement*> vec_all_sequence=AfxGetXmlParser().GetSubElements(m_AllPaneStringsNode);
		size_t i=0;
		for (;i<vec_all_sequence.size();i++)
		{
			if (AfxGetXmlParser().HasAttribute(vec_all_sequence[i],"key"))
			{
				string attrValue=AfxGetXmlParser().GetAttributeValue(vec_all_sequence[i],"key");
				if (attrValue==list_name)
				{
					list_element=vec_all_sequence[i];
					break;
				}
			}
		}
	}
	if (NULL==list_element)
	{
		//该结点下子结点为空
		map<string,string> attr2val;
		attr2val.insert(make_pair("key",list_name));
		list_element=AfxGetXmlParser().CreateElementWithAttributes(m_AllPaneStringsNode,"sequence",attr2val,"");
		if (NULL==list_element)
		{
			return false;
		}
	}
	if (str_map.size()<=0)
	{
		load_default_string(list_name,str_map);
	}
	if (str_map.size()>0)
	{
		AfxGetXmlParser().RemoveAllChildNodes(list_element);
	}
	else
	{
		return true;
	}
	map<int,SLH_CTP_DATA>::iterator it=str_map.begin();
	for (;it!=str_map.end();++it)
	{
		map<string,string> attr2value;
		attr2value.insert(make_pair("EName",it->second.ename));
		attr2value.insert(make_pair("Name",it->second.name));
		char str_id[8]={0};
#ifndef _WIN32
        sprintf(str_id,"%d",it->first);
#else
		itoa(it->first,str_id,10);
#endif
		attr2value.insert(make_pair("Id",str_id));
		AfxGetXmlParser().CreateElementWithAttributes(list_element,"Item",attr2value,"");
	}
	AfxGetXmlParser().save();
	return true;
}

bool config_manager::save_config_extra( tagInstrumentListConfig* config,const string& root_node_name/*="InstrumentList"*/,string config_file/*="config.xml"*/ )
{
	//PRTINF("ConfigManager::save_config_extra:InstrumentListConfig-->Enter");
	if (NULL==config)
	{
		return false;
	}
	if(!AfxGetXmlParser().load(config_file))
	{
		return false;
	}
	string m_AllPanes="m_AllPanes";
	TinyXmlParser::xml_elem_type* paneRootNode=AfxGetXmlParser().GetElementByTagName(m_AllPanes);
	if (NULL==paneRootNode)
	{
		map<string,string> attr_value;
		paneRootNode=AfxGetXmlParser().CreateElementWithAttributes(AfxGetXmlParser().GetRootNode(),
			m_AllPanes,attr_value,"");
		if (NULL==paneRootNode)
		{
			return false;
		}
	}
	vector<TinyXmlParser::xml_elem_type*> panes=AfxGetXmlParser().GetSubElements(paneRootNode);
	//PRTINF("Find %s in %d Panes",root_node_name.c_str(),panes.size());
	int pane_idx=-1;
	for (size_t i=0;i<panes.size();++i)
	{
		string paneName=AfxGetXmlParser().GetAttributeValue(panes[i],"EName");
		if (paneName==root_node_name)
		{
			pane_idx=i;
			break;
		}
	}
	TinyXmlParser::xml_elem_type* paneNode=NULL;
	if (-1==pane_idx)
	{
		map<string,string> attr2value;
		attr2value.insert(make_pair("EName",root_node_name));
		attr2value.insert(make_pair("Name","合约列表"));
		char id_str[32];
#ifndef _WIN32
        sprintf(id_str,"%d",config->m_paneConfig.m_PaneId);
#else
        itoa(config->m_paneConfig.m_PaneId,id_str,10);
#endif

		attr2value.insert(make_pair("Id",id_str));
		paneNode=AfxGetXmlParser().CreateElementWithAttributes(paneRootNode,"Pane",attr2value,"");
		if (NULL==paneNode)
		{
			return false;
		}
		//PRTINF("Save Pane Node by InstrumentList...");
		AfxGetXmlParser().save();
	}
	else
	{
		paneNode=panes[pane_idx];
	}
	//读取其他属性;
	TinyXmlParser::xml_elem_type* m_TabItems=AfxGetXmlParser().GetSubElementByTagName(paneNode,"m_ActiveTab");
	if (NULL==m_TabItems)
	{
		work_tab_item item = config->m_ActiveTab;
		map<string, string> attr2value;
		char str_id[32] = {0};
		sprintf(str_id, "%d", item.id);
		attr2value.insert(make_pair("Id", str_id));
		attr2value.insert(make_pair("EName", item.EName));
		attr2value.insert(make_pair("Name", item.name));

		m_TabItems=AfxGetXmlParser().CreateElementWithAttributes(paneNode,"m_ActiveTab",
			attr2value, "");
		if (NULL==m_TabItems)
		{
			return false;
		}
	}
	string sub_node_name;
	sub_node_name="m_bEnableEdit";
	TinyXmlParser::xml_elem_type* m_bEnableEdit=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_bEnableEdit)
	{
		m_bEnableEdit=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
 	AfxGetXmlParser().SetInnerText(m_bEnableEdit,config->m_bEnableEdit);
	sub_node_name="m_bEnableSelection";
	TinyXmlParser::xml_elem_type* m_bEnableSelection=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_bEnableSelection)
	{
		m_bEnableSelection=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_bEnableSelection,config->m_bEnableSelection);

	sub_node_name="m_bEnableDragDrop";
	TinyXmlParser::xml_elem_type* m_bEnableDragDrop=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_bEnableDragDrop)
	{
		m_bEnableDragDrop=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_bEnableDragDrop,config->m_bEnableDragDrop);

	sub_node_name="m_bEnableTabKey";
	TinyXmlParser::xml_elem_type* m_bEnableTabKey=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_bEnableTabKey)
	{
		m_bEnableTabKey=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_bEnableTabKey,config->m_bEnableTabKey);

	sub_node_name="m_bEnableTitleTip";
	TinyXmlParser::xml_elem_type* m_bEnableTitleTip=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_bEnableTitleTip)
	{
		m_bEnableTitleTip=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_bEnableTitleTip,config->m_bEnableTitleTip);

	sub_node_name="m_bEnableSort";
	TinyXmlParser::xml_elem_type* m_bEnableSort=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_bEnableSort)
	{
		m_bEnableSort=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_bEnableSort,config->m_bEnableSort);

	sub_node_name="m_nGridLines";
	TinyXmlParser::xml_elem_type* m_nGridLines=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_nGridLines)
	{
		m_nGridLines=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_nGridLines,config->m_nGridLines);

	sub_node_name="m_nColSort";
	TinyXmlParser::xml_elem_type* m_nColSort=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_nColSort)
	{
		m_nColSort=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_nColSort,config->m_nColSort);

	sub_node_name="m_nWorkTabHeight";
	TinyXmlParser::xml_elem_type* m_nWorkTabHeight=AfxGetXmlParser().GetSubElementByTagName(paneNode,sub_node_name);
	if (NULL==m_nWorkTabHeight)
	{
		m_nWorkTabHeight=AfxGetXmlParser().CreateElementWithAttributes(paneNode,sub_node_name,map<string,string>(),"");
	}
	AfxGetXmlParser().SetInnerText(m_nWorkTabHeight,config->m_nWorkTabHeight);
	if (config->m_paneConfig.m_PaneEName.empty())
	{
		config->m_paneConfig.m_PaneEName=root_node_name;
	}
	if (config->m_paneConfig.m_PaneEName.empty())
	{
		config->m_paneConfig.m_PaneEName="InstrumentList";
	}
	if (config->m_strTabTitle.empty())
	{
		config->m_strTabTitle="合约列表";
	}
	//PRTINF("ConfigManager::save_config_extra:InstrumentListConfig-->Leave");
	AfxGetXmlParser().save();
	return save_config(config->m_paneConfig,config_file);
}

string config_manager::get_filename()
{
	string path=AfxGetXmlParser().GetXmlFilePath();
	return path.empty()?string("config.xml"):string(path.c_str());
}

void config_manager::set_default_color( tagTickGraphConfig& config )
{
	//背景黑色;
	config.m_paneConfig.m_Colors[clrBK].Color = RGB(0,0,0);
	//标题颜色;
	config.m_paneConfig.m_Colors[clrTitle].Color = RGB(0x80,0x80,0x80);
	//坐标颜色;
	config.m_paneConfig.m_Colors[clrAxis].Color =RGB(0xE7,0xE7,0xE7);
	//成交量颜色;
	config.m_paneConfig.m_Colors[clrVolume].Color=RGB(0xFF,0xF8,0x50);
	//文字;
	config.m_paneConfig.m_Colors[clrText].Color=RGB(0xE7,0xE7,0xE7);
	//上涨色;
	config.m_paneConfig.m_Colors[clrRise].Color=RGB(0xFF,0x3C,0x3C);
	//下跌色;
	config.m_paneConfig.m_Colors[clrFall].Color=RGB(0x00,0xFF,0xFF);
	//平盘色;
	config.m_paneConfig.m_Colors[clrPlane].Color=RGB(0xC0,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrBorder].Color=RGB(0xE1,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine1].Color=RGB(0x00,0xFF,0xFF);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine2].Color=RGB(0xFF,0xFF,0x00);
}

void config_manager::set_default_color( tagMultiSortConfig& config )
{
	//背景黑色;
	config.m_paneConfig.m_Colors[clrBK].Color = RGB(0,0,0);
	//标题颜色;
	config.m_paneConfig.m_Colors[clrTitle].Color = RGB(0xFF,0xFF,0x00);
	//坐标颜色;
	config.m_paneConfig.m_Colors[clrAxis].Color =RGB(0xE7,0xE7,0xE7);
	//成交量颜色;
	config.m_paneConfig.m_Colors[clrVolume].Color=RGB(0xFF,0xF8,0x50);
	//文字;
	config.m_paneConfig.m_Colors[clrText].Color=RGB(0xE7,0xE7,0xE7);
	//上涨色;
	config.m_paneConfig.m_Colors[clrRise].Color=RGB(0xFF,0x3C,0x3C);
	//下跌色;
	config.m_paneConfig.m_Colors[clrFall].Color=RGB(0x00,0xFF,0xFF);
	//平盘色;
	config.m_paneConfig.m_Colors[clrPlane].Color=RGB(0xC0,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrBorder].Color=RGB(0xE1,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine1].Color=RGB(0x00,0xFF,0xFF);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine2].Color=RGB(0x00,0xFF,0xFF);

	config.m_paneConfig.m_Colors[clrDJ].Color=RGB(0x00,0xFF,0xFF);

	config.m_paneConfig.m_Colors[clrNewKLine].Color=RGB(0x00,0xFF,0xFF);

	config.m_paneConfig.m_Colors[clrFallEntity].Color=RGB(0x00,0xFF,0xFF);
}


void config_manager::set_default_color( tagKLineGraphConfig& config )
{
	//背景黑色;
	config.m_paneConfig.m_Colors[clrBK].Color = RGB(0,0,0);
	//标题颜色;
	config.m_paneConfig.m_Colors[clrTitle].Color = RGB(0x80,0x80,0x80);
	//坐标颜色;
	config.m_paneConfig.m_Colors[clrAxis].Color =RGB(0xE7,0xE7,0xE7);
	//成交量颜色;
	config.m_paneConfig.m_Colors[clrVolume].Color=RGB(0xFF,0xF8,0x50);
	//文字;
	config.m_paneConfig.m_Colors[clrText].Color=RGB(0xE7,0xE7,0xE7);
	//上涨色;
	config.m_paneConfig.m_Colors[clrRise].Color=RGB(0xFF,0x3C,0x3C);
	//下跌色;
	config.m_paneConfig.m_Colors[clrFall].Color=RGB(0x00,0xFF,0xFF);
	//平盘色;
	config.m_paneConfig.m_Colors[clrPlane].Color=RGB(0xC0,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrBorder].Color=RGB(0xE1,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine1].Color=RGB(0x00,0xFF,0xFF);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine2].Color=RGB(0x00,0xFF,0xFF);

	config.m_paneConfig.m_Colors[clrDJ].Color=RGB(0x00,0xFF,0xFF);

	config.m_paneConfig.m_Colors[clrNewKLine].Color=RGB(0x00,0xFF,0xFF);

	config.m_paneConfig.m_Colors[clrFallEntity].Color=RGB(0x00,0xFF,0xFF);
}

void config_manager::set_default_color( tagRealTimeConfig& config )
{
	//背景黑色;
	config.m_paneConfig.m_Colors[clrBK].Color = RGB(0,0,0);
	//标题颜色;
	config.m_paneConfig.m_Colors[clrTitle].Color = RGB(0x80,0x80,0x80);
	//坐标颜色;
	config.m_paneConfig.m_Colors[clrAxis].Color =RGB(0xE7,0xE7,0xE7);
	//成交量颜色;
	config.m_paneConfig.m_Colors[clrVolume].Color=RGB(0xFF,0xF8,0x50);
	//文字;
	config.m_paneConfig.m_Colors[clrText].Color=RGB(0xE7,0xE7,0xE7);
	//上涨色;
	config.m_paneConfig.m_Colors[clrRise].Color=RGB(0xFF,0x3C,0x3C);
	//下跌色;
	config.m_paneConfig.m_Colors[clrFall].Color=RGB(0x00,0xFF,0xFF);
	//平盘色;
	config.m_paneConfig.m_Colors[clrPlane].Color=RGB(0xC0,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrBorder].Color=RGB(0xE1,0xC0,0xC0);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine1].Color=RGB(0x00,0xFF,0xFF);
	//边框色;
	config.m_paneConfig.m_Colors[clrLine2].Color=RGB(0x00,0xFF,0xFF);
}

bool config_manager::load_q7_config(const std::string& config_file,tagBrokerListConfig& brokerList)
{
	TinyXmlParser parser;
	if(!parser.load(config_file))
	{
		return false;
	}

	TinyXmlParser::xml_elem_type* root=parser.GetRootNode();
	if(NULL==root)
	{
		return false;
	}
	std::string broker_tag_name="broker";
	std::vector<TinyXmlParser::xml_elem_type*> brokersElem=
		parser.GetSubElements(root);
	std::vector<TinyXmlParser::xml_elem_type*>::iterator bElem=brokersElem.begin();
	//while (bElem!=brokersElem.end())
	//{
	//	std::string broker_name=parser.GetAttributeValue(*bElem,"BrokerName");
	//	server_info si;
	//	si.BrokerName=broker_name;
	//	si.server_id=parser.GetAttributeValue(*bElem,"BrokerID");
	//	si.service_provider=parser.GetAttributeValue(*bElem,"provider");
	//	if (si.service_provider.empty())
	//	{
	//		si.service_provider="ctp_platform_server";
	//	}
	//	TinyXmlParser::xml_elem_type* servers_elem=
	//		parser.GetSubElementByTagName(*bElem,("Servers"));
	//	if (!servers_elem)
	//	{
	//		++bElem;
	//		continue;
	//	}
	//	std::vector<TinyXmlParser::xml_elem_type*> svr_elem=
	//		parser.GetSubElements(servers_elem);
	//	for (std::size_t i=0;i<svr_elem.size();++i)
	//	{
	//		ServerInfo bs;
	//		TinyXmlParser::xml_elem_type* name_elem=
	//			parser.GetSubElementByTagName(svr_elem[i],"Name");
	//		if (name_elem!=NULL)
	//		{
	//			bs.name=parser.GetInnerText(name_elem);
	//		}
	//		TinyXmlParser::xml_elem_type* trade_elem=
	//			parser.GetSubElementByTagName(svr_elem[i],"Trading");
	//		if (trade_elem!=NULL)
	//		{
	//			std::vector<TinyXmlParser::xml_elem_type*> trading_addrs=parser.GetSubElements(trade_elem);
	//			for(std::size_t j=0;j<trading_addrs.size();++j)
	//			{
	//				bs.trade_server_front.push_back(parser.GetInnerText(trading_addrs[j]));
	//			}
	//		}
	//		TinyXmlParser::xml_elem_type* market_elem=
	//			parser.GetSubElementByTagName(svr_elem[i],"MarketData");
	//		if (trade_elem!=NULL)
	//		{
	//			std::vector<TinyXmlParser::xml_elem_type*> addrs=parser.GetSubElements(market_elem);
	//			for(std::size_t j=0;j<addrs.size();++j)
	//			{
	//				bs.market_server_front.push_back(parser.GetInnerText(addrs[j]));
	//			}
	//		}
	//		si.server_addrs.push_back(bs);
	//	}
	//	brokerList.m_Broker.push_back(si);
	//	++bElem;
	//}
	return true;
}



