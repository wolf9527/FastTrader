#ifndef _REALTIME_CONFIG_H_
#define _REALTIME_CONFIG_H_
#include "ViewConfig.h"
#include "AllConfig.h"
#include <string>
#include <vector>
#include <map>
using namespace std;



class CONFIGLIB_API RealTimeConfig :
	public ViewConfig
{
public:
	static RealTimeConfig* GetInstance();
protected:
	RealTimeConfig(void);
	virtual ~RealTimeConfig(void);
public:
	virtual bool load(TinyXmlParser* parser,string section,int sectionId);
	virtual bool save();
public:
	tagRealTimeConfig m_config;
protected:
	static RealTimeConfig* g_Instance;
};
#endif

