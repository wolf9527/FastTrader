#ifndef _KLINE_CONFIG_H_
#define _KLINE_CONFIG_H_
#include "ViewConfig.h"
#include <string>
#include <vector>
using namespace std;
#include "AllConfig.h"

//K��ͼ��������
class CONFIGLIB_API KLineConfig :
	public ViewConfig
{
public:
	static KLineConfig*  GetInstance();
public:
	KLineConfig(void);
	virtual ~KLineConfig(void);
public:
	virtual bool load(TinyXmlParser* parser,string section,int sectionId);
	virtual bool save();
public:
	tagKLineGraphConfig m_config;
protected:
	static KLineConfig* g_Instance;
};
#endif

