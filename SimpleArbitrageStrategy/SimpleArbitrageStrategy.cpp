#include "stdafx.h"
#include "SimpleArbitrageStrategy.h"
#include <boost/make_shared.hpp>
#include <boost/dll/alias.hpp>
#include <boost/dll.hpp>
#include "../Log/logging.h"
#include <boost/date_time/gregorian/gregorian.hpp>
#include "../FacilityBaseLib/Container.h"
#include "../tinyxml/tinyxml.h"
#include "../ConfigLib/ConfigReader.h"

SimpleArbitrageStrategy::SimpleArbitrageStrategy(const std::string& sid)
	:IStrategy(sid)
{
	LOGINIT("SimpleArbitrageStrategy");
}


SimpleArbitrageStrategy::~SimpleArbitrageStrategy()
{
}

void SimpleArbitrageStrategy::Initialize()
{
	ReadConfig();
	std::map<std::string, int>& InstrumentHandsMap = GetInstruments();
	for (auto paramIter = m_ParamsMap.begin(); paramIter != m_ParamsMap.end(); ++paramIter)
	{
		InstPriceMarginParam& params = *paramIter->second;

		InstrumentInfo infoL1;
		InstrumentContainer::get_instance().GetInstrumentInfo(params.InstOrderPair.first.InstrumentID.c_str(), &infoL1);
		params.InstOrderPair.first.priceTick = infoL1.PriceTick;
		params.InstOrderPair.first.defaultHands = GetInstruments()[params.InstOrderPair.first.InstrumentID];

		InstrumentInfo infoL2;
		InstrumentContainer::get_instance().GetInstrumentInfo(params.InstOrderPair.second.InstrumentID.c_str(), &infoL2);
		params.InstOrderPair.second.priceTick = infoL2.PriceTick;
		params.InstOrderPair.second.defaultHands = GetInstruments()[params.InstOrderPair.second.InstrumentID];
	}
}

void SimpleArbitrageStrategy::OnTick(boost::shared_ptr<Tick> tick)
{
	for (auto paramIter = m_ParamsMap.begin();paramIter!=m_ParamsMap.end();++paramIter)
	{
		InstPriceMarginParam& params = *paramIter->second;
		if (params.Update(tick) && params.IsSameTime() && params.EnableTrading)
		{
			LOGDEBUG("PriceMargin={}",params.CurPriceMargin());
			bool bOpened = Open(paramIter->second);
			bool bClosed = Close(paramIter->second);
			bool bCanceled = Cancel(paramIter->second);
		}
	}
}

void SimpleArbitrageStrategy::OnOrder(boost::shared_ptr<Order> order)
{
	if (order->ErrorID!=0)
	{
		LOGDEBUG("报单回报出错 {}",order->ErrorMsg);
		return;
	}
	for (auto pmIter = m_ParamsMap.begin();pmIter!=m_ParamsMap.end();++pmIter)
	{
		auto& params = pmIter->second;
		if (order->OrderStatus == OST_Canceled)
		{
			if (order->CombOffsetFlag[0] == OF_Open)
			{
				//开仓单被撤掉;
				if (params->InstOrderPair.second.openInputOrder && params->InstOrderPair.second.openInputOrder->OrderRef
					== order->OrderRef)
				{
					params->InstOrderPair.second.openInputOrder = nullptr;
					params->InstOrderPair.second.openInputOrderAction = nullptr;
				}
				if (params->InstOrderPair.first.openInputOrder && params->InstOrderPair.first.openInputOrder->OrderRef
					== order->OrderRef)
				{
					params->InstOrderPair.first.openInputOrder = nullptr;
					params->InstOrderPair.first.openInputOrderAction = nullptr;
					//腿1要立即进行重新报单;
					OpenL1(params);
				}
			}
			else
			{
				//平仓单被撤;
				if (params->InstOrderPair.second.closeInputOrder && params->InstOrderPair.second.closeInputOrder->OrderRef
					== order->OrderRef)
				{
					params->InstOrderPair.second.closeInputOrder = nullptr;
					params->InstOrderPair.second.closeInputOrderAction = nullptr;
				}
				if (params->InstOrderPair.first.closeInputOrder && params->InstOrderPair.first.closeInputOrder->OrderRef
					== order->OrderRef)
				{
					params->InstOrderPair.first.closeInputOrder = nullptr;
					params->InstOrderPair.first.closeInputOrderAction = nullptr;
					//腿1要立即进行重新报单;
					CloseL1(params);
				}
			}
		}
		else
		{
			if (order->CombOffsetFlag[0] == OF_Open)
			{
				//定位开仓单;
				if (params->InstOrderPair.second.openInputOrder && params->InstOrderPair.second.openInputOrder->OrderRef
					==order->OrderRef)
				{
					params->InstOrderPair.second.openOrder = order;
				}

				if (params->InstOrderPair.first.openInputOrder && params->InstOrderPair.first.openInputOrder->OrderRef
					== order->OrderRef)
				{
					params->InstOrderPair.first.openOrder = order;
				}
			}
			else
			{
				//定位平仓单;
				if (params->InstOrderPair.second.closeInputOrder && params->InstOrderPair.second.closeInputOrder->OrderRef
					== order->OrderRef)
				{
					params->InstOrderPair.second.closeOrder = order;
				}

				if (params->InstOrderPair.first.closeInputOrder && params->InstOrderPair.first.closeInputOrder->OrderRef
					== order->OrderRef)
				{
					params->InstOrderPair.first.closeOrder = order;
				}
			}
		}
	}
}


bool SimpleArbitrageStrategy::OpenL1(boost::shared_ptr<InstPriceMarginParam> params)
{
	int L1Volume = abs(params->GetPositionDiff());
	EnumDirection L1Direction = params->direct == LongVergence ? D_Buy : D_Sell;
	if (!params->InstOrderPair.first.openInputOrder)
	{
		params->InstOrderPair.first.openInputOrder =
			OrderInsert(params->InstOrderPair.first.InstrumentID,
			L1Volume,
			params->InstOrderPair.first.OpenPrice(L1Direction), OF_Open,
			L1Direction, GetMainTrader());
		return true;
	}
	return false;
}



bool SimpleArbitrageStrategy::CloseL1(boost::shared_ptr<InstPriceMarginParam> params)
{
	int L1Volume = abs(params->GetPositionDiff());
	EnumDirection L1Direction = params->direct == LongVergence ? D_Sell : D_Buy;

	if (!params->InstOrderPair.first.closeInputOrder)
	{
		//立即报第一腿的开仓单;
		params->InstOrderPair.first.closeInputOrder =
			OrderInsert(params->InstOrderPair.first.InstrumentID,
			L1Volume,
			params->InstOrderPair.first.ClosePrice(L1Direction), OF_CloseToday, L1Direction, GetMainTrader());
		return true;
	}
	return false;
}


void SimpleArbitrageStrategy::OnTrade(boost::shared_ptr<Trade> trade)
{
	for (auto pmIter = m_ParamsMap.begin(); pmIter != m_ParamsMap.end(); ++pmIter)
	{
		auto& params = pmIter->second;
		if (trade->OffsetFlag == OF_Open)
		{
			//定位开仓单;
			if (params->InstOrderPair.second.openInputOrder && params->InstOrderPair.second.openInputOrder->OrderRef
				== trade->OrderRef)
			{
				//第二腿的开仓单,根据成交量修正仓位;
				UpdatePositionByTrade(trade,params->InstOrderPair.second.openPosition);
				//立即报第一腿的开仓单;
				OpenL1(params);
// 				int L1Volume = trade->Volume*params->InstOrderPair.first.defaultHands / params->InstOrderPair.second.defaultHands;
// 				if (!params->InstOrderPair.first.openInputOrder)
// 				{
// 					params->InstOrderPair.first.openInputOrder =
// 						OrderInsert(params->InstOrderPair.first.InstrumentID,
// 						L1Volume,
// 						params->InstOrderPair.first.OpenPrice(!trade->Direction), OF_Open,
// 						!trade->Direction, GetMainTrader());
// 				}
			}
			//第一腿的开仓单;
			if (params->InstOrderPair.first.openInputOrder && params->InstOrderPair.first.openInputOrder->OrderRef
				== trade->OrderRef)
			{
				UpdatePositionByTrade(trade, params->InstOrderPair.first.openPosition);
				//params->GetPositionDiff() == 0;//?
				params->InstOrderPair.second.openInputOrder = nullptr;
				params->InstOrderPair.second.openOrder = nullptr;

				params->InstOrderPair.first.openInputOrder = nullptr;
				params->InstOrderPair.first.openOrder = nullptr;
			}
		}
		else
		{
			//定位平仓单;
			if (params->InstOrderPair.second.closeInputOrder && params->InstOrderPair.second.closeInputOrder->OrderRef
				== trade->OrderRef)
			{
				//第二腿的平仓单;
				UpdatePositionByTrade(trade, params->InstOrderPair.second.openPosition);
				CloseL1(params);
			}
			if (params->InstOrderPair.first.closeInputOrder && params->InstOrderPair.first.closeInputOrder->OrderRef
				== trade->OrderRef)
			{
				UpdatePositionByTrade(trade, params->InstOrderPair.first.openPosition);

				params->InstOrderPair.second.closeInputOrder = nullptr;
				params->InstOrderPair.second.closeOrder = nullptr;

				params->InstOrderPair.first.closeInputOrder = nullptr;
				params->InstOrderPair.first.closeOrder = nullptr;

				if (params->InstOrderPair.first.GetPosition() == 0 && params->InstOrderPair.second.GetPosition() == 0)
				{
					//重置主方向;
					params->direct = NoneVergence;
				}
			}
		}
	}
}



void SimpleArbitrageStrategy::Finalize()
{
	WriteConfig();
}


bool SimpleArbitrageStrategy::ReadConfig()
{
	//读取配置文件;
	bool bRet = false;
	TiXmlDocument document("user.xml");
	if (!document.LoadFile())
	{
		return false;
	}

	//获得根元素，即Persons。
	TiXmlElement* root = document.RootElement();
	if (NULL == root)
	{
		return false;
	}

	string broker_tag_name = "Strategy";
	TiXmlElement* elem = root->FirstChildElement(broker_tag_name.c_str());
	if (!elem)
	{
		bRet = false;
	}

	std::map<std::string, EnumOrderPriceType> typeName2typeValue;
	typeName2typeValue.insert(std::make_pair("OPT_BestPrice", OPT_BestPrice));
	typeName2typeValue.insert(std::make_pair("OPT_LastPrice", OPT_LastPrice));
	typeName2typeValue.insert(std::make_pair("OPT_AnyPrice", OPT_AnyPrice));
	typeName2typeValue.insert(std::make_pair("OPT_LimitPrice", OPT_LimitPrice));

	while (NULL != elem)
	{
		do
		{
			std::string StrategyID = GetAttribute(elem, "StrategyID");
			const std::string& myId = GetId();
			//std::cout << "策略ID:" << myId << ",配置文件ID" << StrategyID << std::endl;
			if (StrategyID != myId)
			{
				continue;
			}
			std::string mainUserID = GetAttribute(elem, "UserID");
			TiXmlElement* multiuser_elem = elem->FirstChildElement("PriceMarginList");
			if (multiuser_elem)
			{
				std::vector<TiXmlElement*> users_elems = GetSubElements(multiuser_elem, "PriceMargin");

				for (std::size_t k = 0; k < users_elems.size(); ++k)
				{
					if (!users_elems[k])
					{
						continue;
					}



					boost::shared_ptr<InstPriceMarginParam> imp = boost::make_shared<InstPriceMarginParam>();

					std::string szEnableOpenLong = GetAttribute(users_elems[k], "EnableOpenLong");
					imp->EnableOpenLong = boost::lexical_cast<bool>(szEnableOpenLong);

					std::string szEnableOpenShort = GetAttribute(users_elems[k], "EnableOpenShort");
					imp->EnableOpenShort = boost::lexical_cast<bool>(szEnableOpenShort);

					imp->TotalHands = std::stoi(GetAttribute(users_elems[k], "TotalHands"));
					imp->EachHands = std::stoi(GetAttribute(users_elems[k], "EachHands"));


					imp->LongOpenPriceDiff = std::stod(GetAttribute(users_elems[k], "LongOpenPriceDiff"));
					imp->LongClosePriceDiff = std::stod(GetAttribute(users_elems[k], "LongClosePriceDiff"));
					imp->ShortOpenPriceDiff = std::stod(GetAttribute(users_elems[k], "ShortOpenPriceDiff"));
					imp->ShortClosePriceDiff = std::stod(GetAttribute(users_elems[k], "ShortClosePriceDiff"));

					TiXmlElement* instA = users_elems[k]->FirstChildElement("L1");
					if (instA)
					{
						imp->InstOrderPair.first.SetInstrumentID(GetInnerText(instA));
						//开仓使用的报价方式;
						imp->InstOrderPair.first.OpenOrderPriceType =
							typeName2typeValue[GetAttribute(instA, "OpenOrderPriceType")];
						//平仓使用的报价方式;
						imp->InstOrderPair.first.CloseOrderPriceType =
							typeName2typeValue[GetAttribute(instA, "CloseOrderPriceType")];
					}

					TiXmlElement* instB = users_elems[k]->FirstChildElement("L2");
					if (instB)
					{
						imp->InstOrderPair.second.SetInstrumentID(GetInnerText(instB));
						imp->InstOrderPair.second.OpenOrderPriceType =
							typeName2typeValue[GetAttribute(instB, "OpenOrderPriceType")];

						imp->InstOrderPair.second.CloseOrderPriceType =
							typeName2typeValue[GetAttribute(instB, "CloseOrderPriceType")];

					}
					m_ParamsMap[k]=imp;
				}
				bRet = true;
			}
		} while (0);
		elem = elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}

bool SimpleArbitrageStrategy::WriteConfig()
{
	return true;
}

bool SimpleArbitrageStrategy::Open(boost::shared_ptr<InstPriceMarginParam> priceMarginParam)
{
	if (!priceMarginParam)
	{
		return false;
	}
	if (priceMarginParam->EnableOpenLong)
	{
		if (priceMarginParam->CurPriceMargin()<priceMarginParam->LongClosePriceDiff &&
			(priceMarginParam->direct == NoneVergence || priceMarginParam->direct == LongVergence))
		{
			priceMarginParam->direct = LongVergence;

			bool L2CanOpen = priceMarginParam->GetPositionDiff() == 0 &&
				priceMarginParam->InstOrderPair.second.GetPosition() < 
				priceMarginParam->TotalHands*priceMarginParam->InstOrderPair.second.defaultHands;
			//做多先报腿2,腿2的方向是Sell;
			if (L2CanOpen && !priceMarginParam->InstOrderPair.second.openInputOrder)
			{
				//先报第二腿的单子,Sell;
				int L2Volume = std::min<int>(
					priceMarginParam->TotalHands*priceMarginParam->InstOrderPair.second.defaultHands-priceMarginParam->InstOrderPair.second.GetPosition(),
					priceMarginParam->InstOrderPair.second.defaultHands*priceMarginParam->EachHands);
				priceMarginParam->InstOrderPair.second.openInputOrder =
					OrderInsert(priceMarginParam->InstOrderPair.second.InstrumentID,
					L2Volume,
					priceMarginParam->InstOrderPair.second.OpenPrice(D_Sell), OF_Open, D_Sell, GetMainTrader());
				return true;
			}
		}
	}

	if (priceMarginParam->EnableOpenShort)
	{
		if (priceMarginParam->CurPriceMargin() >= priceMarginParam->ShortClosePriceDiff &&
			(priceMarginParam->direct == NoneVergence || priceMarginParam->direct == ShortVergence))
		{
			priceMarginParam->direct = ShortVergence;
			//做空,先报腿2;
			bool L2CanOpen = priceMarginParam->GetPositionDiff() == 0 &&
				priceMarginParam->InstOrderPair.second.GetPosition() <
				priceMarginParam->TotalHands*priceMarginParam->InstOrderPair.second.defaultHands;
			//做多先报腿2,腿2的方向是Buy;
			if (L2CanOpen && !priceMarginParam->InstOrderPair.second.openInputOrder)
			{
				//先报第二腿的单子,Sell;
				int L2Volume = std::min<int>(
					priceMarginParam->TotalHands*priceMarginParam->InstOrderPair.second.defaultHands - priceMarginParam->InstOrderPair.second.GetPosition(),
					priceMarginParam->InstOrderPair.second.defaultHands*priceMarginParam->EachHands);
				priceMarginParam->InstOrderPair.second.openInputOrder =
					OrderInsert(priceMarginParam->InstOrderPair.second.InstrumentID,
					L2Volume,
					priceMarginParam->InstOrderPair.second.OpenPrice(D_Buy), OF_Open, D_Buy, GetMainTrader());
				return true;
			}
		}
	}
	return false;
}

bool SimpleArbitrageStrategy::Close(boost::shared_ptr<InstPriceMarginParam> priceMarginParam)
{
	if (!priceMarginParam)
	{
		return false;
	}
	if (priceMarginParam->direct ==  LongVergence)
	{
		//做多;
		if (priceMarginParam->CurPriceMargin() >= priceMarginParam->LongClosePriceDiff)
		{
			//发送平仓单;
			bool L2CanClose = priceMarginParam->GetPositionDiff() == 0 &&
				priceMarginParam->InstOrderPair.second.GetPosition() > 0;

			if (!priceMarginParam->InstOrderPair.second.closeInputOrder)
			{
				int L2Volume = std::min<int>(priceMarginParam->InstOrderPair.second.GetPosition(), 
					priceMarginParam->InstOrderPair.second.defaultHands*priceMarginParam->EachHands);
				//先报第二腿的单子,Buy;
				priceMarginParam->InstOrderPair.second.closeInputOrder =
					OrderInsert(priceMarginParam->InstOrderPair.second.InstrumentID,
					L2Volume,
					priceMarginParam->InstOrderPair.second.ClosePrice(D_Buy), OF_CloseToday, D_Buy, GetMainTrader());
				return true;
			}
		}
	}

	if (priceMarginParam->direct == ShortVergence)
	{
		//做多;
		if (priceMarginParam->CurPriceMargin() <= priceMarginParam->ShortClosePriceDiff)
		{
			//发送平仓单;
			//先报第二腿的单子,Buy;
			if (!priceMarginParam->InstOrderPair.second.closeInputOrder)
			{
				int L2Volume = std::min<int>(priceMarginParam->InstOrderPair.second.GetPosition(),
					priceMarginParam->InstOrderPair.second.defaultHands*priceMarginParam->EachHands);
				//先报第二腿的单子,Sell;
				priceMarginParam->InstOrderPair.second.closeInputOrder =
					OrderInsert(priceMarginParam->InstOrderPair.second.InstrumentID,
					L2Volume,
					priceMarginParam->InstOrderPair.second.ClosePrice(D_Sell), OF_CloseToday, D_Sell, GetMainTrader());
				return true;
			}
		}
	}
	return false;
}

bool SimpleArbitrageStrategy::Cancel(boost::shared_ptr<InstPriceMarginParam> priceMarginParam)
{
	if (priceMarginParam->direct == LongVergence)
	{
		//判断要不要撤开仓单;
		if (priceMarginParam->CurPriceMargin() > priceMarginParam->LongOpenPriceDiff)
		{
			if (priceMarginParam->InstOrderPair.second.openInputOrder && !priceMarginParam->InstOrderPair.second.openInputOrderAction)
			{
				priceMarginParam->InstOrderPair.second.openInputOrderAction =
					SafeCancelOrder(priceMarginParam->InstOrderPair.second.openOrder, 
					priceMarginParam->InstOrderPair.second.openInputOrder);
				return true;
			}
		}
		//判断要不要撤平仓单;
		if (priceMarginParam->CurPriceMargin() < priceMarginParam->LongClosePriceDiff)
		{
			//发送平仓单;
			//先报第二腿的单子,Buy;
			if (priceMarginParam->InstOrderPair.second.closeInputOrder && priceMarginParam->InstOrderPair.second.closeInputOrderAction)
			{
				priceMarginParam->InstOrderPair.second.closeInputOrderAction =
					SafeCancelOrder(priceMarginParam->InstOrderPair.second.closeOrder,
					priceMarginParam->InstOrderPair.second.closeInputOrder);
				return true;
			}
		}
	}
	if (priceMarginParam->direct == ShortVergence)
	{
		//判断要不要撤开仓单;
		if (priceMarginParam->CurPriceMargin() < priceMarginParam->LongOpenPriceDiff)
		{
			if (priceMarginParam->InstOrderPair.second.openInputOrder && !priceMarginParam->InstOrderPair.second.openInputOrderAction)
			{
				priceMarginParam->InstOrderPair.second.openInputOrderAction =
					SafeCancelOrder(priceMarginParam->InstOrderPair.second.openOrder,
					priceMarginParam->InstOrderPair.second.openInputOrder);
				return true;
			}
		}
		//判断要不要撤平仓单;
		if (priceMarginParam->CurPriceMargin() > priceMarginParam->LongClosePriceDiff)
		{
			//发送平仓单;
			//先报第二腿的单子,Buy;
			if (priceMarginParam->InstOrderPair.second.closeInputOrder && priceMarginParam->InstOrderPair.second.closeInputOrderAction)
			{
				priceMarginParam->InstOrderPair.second.closeInputOrderAction =
					SafeCancelOrder(priceMarginParam->InstOrderPair.second.closeOrder,
					priceMarginParam->InstOrderPair.second.closeInputOrder);
				return true;
			}
		}
	}

	//如果腿1没成交,立即撤单;
	if (priceMarginParam->InstOrderPair.first.closeInputOrder && priceMarginParam->InstOrderPair.first.closeInputOrderAction)
	{
		priceMarginParam->InstOrderPair.first.closeInputOrderAction =
			SafeCancelOrder(priceMarginParam->InstOrderPair.first.closeOrder,
			priceMarginParam->InstOrderPair.first.closeInputOrder);
		return true;
	}
	return false;
}

bool SimpleArbitrageStrategy::UpdatePositionByTrade(boost::shared_ptr<Trade> trade, boost::shared_ptr<InvestorPosition>& position)
{
	if (position ==  nullptr)
	{
		if (trade->OffsetFlag != OF_Open)
		{
			return false;
		}
		else
		{
			position = boost::make_shared<InvestorPosition>();
			position->InstrumentID = trade->InstrumentID;
			position->PosiDirection = trade->Direction == D_Buy ? PD_Long : PD_Short;
			position->PositionDate = PSD_Today;
			position->Position = trade->Volume;
			position->TodayPosition = trade->Volume;
			position->YdPosition = 0;
			position->OpenVolume = trade->Volume;
			position->CloseVolume = 0;
			position->TradingDay = trade->TradingDay;
			position->BrokerID = trade->BrokerID;
			position->OpenCost = trade->Price;
			position->OpenDate = trade->TradingDay;
		}
	}
	else
	{
		if (position->InstrumentID != trade->InstrumentID)
		{
			return false;
		}
		if (trade->OffsetFlag == OF_Open)
		{
			//加仓;
			if (position->Position == 0)
			{
				position->PosiDirection = trade->Direction == D_Buy ? PD_Long : PD_Short;
				position->PositionDate = PSD_Today;
			}
			position->Position += trade->Volume;
			position->TodayPosition += trade->Volume;
			position->OpenVolume += trade->Volume;
		}
		else
		{
			//平仓;
			if (trade->OffsetFlag ==  OF_CloseYesterday)
			{
				position->YdPosition -= trade->Volume;
			}
			else
			{
				position->TodayPosition -= trade->Volume;
			}
			position->Position -= trade->Volume;
			position->CloseVolume += trade->Volume;
		}
	}
	return true;
}

void SimpleArbitrageStrategy::OnOrderAction(boost::shared_ptr<InputOrderAction> orderAction)
{
	for (auto pmIter = m_ParamsMap.begin(); pmIter != m_ParamsMap.end(); ++pmIter)
	{
		auto& params = pmIter->second;
		//开仓单被撤掉;
		if (params->InstOrderPair.second.openInputOrder && params->InstOrderPair.second.openInputOrder->OrderRef
			== orderAction->OrderRef)
		{
			params->InstOrderPair.second.openInputOrderAction = nullptr;
			break;
		}
		if (params->InstOrderPair.first.openInputOrder && params->InstOrderPair.first.openInputOrder->OrderRef
			== orderAction->OrderRef)
		{
			params->InstOrderPair.first.openInputOrderAction = nullptr;
			break;
		}
		if (params->InstOrderPair.second.closeInputOrder && params->InstOrderPair.second.closeInputOrder->OrderRef
			== orderAction->OrderRef)
		{
			params->InstOrderPair.second.closeInputOrderAction = nullptr;
			break;
		}
		if (params->InstOrderPair.first.closeInputOrder && params->InstOrderPair.first.closeInputOrder->OrderRef
			== orderAction->OrderRef)
		{
			params->InstOrderPair.first.closeInputOrderAction = nullptr;
			break;
		}
	}
}




boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid)
{
	return boost::make_shared<SimpleArbitrageStrategy>(sid);
}




boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid);
BOOST_DLL_ALIAS(CreateStrategy, create_strategy)