#pragma once

#include "../SimpleStrategyLib/IStrategy.h"

#include "../FacilityBaseLib/InstrumentData.h"
#include "../FacilityBaseLib/Express.h"

#include <boost/date_time/posix_time/ptime.hpp>

#include <boost/algorithm/string.hpp>

//每个腿的报单参数;
struct InstOrderParam
{
	std::string InstrumentID;

	boost::shared_ptr<InputOrder> openInputOrder;
	boost::shared_ptr<InputOrder> closeInputOrder;

	boost::shared_ptr<InputOrderAction> openInputOrderAction;

	boost::shared_ptr<InputOrderAction> closeInputOrderAction;

	boost::shared_ptr<Order> openOrder;
	boost::shared_ptr<Order> closeOrder;

	boost::shared_ptr<Trade> openTrade;
	boost::shared_ptr<InvestorPosition> openPosition;
	//合约报单的帐户,保留给跨市交易用;
	std::string UserID;
	//默认手数;
	int defaultHands;

	double priceTick;

	//开仓报单价格类型;
	EnumOrderPriceType OpenOrderPriceType;
	//平仓报单价格类型;
	EnumOrderPriceType CloseOrderPriceType;

	boost::shared_ptr<Tick> InstTick;

	InstOrderParam()
	{
		defaultHands = 1;
		OpenOrderPriceType = CloseOrderPriceType = OPT_BestPrice;
		defaultHands = 1;
	}

	void SetInstrumentID(const std::string& InstrumentID)
	{
		if (!InstTick)
		{
			InstTick = boost::make_shared<Tick>();
		}
		InstTick->InstrumentID(InstrumentID);
		this->InstrumentID = InstrumentID;
	}

	double PriceByDirection(EnumDirection direct, EnumOrderPriceType orderPriceType, double jumps = 0)
	{
		if (orderPriceType !=  OPT_AnyPrice)
		{
			double price = 0;
			if (orderPriceType ==  OPT_BestPrice)
			{
				//对手价;
				if (direct == D_Buy)
				{
					price = InstTick->AskPrice[0];
				}
				else
				{
					price = InstTick->BidPrice[0];
				}
			}
			else if (orderPriceType == OPT_LastPrice)
			{
				//最新价;
				price = InstTick->LastPrice;
			}
			else if (orderPriceType ==  OPT_LimitPrice)
			{
				//排队价;
				if (direct == D_Buy)
				{
					price = InstTick->BidPrice[0];

				}
				else
				{
					price = InstTick->AskPrice[0];
				}
			}

			if (direct ==  D_Buy)
			{
				price += jumps * priceTick;
			}
			else
			{
				price -= jumps * priceTick;
			}
			return price;
		}
		else
		{
			return 0;
		}
	}
	double OpenPrice(EnumDirection direct,int jumps=0)
	{
		return PriceByDirection(direct,OpenOrderPriceType,jumps);
	}
	double ClosePrice(EnumDirection direct, int jumps = 0)
	{
		return PriceByDirection(direct, CloseOrderPriceType, jumps);
	}
	int GetPosition()
	{
		int L1Position = 0;
		if (openPosition)
		{
			L1Position = openPosition->Position;
		}
		return L1Position;
	}
};

enum VergenceDirection
{
	NoneVergence=0,
	LongVergence=1,
	ShortVergence=2
};

struct InstPriceMarginParam
{
	int index;

	//整个套利对的合约的方向,和第一腿合约的开仓方向一致;
	VergenceDirection direct;

	bool EnableTrading; //是否允许交易;

	bool EnableOpenLong;//允许开多;
	bool EnableOpenShort;//允许开空;

	int TotalHands;//套利允许的最大下单手数;
	int EachHands;//每次报单数量;

	double LongOpenPriceDiff;//开多价差,价差低于这个值时做多;
	double LongClosePriceDiff;//平多;价差高于这个值时平多;

	double ShortOpenPriceDiff;
	double ShortClosePriceDiff;

	//腿1和腿2的报单参数;
	std::pair<InstOrderParam, InstOrderParam> InstOrderPair;

	//定时平仓时间点;
	boost::posix_time::time_duration DayClosePositionTime;
	boost::posix_time::time_duration NightClosePositionTime;

	//合约的数据;
	boost::shared_ptr<InstrumentData> pInstrument;

	InstPriceMarginParam()
	{
		direct = NoneVergence;


		DayClosePositionTime = boost::posix_time::duration_from_string("14:59:00");
		DayClosePositionTime = boost::posix_time::duration_from_string("23:29:00");

		EnableTrading=true; //是否允许交易;

		EnableOpenLong=true;//允许开多;
		EnableOpenShort=true;//允许开空;

		TotalHands=1;//套利允许的最大下单手数;
		EachHands=1;//每次数数;


		LongOpenPriceDiff=-9999999.;
		LongClosePriceDiff=9999999.;//平多;价差高于这个值时平多;

		ShortOpenPriceDiff=9999999.;
		ShortClosePriceDiff=-9999999.;
	}

	//获取2个合约的仓位数量差;
	int GetPositionDiff()
	{
		int L1Position = 0;
		if (InstOrderPair.first.openPosition)
		{
			L1Position = InstOrderPair.first.openPosition->Position;
		}
		int L2Position = 0;
		if (InstOrderPair.second.openPosition)
		{
			L2Position = InstOrderPair.second.openPosition->Position;
		}
		return L1Position*InstOrderPair.second.defaultHands - L2Position*InstOrderPair.first.defaultHands;
	}

	double CurPriceMargin()
	{
		return InstOrderPair.first.InstTick->LastPrice - InstOrderPair.second.InstTick->LastPrice;
	}

	bool IsSameTime()
	{

		return InstOrderPair.first.InstTick 
			&& InstOrderPair.second.InstTick 
			&& boost::algorithm::iequals(InstOrderPair.first.InstTick->UpdateTimeStr,
		 			InstOrderPair.second.InstTick->UpdateTimeStr);
	}

	bool Update(boost::shared_ptr<Tick> tick)
	{
		if (InstOrderPair.first.InstTick->InstrumentID() == tick->InstrumentID())
		{
			InstOrderPair.first.InstTick = tick;
			return true;
		}
		if (InstOrderPair.second.InstTick->InstrumentID() == tick->InstrumentID())
		{
			InstOrderPair.second.InstTick = tick;
			return true;
		}
		return false;
	}
};

class SimpleArbitrageStrategy :
	public IStrategy
{
public:
	explicit SimpleArbitrageStrategy(const std::string& sid);
	virtual ~SimpleArbitrageStrategy();

	virtual void OnTick(boost::shared_ptr<Tick> tick) ;

	virtual void OnOrder(boost::shared_ptr<Order> order) ;

	virtual void OnTrade(boost::shared_ptr<Trade> trade) ;

	//撤单失败的回报;
	virtual void OnOrderAction(boost::shared_ptr<InputOrderAction> orderAction);

	virtual void Initialize() ;

	virtual void Finalize() ;
public:
	//处理开仓逻辑;
	bool Open(boost::shared_ptr<InstPriceMarginParam> priceMarginParam);
	bool OpenL1(boost::shared_ptr<InstPriceMarginParam> priceMarginParam);
	//处理平仓逻辑;
	bool Close(boost::shared_ptr<InstPriceMarginParam> priceMarginParam);
	bool CloseL1(boost::shared_ptr<InstPriceMarginParam> priceMarginParam);
	//处理撤单逻辑;
	bool Cancel(boost::shared_ptr<InstPriceMarginParam> priceMarginParam);
	//通过成交更新仓位;
	bool UpdatePositionByTrade(boost::shared_ptr<Trade> trade, boost::shared_ptr<InvestorPosition>& investorPosition);
public:
	bool ReadConfig();
	bool WriteConfig();
protected:
	std::map<int, boost::shared_ptr<InstPriceMarginParam> > m_ParamsMap;
};

