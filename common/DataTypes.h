#ifndef _DATA_TYPES_H_
#define _DATA_TYPES_H_
#include <string>
#include <cstdint>
#include <vector>
#include <tuple>
#include <utility>
#include <map>
#include <functional>
#ifdef _MSC_VER
#if _MSC_VER >=1700 
#include <memory>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <thread>
using namespace std;
#else
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/chrono/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
using namespace boost;
#endif
#else
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <thread>
using namespace std;
#endif

#include "Tick.h"
#include "Rate.h"
#include "Order.h"
#include "Trade.h"
#include "Notice.h"
#include "Investor.h"
#include "Exchange.h"
#include "ParkOrder.h"
#include "Instrument.h"
#include "TradingCode.h"
#include "TransferBank.h"
#include "TradingAccount.h"
#include "SettlementInfo.h"
#include "InvestorPosition.h"
#include "InstrumentStatus.h"
#include "BrokerTradingParams.h"
#include "TraderSharedData.h"
#include <functional>
#include <ctime>
typedef std::function<void(void)> OnEventFun;
#define CTP_TRADER_PRODUCT_INFO "CtpTrader V10.0"

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/shared_ptr.hpp>

namespace boost
{
	namespace serialization
	{
		template<class Archive>
		inline void serialize(Archive& ar, struct InputOrderAction& ioa, const unsigned int version)
		{
			ar & ioa.BrokerID;
			ar & ioa.InvestorID;
			ar & ioa.OrderActionRef;
			ar & ioa.OrderRef;
			ar & ioa.RequestID;
			ar & ioa.FrontID;
			ar & ioa.SessionID;
			ar & ioa.ExchangeID;
			ar & ioa.OrderSysID;
			ar & ioa.ActionFlag;
			ar & ioa.InstrumentID;
		}
		BOOST_SERIALIZATION_SHARED_PTR(InputOrderAction)

		template<class Archive>
		inline void serialize(Archive& ar, struct InputOrder& io, const unsigned int version)
		{
			ar & io.BrokerID;
			ar & io.InvestorID;
			ar & io.InstrumentID;
			ar & io.OrderRef;
			ar & io.UserID;
			ar & io.OrderPriceType;
			ar & io.Direction;
			ar & io.CombOffsetFlag[0] & io.CombOffsetFlag[1] & io.CombOffsetFlag[2]
				& io.CombOffsetFlag[3] & io.CombOffsetFlag[4];

			ar & io.CombHedgeFlag[0] & io.CombHedgeFlag[1] & io.CombHedgeFlag[2]
				& io.CombHedgeFlag[3] & io.CombHedgeFlag[4];

			ar & io.LimitPrice;
			ar & io.VolumeTotalOriginal;
			ar & io.TimeCondition;
			ar & io.GTDDate;
			ar & io.MinVolume;
			ar & io.ContingentCondition;
			ar & io.StopPrice;
			ar & io.RequestID;
			ar & io.FrontID;
			ar & io.SessionID;
			ar & io.OrderSysID;
			ar & io.ExchangeID;
		}

// 		template<class Archive>
// 		inline void serialize(Archive& ar, boost::shared_ptr<InputOrder> io, const unsigned int version)
// 		{
// 			serialization(ar, *io, version);
// 		}
		BOOST_SERIALIZATION_SHARED_PTR(InputOrder)

		template<class Archive>
		inline void serialize(Archive& ar, struct Order& o, const unsigned int version)
		{
			serialize(ar, (InputOrder&)o, version);
			ar & o.ForceCloseReason;
			ar & o.IsAutoSuspend;
			ar & o.BusinessUnit;
			ar & o.OrderLocalID;
			ar & o.ParticipantID;
			ar & o.ClientID;
			ar & o.ExchangeInstID;
			ar & o.TraderID;
			ar & o.InstallID;
			ar & o.OrderSubmitStatus;
			ar & o.NotifySequence;
			ar & o.TradingDay;
			ar & o.SettlementID;
			ar & o.OrderSource;
			ar & o.OrderStatus;
			ar & o.OrderType;
			ar & o.VolumeTraded;
			ar & o.VolumeTotal;
			ar & o.InsertDate;
			ar & o.InsertTime;
			ar & o.ActiveTime;
			ar & o.SuspendTime;
			ar & o.UpdateTime;
			ar & o.CancelTime;
			ar & o.ActiveTraderID;
			ar & o.ClearingPartID;
			ar & o.SequenceNo;
			ar & o.UserProductInfo;
			ar & o.StatusMsg;
			ar & o.UserForceClose;
			ar & o.ActiveUserID;
			ar & o.BrokerOrderSeq;
			ar & o.RelativeOrderSysID;
			ar & o.ZCETotalTradedVolume;
			ar & o.IsSwapOrder;
			ar & o.SeatID;
		};

		

// 		template<class Archive>
// 		inline void serialize(Archive& ar, boost::shared_ptr<Order> o, const unsigned int version)
// 		{
// 			serialization(ar, *o, version);
// 		}
		BOOST_SERIALIZATION_SHARED_PTR(Order)


		template<class Archive>
		inline void serialize(Archive& ar, struct Trade& t, const unsigned int version)
		{
			ar & t.BrokerID;
			ar & t.InvestorID;
			ar & t.InstrumentID;
			ar & t.OrderRef;
			ar & t.UserID;
			ar & t.ExchangeID;
			ar & t.TradeID;
			ar & t.Direction;
			ar & t.OrderSysID;
			ar & t.ParticipantID;
			ar & t.ClientID;
			ar & t.TradingRole;
			ar & t.ExchangeInstID;
			ar & t.OffsetFlag;
			ar & t.HedgeFlag;
			ar & t.Price;
			ar & t.Volume;
			ar & t.TradeDate;
			ar & t.TradeTime;
			ar & t.TradeType;
			ar & t.PriceSource;
			ar & t.TraderID;
			ar & t.OrderLocalID;
			ar & t.ClearingPartID;
			ar & t.BusinessUnit;
			ar & t.SequenceNo;
			ar & t.TradingDay;
			ar & t.SettlementID;
			ar & t.BrokerOrderSeq;
			ar & t.TradeSource;
			ar & t.SeatID;
		};

// 		template<class Archive>
// 		inline void serialize(Archive& ar, boost::shared_ptr<Trade> t, const unsigned int version)
// 		{
// 			serialize(ar, *t, version);
// 		}
		BOOST_SERIALIZATION_SHARED_PTR(Trade)

		template<class Archive>
		inline void serialize(Archive& ar, struct InvestorPosition& ip, const unsigned int version)
		{
			ar & ip.InstrumentID;
			ar & ip.BrokerID;
			ar & ip.InvestorID;
			ar & ip.ExchangeID;
			ar & ip.ClientID;
			ar & ip.OpenDate;
			ar & ip.PosiDirection;
			ar & ip.HedgeFlag;
			ar & ip.PositionDate;
			ar & ip.YdPosition;
			ar & ip.Position;
			ar & ip.LongFrozen;
			ar & ip.LongFrozenAmount;
			ar & ip.ShortFrozen;
			ar & ip.ShortFrozenAmount;
			ar & ip.OpenVolume;
			ar & ip.CloseVolume;
			ar & ip.OpenAmount;
			ar & ip.CloseAmount;
			ar & ip.PositionCost;
			ar & ip.PreMargin;
			ar & ip.UseMargin;
			ar & ip.FrozenMargin;
			ar & ip.FrozenCash;
			ar & ip.FrozenCommission;
			ar & ip.CashIn;
			ar & ip.Commission;
			ar & ip.CloseProfit;
			ar & ip.PositionProfit;
			ar & ip.PreSettlementPrice;
			ar & ip.SettlementPrice;
			ar & ip.TradingDay;
			ar & ip.SettlementID;
			ar & ip.OpenCost;
			ar & ip.ExchangeMargin;
			ar & ip.CombPosition;
			ar & ip.CombLongFrozen;
			ar & ip.CombShortFrozen;
			ar & ip.CloseProfitByDate;
			ar & ip.CloseProfitByTrade;
			ar & ip.TodayPosition;
			ar & ip.MarginRateByMoney;
			ar & ip.MarginRateByVolume;
			ar & ip.StrikeFrozen;
			ar & ip.StrikeFrozenAmount;
			ar & ip.AbandonFrozen;
			ar & ip.OpenPrice;
		}

// 		template<class Archive>
// 		inline void serialize(Archive& ar, boost::shared_ptr<InvestorPosition> ip, const unsigned int version)
// 		{
// 			serialize(ar, *ip, version);
// 		}
		BOOST_SERIALIZATION_SHARED_PTR(InvestorPosition)

		template<class Archive>
		inline void serialize(Archive& ar, InstrumentMarginRate& imr, const unsigned int version)
		{
			ar & imr.BrokerID;
			ar & imr.HedgeFlag;
			ar & imr.InstrumentID;
			ar & imr.InvestorID;
			ar & imr.InvestorRange;
			ar & imr.IsRelative;
			ar & imr.LongMarginRatioByMoney;
			ar & imr.LongMarginRatioByVolume;
			ar & imr.ShortMarginRatioByMoney;
			ar & imr.ShortMarginRatioByVolume;
		}

// 		template<class Archive>
// 		inline void serialize(Archive& ar, boost::shared_ptr<InstrumentMarginRate> imr, const unsigned int version)
// 		{
// 			serialize(ar, *imr, version);
// 		}
		BOOST_SERIALIZATION_SHARED_PTR(InstrumentMarginRate)



		template<class Archive>
		inline void serialize(Archive& ar, InstrumentCommisionRate& icr, const unsigned int version)
		{
			ar & icr.BrokerID;
			ar & icr.CloseRatioByMoney;
			ar & icr.CloseRatioByVolume;
			ar & icr.CloseTodayRatioByMoney;
			ar & icr.CloseTodayRatioByVolume;
			ar & icr.InstrumentID;
			ar & icr.InvestorID;
			ar & icr.InvestorRange;
			ar & icr.IsInitied;
			ar & icr.OpenRatioByMoney;
			ar & icr.OpenRatioByVolume;
		}

// 		template<class Archive>
// 		inline void serialize(Archive& ar, boost::shared_ptr<InstrumentCommisionRate> icr, const unsigned int version)
// 		{
// 			serialize(ar, *icr, version);
// 		}
		BOOST_SERIALIZATION_SHARED_PTR(InstrumentCommisionRate)
	}
}


enum FIELD_ID
{
	FIELD_code					=100,
	FIELD_name					=101,
	FIELD_TradingDay			=102,
	FIELD_LoginTime				=103,
	FIELD_BrokerID				=104,
	FIELD_UserID				=105,
	FIELD_ExchangeTime			=106,
	//...
	///交易所代码
	FIELD_exchange              =107,
	///合约在交易所的代码
	//TThostFtdcExchangeInstIDType	ExchangeInstID;
	///产品代码
	FIELD_ProductID				=108,
	///产品类型
	FIELD_ProductClass          =109,
	///交割年份
	//int	DeliveryYear;
	///交割月
	//int	DeliveryMonth;
	///市价单最大下单量
	FIELD_MaxMarketOrderVolume   =110,
	///市价单最小下单量
	FIELD_MinMarketOrderVolume   =111,
	///限价单最大下单量
	FIELD_MaxLimitOrderVolume    =112,
	///限价单最小下单量
	FIELD_MinLimitOrderVolume    =113,
	///合约数量乘数
	FIELD_VolumeMultiple         =114,
	///最小变动价位
	FIELD_PriceTick              =115,
	///创建日
	FIELD_CreateDate             =116,
	///上市日
	FIELD_OpenDate               =117,
	///到期日
	FIELD_ExpireDate             =118,
	///开始交割日
	FIELD_StartDelivDate         =119,
	///结束交割日
	FIELD_EndDelivDate           =120,
	///合约生命周期状态
	//TThostFtdcInstLifePhaseType	InstLifePhase;
	///当前是否交易
	FIELD_IsTrading              =121,
	///持仓类型
	FIELD_PositionType           =122,
	///持仓日期类型
	//TThostFtdcPositionDateTypeType	PositionDateType;
	///多头保证金率
	FIELD_LongMarginRatio        =123,
	///空头保证金率
	FIELD_ShortMarginRatio       =124,
	FIELD_LastPrice				=125,
	FIELD_PreSetttlementPrice	=126,
	FIELD_PreClosePrice			=127,
	FIELD_PreOpenInsterest		=128,
	FIELD_OpenPrice				=129,
	FIELD_HighestPrice			=130,
	FIELD_LowestPrice			=131,
	FILED_Volume				=132,
	FILED_Turnover				=133,
	FIELD_ClosePrice			=134,
	FIELD_SettlementPrice		=135,
	FIELD_UpperLimitPrice		=136,
	FIELD_LowerLimitPrice		=137,
	FIELD_UpdateTime			=138,
	FIELD_UpdateMillisec		=139,
	FIELD_BidPrice1				=140,
	FIELD_BidVolume1			=141,
	FIELD_AskPrice1				=142,
	FIELD_AskVolume1			=143,
	FIELD_AveragePrice			=144

};
enum LoginState
{
	nologin=1,//没登录
	logining=2,//登录中
	logined=3,//以登录
	logouted=4,//登出
};

//股票代码表;
struct InstrumentCode
{
	//市场简写代码;
	std::string MarketCode;
	std::string ExchangeID;
	std::string InstrumentID;
};

inline bool operator == (const InstrumentCode& ic1,const InstrumentCode& ic2)
{
	return ic1.ExchangeID==ic2.ExchangeID &&
		ic1.InstrumentID==ic2.InstrumentID;
}

//策略的配置参数;
struct StrategyParam
{
	std::string StrategyID;
	std::string StrategyPath;
	std::string UserID;
	double Available; //为0的情况下,可以使用该账户的所有资金;
	std::string TradingDay;
	std::map<std::string,int> Instruments;
	std::vector<std::string> Exchanges;//交易所;
	std::map<std::string, double> SubUsers;
};

inline bool operator == (const StrategyParam& ic1, const StrategyParam& ic2)
{
	return ic1.StrategyID == ic2.StrategyID;
}

///合约手续费率;
struct CommissionRate
{
	///合约代码;
	std::string	InstrumentID;
	///投资者范围;
	char	InvestorRange;
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	///开仓手续费率;
	double	OpenRatioByMoney;
	///开仓手续费;
	double	OpenRatioByVolume;
	///平仓手续费率;
	double	CloseRatioByMoney;
	///平仓手续费;
	double	CloseRatioByVolume;
	///平今手续费率;
	double	CloseTodayRatioByMoney;
	///平今手续费;
	double	CloseTodayRatioByVolume;
};

///转账请求;
struct BankFutureTransParam
{
	///业务功能码;
	std::string	TradeCode;
	///银行代码;
	std::string	BankID;
	///银行分支机构代码;
	std::string	BankBranchID;
	///期商代码;
	std::string	BrokerID;
	///期商分支机构代码;
	std::string	BrokerBranchID;
	///交易日期;
	std::string	TradeDate;
	///交易时间;
	std::string	TradeTime;
	///银行流水号
// 	TThostFtdcBankSerialType	BankSerial;
// 	///交易系统日期 
// 	TThostFtdcTradeDateType	TradingDay;
// 	///银期平台消息流水号
// 	TThostFtdcSerialType	PlateSerial;
// 	///最后分片标志
// 	TThostFtdcLastFragmentType	LastFragment;
	///会话号;
	int	SessionID;
	///客户姓名;
	std::string	CustomerName;
	///证件类型;
	char	IdCardType;
	///证件号码;
	std::string	IdentifiedCardNo;
	///客户类型;
	char	CustType;
	///银行帐号;
	std::string	BankAccount;
	///银行密码;
	std::string	BankPassWord;
	///投资者帐号;
	std::string	AccountID;
	///期货密码;
	std::string	Password;
// 	///安装编号
// 	TThostFtdcInstallIDType	InstallID;
// 	///期货公司流水号
// 	TThostFtdcFutureSerialType	FutureSerial;
	///用户标识
	std::string	UserID;
	///验证客户证件号码标志
	char	VerifyCertNoFlag;
	///币种代码
	std::string	CurrencyID;
	///转帐金额
	double	TradeAmount;
	///期货可取金额
	double	FutureFetchAmount;
// 	///费用支付标志
// 	TThostFtdcFeePayFlagType	FeePayFlag;
// 	///应收客户费用
// 	TThostFtdcCustFeeType	CustFee;
// 	///应收期货公司费用
// 	TThostFtdcFutureFeeType	BrokerFee;
// 	///发送方给接收方的消息
// 	TThostFtdcAddInfoType	Message;
// 	///摘要
// 	TThostFtdcDigestType	Digest;
// 	///银行帐号类型
// 	TThostFtdcBankAccTypeType	BankAccType;
// 	///渠道标志
// 	TThostFtdcDeviceIDType	DeviceID;
// 	///期货单位帐号类型
// 	TThostFtdcBankAccTypeType	BankSecuAccType;
// 	///期货公司银行编码
// 	TThostFtdcBankCodingForFutureType	BrokerIDByBank;
// 	///期货单位帐号
// 	TThostFtdcBankAccountType	BankSecuAcc;
// 	///银行密码标志
// 	TThostFtdcPwdFlagType	BankPwdFlag;
// 	///期货资金密码核对标志
// 	TThostFtdcPwdFlagType	SecuPwdFlag;
// 	///交易柜员
// 	TThostFtdcOperNoType	OperNo;
// 	///请求编号
// 	TThostFtdcRequestIDType	RequestID;
// 	///交易ID
// 	TThostFtdcTIDType	TID;
// 	///转账交易状态
// 	TThostFtdcTransferStatusType	TransferStatus;
};

///查询投资者持仓明细;
struct PositionParam
{
	int PositionType;//持仓，组合持仓，持仓明细;
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	///合约代码;
	std::string	InstrumentID;
};



///用户口令变更;
struct UpdatePasswordParam
{
	int PasswordType;//类型，是资金还是账户密码;
	///经纪公司代码;
	std::string	BrokerID;
	///用户代码;
	std::string	UserID;
	///原来的口令;
	std::string	OldPassword;
	///新的口令;
	std::string	NewPassword;
};



//K线数据;
struct KData{
	char   InstrumentID[INSTRUMENT_ID_LENGTH];//合约ID;
	char   ExchangeID[EXCHANGE_ID_LENGTH];//交易所;
	char   TradingDate[DATE_STRING_LENGTH];//交易日期;
	time_t TradingTime;//交易时间;
	double OpenPrice;//开盘价;
	double HighestPrice;//最高价;
	double LowestPrice;//最低价;
	double ClosePrice;//收盘价;
	double  Volume;//成交量;
	double LastVolume;//最新的总成交量;
	double Turnover;//成交额;
	double LastTurnover;//最新的总成交额;
	double AveragePrice;//平均价;
	int dwType;
};






enum KTypes{
	ktypeNone=0x00,
	ktypeSec1=0x01,        //1.秒;
	ktypeMin=1*60,         //1.分;
	ktypeMin3=3*60,		   //3.分;
	ktypeMin5=5*60,        //5.分;
	ktypeMin15=15*60,      //15.分;
	ktypeMin30=30*60,      //30.分;
	ktypeMin60=60*60,      //60.分;
	ktypeDay=24*60*60,     //日线;
	//以下的秒数,只是理论的秒数;
	ktypeWeek=7*24*60*60,  //周线;
	ktypeMonth=30*24*60*60,  //月线;
	ktypeSeason=3*30*24*60*60,//季线;
	ktypeYear=365*30*24*60*60,//年线;
	ktypeMax=ktypeYear+1      //多年;
};

inline std::string time_t2str(time_t t)
{
	char mbstr[100]={0};
#ifdef _WIN32
	std::tm* tmm=std::gmtime(&t);
	std::strftime(mbstr, sizeof(mbstr), "%H:%M:%S", tmm);
#else
	std::strftime(mbstr, sizeof(mbstr), "%T", std::localtime(&t));
#endif
	return std::string(mbstr);
}

inline std::string time_t2hm(time_t t)
{

	char mbstr[16]={0};
#ifdef _WIN32
	std::tm* tmm=std::gmtime(&t);
	std::strftime(mbstr, sizeof(mbstr), "%H%M", tmm);
#else
	std::strftime(mbstr, sizeof(mbstr), "%T", std::localtime(&t));
#endif
	return std::string(mbstr);
}

inline std::string time_t2ymd(time_t t)
{
	char mbstr[32]={0};
#ifdef _WIN32
	std::tm* tmm=std::localtime(&t);
	std::strftime(mbstr, sizeof(mbstr), "%Y%m%d", tmm);
#else
	std::strftime(mbstr, sizeof(mbstr), "%Y%m%d", std::localtime(&t));
#endif
	return std::string(mbstr);
}


int convertTick2KData(const std::vector<Tick>& ticks,std::vector<KData>& kdatas, int ktype);

bool UpdateKDATAByREPORT( KData &kd,const Tick * pReport );

bool TimeIsNextSection(time_t oldTime,time_t newTime,int dwType);

#endif // !_DATA_TYPES_H_
