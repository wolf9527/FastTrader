#ifndef _ENUMS_H_
#define _ENUMS_H_
#include <string>

enum EnumCommonValues
{
	INSTRUMENT_ID_LENGTH=31,
	INSTRUMENT_NAME_LENGTH=21,
	EXCHANGE_ID_LENGTH=9,
	EXCHANGE_NAME_LENGTH=61,
	DATE_STRING_LENGTH=9,
	TIME_STRING_LENGTH=9,
	INVESTOR_ID_LENGTH=13,
	USER_ID_LENGTH=16,
	BROKER_ID_LENGTH=11,
	PRICE_VOLUME_LEVEL=5,//行情档数;
};
///TFtdcPosiDirectionType是一个持仓多空方向类型
/////////////////////////////////////////////////////////////////////////
enum /*class*/ EnumPosiDirection:char
{
	///净
	PD_Net='1',
	///多头
	PD_Long='2',
	///空头
	PD_Short='3'
};

///TFtdcHedgeFlagType是一个投机套保标志类型
/////////////////////////////////////////////////////////////////////////
enum /*class*/   EnumHedgeFlag:char
{
	HF_None=0,
	///投机;
	HF_Speculation='1',
	///套利;
	HF_Arbitrage='2',
	///套保;
	HF_Hedge='3',
	//做市商;
	HF_MarketMaker='4',
};

///TFtdcPositionDateType是一个持仓日期类型
/////////////////////////////////////////////////////////////////////////
enum /*class*/ EnumPositionDateType:char
{
	///今日持仓;
	PSD_Today='1',
	///历史持仓;
	PSD_History='2',
};


enum EnumPositionType :char
{
	///净持仓
	PT_Net='1',
	///综合持仓
	PT_Gross='2'
};

///TFtdcDirectionType是一个买卖方向类型;
/////////////////////////////////////////////////////////////////////////
enum /*class*/  EnumDirection:char
{
	///买;
	D_Buy='0',
	///卖;
	D_Sell='1'
};
inline EnumDirection operator!(EnumDirection d)
{
	if (d==D_Buy)
	{
		return D_Sell;
	}
	else
	{
		return D_Buy;
	}
}

///TFtdcTradeTypeType是一个成交类型类型;
/////////////////////////////////////////////////////////////////////////
enum /*class*/ EnumTradeType:char
{
	///组合持仓拆分为单一持仓,初始化不应包含该类型的持仓;
	TRDT_SplitCombination='#',
	///普通成交;
	TRDT_Common='0',
	///期权执行;
	TRDT_OptionsExecution='1',
	///OTC成交;
	TRDT_OTC='2',
	///期转现衍生成交;
	TRDT_EFPDerived='3',
	///组合衍生成交;
	TRDT_CombinationDerived='4',
};
///TFtdcOrderPriceTypeType是一个报单价格条件类型
/////////////////////////////////////////////////////////////////////////
enum EnumOrderPriceType:char
{
	///任意价;
	OPT_AnyPrice='1',
	///限价;
	OPT_LimitPrice='2',
	///最优价;
	OPT_BestPrice='3',
	///最新价;
	OPT_LastPrice='4',
	///最新价浮动上浮1个ticks;
	OPT_LastPricePlusOneTicks='5',
	///最新价浮动上浮2个ticks;
	OPT_LastPricePlusTwoTicks='6',
	///最新价浮动上浮3个ticks;
	OPT_LastPricePlusThreeTicks='7',
	///卖一价;
	OPT_AskPrice1='8',
	///卖一价浮动上浮1个ticks;
	OPT_AskPrice1PlusOneTicks='9',
	///卖一价浮动上浮2个ticks;
	OPT_AskPrice1PlusTwoTicks='A',
	///卖一价浮动上浮3个ticks;
	OPT_AskPrice1PlusThreeTicks='B',
	///买一价;
	OPT_BidPrice1='C',
	///买一价浮动上浮1个ticks;
	OPT_BidPrice1PlusOneTicks='D',
	///买一价浮动上浮2个ticks;
	OPT_BidPrice1PlusTwoTicks='E',
	///买一价浮动上浮3个ticks;
	OPT_BidPrice1PlusThreeTicks='F',
	///五档价;
	OPT_FiveLevelPrice='G',
};

///TFtdcOffsetFlagType是一个开平标志类型
/////////////////////////////////////////////////////////////////////////
enum  EnumOffsetFlag:char
{
	OF_None=0,
	///开仓;
	OF_Open='0',
	///平仓;
	OF_Close='1',
	///强平;
	OF_ForceClose='2',
	///平今;
	OF_CloseToday='3',
	///平昨;
	OF_CloseYesterday='4',
	///强减;
	OF_ForceOff='5',
	///本地强平;
	OF_LocalForceClose='6',
};

///TFtdcTimeConditionType是一个有效期类型类型
/////////////////////////////////////////////////////////////////////////
enum EnumTimeCondition:char
{
	///立即完成，否则撤销;
	TC_IOC='1',
	///本节有效;
	TC_GFS='2',
	///当日有效;
	TC_GFD='3',
	///指定日期前有效;
	TC_GTD='4',
	///撤销前有效;
	TC_GTC='5',
	///集合竞价有效;
	TC_GFA='6',
};

///TFtdcVolumeConditionType是一个成交量类型类型;
/////////////////////////////////////////////////////////////////////////
enum EnumVolumeCondition:char
{
	///任何数量;
	VC_AV='1',
	///最小数量;
	VC_MV='2',
	///全部数量;
	VC_CV='3',
};

///TFtdcContingentConditionType是一个触发条件类型;
/////////////////////////////////////////////////////////////////////////
enum EnumContingentCondition:char
{

	///立即;
	CC_Immediately = '1',
	///止损;
	CC_Touch = '2',
	///止赢;
	CC_TouchProfit = '3',
	///预埋单;
	CC_ParkedOrder = '4',
	///最新价大于条件价;
	CC_LastPriceGreaterThanStopPrice = '5',
	///最新价大于等于条件价;
	CC_LastPriceGreaterEqualStopPrice = '6',
	///最新价小于条件价;
	CC_LastPriceLesserThanStopPrice = '7',
	///最新价小于等于条件价;
	CC_LastPriceLesserEqualStopPrice = '8',
	///卖一价大于条件价;
	CC_AskPriceGreaterThanStopPrice = '9',
	///卖一价大于等于条件价;
	CC_AskPriceGreaterEqualStopPrice = 'A',
	///卖一价小于条件价;
	CC_AskPriceLesserThanStopPrice = 'B',
	///卖一价小于等于条件价;
	CC_AskPriceLesserEqualStopPrice = 'C',
	///买一价大于条件价;
	CC_BidPriceGreaterThanStopPrice = 'D',
	///买一价大于等于条件价;
	CC_BidPriceGreaterEqualStopPrice = 'E',
	///买一价小于条件价;
	CC_BidPriceLesserThanStopPrice = 'F',
	///买一价小于等于条件价;
	CC_BidPriceLesserEqualStopPrice = 'H',
	//手动发出;
	CC_ManualContinous = 'I',
	//进入连续交易状态;
	CC_EnterContinousTrade = 'J'
};

///TFtdcForceCloseReasonType是一个强平原因类型
/////////////////////////////////////////////////////////////////////////
enum EnumForceCloseReason:char
{
	///非强平;
	FCC_NotForceClose='0',
	///资金不足;
	FCC_LackDeposit='1',
	///客户超仓;
	FCC_ClientOverPositionLimit='2',
	///会员超仓;
	FCC_MemberOverPositionLimit='3',
	///持仓非整数倍;
	FCC_NotMultiple='4',
	///违规;
	FCC_Violation='5',
	///其它;
	FCC_Other='6',
	///自然人临近交割;
	FCC_PersonDeliv='7',
};

///TFtdcOrderSubmitStatusType是一个报单提交状态类型;
/////////////////////////////////////////////////////////////////////////
enum EnumOrderSubmitStatus:char
{
	///已经提交;
	OSS_InsertSubmitted='0',
	///撤单已经提交;
	OSS_CancelSubmitted='1',
	///修改已经提交;
	OSS_ModifySubmitted='2',
	///已经接受;
	OSS_Accepted='3',
	///报单已经被拒绝;
	OSS_InsertRejected='4',
	///撤单已经被拒绝;
	OSS_CancelRejected='5',
	///改单已经被拒绝;
	OSS_ModifyRejected='6',
};

///TFtdcOrderSourceType是一个报单来源类型;
/////////////////////////////////////////////////////////////////////////
enum EnumOrderSource:char
{
	///来自参与者;
	OSRC_Participant='0',
	///来自管理员;
	OSRC_Administrator='1',
};

///TFtdcOrderStatusType是一个报单状态类型
/////////////////////////////////////////////////////////////////////////
enum EnumOrderStatus:char
{
	///全部成交;
	OST_AllTraded='0',
	///部分成交还在队列中;
	OST_PartTradedQueueing='1',
	///部分成交不在队列中;
	OST_PartTradedNotQueueing='2',
	///未成交还在队列中;
	OST_NoTradeQueueing='3',
	///未成交不在队列中;
	OST_NoTradeNotQueueing='4',
	///撤单;
	OST_Canceled='5',
	///未知;
	OST_Unknown='a',
	///尚未触发;
	OST_NotTouched='b',
	///已触发;
	OST_Touched='c',
};

///TFtdcOrderTypeType是一个报单类型类型
/////////////////////////////////////////////////////////////////////////
enum EnumOrderType:char
{
	///正常;
	ORDT_Normal='0',
	///报价衍生;
	ORDT_DeriveFromQuote='1',
	///组合衍生;
	ORDT_DeriveFromCombination='2',
	///组合报单;
	ORDT_Combination='3',
	///条件单;
	ORDT_ConditionalOrder='4',
	///互换单;
	ORDT_Swap='5',
};

enum EnumInvestorRange:char
{
	/////////////////////////////////////////////////////////////////////////
	///TFtdcInvestorRangeType是一个投资者范围类型
	/////////////////////////////////////////////////////////////////////////
	///所有
	IR_All='1',
	///投资者组
	IR_Group='2',
	///单一投资者
	IR_Single='3',
};

enum EnumProductClass:char
{

	///期货
	PC_Futures='1',
	///期权
	PC_Options='2',
	///组合
	PC_Combination='3',
	///即期
	PC_Spot='4',
	///期转现
	PC_EFP='5',
	//现货期权;
	PC_SpotOption='6',
	///证券A股
	PC_StockA='7',
	///证券B股
	PC_StockB='8',
	///ETF
	PC_ETF='9',
	///ETF申赎
	PC_ETFPurRed='A',
};

enum EnumCloseDealType :char
{
	///正常;
	CDT_Normal='0',
	///投机平仓优先;
	CDT_SpecFirst='1'
};


/////////////////////////////////////////////////////////////////////////
///TFtdcMortgageFundUseRangeType是一个货币质押资金可用范围类型
/////////////////////////////////////////////////////////////////////////
enum EnumMortgageFundUseRange
{
	///不能使用;
	MFUR_None='0',
	///用于保证金;
	MFUR_Margin='1',
	///用于手续费、盈亏、保证金;
	MFUR_All='2'
};

///错误信息;
struct ErrorMessage
{
	///错误代码;
	int	ErrorID;
	///错误信息;
	std::string	ErrorMsg;
	ErrorMessage():ErrorID(0)
	{
		
	}
};
#endif