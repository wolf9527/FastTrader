#ifndef _RATE_H_
#define _RATE_H_
#include "Enums.h"
#include <string>


//各种费率;
struct InstrumentMarginRate
{
	///合约代码
	std::string	InstrumentID;
	///投资者范围
	EnumInvestorRange	InvestorRange;
	///经纪公司代码
	std::string	BrokerID;
	///投资者代码
	std::string	InvestorID;
	///投机套保标志
	EnumHedgeFlag	HedgeFlag;
	///多头保证金率
	double	LongMarginRatioByMoney;
	///多头保证金费
	double	LongMarginRatioByVolume;
	///空头保证金率
	double	ShortMarginRatioByMoney;
	///空头保证金费
	double	ShortMarginRatioByVolume;
	///是否相对交易所收取
	int	IsRelative;
};

//合约手续费率;
struct InstrumentCommisionRate
{
	///合约代码
	std::string	InstrumentID;
	///投资者范围
	EnumInvestorRange	InvestorRange;
	///经纪公司代码
	std::string	BrokerID;
	///投资者代码
	std::string	InvestorID;
	///开仓手续费率
	double	OpenRatioByMoney;
	///开仓手续费
	double	OpenRatioByVolume;
	///平仓手续费率
	double	CloseRatioByMoney;
	///平仓手续费
	double	CloseRatioByVolume;
	///平今手续费率
	double	CloseTodayRatioByMoney;
	///平今手续费
	double	CloseTodayRatioByVolume;
	bool    IsInitied;
};

//交易所保证金率;
struct ExchangeMarginRate
{
	///经纪公司代码
	std::string	BrokerID;
	///合约代码
	std::string	InstrumentID;
	///投机套保标志
	EnumHedgeFlag	HedgeFlag;
	///多头保证金率
	double	LongMarginRatioByMoney;
	///多头保证金费
	double	LongMarginRatioByVolume;
	///空头保证金率
	double	ShortMarginRatioByMoney;
	///空头保证金费
	double	ShortMarginRatioByVolume;
};
#endif
