#ifndef _EXCHANGE_H_
#define _EXCHANGE_H_

#include <string>
#include <chrono>
#include <cstdint>

///TFtdcExchangePropertyType是一个交易所属性类型
/////////////////////////////////////////////////////////////////////////
enum EnumExchangeProperty:char
{
	///正常;
	EXP_Normal='0',
	///根据成交生成报单;
	EXP_GenOrderByTrade='1'
};

///交易所;
struct Exchange
{
	///交易所代码;
	std::string	ExchangeID;
	///交易所名称;
	std::string	ExchangeName;
	///交易所属性;
	char	ExchangeProperty;
	//市场编号;
	std::uint32_t  dwMarket;
};

typedef struct exchange_t {
	//市场编号;
	std::uint32_t  dwMarket;
	//交易所ID;
	char ExchangeID[32];
	//交易所名字;
	char ExchangeName[32];
	//交易所属性;
	int ExchangeProperty;

	//其他时间段;
	//日盘时间;
	time_t DayTimeSegments[4][2];
	//夜盘时间;
	time_t NightTimeSegments[2][2];
}EXCHANGE, *PEXCHANGE;
#endif