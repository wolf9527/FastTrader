#include "stdafx.h"
#include "BOLLStrategy.h"
#include "../ConfigLib/ConfigReader.h"
#include "../Log/logging.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/InstrumentData.h"
#include "../FacilityBaseLib/Express.h"
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/archive/tmpdir.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
#include <fstream>



BOLLStrategy::BOLLStrategy(const std::string& id)
	:IStrategy(id)
{
	LOGINIT("BOLLStrategy");
}

void BOLLStrategy::Initialize()
{
	//保存报单信息;
	std::ifstream ifs(GetId() + "_BOLL_Params.bin", std::ios::binary);
	if (ifs.is_open())
	{
		try
		{
			boost::archive::binary_iarchive oa(ifs);

			oa >> m_ParamsMap;

		}
		catch (const boost::archive::archive_exception& e)
		{
			LOGDEBUG("Load DualMA_Params Exception:{}", e.what());
		}
		ifs.close();
	}

	ReadConfig();

	//数据存储;
	auto& InstrumentHandsMap = GetInstruments();
	for (auto iIter = InstrumentHandsMap.begin(); iIter != InstrumentHandsMap.end();++iIter)
	{
		BOLL_Params params = m_ParamsMap[iIter->first];

		params.openSignalIndex = -1;
		params.closeSignalIndex = -1;

		params.nHands = GetInstruments()[iIter->first];

		params.pInstrument = boost::make_shared<InstrumentData>(iIter->first.c_str());
		GetDataStoreFace()->PrepareData(params.pInstrument, InstrumentData::dataInfo, params.kType, params.nHistoryKCount);
		//准备历史数据;
		params.nHistoryKCount = 0;

		params.backtraceKLineCount = 1;

		params.lastSignal = ITS_NOTHING;

		std::string startDay = GetStartDay();
		auto start_date = boost::gregorian::date_from_iso_string(startDay);

		params.pInstrument->m_ptBegin = boost::posix_time::ptime(start_date);
		
		int dataCount = GetDataStoreFace()->PrepareData(params.pInstrument, InstrumentData::dataK, params.kType, params.nHistoryKCount);

		LOGDEBUG("Init Load data count={}",dataCount);

		params.pInstIndex = GetDataStoreFace()->PrepareIndexData(params.IndexID, InstrumentData::dataK, params.kType);

		//准备指标;
		kdata_container& min1 = params.pInstrument->GetKData(params.kType);

		params.pBOLL = boost::make_shared<CBOLL>(&min1);

		params.pVolume = boost::make_shared<CVOLUME>(&min1);

		params.pBOLL->m_nMADays = params.nDays;

		//长周期指标;
		params.pTechLong = boost::shared_ptr<technical_indicator>(technical_indicator::create(
			params.nTechIndex,&min1));

		//指数指标;
		if (params.pInstIndex)
		{
			params.pTechIndex = boost::shared_ptr<technical_indicator>(technical_indicator::create(
				params.nTechIndex, &params.pInstIndex->GetKData(params.kType)));
		}
		m_ParamsMap[iIter->first] = params;
	}
}

void BOLLStrategy::Finalize()
{
	//界面退出后停止交易;
	WriteConfig();

	std::ofstream ofs(GetId() + "_BOLL_Params.bin", std::ios::binary);
	if (ofs.is_open())
	{
		try
		{
			boost::archive::binary_oarchive oa(ofs);

			oa << m_ParamsMap;

		}
		catch (const boost::archive::archive_exception& e)
		{
			LOGDEBUG("Save DualMA_Params Exception:{}", e.what());
		}
		ofs.close();
	}

	auto self = shared_from_this();
	//开一个线程存数据;
	for (auto iIter = self->m_ParamsMap.begin(); iIter != self->m_ParamsMap.end(); ++iIter)
	{
		BOLL_Params params = iIter->second;
		if (params.pBOLL)
		{
			self->GetDataStoreFace()->GetDataStore()->StoreKData(*params.pBOLL->get_kdata(), true);
		}
	}
	LOGDEBUG("DataStore ReStore Finished!");
}




void BOLLStrategy::OnOrder(boost::shared_ptr<Order> order)
{
	if (!order)
	{
		return;
	}
	if (order->ErrorID != 0)
	{
		LOGDEBUG("报单回报出错:{}",order->ErrorMsg);
		order->OrderStatus = OST_Canceled;
	}
	if (order->OrderSysID.empty() && order->OrderStatus == OST_Unknown)
	{
		return;
	}
	auto pmIter = m_ParamsMap.find(order->InstrumentID);
	if (pmIter == m_ParamsMap.end())
	{
		LOGDEBUG("Order Error InstrumentID {}",order->InstrumentID);
		return;
	}
	BOLL_Params& params = pmIter->second;
	int nIndex = params.pBOLL->get_kdata()->size() - 1;
	if (order->OrderStatus == OST_Canceled)
	{
		//撤单之后重新报单;
		if (params.openInputOrder && order->OrderRef == params.openInputOrder->OrderRef)
		{
			params.openOrder = nullptr;
			params.openOrderAction = nullptr;
			params.openSignalIndex = nIndex;
			params.openInputOrder = MyOrderInsert(order->InstrumentID,
				order->VolumeTotalOriginal-order->VolumeTraded,
				order->Direction == D_Buy ? params.LastTick->AskPrice[0] : params.LastTick->BidPrice[0],
				order->CombOffsetFlag[0], order->Direction);
		}
		else if (params.closeInputOrder && order->OrderRef == params.closeInputOrder->OrderRef)
		{
			params.closeOrder = nullptr;
			params.closeOrderAction = nullptr;
			params.closeSignalIndex = nIndex;
			params.closeInputOrder = MyOrderInsert(order->InstrumentID,
				order->VolumeTotalOriginal - order->VolumeTraded,
				order->Direction == D_Buy ? params.LastTick->AskPrice[0] : params.LastTick->BidPrice[0],
				order->CombOffsetFlag[0], order->Direction);
		}

		return;
	}
	else
	{
		//更新报单信息;
		if (order->CombOffsetFlag[0] == OF_Open)
		{
			params.openOrder = order;
		}
		else
		{
			params.closeOrder = order;
		}
	}
	
}



void BOLLStrategy::OnTrade(boost::shared_ptr<Trade> trade)
{
	if (!trade)
	{
		return;
	}
	//成交之后将InputOrder重置以便再次开仓;
	if (trade->OffsetFlag == OF_Open)
	{
		//开仓;
		LOGDEBUG("开仓成功");
	}
	else
	{
		//平仓;
		auto pmIter = m_ParamsMap.find(trade->InstrumentID);
		if (pmIter == m_ParamsMap.end())
		{
			LOGDEBUG("Trade Error InstrumentID {}", trade->InstrumentID);
			return;
		}
		BOLL_Params& params = pmIter->second;
		params.openInputOrder = nullptr;
		params.closeInputOrder = nullptr;

		params.openOrder = nullptr;
		params.closeOrder = nullptr;

		params.openSignalIndex = -1;
		params.closeSignalIndex = -1;
		LOGDEBUG("平仓成功");

	}
}



void BOLLStrategy::OnTick(boost::shared_ptr< Tick > tick)
{
	BOLL_Params& params = m_ParamsMap[tick->InstrumentID()];
	params.LastTick = tick;
	auto pInstrunment = params.pInstrument;

	//判断当前有没有挂单没成交的,如果有将这个挂单撤掉;
	if (params.openOrder && params.openOrder->OrderStatus != OST_AllTraded)
	{
		//撤单;
		if (!params.openOrderAction)
		{
			params.openOrderAction = SafeCancelOrder(params.openOrder, params.openInputOrder);
		}
		
		return;
	}

	if (params.closeOrder && params.closeOrder->OrderStatus != OST_AllTraded)
	{
		//撤单;
		if (!params.closeOrderAction)
		{
			params.closeOrderAction=SafeCancelOrder(params.closeOrder, params.closeInputOrder);
		}
		
		return;
	}
	auto pBOLL = params.pBOLL;
	if (pBOLL && pBOLL->get_kdata()->size() > 0)
	{
		int nIndex = pBOLL->get_kdata()->size() - 1;
		
		int sig = pBOLL->signal(nIndex);

		static int nPreIndex = nIndex;
		if (nIndex != nPreIndex)
		{
			LOGDEBUG("sig ={},nIndex={},Price={}", sig, nIndex, (*pBOLL->get_kdata())[nIndex].ClosePrice);
			nPreIndex = nIndex;
		}

		//同一根K线上不能触发2次信号;
		if (/*params.openSignalIndex == nIndex || */ params.closeSignalIndex == nIndex )
		{
			LOGDEBUG("The {}th has open signals,skip it!", params.openSignalIndex);
			return;
		}
		bool bSell = false;
		bool bBuy = false;
		bool bIsTrendDown = false;//是否出现了趋势下跌;
		bool bIsTrendUp = false;//是否出现了趋势上涨;
		int backtraceKLineCount = params.backtraceKLineCount;
		if (sig >= params.nBuyLimit)
		{
			//卖出信号;
			LOGDEBUG("sig > params.nBuyLimit,buy");

			if (nIndex >= backtraceKLineCount)
			{
				//往回看3根K线;
				int buySigCount = 0;
				//成交量在不断的增加吗;
				bool volumeIsInc = true;
				std::vector<std::string> opens, closes;
				for (int i = nIndex-1 ; i >= nIndex-backtraceKLineCount; --i)
				{
					if (pBOLL->signal(i) >= params.nBuyLimit)
					{
						buySigCount++;
					}
					if (pBOLL->get_kdata()->at(i+1).Volume<pBOLL->get_kdata()->at(i).Volume)
					{
						volumeIsInc = false;
					}
					
					double dMA1 = 0, dUp1=0, dDown1 = 0,dMA2 = 0,dUp2 = 0,dDown2 = 0;
					if (pBOLL->calc(&dMA1, &dUp1, &dDown1, i, true) && (pBOLL->calc(&dMA2, &dUp2, &dDown2, i+1, true)))
					{
						//标准差;
						double dS1 = (dUp1 - dMA1) / pBOLL->m_dMultiUp;
						double dS2 = (dUp2- dMA2) / pBOLL->m_dMultiDown;
						if (dS2>dS1)
						{
							//开口;
							opens.push_back("true");
							closes.push_back("false");
						}
						else
						{
							opens.push_back("false");
							closes.push_back("true");
						}
					}
				}
				if (std::find(opens.begin(), opens.end(), "false") == opens.end())
				{
					//趋势行情;
					LOGDEBUG("buy current is a trend,attention please! {}", sig, buySigCount);
				}
				if ((buySigCount >= backtraceKLineCount) && volumeIsInc)
				{
					//前面3根K线都突破,了有可能是一个下跌趋势;
					sig = -sig;
					bIsTrendDown = true;
					bSell = true;
					LOGDEBUG("sig={} < params.nBuyLimit,buy,but maybe a trend,attention please! {}", sig, buySigCount);
				}
				else
				{
					bBuy = true;
				}
			}
		}
		else if (sig <= params.nSellLimit)
		{
			//突破上轨,卖出信号;
			LOGDEBUG("sig={} < params.nSellLimit,sell",sig);
			if (nIndex >= backtraceKLineCount)
			{
				//往回看3根K线;
				int sellSigCount = 0;
				bool volumeIsInc = true;
				std::vector<std::string> opens, closes;
				for (int i = nIndex - 1; i >= nIndex - backtraceKLineCount; --i)
				{
					if (pBOLL->signal(i) <= params.nSellLimit)
					{
						sellSigCount++;
					}
					if (pBOLL->get_kdata()->at(i + 1).Volume < pBOLL->get_kdata()->at(i).Volume)
					{
						volumeIsInc = false;
					}
					double dMA1 = 0, dUp1 = 0, dDown1 = 0, dMA2 = 0, dUp2 = 0, dDown2 = 0;
					if (pBOLL->calc(&dMA1, &dUp1, &dDown1, i, true) && (pBOLL->calc(&dMA2, &dUp2, &dDown2, i + 1, true)))
					{
						//标准差;
						double dS1 = (dUp1 - dMA1) / pBOLL->m_dMultiUp;
						double dS2 = (dUp2 - dMA2) / pBOLL->m_dMultiDown;
						if (dS2 > dS1)
						{
							//开口;
							opens.push_back("true");
							closes.push_back("false");
						}
						else
						{
							opens.push_back("false");
							closes.push_back("true");
						}
					}
				}
				if (std::find(opens.begin(), opens.end(), "false") == opens.end())
				{
					//趋势行情;
					LOGDEBUG("sell current is a trend,attention please! {}", sig, sellSigCount);
				}
				if ((sellSigCount >= backtraceKLineCount) && volumeIsInc)
				{
					//有可能是一个小趋势;
					sig = -sig;
					bIsTrendUp = true;
					bBuy = true;
					LOGDEBUG("sig={} < params.nSellLimit,sell,but maybe a trend,attention please! {}", sig, sellSigCount);
				}
				else
				{
					bSell = true;
				}
			}
		}
		//如果上一次的信号和这一次的信号一样;
		if (sig != params.lastSignal)
		{
			params.lastSignal = sig;
			bBuy = false;
			bSell = false;
		}
		else
		{
			if (sig >= params.nBuyLimit)
			{
				bBuy = true;
				bSell = false;
			}
			else if (sig <=params.nSellLimit)
			{
				bSell = true;
				bBuy = false;
			}
			//将上次的信号清除;
			params.lastSignal = ITS_NOTHING;
		}
		double priceTick = params.pInstrument->GetInstrumentInfo().PriceTick;
		//判断止盈止损;
		if (GetSignedPosition(tick->InstrumentID()) > 0)
		{
			//当前持有多仓;
			double PositionCost = 0;
			if (params.bStopLoss)
			{
				if (GetPositionCost(tick->InstrumentID(), PositionCost))
				{
					if ((PositionCost - tick->LastPrice) >= (params.dStopLossJumps * priceTick) || bIsTrendDown)
					{
						LOGDEBUG("dStopLoss,sell,LastPrice={},PositionCost={},StopLoss={}", tick->LastPrice, PositionCost,
							(params.dStopLossJumps * priceTick));
						bSell = true;
					}
				}
			}
			if (params.bStopProfit)
			{
				if (GetPositionCost(tick->InstrumentID(), PositionCost))
				{
					if ( (tick->LastPrice - PositionCost) >= (params.dStopProfitJumps * priceTick) )
					{
						LOGDEBUG("StopProfit,sell,LastPrice={},PositionCost={},StopProfit={}", tick->LastPrice, PositionCost,
							(params.dStopProfitJumps * priceTick));
						bSell = true;
					}
				}
			}
			LOGDEBUG("LongPosition,LastPrice={},PositionCost={},StopLoss={},StopProfit={}", tick->LastPrice, PositionCost,
				(params.dStopLossJumps*priceTick), (params.dStopProfitJumps*priceTick));
		}


		if (bSell)
		{
			int curPosition = GetSignedPosition(tick->InstrumentID());
			if (curPosition <= 0)
			{
				//当前持仓是空仓;
				int nRestPosition = params.nMaxPositions - abs(curPosition);
				if (nRestPosition <= 0)
				{
					bSell = false;
					LOGDEBUG("max position,not sell");
					return;
				}
			}
			LOGDEBUG("sell it!");
			if (!Sell(tick->InstrumentID(), params.nHands, tick->BidPrice[0],nIndex))
			{
				//报单出错;
				LOGDEBUG("sell error!");
			}
			//return ;
		}

		if (GetSignedPosition(tick->InstrumentID()) < 0)
		{
			double PositionCost = 0;
			//判断止盈止损;
			if (params.bStopLoss)
			{
				if (GetPositionCost(tick->InstrumentID(), PositionCost))
				{
					if ((tick->LastPrice - PositionCost) >= (params.dStopLossJumps * priceTick) || bIsTrendUp)
					{
						LOGDEBUG("StopLoss,buy,LastPrice={},PositionCost={},StopLoss={}", tick->LastPrice, PositionCost,
							(params.dStopLossJumps*priceTick));
						bBuy = true;
					}
				}
			}
			if (params.bStopProfit)
			{
				if (GetPositionCost(tick->InstrumentID(), PositionCost))
				{
					if ((PositionCost- tick->LastPrice) >= (params.dStopProfitJumps * priceTick))
					{
						LOGDEBUG("StopProfit,buy,LastPrice={},PositionCost={},StopProfit={}", tick->LastPrice, PositionCost,
							(params.dStopProfitJumps*priceTick));
						bBuy = true;
					}
				}
			}
			LOGDEBUG("ShortPosition,LastPrice={},PositionCost={},StopLoss={},StopProfit={}", tick->LastPrice, PositionCost,
				(params.dStopLossJumps*priceTick), (params.dStopProfitJumps*priceTick));
		}
		

		if (bBuy)
		{
			//看趋势;
			if (params.pTechLong)
			{
				int nIntensity = params.pTechLong->intensity(nIndex);
				if (!ITS_ISBUY(nIntensity))
				{
					LOGDEBUG("TechLong,not buy");
					bBuy = false;
				}
			}

			//看指数;
			if (params.pTechIndex)
			{
				int nIntensity = params.pTechLong->intensity(nIndex);
				if (!ITS_ISBUY(nIntensity))
				{
					LOGDEBUG("TechIndex,not buy");
					bBuy = false;
				}
			}
		}

		if (bBuy)
		{
			int curPosition = GetSignedPosition(tick->InstrumentID());
			if (curPosition >= 0)
			{
				//当前持仓是空仓;
				int nRestPosition = params.nMaxPositions - abs(curPosition);
				if (nRestPosition <= 0)
				{
					bBuy = false;
					LOGDEBUG("max position,not buy");
					return;
				}
			}
			LOGDEBUG("buy it!");
			if (!Buy(tick->InstrumentID(), params.nHands, tick->AskPrice[0],nIndex))
			{
				//报单出错;
				LOGDEBUG("buy error!");
			}
			else
			{
				params.openSignalIndex = nIndex;
			}
		}
	}
}




bool BOLLStrategy::ReadConfig()
{
	//读取配置文件;
	bool bRet = false;
	TiXmlDocument document("user.xml");
	if (!document.LoadFile())
	{
		return false;
	}

	//获得根元素，即Persons。
	TiXmlElement* root = document.RootElement();
	if (NULL == root)
	{
		return false;
	}

	std::string broker_tag_name = "Strategy";
	TiXmlElement* elem = root->FirstChildElement(broker_tag_name.c_str());
	if (!elem)
	{
		bRet = false;
	}
	while (NULL != elem)
	{
		do
		{
			std::string StrategyID = GetAttribute(elem, "StrategyID");
			const std::string& myId = GetId();
			//std::cout << "策略ID:" << myId << ",配置文件ID" << StrategyID << std::endl;
			if (StrategyID != myId)
			{
				continue;
			}
			std::string mainUserID = GetAttribute(elem, "UserID");
			TiXmlElement* multiuser_elem = elem->FirstChildElement("DualMAParams");
			if (multiuser_elem)
			{
				std::vector<TiXmlElement*> users_elems = GetSubElements(multiuser_elem, "Item");

				for (std::size_t k = 0; k < users_elems.size(); ++k)
				{
					if (users_elems[k])
					{
						BOLL_Params params;
						std::string InstrumentID = GetAttribute(users_elems[k], "InstrumentID");
						params.IndexID = GetAttribute(users_elems[k], "IndexID");
						params.nSellLimit = std::stoi(GetAttribute(users_elems[k], "SellLimit"));
						params.nBuyLimit = std::stoi(GetAttribute(users_elems[k], "BuyLimit"));
						params.nMaxPositions = std::stoi(GetAttribute(users_elems[k], "MaxPosition"));
						params.kType = std::stoi(GetAttribute(users_elems[k], "kType"));
						params.bStopLoss = std::stoi(GetAttribute(users_elems[k], "EnableStopLoss")) != 0;
						params.bStopProfit = std::stoi(GetAttribute(users_elems[k], "EnableStopProfit")) != 0;

						params.dStopLossJumps = std::stoi(GetAttribute(users_elems[k], "StopLossJumps"));
						params.dStopProfitJumps = std::stoi(GetAttribute(users_elems[k], "StopProfitJumps"));
						std::string szAttr = GetAttribute(users_elems[k], "Days");
						int day = std::stoi(szAttr);
						params.nDays = day;
						m_ParamsMap[InstrumentID] = params;
					}
				}
				bRet = true;
			}
		} while (0);
		elem = elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}

bool BOLLStrategy::WriteConfig()
{
	//读取配置文件;
	bool bRet = false;
	TiXmlDocument document("user.xml");
	if (!document.LoadFile())
	{
		return false;
	}

	//获得根元素，即Persons。
	TiXmlElement* root = document.RootElement();
	if (NULL == root)
	{
		return false;
	}

	std::string broker_tag_name = "Strategy";
	TiXmlElement* elem = root->FirstChildElement(broker_tag_name.c_str());
	if (!elem)
	{
		bRet = false;
	}
	while (NULL != elem)
	{
		do
		{
			std::string StrategyID = GetAttribute(elem, "StrategyID");
			const std::string& myId = GetId();
			if (StrategyID != myId)
			{
				continue;
			}
			std::string mainUserID = GetAttribute(elem, "UserID");
			TiXmlElement* multiuser_elem = elem->FirstChildElement("PriceMarginList");
			if (multiuser_elem)
			{
				bRet = true;
				document.SaveFile();
			}
		} while (0);
		if (bRet)
		{
			break;
		}
		elem = elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}

bool BOLLStrategy::Buy(const std::string& InstrumenntID, int Volume, double Price,int kIndex)
{
	//获取当前的持仓;
	BOLL_Params& params = m_ParamsMap[InstrumenntID];
	int curPosition = GetSignedPosition(InstrumenntID);
	if (curPosition < 0)
	{
		//当前是多仓则平仓;
		if (!params.closeInputOrder)
		{
			params.closeSignalIndex = kIndex;
			boost::shared_ptr<InvestorPosition> pPosition = GetPosition(InstrumenntID);
			params.closeInputOrder = ClosePosition(*pPosition);
			return true;
		}
	}
	else
	{
		//开仓;
		if (!params.openInputOrder)
		{
			//开仓;
			params.openSignalIndex = kIndex;
			params.openInputOrder = MyOrderInsert(InstrumenntID, Volume, Price, OF_Open, D_Buy);
			return true;
		}
	}
	LOGDEBUG("Buy failed,Position:{},closeInputOrder:{},openInputOrder:{}", curPosition, params.closeInputOrder == nullptr,
		params.openInputOrder == nullptr);
	return false;
}

bool BOLLStrategy::Sell(const std::string& InstrumenntID, int Volume, double Price,int kIndex)
{
	//获取当前的持仓;
	BOLL_Params& params = m_ParamsMap[InstrumenntID];
	int curPosition = GetSignedPosition(InstrumenntID);
	if (curPosition > 0)
	{
		//当前是多仓则平仓;
		if (!params.closeInputOrder)
		{
			params.closeSignalIndex = kIndex;
			boost::shared_ptr<InvestorPosition> pPosition = GetPosition(InstrumenntID);
			params.closeInputOrder = ClosePosition(*pPosition);
			return true;
		}
	}
	else
	{
		//开仓;
		if (!params.openInputOrder)
		{
			//开仓;
			params.openSignalIndex = kIndex;
			params.openInputOrder = MyOrderInsert(InstrumenntID, Volume, Price, OF_Open, D_Sell);
			return true;
		}
	}
	LOGDEBUG("Sell failed,Position:{},closeInputOrder:{},openInputOrder:{}", curPosition, params.closeInputOrder == nullptr, 
		params.openInputOrder == nullptr);
	return false;
}





BOLLStrategy::~BOLLStrategy()
{

}



boost::shared_ptr<InputOrder> BOLLStrategy::MyOrderInsert(const std::string& instId, int Volume, double Price, 
	EnumOffsetFlag OpenOrClose, EnumDirection BuyOrSell)
{
	boost::shared_ptr<InputOrder> inputOrder = boost::make_shared<InputOrder>();
	inputOrder->InstrumentID = instId;
	inputOrder->VolumeTotalOriginal = Volume;
	inputOrder->LimitPrice = Price;
	inputOrder->MinVolume = Volume;
	inputOrder->CombOffsetFlag[0] = OpenOrClose;
	inputOrder->CombOffsetFlag[1] = OF_None;
	inputOrder->CombOffsetFlag[2] = OF_None;
	inputOrder->CombOffsetFlag[3] = OF_None;
	inputOrder->CombOffsetFlag[4] = OF_None;
	inputOrder->Direction = BuyOrSell;
	inputOrder->OrderPriceType = OPT_LimitPrice;
	inputOrder->CombHedgeFlag[0] = HF_Speculation;
	inputOrder->CombHedgeFlag[1] = HF_None;
	inputOrder->CombHedgeFlag[2] = HF_None;
	inputOrder->CombHedgeFlag[3] = HF_None;
	inputOrder->CombHedgeFlag[4] = HF_None;
	return OrderInsert(inputOrder, GetMainTrader());
}




// boost::shared_ptr<InputOrderAction> DualMAStrategy::SafeCancelOrder(boost::shared_ptr<Order> order)
// {
// 	if (!order)
// 	{
// 		return false;
// 	}
// 	//DEBUG_METHOD();
// 	int frontId = order->FrontID, sessionId = order->SessionID;
// 	auto traderid = GetTraderIDByFronIDSessionID(frontId, sessionId);
// 	if (!traderid)
// 	{
// 		return false;
// 	}
// 	boost::shared_ptr<InputOrderAction> inputOrderAction = boost::make_shared<InputOrderAction>();
// 	inputOrderAction->FrontID = frontId;
// 	inputOrderAction->SessionID = sessionId;
// 	inputOrderAction->OrderRef = order->OrderRef;
// 	inputOrderAction->InvestorID = order->InvestorID;
// 	inputOrderAction->BrokerID = order->BrokerID;
// 	inputOrderAction->InstrumentID = order->InstrumentID;
// 	inputOrderAction->ExchangeID = order->ExchangeID;
// 	inputOrderAction->OrderSysID = order->OrderSysID;
// 	return OrderCancel(inputOrderAction, traderid);
// 
// }

