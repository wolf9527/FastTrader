#pragma once

#include "../SimpleStrategyLib/IStrategy.h"
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread/mutex.hpp>
#include "../TechLib/MA.h"
#include "../TechLib/BOLL.h"
#include "../TechLib/VOLUME.h"
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/shared_ptr.hpp>
//双均线策略;

struct BOLL_Params 
{
	//开平仓报单;
	boost::shared_ptr<InputOrder> openInputOrder;
	boost::shared_ptr<InputOrder> closeInputOrder;

	//报单回报信息;
	boost::shared_ptr<Order> openOrder;
	boost::shared_ptr<Order> closeOrder;

	boost::shared_ptr<InputOrderAction> openOrderAction;
	boost::shared_ptr<InputOrderAction> closeOrderAction;

	boost::shared_ptr<Tick> LastTick;

	boost::shared_ptr<InstrumentData> pInstrument;

	std::string IndexID;

	//触发K线的的信号
	int openSignalIndex;
	int closeSignalIndex;
	int lastSignal;//上一根K线的信号;
	int backtraceKLineCount;

	boost::shared_ptr<InstrumentData> pInstIndex;

	//布林带指标;
	boost::shared_ptr<CBOLL> pBOLL;

	//成交量指标;
	boost::shared_ptr<CVOLUME> pVolume;

	//历史K线数据长度(个数);
	int nHistoryKCount;

	//买入判断时的趋势指标;
	int nTecLong;
	//长周期趋势指标(目前没用);
	boost::shared_ptr<technical_indicator> pTechLong;

	//买入判断时查看品种的指数合约指标;
	int nTechIndex;
	boost::shared_ptr<technical_indicator> pTechIndex;

	//最大持仓数量;
	int nMaxPositions;

	int nSellLimit;//卖出信号的强度;
	int nBuyLimit;//买入信号的强度;
	int kType;

	//止盈和止损;
	bool bStopLoss;
	bool bStopProfit;

	//止盈止损幅度;
	int dStopLossJumps;
	int dStopProfitJumps;

	int nHands;

	uint32_t nDays;
};


namespace boost
{
	namespace serialization
	{
		template<class Archive>
		inline void serialize(Archive& ar, struct BOLL_Params& params, const unsigned int version)
		{
			ar & params.openSignalIndex;
			ar & params.openInputOrder;
			ar & params.closeInputOrder;
			ar & params.openOrder;
			ar & params.closeOrder;
			ar & params.openOrderAction;
			ar & params.closeOrderAction;
		}

		BOOST_SERIALIZATION_SHARED_PTR(BOLL_Params)
	}
}

class BOLLStrategy :public IStrategy, public boost::enable_shared_from_this<BOLLStrategy>
{
public:
	// 标准构造函数;
	BOLLStrategy(const std::string& id);
	std::shared_ptr<std::thread> thrd;
	
	virtual ~BOLLStrategy();
	//初始化函数;
	virtual void Initialize();
	//结束时调用;
	virtual void Finalize();
	//行情触发 ;
	virtual void OnTick(boost::shared_ptr<Tick> tick);
	//报单回报;
	virtual void OnOrder(boost::shared_ptr<Order> order);
	//成交回报;
	virtual void OnTrade(boost::shared_ptr<Trade> trade);
public:
	//买入;
	bool Buy(const std::string& InstrumenntID, int Volume, double Price,int kIndex);
	//卖出;
	bool Sell(const std::string& InstrumenntID,int Volume, double Price,int kIndex);
	

	boost::shared_ptr<InputOrder> MyOrderInsert(const std::string& instId, int Volume, double Price,
		EnumOffsetFlag OpenOrClose, EnumDirection BuyOrSell);
protected:
	//读取配置文件;
	bool ReadConfig();
	bool WriteConfig();
protected:
	//参数;
	std::map<std::string, BOLL_Params> m_ParamsMap;
};