#pragma once

//#include "../MultiPriceMarginStrategy/PriceMarginParams.h"
#include "../SimpleStrategyLib/IStrategy.h"
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/array.hpp>
#include <boost/circular_buffer.hpp>

struct PriceLevelOrder
{
	boost::shared_ptr<InputOrder> openInputOrder;
	boost::shared_ptr<Order> openOrder;
	boost::shared_ptr<Trade> openTrade;
	boost::shared_ptr<InputOrderAction> cancelOpenOrder;

	//平仓;
	boost::shared_ptr<InputOrder> closeInputOrder;
	boost::shared_ptr<InputOrderAction> cancelCloseOrder;
	boost::shared_ptr<Order> closeOrder;
	boost::shared_ptr<Trade> closeTrade;
};

inline bool double_equal(double d1, double d2, double elapse = 0.00005)
{
	return std::abs(d1 - d2) < elapse;
}

struct MakeMarket_Params 
{
	MakeMarket_Params() :BuyOrderQueue()
	{
		priceLevel = 2;
		BuyOrderQueue.resize(priceLevel);
		SellOrderQueue.resize(priceLevel);
	}
	std::string InstrumentID;

	int priceLevel;

	//买单队列 ;
	boost::circular_buffer<PriceLevelOrder> BuyOrderQueue;
	//卖单列列;
	boost::circular_buffer<PriceLevelOrder> SellOrderQueue;
	//平仓单队列;
	boost::circular_buffer<PriceLevelOrder> CloseOrderQueue;

	//平仓单;
	PriceLevelOrder				CloseOrder;
	double priceTick;
	//平仓停止交易时间;
	boost::posix_time::time_duration ClosePositionTime;
	int nHands;
	int cancelCount;
	boost::shared_ptr<Tick> lastTick;
};

class MakeMarketStrategy :public IStrategy, public boost::enable_shared_from_this<MakeMarketStrategy>
{
public:
	// 标准构造函数;
	MakeMarketStrategy(const std::string& id);
	std::shared_ptr<std::thread> thrd;
	
	virtual ~MakeMarketStrategy();
	virtual void Initialize();
	virtual void Finalize();

	virtual void OnTick(boost::shared_ptr<Tick> tick);
	virtual void OnOrder(boost::shared_ptr<Order> order);
	virtual void OnTrade(boost::shared_ptr<Trade> trade);
public:
	//调整报单;
	bool AdjustBuyOrders(MakeMarket_Params& params, boost::shared_ptr<Trade> trade);
	bool AdjustSellOrders(MakeMarket_Params& params, boost::shared_ptr<Trade> trade);
	boost::shared_ptr<InputOrder> MyOrderInsert(const std::string& instId, int Volume, double Price,
		EnumOffsetFlag OpenOrClose, EnumDirection BuyOrSell);
protected:
	//读取配置文件;
	bool ReadConfig();
	bool WriteConfig();
protected:
	//参数;
	std::map<std::string, MakeMarket_Params> m_ParamsMap;
};