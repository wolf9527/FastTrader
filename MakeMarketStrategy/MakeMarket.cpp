// ArbitrageStrategy.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "MakeMarket.h"
#include "MakeMarketStrategy.h"
#include <boost/make_shared.hpp>

boost::shared_ptr<IStrategy>  CreateStrategy(const std::string& id)
{
	return boost::make_shared<MakeMarketStrategy>(id);
}