#include "stdafx.h"
#include "MakeMarketStrategy.h"
#include "../ConfigLib/ConfigReader.h"
#include "../Log/logging.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/InstrumentData.h"
#include "../FacilityBaseLib/Express.h"
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>


MakeMarketStrategy::MakeMarketStrategy(const std::string& id)
	:IStrategy(id)
{
	LOGINIT("SimpleBreakoutStrategy");
}

void MakeMarketStrategy::Initialize()
{
	ReadConfig();

	//数据存储;

	auto& InstrumentHandsMap = GetInstruments();

	for (auto iIter = InstrumentHandsMap.begin(); iIter != InstrumentHandsMap.end(); ++iIter)
	{
		MakeMarket_Params& params = m_ParamsMap[iIter->first];
		params.cancelCount = 0;
		params.nHands = iIter->second;
		InstrumentInfo infoA;
		auto bFindA = InstrumentContainer::get_instance().GetInstrumentInfo(iIter->first.c_str(), &infoA);
		if (bFindA)
		{
			params.priceTick = infoA.PriceTick;
		}
		else
		{
			params.priceTick = 1;
		}
	}
}

void MakeMarketStrategy::Finalize()
{
	//界面退出后停止交易;
	WriteConfig();
}




void MakeMarketStrategy::OnOrder(boost::shared_ptr<Order> order)
{
	if (!order)
	{
		return;
	}
	if (order->ErrorID != 0)
	{
		//LOGDEBUG("报单回报出错:"<<order->ErrorMsg);
	}
	if (order->OrderSysID.empty())
	{
		return;
	}
	//boost::mutex::scoped_lock lck(m_Mutex4Param);
// 	bool bPriceMarginChanged = false;
// 	MakeMarket_Params* param = &m_ParamsMap[order->InstrumentID];
// 	if (order->CombOffsetFlag[0] == OF_Open)
// 	{
// 		//开仓;
// 		if (param->openInputOrder
// 			&& (order->OrderRef == param->openInputOrder->OrderRef)
// 			&& order->InstrumentID == param->InstrumentID)
// 		{
// 			if (order->ErrorID != 0 || order->OrderStatus == EnumOrderStatus::OST_Canceled)
// 			{
// 				param->openInputOrder = nullptr;
// 				param->cancelOpenOrder = nullptr;
// 				param->openOrder = nullptr;
// 				param->cancelCount += 1;
// 				LOGDEBUG("撤开仓单成功...");
// 			}
// 			else
// 			{
// 				param->openOrder = order;
// 				if (param->openInputOrder)
// 				{
// 					param->openInputOrder->OrderSysID
// 						= order->OrderSysID;
// 					param->openInputOrder->InstrumentID
// 						= order->InstrumentID;
// 					param->openInputOrder->ExchangeID
// 						= order->ExchangeID;
// 				}
// 			}
// 
// 			bPriceMarginChanged = true;
// 		}
// 	}
// 	else
// 	{
// 		//平仓的情况;
// 		if (param->closeInputOrder
// 			&& (order->OrderRef == param->closeInputOrder->OrderRef)
// 			&& order->InstrumentID == param->InstrumentID)
// 		{
// 			if (order->ErrorID != 0 || order->OrderStatus == EnumOrderStatus::OST_Canceled)
// 			{
// 				param->closeInputOrder = nullptr;
// 				param->cancelCloseOrder = nullptr;
// 				param->closeOrder = nullptr;
// 				param->cancelCount += 1;
// 				LOGDEBUG("平仓单撤单成功...");
// 			}
// 			else
// 			{
// 				param->closeOrder = order;
// 				param->closeInputOrder->OrderSysID
// 					= order->OrderSysID;
// 				param->closeInputOrder->InstrumentID
// 					= order->InstrumentID;
// 				param->closeInputOrder->ExchangeID
// 					= order->ExchangeID;
// 			}
// 
// 			bPriceMarginChanged = true;
// 		}
// 	}
}



bool MakeMarketStrategy::AdjustSellOrders(MakeMarket_Params& params,boost::shared_ptr<Trade> trade)
{
	std::vector<PriceLevelOrder>	reInputOrders;
	std::vector<PriceLevelOrder>	needCancelOrders(params.SellOrderQueue.begin(), params.SellOrderQueue.end());
	for (size_t i = 0; i < params.SellOrderQueue.size(); ++i)
	{
		//盘口价格;
		double AskPrice = params.lastTick->AskPrice[0] + i*params.priceTick;
		bool bFindPrice = false;
		size_t j = 0;
		for (; j < needCancelOrders.size(); ++j)
		{
			if (needCancelOrders[i].openInputOrder &&
				double_equal(needCancelOrders[i].openInputOrder->LimitPrice, AskPrice))
			{
				bFindPrice = true;
			}
			if (needCancelOrders[i].closeInputOrder &&
				double_equal(needCancelOrders[i].closeInputOrder->LimitPrice, AskPrice))
			{
				bFindPrice = true;
			}
			if (bFindPrice)
			{
				reInputOrders.push_back(needCancelOrders[j]);
				LOGDEBUG("盘口空单价格:{} 已经报单,不需要撤单", AskPrice);
				//已存在,将它从要撤单的列表中删除;
				needCancelOrders.erase(needCancelOrders.begin() + j);
				break;
			}
		}
		if (!bFindPrice)
		{
			//看持仓;
			bFindPrice = double_equal(AskPrice, trade->Price);
			if (!bFindPrice)
			{
				LOGDEBUG("卖价格:{} 需要报单", AskPrice);
				PriceLevelOrder newInputOrder;
				newInputOrder.openInputOrder =
					MyOrderInsert(params.InstrumentID, params.nHands, AskPrice, OF_Open, D_Sell);
				reInputOrders.push_back(newInputOrder);
			}
		}

		//再确定哪些要撤单;
		for (size_t i = 0; i < needCancelOrders.size(); ++i)
		{
			LOGDEBUG("报价 {} 需要撤单", needCancelOrders[i].openInputOrder->LimitPrice);
			needCancelOrders[i].cancelOpenOrder =
				SafeCancelOrder(needCancelOrders[i].openOrder, needCancelOrders[i].openInputOrder);
		}
	}
	for (size_t i = 0; i < params.SellOrderQueue.size(); ++i)
	{
		params.SellOrderQueue[i] = reInputOrders[i];
	}
	return true;
}


bool MakeMarketStrategy::AdjustBuyOrders(MakeMarket_Params& params, boost::shared_ptr<Trade> trade)
{
	std::vector<PriceLevelOrder>	reInputOrders;
	std::vector<PriceLevelOrder>	needCancelOrders(params.BuyOrderQueue.begin(), params.BuyOrderQueue.end());
	for (size_t i = 0; i < params.BuyOrderQueue.size(); ++i)
	{
		//盘口价格;
		double BidPrice = params.lastTick->BidPrice[0] - i*params.priceTick;
		bool bFindPrice = false;
		size_t j = 0;
		for (; j < needCancelOrders.size(); ++j)
		{
			if (needCancelOrders[i].openInputOrder &&
				double_equal(needCancelOrders[i].openInputOrder->LimitPrice, BidPrice))
			{
				bFindPrice = true;
			}
			if (needCancelOrders[i].closeInputOrder &&
				double_equal(needCancelOrders[i].closeInputOrder->LimitPrice, BidPrice))
			{
				bFindPrice = true;
			}
			if (bFindPrice)
			{
				reInputOrders.push_back(needCancelOrders[j]);
				LOGDEBUG("盘口空单价格:{} 已经报单,不需要撤单", BidPrice);
				//已存在,将它从要撤单的列表中删除;
				needCancelOrders.erase(needCancelOrders.begin() + j);

				break;
			}
		}
		if (!bFindPrice)
		{
			//看持仓;
			bFindPrice = double_equal(BidPrice, trade->Price);
			if (!bFindPrice)
			{
				LOGDEBUG("买价格:{} 需要报单", BidPrice);
				PriceLevelOrder newInputOrder;
				newInputOrder.openInputOrder =
					MyOrderInsert(params.InstrumentID, params.nHands, BidPrice, OF_Open, D_Buy);
				reInputOrders.push_back(newInputOrder);
			}
		}

		//再确定哪些要撤单;
		for (size_t i = 0; i < needCancelOrders.size(); ++i)
		{
			LOGDEBUG("报价 {} 需要撤单", needCancelOrders[i].openInputOrder->LimitPrice);
			needCancelOrders[i].cancelOpenOrder =
				SafeCancelOrder(needCancelOrders[i].openOrder, needCancelOrders[i].openInputOrder);
		}
	}
	for (size_t i = 0; i < params.BuyOrderQueue.size(); ++i)
	{
		params.BuyOrderQueue[i] = reInputOrders[i];
	}
	return true;
}




void MakeMarketStrategy::OnTrade(boost::shared_ptr<Trade> trade)
{
	if (!trade)
	{
		return;
	}
	MakeMarket_Params& params = m_ParamsMap[trade->InstrumentID];
	//成交之后将InputOrder重置以便再次开仓;
	if (trade->OffsetFlag == OF_Open)
	{
		//立即以优一跳的价格平仓;
		if (trade->Direction ==  D_Sell)
		{
			//将这个价位的开仓单,撤掉;
			//计算平仓价;
			double closePrice = trade->Price - params.priceTick;
			LOGDEBUG("空单 {} 开仓成功,平仓价 {}", trade->Price,closePrice);
			for (size_t i = 0; i < params.BuyOrderQueue.size();++i)
			{
				if (params.BuyOrderQueue[i].openInputOrder && double_equal(trade->Price,
					params.BuyOrderQueue[i].openInputOrder->LimitPrice))
				{
					LOGDEBUG("空单 {} 开仓成功,立即报平仓单 {}",
						params.BuyOrderQueue[i].openInputOrder->LimitPrice, trade->Price);
					if (!params.BuyOrderQueue[i].closeInputOrder)
					{
						params.BuyOrderQueue[i].closeInputOrder = MyOrderInsert(params.InstrumentID, params.nHands,
							closePrice, OF_CloseToday, D_Buy);
					}
				}
				if (params.BuyOrderQueue[i].openInputOrder && double_equal(closePrice,
					params.BuyOrderQueue[i].openInputOrder->LimitPrice))
				{
					LOGDEBUG("相同价位开仓单立即撤单");
					params.BuyOrderQueue[i].cancelOpenOrder =
						SafeCancelOrder(params.BuyOrderQueue[i].openOrder, params.BuyOrderQueue[i].openInputOrder);
					params.CloseOrder = params.BuyOrderQueue[i];
					params.BuyOrderQueue[i] = PriceLevelOrder();
					break;
				}
			}
			AdjustBuyOrders(params, trade);
			AdjustSellOrders(params, trade);
		}
		else
		{
			//将这个价位的开仓单,撤掉;
			//计算平仓价;
			double closePrice = trade->Price + params.priceTick;
			LOGDEBUG("多单 {} 开仓成功,平仓价 {}", trade->Price, closePrice);
			for (size_t i = 0; i < params.SellOrderQueue.size(); ++i)
			{
				if (params.SellOrderQueue[i].openInputOrder && double_equal(trade->Price,
					params.SellOrderQueue[i].openInputOrder->LimitPrice))
				{
					LOGDEBUG("空单 {} 开仓成功,立即报平仓单 {}",
						params.SellOrderQueue[i].openInputOrder->LimitPrice, trade->Price);
					if (!params.SellOrderQueue[i].closeInputOrder)
					{
						params.SellOrderQueue[i].closeInputOrder = MyOrderInsert(params.InstrumentID, params.nHands,
							closePrice, OF_CloseToday, D_Buy);
					}
				}
				if (params.SellOrderQueue[i].openInputOrder && double_equal(closePrice,
					params.SellOrderQueue[i].openInputOrder->LimitPrice))
				{
					LOGDEBUG("相同价位开仓单立即撤单");
					params.SellOrderQueue[i].cancelOpenOrder =
						SafeCancelOrder(params.SellOrderQueue[i].openOrder, params.SellOrderQueue[i].openInputOrder);
					params.CloseOrder = params.SellOrderQueue[i];
					params.SellOrderQueue[i] = PriceLevelOrder();
					break;
				}
			}
			AdjustBuyOrders(params, trade);
			AdjustSellOrders(params, trade);
		}
	}
	else
	{
		if (trade->Direction ==  D_Buy)
		{
			//平卖方向的报单;
			for (size_t i = 0; i < params.SellOrderQueue.size(); ++i)
			{
				//平仓成交;
				if (params.SellOrderQueue[i].closeInputOrder && params.SellOrderQueue[i].closeInputOrder->OrderRef == trade->OrderRef)
				{

				}
			}
		}
		else
		{
			//平买方向的报单;
			for (size_t i = 0; i < params.BuyOrderQueue.size(); ++i)
			{
				//平仓成交;
				if (params.BuyOrderQueue[i].closeInputOrder && params.BuyOrderQueue[i].closeInputOrder->OrderRef == trade->OrderRef)
				{

				}
			}
		}
	}
}



void MakeMarketStrategy::OnTick(boost::shared_ptr< Tick > tick)
{
	auto& param = m_ParamsMap[tick->InstrumentID()];

	param.lastTick = tick;

	boost::posix_time::ptime tickTime = boost::posix_time::from_time_t(tick->UpdateTime);

	if (tickTime.time_of_day() >= param.ClosePositionTime)
	{
		//平所有仓位不再开;
		LOGDEBUG("开始平仓");
		CloseAllPositions();
		return;
	}

	//盘口价格报单;
	for (size_t i = 0; i < param.BuyOrderQueue.size();++i)
	{
		if (!param.BuyOrderQueue[i].openInputOrder)
		{
			LOGDEBUG("买{}价 {} 做多", i + 1, tick->BidPrice[0] - i*param.priceTick);
			param.BuyOrderQueue[i].openInputOrder = MyOrderInsert(param.InstrumentID, param.nHands, 
				tick->BidPrice[0]-i*param.priceTick,
				OF_Open, D_Buy);
		}
	}
		
	for (size_t i = 0; i < param.SellOrderQueue.size();++i)
	{
		//做空;
		if (!param.SellOrderQueue[i].openInputOrder)
		{
			LOGDEBUG("卖 {} 价 {} 做空",i+1, tick->AskPrice[0] + param.priceTick*i);
			param.SellOrderQueue[i].openInputOrder = MyOrderInsert(param.InstrumentID, param.nHands, tick->AskPrice[0] + i*param.priceTick, OF_Open, D_Sell);
		}
	}
}




bool MakeMarketStrategy::ReadConfig()
{
	//读取配置文件;
	bool bRet = false;
	TiXmlDocument document("user.xml");
	if (!document.LoadFile())
	{
		return false;
	}

	//获得根元素，即Persons。
	TiXmlElement* root = document.RootElement();
	if (NULL == root)
	{
		return false;
	}

	std::string broker_tag_name = "Strategy";
	TiXmlElement* elem = root->FirstChildElement(broker_tag_name.c_str());
	if (!elem)
	{
		bRet = false;
	}
	while (NULL != elem)
	{
		do
		{
			std::string StrategyID = GetAttribute(elem, "StrategyID");
			const std::string& myId = GetId();
			if (StrategyID != myId)
			{
				continue;
			}
			std::string mainUserID = GetAttribute(elem, "UserID");
			TiXmlElement* multiuser_elem = elem->FirstChildElement("MakeMarketList");
			if (multiuser_elem)
			{
				std::vector<TiXmlElement*> users_elems = GetSubElements(multiuser_elem, "Item");

				for (std::size_t k = 0; k < users_elems.size(); ++k)
				{
					if (users_elems[k])
					{
						MakeMarket_Params params;
						params.InstrumentID = GetAttribute(users_elems[k],"InstrumentID");
						params.nHands = GetInstruments()[params.InstrumentID];
						std::string szCloseTime = GetAttribute(users_elems[k], "ClosePositionTime");
						params.ClosePositionTime = boost::posix_time::duration_from_string(szCloseTime);
						m_ParamsMap[params.InstrumentID] = params;
					}
				}
				bRet = true;
			}
		} while (0);
		elem = elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}

bool MakeMarketStrategy::WriteConfig()
{
	//读取配置文件;
	bool bRet = false;
	TiXmlDocument document("user.xml");
	if (!document.LoadFile())
	{
		return false;
	}

	//获得根元素，即Persons。
	TiXmlElement* root = document.RootElement();
	if (NULL == root)
	{
		return false;
	}

	string broker_tag_name = "Strategy";
	TiXmlElement* elem = root->FirstChildElement(broker_tag_name.c_str());
	if (!elem)
	{
		bRet = false;
	}
	while (NULL != elem)
	{
		do
		{
			std::string StrategyID = GetAttribute(elem, "StrategyID");
			const std::string& myId = GetId();
			if (StrategyID != myId)
			{
				continue;
			}
			std::string mainUserID = GetAttribute(elem, "UserID");
			TiXmlElement* multiuser_elem = elem->FirstChildElement("MakeMarketList");
			if (multiuser_elem)
			{
				std::vector<TiXmlElement*> users_elems = GetSubElements(multiuser_elem, "Item");

				for (size_t k = 0; k < users_elems.size(); ++k)
				{
					if (users_elems[k])
					{
						std::string InstrumentID = GetAttribute(users_elems[k],"InstrumentID" );
						MakeMarket_Params& params = m_ParamsMap[InstrumentID];
						SetAttribute(users_elems[k], "InstrumentID", params.InstrumentID);
					}
				}
				bRet = true;
				document.SaveFile();
			}
			
		} while (0);
		if (bRet)
		{
			break;
		}
		elem = elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}



MakeMarketStrategy::~MakeMarketStrategy()
{

}



boost::shared_ptr<InputOrder> MakeMarketStrategy::MyOrderInsert(const std::string& instId, int Volume, double Price, 
	EnumOffsetFlag OpenOrClose, EnumDirection BuyOrSell)
{
	boost::shared_ptr<InputOrder> inputOrder = boost::make_shared<InputOrder>();
	inputOrder->InstrumentID = instId;
	inputOrder->VolumeTotalOriginal = Volume;
	inputOrder->LimitPrice = Price;
	inputOrder->MinVolume = Volume;
	inputOrder->CombOffsetFlag[0] = OpenOrClose;
	inputOrder->CombOffsetFlag[1] = OF_None;
	inputOrder->CombOffsetFlag[2] = OF_None;
	inputOrder->CombOffsetFlag[3] = OF_None;
	inputOrder->CombOffsetFlag[4] = OF_None;
	inputOrder->Direction = BuyOrSell;
	inputOrder->OrderPriceType = OPT_LimitPrice;
	inputOrder->CombHedgeFlag[0] = HF_Speculation;
	inputOrder->CombHedgeFlag[1] = HF_None;
	inputOrder->CombHedgeFlag[2] = HF_None;
	inputOrder->CombHedgeFlag[3] = HF_None;
	inputOrder->CombHedgeFlag[4] = HF_None;
	return OrderInsert(inputOrder, GetMainTrader());
}


// boost::shared_ptr<InputOrderAction> DualMAStrategy::SafeCancelOrder(boost::shared_ptr<Order> order)
// {
// 	if (!order)
// 	{
// 		return false;
// 	}
// 	//DEBUG_METHOD();
// 	int frontId = order->FrontID, sessionId = order->SessionID;
// 	auto traderid = GetTraderIDByFronIDSessionID(frontId, sessionId);
// 	if (!traderid)
// 	{
// 		return false;
// 	}
// 	boost::shared_ptr<InputOrderAction> inputOrderAction = boost::make_shared<InputOrderAction>();
// 	inputOrderAction->FrontID = frontId;
// 	inputOrderAction->SessionID = sessionId;
// 	inputOrderAction->OrderRef = order->OrderRef;
// 	inputOrderAction->InvestorID = order->InvestorID;
// 	inputOrderAction->BrokerID = order->BrokerID;
// 	inputOrderAction->InstrumentID = order->InstrumentID;
// 	inputOrderAction->ExchangeID = order->ExchangeID;
// 	inputOrderAction->OrderSysID = order->OrderSysID;
// 	return OrderCancel(inputOrderAction, traderid);
// 
// }

