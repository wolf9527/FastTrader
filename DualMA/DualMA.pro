TARGET = DualMA
TEMPLATE = lib
CONFIG -= qt
INCLUDEPATH += ../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += ../spdlog/include

DEFINES += _USE_SPDLOG
CONFIG += debug_and_release

linux-g++|macx-g++{
    QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
    QMAKE_CFLAGS = -arch x86_64 -lpthread

    INCLUDEPATH += /home/rmb338/boost_1_64_0
    LIBS += -L/home/rmb338/boost_1_64_0/stage/lib
}

win32{
    DEFINES += WIN32_LEAN_AND_MEAN _CRT_SECURE_NO_WARNINGS
    INCLUDEPATH += F:/boost_1_63_0
    LIBS += -LF:/boost_1_63_0/lib32-msvc-12.0
}

CONFIG(debug, debug|release) {
        DESTDIR = ../build/v120/debug
        LIBS  += -L$$PWD/../build/v120/debug/ -lFacilityBaseLib -lTechLib -lServiceLib -lSimpleStrategyLib -lDataStoreLib
} else {
        DESTDIR = ../build/v120/release
        LIBS = -L$$PWD/../build/v120/release/ -lFacilityBaseLib -lTechLib -lServiceLib -lSimpleStrategyLib -lDataStoreLib
}

HEADERS += \
    DualMAStrategy.h \
    stdafx.h \
    targetver.h

SOURCES += \
    DualMA.cpp \
    DualMAStrategy.cpp \
    stdafx.cpp





