#include "stdafx.h"
#include "DualMAStrategy.h"
#include <boost/make_shared.hpp>
#include "../Log/logging.h"


DualMAStrategy::DualMAStrategy(const std::string& sid)
	:IStrategy(sid)
{
	LOGINIT("DualMAStrategy");
}


DualMAStrategy::~DualMAStrategy()
{
}

void DualMAStrategy::Initialize()
{
	std::map<std::string, int>& InstrumentHandsMap = GetInstruments();
	for (auto iIter = InstrumentHandsMap.begin(); iIter != InstrumentHandsMap.end(); ++iIter)
	{
		DualMA_Params& params = m_ParamsMap[iIter->first];
		params.pInstrument = GetDataStoreFace()->PrepareData(iIter->first, InstrumentData::dataK,
			params.nKtype, params.nHistoryKCount);
		KdataContainer& kdata = params.pInstrument->GetKData(params.nKtype);

		params.pMA = boost::make_shared<CMA>(&kdata);

		//params.pMA->m_nType = CMA::typeSMA;

		params.pMA->m_adwMADays = params.nDays;

	}
}

void DualMAStrategy::OnTick(boost::shared_ptr<Tick> tick)
{
	auto paramIter = m_ParamsMap.find(tick->InstrumentID());
	if (paramIter==m_ParamsMap.end())
	{
		return;
	}
	DualMA_Params& params = paramIter->second;

	if (params.openOrder && params.openOrder->OrderStatus !=  OST_AllTraded)
	{
		if (!params.openInputOrderAction)
		{
			params.openInputOrderAction = SafeCancelOrder(params.openOrder, params.openInputOrder);
		}
	}

	if (params.closeOrder && params.closeOrder->OrderStatus != OST_AllTraded)
	{
		if (!params.closeInputOrderAction)
		{
			params.closeInputOrderAction = SafeCancelOrder(params.closeOrder, params.closeInputOrder);
		}
	}

	auto pMA = params.pMA;
	if (pMA && pMA->get_kdata()->size()>0)
	{
		//当前K线的编号索引;
		int nIndex = pMA->get_kdata()->size() - 1;

		uint32_t szCode = ITSC_NOTHING;
		int sig = pMA->signal(nIndex, &szCode);

		double MA5=0, MA10 = 0;

		pMA->calc(&MA5, nIndex, 5, false);
		pMA->calc(&MA10, nIndex, 10, false);

		LOGDEBUG("nIndex={} LastPrice={} MA5={} MA10={} sig={} szCode={}", nIndex, tick->LastPrice, MA5,MA10,sig,szCode);

		//止损止盈检查;
		int curPosition = GetSignedPosition(tick->InstrumentID());

		bool bSell = false;

		if (sig <= params.nSellLimit)
		{
			bSell = true;
		}

		if (params.nSignalIndex == nIndex)
		{
			//已经出过信号了;
			LOGDEBUG("kIndex={} has signaled",params.nSignalIndex);
			return;
		}

		if (curPosition > 0)
		{
			double PositionCost = 0;
			GetPositionCost(tick->InstrumentID(), PositionCost);
			//当前持有多仓;
			if (params.bStopLoss)
			{
				if (tick->LastPrice < PositionCost*(1 - params.dStopLossPercent))
				{
					//需要止损;
					bSell = true;
				}
			}
			if (params.bStopProfit)
			{
				if (tick->LastPrice>PositionCost*(1+params.dStopProfitPercent))
				{
					bSell = true;
				}
			}
		}

		if (bSell)
		{
			if (curPosition <= 0)
			{
				int nRestPosition = params.nMaxPositions - abs(curPosition);
				if (nRestPosition <= 0)
				{
					bSell = false;
					return;
				}
			}
			Sell(tick->InstrumentID(), GetInstruments()[tick->InstrumentID()], tick->BidPrice[0],nIndex);
		}
		

		bool bBuy = false;

		if (sig >= params.nBuyLimit)
		{
			bBuy = true;
		}

		if (curPosition < 0)
		{
			double PositionCost = 0;
			GetPositionCost(tick->InstrumentID(), PositionCost);
			//当前持有多仓;
			if (params.bStopLoss)
			{
				if (tick->LastPrice > PositionCost*(1 + params.dStopLossPercent))
				{
					//需要止损;
					bSell = true;
				}
			}
			if (params.bStopProfit)
			{
				if (tick->LastPrice < PositionCost*(1 - params.dStopProfitPercent))
				{
					bSell = true;
				}
			}
		}

		if (bBuy)
		{
			if (curPosition >= 0)
			{
				int nRestPosition = params.nMaxPositions - abs(curPosition);
				if (nRestPosition <= 0)
				{
					bBuy = false;
					return;
				}
			}
			Buy(tick->InstrumentID(), GetInstruments()[tick->InstrumentID()], tick->AskPrice[0],nIndex);
		}
	}
}

void DualMAStrategy::OnOrder(boost::shared_ptr<Order> order)
{
	if (order->ErrorID!=0)
	{
		LOGDEBUG("报单回报出错 {}",order->ErrorMsg);
		return;
	}
	auto pmIter = m_ParamsMap.find(order->InstrumentID);
	if (pmIter==m_ParamsMap.end())
	{
		LOGDEBUG("合约出错 {},非本策略报单", order->InstrumentID);
		return ;
	}
	auto& params = pmIter->second;
	if (order->OrderStatus == OST_Canceled)
	{
		if (params.openInputOrder && order->OrderRef == params.openInputOrder->OrderRef)
		{
			params.openInputOrder = nullptr;
			params.openInputOrderAction = nullptr;
			if (order->Direction ==  D_Buy)
			{
				Buy(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->AskPrice[0],params.pMA->get_kdata()->size());
			}
			else
			{
				
				Sell(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->BidPrice[0], params.pMA->get_kdata()->size());
			}
		}
		else if (params.closeInputOrder && order->OrderRef == params.closeInputOrder->OrderRef)
		{
			params.closeInputOrderAction = nullptr;
			params.closeInputOrder = nullptr;
			if (order->Direction == D_Buy)
			{
				Buy(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->AskPrice[0], params.pMA->get_kdata()->size());
			}
			else
			{
				Sell(order->InstrumentID, order->VolumeTotalOriginal - order->VolumeTraded,
					params.pInstrument->GetLastTick()->BidPrice[0], params.pMA->get_kdata()->size());
			}
		}
		else
		{
			LOGDEBUG("OrderRef出错 {},非本策略报单", order->OrderRef);
		}
	}
	else
	{
		if (order->CombOffsetFlag[0]==OF_Open)
		{
			params.openOrder = order;
		}
		else
		{
			params.closeOrder = order;
		}
	}
}

void DualMAStrategy::OnTrade(boost::shared_ptr<Trade> trade)
{
	auto pmIter = m_ParamsMap.find(trade->InstrumentID);
	if (pmIter==m_ParamsMap.end())
	{
		return;
	}
	if (trade->OffsetFlag != OF_Open)
	{
		auto& params = pmIter->second;
		params.openInputOrder = nullptr;
		params.closeInputOrder = nullptr;
		params.openInputOrderAction = nullptr;
		params.closeInputOrderAction = nullptr;
	}
	else
	{

	}
}



void DualMAStrategy::Finalize()
{
	
}

bool DualMAStrategy::Buy(const std::string& InstrumentID, int Volume, double Price, int nKIndex)
{
	DualMA_Params& params = m_ParamsMap[InstrumentID];
	boost::shared_ptr<InvestorPosition> pPosition = GetPosition(InstrumentID);

	if (!pPosition || pPosition->PosiDirection ==  PD_Long)
	{
		if (!params.openInputOrder)
		{
			//开仓;
			params.nSignalIndex = nKIndex;
			params.openInputOrder = OrderInsert(InstrumentID, Volume, Price, OF_Open, D_Buy, GetMainTrader());
			return true;
		}
	}
	else
	{
		if (pPosition || pPosition->PosiDirection ==  PD_Short)
		{
			//平仓;
			if (!params.closeInputOrder)
			{
				params.nSignalIndex = nKIndex;
				params.closeInputOrder = ClosePosition(*pPosition);
				return true;
			}
		}
	}
	return false;
}

bool DualMAStrategy::Sell(const std::string& InstrumentID, int Volume, double Price, int nKIndex)
{
	DualMA_Params& params = m_ParamsMap[InstrumentID];
	boost::shared_ptr<InvestorPosition> pPosition = GetPosition(InstrumentID);

	if (!pPosition || pPosition->PosiDirection == PD_Short)
	{
		if (!params.openInputOrder)
		{
			//开仓;
			params.nSignalIndex = nKIndex;
			params.openInputOrder = OrderInsert(InstrumentID, Volume, Price, OF_Open, D_Sell, GetMainTrader());
			return true;
		}
	}
	else
	{
		if (pPosition || pPosition->PosiDirection == PD_Long)
		{
			//平仓;
			if (!params.closeInputOrder)
			{
				params.nSignalIndex = nKIndex;
				params.closeInputOrder = ClosePosition(*pPosition);
				return true;
			}
		}
	}
	return false;
}


boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid)
{
	return boost::make_shared<DualMAStrategy>(sid);
}