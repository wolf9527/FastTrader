#pragma once

#include "../SimpleStrategyLib/IStrategy.h"

#include "../TechLib/MA.h"
#include "../FacilityBaseLib/InstrumentData.h"
#include "../FacilityBaseLib/Express.h"

struct DualMA_Params
{
	boost::shared_ptr<InputOrder> openInputOrder;
	boost::shared_ptr<InputOrder> closeInputOrder;

	boost::shared_ptr<InputOrderAction> openInputOrderAction;

	boost::shared_ptr<InputOrderAction> closeInputOrderAction;

	boost::shared_ptr<Order> openOrder;
	boost::shared_ptr<Order> closeOrder;

	boost::shared_ptr<CMA> pMA;

	//合约的数据;
	boost::shared_ptr<InstrumentData> pInstrument;

	//要加载多长的历史数据;
	int nHistoryKCount;

	//买卖信号的强度;
	int nBuyLimit;
	int nSellLimit;

	//数据的周期;
	int nKtype;

	bool bStopLoss;//是否止损;
	bool bStopProfit;//止盈和止损;


	double dStopLossPercent;//止损幅度;
	double dStopProfitPercent;//止盈幅度;

	int nMaxPositions;//最大持仓数量;

	int nSignalIndex;

	std::vector<uint32_t> nDays;

	DualMA_Params()
	{
		nSignalIndex = -1;
		nMaxPositions = 1;
		dStopLossPercent = 0.05;
		dStopProfitPercent = 0.05;

		bStopLoss = true;
		bStopProfit = true;

		nBuyLimit = ITS_BUYINTENSE;

		nSellLimit = ITS_SELLINTENSE;

		nKtype = ktypeMin;

		nHistoryKCount = 300000;

		nDays.push_back(5);
		nDays.push_back(10);
	}
};

class DualMAStrategy :
	public IStrategy
{
public:
	explicit DualMAStrategy(const std::string& sid);
	virtual ~DualMAStrategy();

	virtual void OnTick(boost::shared_ptr<Tick> tick) ;

	virtual void OnOrder(boost::shared_ptr<Order> order) ;

	virtual void OnTrade(boost::shared_ptr<Trade> trade) ;

	virtual void Initialize() ;

	virtual void Finalize() ;
public:
	bool Buy(const std::string& InstrumentID,int Volume,double Price,int nKIndex);
	bool Sell(const std::string& InstrumentID, int Volume, double Price, int nKIndex);
protected:
	std::map<std::string, DualMA_Params> m_ParamsMap;
};

