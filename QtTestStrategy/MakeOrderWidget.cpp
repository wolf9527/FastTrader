#include "MakeOrderWidget.h"
#include "QtTestStrategy.h"

MakeOrderWidget::MakeOrderWidget(QWidget *parent)
	: QWidget(parent)
{
	m_pStrategy = NULL;
	ui.setupUi(this);
}

MakeOrderWidget::~MakeOrderWidget()
{
}

void MakeOrderWidget::SetStrategy(QtTestStrategy* pStrategy)
{
	m_pStrategy = pStrategy;
	qRegisterMetaType<boost::shared_ptr<Tick> >("boost::shared_ptr<Tick>");
	connect(m_pStrategy, SIGNAL(ontick(boost::shared_ptr<Tick>)),this, SLOT(ontick(boost::shared_ptr<Tick>)));
	connect(ui.m_pbBuy, SIGNAL(clicked()), SLOT(onBtnBuyClicked()));
	connect(ui.m_pbSell, SIGNAL(clicked()), SLOT(onBtnSellClicked()));
}

void MakeOrderWidget::ontick(boost::shared_ptr<Tick> tick)
{
	if (tick)
	{
		ui.m_lblAskPrice0->setText(QString::number(tick->AskPrice[0]));
		ui.m_lblLastPrice->setText(QString::number(tick->LastPrice));
		ui.m_lblBidPrice0->setText(QString::number(tick->BidPrice[0]));
	}
}

void MakeOrderWidget::onBtnBuyClicked()
{
	if (m_pStrategy)
	{
		m_pStrategy->Buy(ui.m_cmbInstID->currentText().toStdString(),ui.m_spbHands->value(),ui.m_ldtPrice->text().toDouble());
	}
}

void MakeOrderWidget::onBtnSellClicked()
{
	if (m_pStrategy)
	{
		m_pStrategy->Sell(ui.m_cmbInstID->currentText().toStdString(), ui.m_spbHands->value(), ui.m_ldtPrice->text().toDouble());
	}
}
