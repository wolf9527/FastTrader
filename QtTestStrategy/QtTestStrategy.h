#pragma once

#include "qtteststrategy_global.h"
#include <boost/dll/alias.hpp>
#include <boost/dll.hpp>
#include <IStrategy.h>
#include <QObject>

class MakeOrderWidget;
class  QtTestStrategy:public QObject
	,public IStrategy
{
	Q_OBJECT
public:
	explicit QtTestStrategy(const std::string& sid);
	virtual ~QtTestStrategy();

signals:
	void ontick(boost::shared_ptr<Tick> tick);
public:

	virtual void OnTick(boost::shared_ptr<Tick> tick);

	virtual void Initialize();

	virtual void Finalize();

	bool Buy(const std::string& InstrumentID, int Volume, double Price);
	bool Sell(const std::string& InstrumentID, int Volume, double Price);
private:
	//MakeOrderWidget* m_MakeOrderWidget;
};





boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid);
BOOST_DLL_ALIAS(CreateStrategy, create_strategy)
