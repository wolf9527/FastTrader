#include "QtTestStrategy.h"
#include <qt_post.h>
#include <QMetaType>
#include "MakeOrderWidget.h"

QtTestStrategy::QtTestStrategy(const std::string& strId)
	:IStrategy(strId)
{
	//m_MakeOrderWidget = NULL;
}

void QtTestStrategy::OnTick(boost::shared_ptr<Tick> tick)
{
	emit ontick(tick);
}

void QtTestStrategy::Initialize()
{
	//auto self = boost::enable_shared_from_this<QtTestStrategy>::shared_from_this();
	post_ui([this](){
		{
			MakeOrderWidget* w = new MakeOrderWidget();
			
			w->SetStrategy(this);
			w->show();
		}
		//qApp->exec();
	});
}

void QtTestStrategy::Finalize()
{
	
}

QtTestStrategy::~QtTestStrategy()
{

}

bool QtTestStrategy::Buy(const std::string& InstrumentID, int Volume, double Price)
{

	return OrderInsert(InstrumentID, Volume, Price, OF_Open, D_Buy, GetMainTrader())!=nullptr;
}

bool QtTestStrategy::Sell(const std::string& InstrumentID, int Volume, double Price)
{

	return OrderInsert(InstrumentID, Volume, Price, OF_Open, D_Sell, GetMainTrader())!=nullptr;
}



boost::shared_ptr<IStrategy> CreateStrategy(const std::string& sid)
{
	return boost::make_shared<QtTestStrategy>(sid);
}