#pragma once

#include <QWidget>
#include "ui_MakeOrderWidget.h"
#include <boost/shared_ptr.hpp>
#include <Tick.h>

class QtTestStrategy;

class MakeOrderWidget : public QWidget
{
	Q_OBJECT

public:
	MakeOrderWidget(QWidget *parent = Q_NULLPTR);
	~MakeOrderWidget();

	void SetStrategy(QtTestStrategy* pStrategy);
public slots:
	void onBtnBuyClicked();
	void onBtnSellClicked();
	void ontick(boost::shared_ptr<Tick> tick);
private:
	Ui::MakeOrderWidget ui;
	QtTestStrategy* m_pStrategy;
};
