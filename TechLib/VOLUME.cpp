#include "VOLUME.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"


//////////////////////////////////////////////////////////////////////
//	CVOLUME
CVOLUME::CVOLUME( )
{
	SetDefaultParameters( );
}

CVOLUME::CVOLUME( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CVOLUME::~CVOLUME()
{
	clear( );
}

void CVOLUME::SetDefaultParameters( )
{
	m_adwMADays.clear();
	m_adwMADays.push_back( 5 );
	m_adwMADays.push_back( 10 );
	m_itsDeviateOnBottom	=	ITS_BUYINTENSE;
	m_itsDeviateOnTop		=	ITS_SELLINTENSE;
	m_itsLong				=	ITS_BUY;
	m_itsShort				=	ITS_SELL;
}

void CVOLUME::attach( CVOLUME & src )
{
	//m_adwMADays.Copy( src.m_adwMADays );
	//std::copy(m_adwMADays.begin(),m_adwMADays.end(),src.m_adwMADays);
	m_adwMADays=src.m_adwMADays;
	m_itsDeviateOnBottom	=	src.m_itsDeviateOnBottom;
	m_itsDeviateOnTop		=	src.m_itsDeviateOnTop;
	m_itsLong				=	src.m_itsLong;
	m_itsShort				=	src.m_itsShort;
}

bool CVOLUME::IsValidParameters( )
{
	STT_VALID_DAYSARRAY( m_adwMADays );
	return ( VALID_ITS(m_itsDeviateOnBottom) && VALID_ITS(m_itsDeviateOnTop)
		&& VALID_ITS(m_itsLong) && VALID_ITS(m_itsShort) );
}

void CVOLUME::clear( )
{
	TechnicalIndicator::clear( );
}

int CVOLUME::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;
	if( !m_pKData || nIndex < 0 || nIndex >= m_pKData->size() )
		return ITS_NOTHING;

	int	nIntensity	=	GetTrendIntensity( nIndex, m_adwMADays, m_itsLong, m_itsShort, pnCode );
	if( ITS_BUY == nIntensity
		&& m_pKData->IsNewValue( nIndex, false, ITS_DAYS_DEVIATE ) )
	{	// 底背离，股价创新低并且成交量趋势向上
		if( pnCode )	*pnCode	=	ITSC_DEVIATEONBOTTOM;
		return m_itsDeviateOnBottom;
	}
	if( ITS_SELL == nIntensity
		&& m_pKData->IsNewValue( nIndex, true, ITS_DAYS_DEVIATE ) )
	{	// 顶背离，股价创新高并且成交量趋势向下
		if( pnCode )	*pnCode	=	ITSC_DEVIATEONTOP;
		return m_itsDeviateOnTop;
	}

	return nIntensity;
}

bool CVOLUME::min_max_info(size_t nStart, size_t nEnd,
				   double *pdMin, double *pdMax )
{
	STT_ASSERT_GETMINMAXINFO( m_pKData, nStart, nEnd );

	double	dMin	=	0;
	double	dMax	=	1;
	for( size_t k=nStart; k<=nEnd; k++ )
	{
		KDATA	& kd	=	m_pKData->at(k);
		if( dMax < kd.Volume )
			dMax	=	(double)kd.Volume;
	}

	dMax	=	dMax + 1;
	if( dMax - dMin < 3 )
		dMax	=	dMin + 3;

	if( pdMin )		*pdMin	=	dMin;

	if( pdMax )		*pdMax	=	dMax;
	return true;
}

/***
	计算nDays的平均成交量
*/
bool CVOLUME::calc( double * pValue, size_t nIndex, size_t nDays, bool bUseLast )
{
	STT_ASSERT_CALCULATE( m_pKData, nIndex, nDays );

	int	nCount	=	0;
	if( nDays > nIndex+1 )
		return false;
	double	dResult	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		dResult	+=	m_pKData->at(k).Volume;
		nCount	++;
		if( nCount == nDays )
		{
			if( pValue )
				*pValue	=	dResult / nDays;
			return true;
		}
	}
	return false;
}
