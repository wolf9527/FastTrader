#pragma once
#include "Technique.h"
#include <vector>

//	去噪周期摆动量DCYO
class TECH_API CDCYO : public TechnicalIndicator
{
public:
	// Constructors
	CDCYO( );
	CDCYO( KdataContainer * pKData );
	virtual ~CDCYO();

public:
	virtual	void clear( );

	// Attributes
	std::vector<uint32_t>	m_adwMTMDays;
	uint32_t		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CDCYO & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
	virtual	bool	calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast);
};

