#pragma once
#include "TechLib.h"
#include "Technique.h"
#include <PlatformStruct.h>
/***
	除权除息数组类
*/
class TECH_API CDRData
{
public:
// Construction
	CDRData();
	CDRData( const CDRData &src );

// Attributes
	int		GetSize() const;
	int		GetUpperBound() const;
	void	SetSize(int nNewSize, int nGrowBy = -1);

// Operations
	// Clean up
	void	FreeExtra();
	void	RemoveAll();

	// Accessing elements
	DRDATA	GetAt(int nIndex) const;
	void	SetAt(int nIndex, DRDATA newElement);
	DRDATA&	ElementAt(int nIndex);

	// Direct Access to the element data (may return NULL)
	const DRDATA* GetData() const;
	DRDATA* GetData();

	// Potentially growing the array
	void	SetAtGrow(int nIndex, DRDATA newElement);
	int		Add(DRDATA newElement);

	// overloaded operator helpers
	DRDATA	operator[](int nIndex) const;
	DRDATA&	operator[](int nIndex);

	// Operations that move elements around
	void	InsertAt(int nIndex, DRDATA newElement, int nCount = 1);
	void	RemoveAt(int nIndex, int nCount = 1);
	void	InsertAt(int nStartIndex, CDRData* pNewArray);
	int		InsertDRDataSort( DRDATA newElement );
	void	Sort( );
	CDRData	& operator = ( const CDRData &src );
	void	Copy( const CDRData &src );
	bool	IsSameAs( CDRData * psrc );

// Implementation
protected:
	DRDATA* m_pData;   // the actual array of data
	int m_nSize;     // # of elements (upperBound - 1)
	int m_nMaxSize;  // max allocated
	int m_nGrowBy;   // grow amount

public:
	~CDRData();
};

