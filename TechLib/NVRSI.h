#pragma once
#include "VRSI.h"


//	新量相对强弱指标VRSI
class  CNVRSI : public CVRSI
{
public:
	// Constructors
	CNVRSI( );
	CNVRSI( KdataContainer * pKData );
	virtual ~CNVRSI();

public:
	// Operations
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
};


