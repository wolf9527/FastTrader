#ifndef _CV_H_
#define _CV_H_
#include "TechLib.h"
#include "Technique.h"
//	����ָ��CV
class TECH_API  CCV : public TechnicalIndicator
{
public:
	// Constructors
	CCV( );
	CCV( KdataContainer * pKData );
	virtual ~CCV();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nMAHLDays;
	uint32_t		m_nCVDays;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	attach( CCV & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};
#endif