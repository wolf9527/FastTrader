#include "stdafx.h"
#include "HLC.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CHLC
CHLC::CHLC()
{
	SetDefaultParameters();
}

CHLC::CHLC(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CHLC::~CHLC()
{
	clear();
}

void CHLC::SetDefaultParameters()
{
	m_nDays = 12;
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CHLC::AttachParameters(CHLC & src)
{
	m_nDays = src.m_nDays;
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CHLC::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CHLC::clear()
{
	TechnicalIndicator::clear();
}

int CHLC::signal(int nIndex, UINT * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = m_nDays + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE * 3, &dLiminalLow, &dLiminalHigh, 0.4, 0.6))
		return ITS_NOTHING;

	double	dHLC;
	if (!calc(&dHLC, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return ITS_NOTHING;
}

bool CHLC::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
TP = (收盘价+收盘价+最高价+最低价)/4
HLC = N日TP平均值
*/
bool CHLC::calc(double * pValue, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex + 1)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dMATP = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 0; k--)
	{
		KDATA&	kd = m_pKData->at(k);
		double	dTP = (kd.HighestPrice + kd.LowestPrice + kd.ClosePrice * 2) / 4.;
		dMATP += dTP;

		nCount++;
		if (nCount == m_nDays)
		{
			if (pValue)	*pValue = dMATP / m_nDays;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}
	return false;
}

/***
计算HLC及其移动平均值
*/
bool CHLC::calc(double * pValue, double * pMA, int nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}
