#pragma once
#include "TechLib.h"
#include "Technique.h"
//	顺势通道指标CCI
class TECH_API CCCI : public TechnicalIndicator
{
public:
	// Constructors
	CCCI( );
	CCCI( KdataContainer * pKData );
	virtual ~CCCI();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	double	m_dQuotiety;
	uint32_t		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CCCI & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
    virtual	bool	calc( double * pValue, double * pMA, size_t nIndex, bool bUseLast );
};
