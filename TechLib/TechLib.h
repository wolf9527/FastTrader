#ifndef _TECHLIB_H_
#define _TECHLIB_H_

//定义开发平台
//定义生成的DLL文件的名称

//#ifndef	TECHLIB_DLL
//	#if defined(TECHLIB_LIB) 
//		#if defined (_DEBUG) 
//			#pragma comment(linker, "/OUT:\"../Debug/InstrumentTechLib_D_S.dll\"") 
//			#pragma message("Automatically linking with InstrumentTechLib_D_S.dll") 
//		#else 
//			#pragma comment(linker, "/OUT:\"../Release/InstrumentTechLib_S.dll\"") 
//			#pragma message("Automatically linking with InstrumentTechLib_S.dll") 
//		#endif 
//	#elif defined(_DEBUG) 
//		#pragma comment(linker, "/OUT:\"../Debug/InstrumentTechLib_D.dll\"")  
//		#pragma message("Automatically linking with InstrumentTechLib_D.dll") 
//	#else 
//		#pragma comment(linker, "/OUT:\"../Release/InstrumentTechLib.dll\"") 
//		#pragma message("Automatically linking with InstrumentTechLib.dll") 
//	#endif 
//#endif


//定义导出标记
#if defined(WIN32)
	#ifdef TECHLIB_EXPORTS
		#define TECH_API __declspec(dllexport)
		#define EXPIMP_TEMPLATE
	#else
		#define TECH_API __declspec(dllimport)
		#define EXPIMP_TEMPLATE extern
	#endif
#else 
	#define	TECH_API
	#define EXPIMP_TEMPLATE
#endif
// #include <vector>
// using namespace std;
// EXPIMP_TEMPLATE template class TECH_API vector<string>;

#include <complier.h>

#include <PlatformStruct.h>
#pragma pack(1)
typedef struct techuser_info_t{
	int m_nStructSize;
	char m_szName[64];
	char m_szShortName[16];
	int m_nLineCount;
	char m_aszLineName[5][8];
	int m_bAutoMA;
	int m_nMADays;
	int m_itsGoldenFork;
	int m_itsDeadFork;
}TECHUSER_INFO,*PTECHUSER_INFO;


class InstrumentData;
class InstrumentInfo;
class KdataContainer;

typedef struct calculate_info_t{
	int m_nStructSize;
	InstrumentData* m_pInstrument;
	InstrumentInfo* m_pInstrumentInfo;
	KdataContainer* m_pKData;
	int m_nIndex;
	int m_bUseLast;

	double m_dValue1;
	double m_dValue2;
	double m_dValue3;
	double m_dValue4;
	double m_dValue5;
}CALCULATE_INFO,*PCALCULATE_INFO;

#pragma pack()
#endif


