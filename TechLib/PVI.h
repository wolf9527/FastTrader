#pragma once
#include "Technique.h"


//	正成交量指标PVI
class  CPVI : public TechnicalIndicator
{
public:
	// Constructors
	CPVI( );
	CPVI( KdataContainer * pKData );
	virtual ~CPVI();

public:
	virtual	void clear( );

	// Attributes
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CPVI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info( int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, double *pMA, int nIndex, bool bUseLast );
};

