#include "stdafx.h"
#include "VMACD.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CVMACD
CVMACD::CVMACD()
{
	SetDefaultParameters();
}

CVMACD::CVMACD(KdataContainer * pKData)
	: CMACD(pKData)
{
	SetDefaultParameters();
}

CVMACD::~CVMACD()
{
	clear();
}

/***
成交量的MACD
EMA  = 短期移动均值
EMA2 = 长期移动均值
DIF  = 短期移动均值 - 长期移动均值
DEA  = DIF的移动平滑值
柱状线值 = DIF - DEA
*/
bool CVMACD::calc(double *pdEMA1, double *pdEMA2, double *pdDIF, double *pdDEA,
	size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nEMA1Days > nIndex + 1 || m_nEMA2Days > nIndex + 1 || m_nDIFDays > nIndex + 1)
		return false;

	if (load_from_cache(nIndex, pdEMA1, pdEMA2, pdDIF, pdDEA))
		return true;

	// Calculate EMA1, EMA2, DIF, DEA
	double	dEMA1New = 0, dEMA2New = 0, dDIFNew = 0, dDEANew = 0;
	if (bUseLast && pdEMA1 && pdEMA2 && pdDEA)
	{
		dEMA1New = (*pdEMA1)*(m_nEMA1Days - 1) / (m_nEMA1Days + 1) + 2 * m_pKData->at(nIndex).Volume / (m_nEMA1Days + 1);
		dEMA2New = (*pdEMA2)*(m_nEMA2Days - 1) / (m_nEMA2Days + 1) + 2 * m_pKData->at(nIndex).Volume / (m_nEMA2Days + 1);
		dDIFNew = dEMA1New - dEMA2New;
		dDEANew = (*pdDEA)*(m_nDIFDays - 1) / (m_nDIFDays + 1) + 2 * dDIFNew / (m_nDIFDays + 1);
	}
	else
	{
		double	factor1 = 1, factor2 = 1;
		int k;
		for (k = nIndex; k > 0; k--)
		{
			factor1 *= ((double)(m_nEMA1Days - 1)) / (m_nEMA1Days + 1);
			factor2 *= ((double)(m_nEMA2Days - 1)) / (m_nEMA2Days + 1);
			if (factor1 < 0.001 && factor2 < 0.001)	// 太久以前的数据影响很小，忽略不计
				break;
		}
		dEMA1New = m_pKData->at(k).Volume;
		dEMA2New = m_pKData->at(k).Volume;
		dDIFNew = dEMA1New - dEMA2New;
		dDEANew = dDIFNew;
		for (; k <= nIndex; k++)
		{
			dEMA1New = dEMA1New * (m_nEMA1Days - 1) / (m_nEMA1Days + 1) + 2 * m_pKData->at(k).Volume / (m_nEMA1Days + 1);
			dEMA2New = dEMA2New * (m_nEMA2Days - 1) / (m_nEMA2Days + 1) + 2 * m_pKData->at(k).Volume / (m_nEMA2Days + 1);
			dDIFNew = dEMA1New - dEMA2New;
			dDEANew = dDEANew * (m_nDIFDays - 1) / (m_nDIFDays + 1) + 2 * dDIFNew / (m_nDIFDays + 1);
		}
	}

	if (pdEMA1)		*pdEMA1 = dEMA1New;
	if (pdEMA2)		*pdEMA2 = dEMA2New;
	if (pdDIF)			*pdDIF = dDIFNew;
	if (pdDEA)			*pdDEA = dDEANew;
	store_to_cache(nIndex, pdEMA1, pdEMA2, pdDIF, pdDEA);
    return true;
}
