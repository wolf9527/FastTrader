#include "stdafx.h"
#include "MOBV.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CMOBV
CMOBV::CMOBV()
{
	SetDefaultParameters();
}

CMOBV::CMOBV(KdataContainer * pKData)
	: COBV(pKData)
{
	SetDefaultParameters();
}

CMOBV::~CMOBV()
{
	clear();
}

void CMOBV::SetDefaultParameters()
{
	COBV::SetDefaultParameters();
	m_nDays1 = 12;
	m_nDays2 = 26;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CMOBV::AttachParameters(CMOBV & src)
{
	COBV::attach(src);
	m_nDays1 = src.m_nDays1;
	m_nDays2 = src.m_nDays2;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CMOBV::IsValidParameters()
{
	return (COBV::IsValidParameters()
		&& VALID_DAYS(m_nDays1) && VALID_DAYS(m_nDays2)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CMOBV::clear()
{
	TechnicalIndicator::clear();
}

int CMOBV::GetSignal(int nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;
	if (!m_pKData || nIndex < 0 || nIndex >= m_pKData->size())
		return ITS_NOTHING;

	prepare_cache(0, -1, false);

	int	nMaxDays = max(m_nDays1, m_nDays2);
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!IntensityPreparePrice(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.5, 0.5))
		return ITS_NOTHING;

	double	dPriceNow = m_pKData->MaindataAt(nIndex);

	if (dPriceNow < dLiminalLow
		&& (is_golden_fork(nIndex, m_pdCache1, m_pdCache2) || is_golden_fork(nIndex, m_pdCache2, m_pdCache3)))
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dPriceNow > dLiminalHigh
		&& (is_dead_fork(nIndex, m_pdCache1, m_pdCache2) || is_dead_fork(nIndex, m_pdCache2, m_pdCache3)))
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool CMOBV::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo3(nStart, nEnd, pdMin, pdMax, this);
}

/***
*pValue1  = 当日OBV
*pValue2  = m_nDays1日OBV平均值
*pValue3  = m_nDays2日OBV平均值
*/
bool CMOBV::calc(double * pValue1, double * pValue2, double * pValue3, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	int	nMaxDays = max(m_nDays1, m_nDays2);
	if (nMaxDays > nIndex + 1)
		return false;

	if (load_from_cache(nIndex, pValue1, pValue2, pValue3))
		return true;

	double	dOBV = 0, dMOBV1 = 0, dMOBV2 = 0;
	int	nCount = 0;
	bool	bHasLast = bUseLast;
	for (int k = nIndex; k >= 0; k--)
	{
		double	dTemp = 0;
		if (bUseLast && nIndex == k && pValue1)
			dTemp = *pValue1;
		if (COBV::calc(&dTemp, k, bUseLast && nIndex == k && pValue1))
		{
			if (nIndex == k)
				dOBV = dTemp;
			if (nCount < m_nDays1)
				dMOBV1 += dTemp;
			if (nCount < m_nDays2)
				dMOBV2 += dTemp;

			nCount++;
			if (nCount >= m_nDays1 && nCount >= m_nDays2)
			{
				if (pValue1)	*pValue1 = dOBV;
				if (pValue2)	*pValue2 = dMOBV1 / m_nDays1;
				if (pValue3)	*pValue3 = dMOBV2 / m_nDays2;
				store_to_cache(nIndex, pValue1, pValue2, pValue3);
				return true;
			}
		}
	}

	return false;
}
