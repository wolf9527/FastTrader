#include "stdafx.h"
#include "KST.h"
#include "ROC.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CKST
CKST::CKST()
{
	SetDefaultParameters();
}

CKST::CKST(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CKST::~CKST()
{
	clear();
}

void CKST::SetDefaultParameters()
{
	m_adwROCDays.clear();
	m_adwROCDays.push_back(9);
	m_adwROCDays.push_back(12);
	m_adwROCDays.push_back(18);
	m_adwROCDays.push_back(24);
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CKST::AttachParameters(CKST & src)
{
	m_adwROCDays/*.Copy*/=(src.m_adwROCDays);
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CKST::IsValidParameters()
{
	STT_VALID_DAYSARRAY(m_adwROCDays);
	return (VALID_DAYS(m_nMADays) && VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CKST::clear()
{
	TechnicalIndicator::clear();
}

int CKST::signal(int nIndex, UINT * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = AfxGetMaxDays(m_adwROCDays) + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.2, 0.6))
		return ITS_NOTHING;

	if (nIndex <= 1)
		return ITS_NOTHING;

	double	dKST;
	if (!calc(&dKST, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dKST < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dKST > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return ITS_NOTHING;
}

bool CKST::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
9日ROC + 12日ROC×2 + 18日ROC×3 + 24日ROC×4
KST = ------------------------------------------------
10
*/
bool CKST::calc(double * pValue, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	UINT	nMaxDays = AfxGetMaxDays(m_adwROCDays);
	if ((int)nMaxDays > nIndex)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dKST = 0;
	int	nCount = 0;
	for (int m = 0; m<m_adwROCDays.size(); m++)
	{
		CROC	roc(m_pKData);
		roc.m_nDays = m_adwROCDays[m];
		double	dROC = 0;
		if (!roc.calc(&dROC, nIndex, false))
			return false;
		dKST += dROC*(m + 1);
		nCount += (m + 1);
	}

	if (nCount <= 0)
		return false;

	dKST = dKST / nCount;
	if (pValue)
		*pValue = dKST;
	store_to_cache(nIndex, pValue);
	return true;
}

/***
计算KST及其移动平均值
*/
bool CKST::calc(double * pValue, double * pMA, int nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}

