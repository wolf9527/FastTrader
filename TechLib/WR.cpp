#include "WR.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CR
CR::CR( )
{
	SetDefaultParameters( );
}

CR::CR( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CR::~CR()
{
	clear( );
}

void CR::SetDefaultParameters( )
{
	m_nDays		=	20;
}

void CR::attach( CR & src )
{
	m_nDays		=	src.m_nDays;
}

bool CR::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) );
}

void CR::clear( )
{
	TechnicalIndicator::clear( );
}

int CR::signal(size_t nIndex, uint32_t * pnCode)
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;
	/*	double	dR;
	if( !Calculate( &dR, nIndex, false ) )
	return ITS_NOTHING;
	if( dR > 80 )
	{
	if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
	return m_itsSold;
	}
	if( dR < 20 )
	{
	if( pnCode )	*pnCode	=	ITSC_OVERBOUGHT;
	return m_itsBought;
	}
	*/	
	// 无买卖信号
	return	ITS_NOTHING;
}

bool CR::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	if( pdMin )	*pdMin	=	0;
	if( pdMax )	*pdMax	=	100;
	return true;
}

/***
H - C
W%R指标值= ———— ×100
H - L                
H = N日内最高价		L = N日内最低价		C = 当天收盘价
*/
bool CR::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex+1 )
		return false;
	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dH = 0, dL = 0, dR = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		if( nIndex == k )
		{
			dH	=	m_pKData->at(k).HighestPrice;
			dL	=	m_pKData->at(k).LowestPrice;
		}
		if( dH < m_pKData->at(k).HighestPrice )
			dH	=	m_pKData->at(k).HighestPrice;
		if( dL > m_pKData->at(k).LowestPrice )
			dL	=	m_pKData->at(k).LowestPrice;

		nCount	++;
		if( nCount == m_nDays )
		{
			if( dH-dL < 1e-4 )
				dR	=	100;
			else
				dR	=	(dH - m_pKData->at(nIndex).ClosePrice) * 100 / (dH - dL);
			if( pValue )	*pValue	=	dR;
			store_to_cache( nIndex, pValue );
			return true;
		}
	}
	return false;
}
