#include "stdafx.h"
#include "MAOSC.h"

#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CMAOSC
CMAOSC::CMAOSC()
{
	SetDefaultParameters();
}

CMAOSC::CMAOSC(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CMAOSC::~CMAOSC()
{
	Clear();
}

void CMAOSC::SetDefaultParameters()
{
	m_nDays1 = 6;
	m_nDays2 = 12;
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CMAOSC::AttachParameters(CMAOSC & src)
{
	m_nDays1 = src.m_nDays1;
	m_nDays2 = src.m_nDays2;
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CMAOSC::IsValidParameters()
{
	return (VALID_DAYS(m_nDays1) && VALID_DAYS(m_nDays2) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CMAOSC::Clear()
{
	TechnicalIndicator::clear();
}

int CMAOSC::signal(int nIndex, UINT * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = m_nMADays + max(m_nDays1, m_nDays2);
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.4, 0.5))
		return ITS_NOTHING;

	double	dMAOSC;
	if (!calc(&dMAOSC, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dMAOSC < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dMAOSC > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool CMAOSC::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
MAOSC = m_nDays1日收盘价移动平均值 - m_nDays2日收盘价移动平均值
*/
bool CMAOSC::calc(double * pValue, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dMA1 = 0, dMA2 = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 0; k--)
	{
		if (nCount < m_nDays1)
			dMA1 += m_pKData->MaindataAt(k);
		if (nCount < m_nDays2)
			dMA2 += m_pKData->MaindataAt(k);

		nCount++;
		if (nCount >= m_nDays1 && nCount >= m_nDays2)
		{
			if (pValue)
				*pValue = (dMA1 / m_nDays1 - dMA2 / m_nDays2);
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}

/***
计算MAOSC及其移动平均值
*/
bool CMAOSC::calc(double * pValue, double * pMA, int nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}

