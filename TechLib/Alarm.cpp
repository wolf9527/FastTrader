#include	"Alarm.h"
#include "../FacilityBaseLib/Express.h"
#include "../FacilityBaseLib/InstrumentInfo.h"
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
using namespace std;


//////////////////////////////////////////////////////////////////////

string CAlarmCondContainer::AlarmCondToString( ALARMCOND & cond )
{
	char strResult[64]={0};
	if( ALARM_TYPE_DIFFPERCENTMORE == cond.m_type )
	{
		sprintf(strResult,("%s %s > %.2f"), cond.m_szCode, AfxGetVariantName( SLH_DIFFPERCENT, false ).c_str(), cond.m_fValue);
		//strResult.Format( _T("%s %s > %.2f"), cond.m_szCode, AfxGetVariantName( SLH_DIFFPERCENT, false ), cond.m_fValue );
	}
	else if( ALARM_TYPE_DIFFPERCENTLESS == cond.m_type )
	{
		sprintf(strResult,("%s %s < %.2f"), cond.m_szCode, AfxGetVariantName( SLH_DIFFPERCENT, false ).c_str(), cond.m_fValue);
		//strResult.Format( _T("%s %s < %.2f"), cond.m_szCode, AfxGetVariantName( SLH_DIFFPERCENT, false ), cond.m_fValue );
	}
	else if( ALARM_TYPE_TRADEVOLUME == cond.m_type )
	{
		sprintf(strResult,("%s %s > %.2f"), cond.m_szCode, AfxGetVariantName( SLH_VOLUME, false ).c_str(), cond.m_fValue*0.01 );
		//strResult.Format( _T("%s %s > %.2f"), cond.m_szCode, AfxGetVariantName( SLH_VOLUME, false ), cond.m_fValue*0.01 );
	}
	return strResult;
}

string CAlarmContainer::GetDescript( ALARM & alarm )
{
	char strResult[64]={0};
	if( ALARM_TYPE_DIFFPERCENTMORE == alarm.cond.m_type )
	{
		sprintf(strResult,("%s >%.2f"), AfxGetVariantName( SLH_DIFFPERCENT, false ).c_str(), alarm.cond.m_fValue );
		//strResult.Format( _T("%s >%.2f"), AfxGetVariantName( SLH_DIFFPERCENT, false ), alarm.cond.m_fValue );
	}
	else if( ALARM_TYPE_DIFFPERCENTLESS == alarm.cond.m_type )
	{
		sprintf(strResult,("%s <%.2f"), AfxGetVariantName( SLH_DIFFPERCENT, false ).c_str(), alarm.cond.m_fValue );
		//strResult.Format( _T("%s <%.2f"), AfxGetVariantName( SLH_DIFFPERCENT, false ), alarm.cond.m_fValue );
	}
	else if( ALARM_TYPE_TRADEVOLUME == alarm.cond.m_type )
	{
		sprintf(strResult,("%s >%.2f"), AfxGetVariantName( SLH_VOLUME, false ).c_str(), alarm.cond.m_fValue*0.01 );
		//strResult.Format( _T("%s >%.2f"), AfxGetVariantName( SLH_VOLUME, false ), alarm.cond.m_fValue*0.01 );
	}
	return strResult;
}

CAlarmContainer & AfxGetAlarmContainer()
{
	static	CAlarmContainer g_alarm;
	return g_alarm;
}

CAlarmCondContainer& AfxGetAlarmCondContainer()
{
	static CAlarmCondContainer conds;
	return conds;
}

int CAlarmContainer::AddAlarm( ALARMCOND * pcond, Tick * pReport, Tick * pReportLast )
{
	std::unique_lock<std::mutex>	l(m_mutex);

	if( NULL == pcond || NULL == pReport || NULL == pReportLast )
		return -1;

	ALARM a;
	memcpy( &(a.cond), pcond, sizeof(a.cond) );
	memcpy( &(a.report), pReport, sizeof(a.report) );
	a.report.Volume = pReport->Volume - pReportLast->Volume;
	a.report.Turnover = pReport->Turnover - pReportLast->Turnover;
	return Add( a );
}

bool CAlarmContainer::OnReceiveReport( InstrumentInfo * pInfo, Tick * pReport, Tick * pReportLast )
{
	if( NULL == pInfo || NULL == pReport || NULL == pReportLast || !pInfo->is_valid() )
		return false;

	double fDiffPercent;
	double fTradeVolume;

	if( pReport->PreClosePrice <= 1e-4 || pReport->LastPrice < 1e-4 )
		return false;
	fDiffPercent = (float)( 100. * pReport->LastPrice / pReport->PreClosePrice - 100 );
	fTradeVolume = pReport->Volume - pReportLast->Volume;
	if( fTradeVolume <= 0 )
		return false;

	CAlarmCondContainer & conds = AfxGetAlarmCondContainer();
	for( size_t i=0; i<conds.size(); i++ )
	{
		ALARMCOND cond = conds[i];
		if( ALARM_TYPE_DIFFPERCENTMORE == cond.m_type )
		{
			if( fDiffPercent > cond.m_fValue && pInfo->is_equal(cond.m_dwMarket,cond.m_szCode) )
			{
				AddAlarm( &cond, pReport, pReportLast );
				return true;
			}
		}
		else if( ALARM_TYPE_DIFFPERCENTLESS == cond.m_type )
		{
			if( fDiffPercent < cond.m_fValue && pInfo->is_equal(cond.m_dwMarket,cond.m_szCode) )
			{
				AddAlarm( &cond, pReport, pReportLast );
				return true;
			}
		}
		else if( ALARM_TYPE_TRADEVOLUME == cond.m_type )
		{
			if( fTradeVolume > cond.m_fValue && pInfo->is_equal(cond.m_dwMarket,cond.m_szCode) )
			{
				AddAlarm( &cond, pReport, pReportLast );
				return true;
			}
		}
	}
	return false;
}

