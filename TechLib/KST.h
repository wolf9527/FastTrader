#pragma once
#include "Technique.h"


//	ȷȻָ��KST
class TECH_API CKST : public TechnicalIndicator
{
public:
	// Constructors
	CKST( );
	CKST( KdataContainer * pKData );
	virtual ~CKST();

public:
	virtual	void clear( );

	// Attributes
	std::vector<uint32_t>	m_adwROCDays;
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CKST & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
	virtual	bool	calc( double * pValue, double * pMA, int nIndex, bool bUseLast );
};


