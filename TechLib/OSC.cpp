#include "OSC.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	COSC
COSC::COSC( )
{
	SetDefaultParameters( );
}

COSC::COSC( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

COSC::~COSC()
{
	clear( );
}

void COSC::SetDefaultParameters( )
{
	m_nDays		=	10;
	m_nMADays	=	6;
	m_itsGoldenFork	=	ITS_BUY;
	m_itsDeadFork	=	ITS_SELL;
}

void COSC::attach( COSC & src )
{
	m_nDays		=	src.m_nDays;
	m_nMADays	=	src.m_nMADays;
	m_itsGoldenFork	=	src.m_itsGoldenFork;
	m_itsDeadFork	=	src.m_itsDeadFork;
}

bool COSC::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork) );
}

void COSC::clear( )
{
	TechnicalIndicator::clear( );
}

int COSC::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	int	nMaxDays	=	m_nDays+m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.3, 0.6 ) )
		return ITS_NOTHING;

	double	dOSC;
	if( !calc( &dOSC, nIndex, false ) )
		return ITS_NOTHING;

	int	nSignal	=	GetForkSignal( nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode );
	if( dOSC < dLiminalLow && nSignal == m_itsGoldenFork )
	{	// 低位金叉
		if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if( dOSC > dLiminalHigh && nSignal == m_itsDeadFork )
	{	// 高位死叉
		if( pnCode )	*pnCode	=	ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool COSC::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo2( nStart, nEnd, pdMin, pdMax, this );
}

/***
	OSC = 今日收盘价/N日收盘价平均值
*/
bool COSC::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );
	
	if( m_nDays > nIndex+1 )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dCt = m_pKData->MaindataAt(nIndex);
	double	dMA = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		dMA	+=	m_pKData->MaindataAt(k);

		nCount	++;
		if( nCount == m_nDays )
		{
			dMA	=	dMA / m_nDays;
			if( dMA < 1e-4 )
				return false;
			if( pValue )
				*pValue	=	(dCt/dMA);
			store_to_cache( nIndex, pValue );
			return true;
		}
	}
	return false;
}

/***
	计算OSC及其移动平均值
*/
bool COSC::calc( double * pValue, double * pMA, size_t nIndex, bool bUseLast )
{
	return calc_ma( pValue, pMA, nIndex, bUseLast, m_nMADays );
}
