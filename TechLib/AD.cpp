#include "stdafx.h"
#include "AD.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CAD
CAD::CAD()
{
	SetDefaultParameters();
}

CAD::CAD(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CAD::~CAD()
{
	clear();
}

void CAD::SetDefaultParameters()
{
	m_nDays = 8;
}

void CAD::AttachParameters(CAD & src)
{
	m_nDays = src.m_nDays;
}

bool CAD::IsValidParameters()
{
	return (VALID_DAYS(m_nDays));
}

void CAD::clear()
{
	TechnicalIndicator::clear();
}

int CAD::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;
	// 无买卖信号
	return	ITS_NOTHING;
}

bool CAD::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1(nStart, nEnd, pdMin, pdMax, this);
}

/***
A = 当日最高价 - 当日最低价
B = 2 * 当日收盘价 - 当日最高价 - 当日最低价
C = 当日成交量 * B / A
AD = N日内C的总和
*/
bool CAD::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex + 1)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dAD = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 0; k--)
	{
		KDATA&	kd = m_pKData->at(k);

		if (kd.HighestPrice - kd.LowestPrice > 1e-4)
			dAD += kd.Volume * (((double)kd.ClosePrice) - kd.LowestPrice - 
			kd.HighestPrice + kd.ClosePrice) / (((double)kd.HighestPrice) - kd.LowestPrice);

		nCount++;
		if (nCount == m_nDays)
		{
			if (pValue)	*pValue = dAD;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}
