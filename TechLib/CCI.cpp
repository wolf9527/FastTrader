#include "CCI.h"
#include "Tech.h"
#include <math.h>
#include "../FacilityBaseLib/KData.h"


//////////////////////////////////////////////////////////////////////
//	CCCI
CCCI::CCCI( )
{
	SetDefaultParameters( );
}

CCCI::CCCI( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CCCI::~CCCI()
{
	clear( );
}

void CCCI::SetDefaultParameters( )
{
	m_nDays		=	14;
	m_dQuotiety	=	0.015;
	m_nMADays	=	10;

	m_itsGoldenFork	=	ITS_BUY;
	m_itsDeadFork	=	ITS_SELL;
}

void CCCI::attach( CCCI & src )
{
	m_nDays		=	src.m_nDays;
	m_dQuotiety	=	src.m_dQuotiety;
	m_nMADays	=	src.m_nMADays;

	m_itsGoldenFork	=	src.m_itsGoldenFork;
	m_itsDeadFork	=	src.m_itsDeadFork;
}

bool CCCI::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && m_dQuotiety > 0 && VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork) );
}

void CCCI::clear( )
{
	TechnicalIndicator::clear( );
}

int CCCI::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	double	dCCI;
	if( !calc( &dCCI, nIndex, false ) )
		return ITS_NOTHING;

	int	nForkSignal	=	GetForkSignal( nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode );

	if( dCCI < -100 && nForkSignal == m_itsGoldenFork )
	{	// 低位金叉
		if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if( dCCI > 100 && nForkSignal == m_itsDeadFork )
	{	// 高位死叉
		if( pnCode )	*pnCode	=	ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return	ITS_NOTHING;
}

bool CCCI::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo2( nStart, nEnd, pdMin, pdMax, this );
}

/***
TP = 收盘价+收盘价+最高价+最低价
A是TP的N日均值
D是TP与A的离差均值
CCI=(C-D)/(0.015D)
*/
bool CCCI::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex+1 )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dTP = 0, dMATP = 0, dD = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		KDATA	kd	=	m_pKData->at(k);
		double	dTemp	=	(kd.HighestPrice+kd.ClosePrice+kd.ClosePrice+kd.LowestPrice)/4;
		if( nIndex == k )
			dTP	=	dTemp;
		dMATP	+=	dTemp;

		nCount	++;
		if( nCount == m_nDays )
			break;
	}
	dMATP	=	dMATP / m_nDays;

	nCount	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		KDATA	kd	=	m_pKData->at(k);
		double	dTemp	=	(kd.HighestPrice+kd.ClosePrice+kd.ClosePrice+kd.LowestPrice)/4;
		dD		+=	fabs(dTemp-dMATP);

		nCount	++;
		if( nCount == m_nDays )
			break;
	}

	dD	=	dD / m_nDays;
	if( fabs(dD) < 1e-4 )
		return false;
	if( pValue )
		*pValue	=	(dTP - dMATP) / (m_dQuotiety * dD);
	store_to_cache( nIndex, pValue );
	return true;
}

/***
CCI 及其 移动平均值
*/
bool CCCI::calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast )
{
	return calc_ma( pValue, pMA, nIndex, bUseLast, m_nMADays );
}
