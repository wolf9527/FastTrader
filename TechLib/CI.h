#pragma once
#include "Technique.h"


//	�ۻ�ָ��CI
class TECH_API CCI : public TechnicalIndicator
{
public:
	// Constructors
	CCI( );
	CCI( KdataContainer * pKData );
	virtual ~CCI();

public:
	virtual	void clear( );

	// Attributes
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CCI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};

