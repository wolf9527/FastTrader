#ifndef _TECH_CW_H_
#define _TECH_CW_H_
#include "TechLib.h"
#include "Tech.h"
#include "Technique.h"
#include <vector>
//	����ֲ�ͼCW
class InstrumentData;
class TECH_API CCW : public TechnicalIndicator
{
public:
	// Constructors
	CCW( );
	CCW( KdataContainer * pKData );
	virtual ~CCW();

public:
	virtual	void clear( );

	// Attributes
	double	m_dChangeHand;
	virtual	void	SetDefaultParameters( );
	void	attach( CCW & src );
	virtual	bool	IsValidParameters( );

	// Operations
	bool			GetRange( std::size_t & nStart,  std::size_t & nEnd, InstrumentInfo & info );

	bool			min_max_info( std::size_t nStart,  std::size_t nEnd, double dMinPrice, double dMaxPrice, double dStep,
		double *pdMinVolume, double *pdMaxVolume );

	bool			CalculateCW( double *pdVolume,  std::size_t nStart,  std::size_t nEnd, double dPrice, double dStep );

	bool	CalculateCW( std::size_t nStart,  std::size_t nEnd, InstrumentInfo & info, double dStep,
		std::vector<uint32_t> & adwPrice, std::vector<uint32_t> & adwVolume,
		double * pdMinVolume, double * pdMaxVolume, double * pdTotalVolume, double * pdVolPercent );

	bool	CalculateRecentCW( std::size_t nEnd,  std::size_t nDays, InstrumentInfo & info, double dStep,
		std::vector<uint32_t> & adwPrice, std::vector<uint32_t> & adwVolume,
		double * pdMinVolume, double * pdMaxVolume, double * pdTotalVolume, double * pdVolPercent );

	bool	CalculatePastCW( std::size_t nEnd,  std::size_t nDays, InstrumentInfo & info, double dStep,
		std::vector<uint32_t> & adwPrice, std::vector<uint32_t> & adwVolume,
		double * pdMinVolume, double * pdMaxVolume, double * pdTotalVolume, double * pdVolPercent );

	// Stat
	static	bool	StatGainPercent(double *pdGainPercent, std::vector<uint32_t> &adwPrice, std::vector<uint32_t> &adwVolume, double dPriceSel);
	static	bool	StatCostAverage(double *pdCostAve, std::vector<uint32_t> &adwPrice, std::vector<uint32_t> &adwVolume);
	static	bool	StatMass(double *pdLower, double *pdUpper, double *pdMassPrice, std::vector<uint32_t> &adwPrice, std::vector<uint32_t> &adwVolume, double dMassVol);
};

#endif