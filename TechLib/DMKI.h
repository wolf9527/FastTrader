#pragma once
#include "Technique.h"

//	�����˶�ָ��DMKI
class TECH_API CDMKI : public TechnicalIndicator
{
public:
	// Constructors
	CDMKI( );
	CDMKI( KdataContainer * pKData );
	virtual ~CDMKI();

public:
	virtual	void clear( );

	// Attributes
	int		m_nDays;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CDMKI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};


