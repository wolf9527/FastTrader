#ifndef _TECH_BBI_H_
#define _TECH_BBI_H_
#include "TechLib.h"
#include "Technique.h"
//	���ָ��BBI
class TECH_API  CBBI : public TechnicalIndicator
{
public:
	// Constructors
	CBBI( );
	CBBI( KdataContainer * pKData );
	virtual ~CBBI();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nMA1Days;
	uint32_t		m_nMA2Days;
	uint32_t		m_nMA3Days;
	uint32_t		m_nMA4Days;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CBBI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info( int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, int nIndex, bool bUseLast );
};
#endif
