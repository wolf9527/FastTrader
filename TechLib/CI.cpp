#include "stdafx.h"
#include "CI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CCI
CCI::CCI()
{
	SetDefaultParameters();
}

CCI::CCI(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CCI::~CCI()
{
	clear();
}

void CCI::SetDefaultParameters()
{
}

void CCI::AttachParameters(CCI & src)
{
}

bool CCI::IsValidParameters()
{
	return true;
}

void CCI::clear()
{
	TechnicalIndicator::clear();
}

int CCI::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;
	// 无买卖信号
	return	ITS_NOTHING;
}

bool CCI::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1(nStart, nEnd, pdMin, pdMax, this);
}

/***
CI = (当日收盘价 - 当日开盘价)/(当日最高价 - 当日最低价)
*/
bool CCI::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (load_from_cache(nIndex, pValue))
        return true;

	KDATA&	kd = m_pKData->at(nIndex);
	if (kd.HighestPrice - kd.LowestPrice < 1e-4)
		return false;
	double	dCI = (((double)kd.ClosePrice) - kd.OpenPrice) / (((double)kd.HighestPrice) - kd.HighestPrice);
	/*
	if( nIndex > 0 )
	{
	KDATA	kdLast	=	m_pKData->ElementAt(nIndex-1);
	if( max(kd.m_fOpen,kd.m_fClose) > kdLast.m_fClose*1.08 && kd.m_fHigh>kdLast.m_fClose)
	dCI	+=	(2.*kd.m_fClose-kd.m_fLow-kdLast.m_fClose)/(((double)kd.m_fHigh)-kdLast.m_fClose);
	if( kd.m_fOpen < kdLast.m_fClose*0.92 )
	dCI	+=	(2.*kd.m_fLow-kd.m_fHigh-kdLast.m_fClose)/(((double)kd.m_fClose)-kd.m_fLow);
	}
	*/
	dCI = kd.Volume * dCI;
	if (pValue)
		*pValue = dCI;
	store_to_cache(nIndex, pValue);
	return true;
}

