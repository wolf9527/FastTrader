#include "stdafx.h"
#include "ATR.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CATR
CATR::CATR()
{
	SetDefaultParameters();
}

CATR::CATR(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CATR::~CATR()
{
	clear();
}

void CATR::SetDefaultParameters()
{
	m_nDays = 14;
	m_itsSold = ITS_BUY;
	m_itsBought = ITS_SELL;
}

void CATR::AttachParameters(CATR & src)
{
	m_nDays = src.m_nDays;
	m_itsSold = src.m_itsSold;
	m_itsBought = src.m_itsBought;
}

bool CATR::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought));
}

void CATR::clear()
{
	TechnicalIndicator::clear();
}

int CATR::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = m_nDays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh))
		return ITS_NOTHING;

	double	dATR;
	if (!calc(&dATR, nIndex, false))
		return ITS_NOTHING;

	if (dATR > dLiminalHigh)
	{	// 超卖
		if (pnCode)	*pnCode = ITSC_OVERSOLD;
		return m_itsSold;
	}
	if (dATR < dLiminalLow)
	{	// 超买
		if (pnCode)	*pnCode = ITSC_OVERBOUGHT;
		return m_itsBought;
	}
	return	ITS_NOTHING;
}

bool CATR::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1(nStart, nEnd, pdMin, pdMax, this);
}

/***
TR 为以下三者中的最大值
最高价-最低价，(昨日收盘价-今日最高价)的绝对值，昨日收盘价-昨日最低价

ATR = TR的N日平均
*/
bool CATR::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dATR = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 1; k--)
	{
		KDATA&	kd = m_pKData->at(k);
		KDATA&	kdLast = m_pKData->at(k - 1);

		double	dTR = fabs((kd.HighestPrice) - kd.LowestPrice);
		double  dCH = fabs((kdLast.ClosePrice) - kd.HighestPrice);
		if (dCH > dTR)
			dTR = dCH;
		double dCL = fabs((kdLast.ClosePrice) - kdLast.LowestPrice);
		if (dCL > dTR)
			dTR = dCL;

		dATR += dTR;

		nCount++;
		if (nCount == m_nDays)
		{
			if (pValue)	*pValue = dATR / m_nDays;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}
	return false;
}

