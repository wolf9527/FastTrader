#ifndef _TECH_WR_H_
#define _TECH_WR_H_
#include "TechLib.h"
#include "Technique.h"
//	����ָ��R
class TECH_API CR : public TechnicalIndicator
{
public:
	// Constructors
	CR( );
	CR( KdataContainer * pKData );
	virtual ~CR();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	virtual	void	SetDefaultParameters( );
	void	attach( CR & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};
#endif