// TraderMonitor.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
//#include "http/https_monitor.h"
#include <boost/shared_ptr.hpp>
#include <signal.h>
#ifdef PY_CTP_MARKET_EXPORTS
#include <boost/python.hpp>
#endif
//#include "websocket_server.h"
//#include "../RedisStore/RedisStore.h"
#include "../ConfigLib/UserConfigReader.h"
#include "../ConfigLib/BrokerConfigReader.h"
#include "../ConfigLib/DataStoreConfigReader.h"
#include "../Log/logging.h"

#include <boost/function.hpp>
#include <boost/lambda/lambda.hpp>
//#include <boost/date_time/gregorian/greg_duration.hpp>
#include "../ServiceLib/UserApiMgr.h"
#include "../ServiceLib/Market.h"
#include "../ServiceLib/Trader.h"

#include "../DataStoreLib/IDataStore.h"
#include "../DataStoreLib/DataStoreFace.h"
#include "../FacilityBaseLib/Container.h"

std::vector<boost::shared_ptr<DataStoreFace> > DataStoreFacePtrList;

void OnExit(int sig)
{
	std::cout << "exit ...";
	//LOGDEBUG("market_exit");
	//StrategyMgr::GetInstance().Stop();
	//UserApiMgr::GetInstance().Release();
	//DataSaverMgr::GetInstance().Stop();
	for (size_t i = 0; i < DataStoreFacePtrList.size();++i)
	{
		DataStoreFacePtrList[i]->stop();
	}
}

#ifdef _WIN32
#include <windows.h>


BOOL CtrlHandler(DWORD fdwCtrlType)
{
	//DEBUG_METHOD();
	switch (fdwCtrlType)
	{
		// Handle the CTRL-C signal.
	case CTRL_C_EVENT:
		//LOGDEBUG("Ctrl-C event");
		Beep(750, 300);
		OnExit(SIGINT);
		return(TRUE);

		// CTRL-CLOSE: confirm that the user wants to exit.
	case CTRL_CLOSE_EVENT:
		Beep(600, 200);
		//LOGDEBUG("Ctrl-Close event");
		OnExit(SIGABRT);
		return(TRUE);

		// Pass other signals to the next handler.
	case CTRL_BREAK_EVENT:
		Beep(900, 200);
		LOGDEBUG("Ctrl-Break event");
		return FALSE;

	case CTRL_LOGOFF_EVENT:
		Beep(1000, 200);
		LOGDEBUG("Ctrl-Logoff event");
		return FALSE;

	case CTRL_SHUTDOWN_EVENT:
		Beep(750, 500);
		LOGDEBUG("Ctrl-Shutdown event");
		return FALSE;

	default:
		return FALSE;
	}
}
#endif

#ifdef PY_CTP_MARKET_EXPORTS
int market_main()
{
	std::cout << "market_main" << std::endl;
#else
int main(int argc, char* argv[])
{

#ifdef _WIN32
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);
#else
	signal(SIGINT, OnExit);
	signal(SIGTERM, OnExit);
#endif
#endif
	LOGINIT("TraderMonitor");
	
// 	auto beginTime=boost::posix_time::duration_from_string("21:00:00");
// 	auto endTime = boost::posix_time::duration_from_string("02:30:00");
// 	
// 	std::cout << boost::posix_time::to_simple_string(endTime + beginTime +boost::posix_time::hours(24)) << std::endl;
// 	return 0;
	if (!BrokerConfigReader::GetInstance().Load("broker.xml"))
	{
		LOGDEBUG("服务器配置文件加载失败!!!");
		return -1;
	}
	std::unordered_map<std::string, ServerInfo>& brokers = BrokerConfigReader::GetInstance().GetBrokers();
	if (brokers.empty())
	{
		LOGDEBUG("没有要登录的服务器!!!");
		return -1;
	}
	if (!UserConfigReader::GetInstance().Load("user.xml"))
	{
		LOGDEBUG("用户配置文件加载失败!!!");
		return -1;
	}

	std::map<std::string, UserLoginParam>& users = UserConfigReader::GetInstance().GetUsers();
	if (users.empty())
	{
		LOGDEBUG("没有要登录的用户!!!");
		return -1;
	}
	//读取数据存储项;
	if (!DataStoreConfigReader::GetInstance().Load("user.xml"))
	{
		LOGDEBUG("DataStoreConfig File Load Failed!!!");
	}


	//查找登录用户;

	std::map<std::string, boost::shared_ptr<IDataStore> > IDataStoreMap;

	std::map<std::string, DataStoreItem>& dataItems=DataStoreConfigReader::GetInstance().GetDataStores();
	for (auto dIter = dataItems.begin();dIter!=dataItems.end();++dIter)
	{
		std::map<std::string, UserLoginParam>::iterator uiter = users.find(dIter->second.UserID);
		if (uiter == users.end())
		{
			LOGDEBUG("数据存储指定的用户没有找到,忽略该策略{}", dIter->second.UserID);
			continue;
		}
		//添加到要登录的用户列表;
		auto siter = brokers.find(uiter->second.Server);
		if (uiter->second.BrokerID.empty() || siter == brokers.end())
		{
			LOGDEBUG("没有用户:{}要登录的前置服务器，忽略该策略的用户!!!", uiter->second.UserID.c_str());
			continue;
		}
		
		//创建存储对象;
		boost::shared_ptr<IDataStore> ptrDataStore;
		auto istoreIter = IDataStoreMap.find(dIter->first);
		if (istoreIter == IDataStoreMap.end())
		{
			ptrDataStore = IDataStore::CreateStore(
				dIter->second.Path.c_str(), IDataStore::dbtypeSqlite3
				);
			IDataStoreMap[dIter->first]=ptrDataStore;
		}
		else
		{
			ptrDataStore = istoreIter->second;
		}

		boost::shared_ptr<DataStoreFace> m_pDataStoreFace = boost::make_shared<DataStoreFace>();

		DataStoreFacePtrList.push_back(m_pDataStoreFace);

		m_pDataStoreFace->SetDataStore(ptrDataStore);

		//启动服务;
		if (!m_pDataStoreFace->start())
		{
			LOGDEBUG("DataStore Start Failed!");
		}

		//加载合约和交易所;
		ExchangeContainer& exchg_container = ExchangeContainer::get_instance();
		ptrDataStore->LoadExchange(exchg_container);


		InstrumentContainer& insts_container = InstrumentContainer::get_instance();
		ptrDataStore->LoadBasetable(insts_container);

		
		UserApiMgr& uam = UserApiMgr::GetInstance();
		//if (insts_container.size() == 0)
		{
			//需要登录交易接口;
			boost::shared_ptr<Trader> trader_api = uam.CreateTrader(siter->second);
			if (!trader_api)
			{
				LOGDEBUG("Trader Api Create Failed!...");
				continue;
			}

			//启动交易登录;
			//建立连接;
			trader_api->sigInstrumentInfo.connect(boost::bind(&DataStoreFace::UpdateInstrument, m_pDataStoreFace, boost::lambda::_1));
			//trader_api->sigInstrumentStatus.connect(boost::bind(&user_api_mgr::sltInstrumentStatus, this, boost::lambda::_1));
			//trader_api->sigOnOrder.connect(boost::bind(&user_api_mgr::sltOnOrder, this, boost::lambda::_1));
			//trader_api->sigOnOrderAction.connect(boost::bind(&user_api_mgr::sltOnOrderAction, this, boost::lambda::_1));
			//trader_api->sigOnTrade.connect(boost::bind(&user_api_mgr::sltOnTrade, this, boost::lambda::_1));
			trader_api->sigExchangeInfo.connect(boost::bind(&DataStoreFace::UpdateExchange, m_pDataStoreFace, boost::lambda::_1));
			if (!trader_api->Login(uiter->second))
			{
				continue;
			}
			ptrDataStore->StoreBasetable(insts_container);
			ptrDataStore->StoreExchange(exchg_container);
			trader_api->Logout();
		}
		
		//创建行情接口;
		boost::shared_ptr<Market> market_api = uam.CreateMarket(siter->second);
		if (!market_api)
		{
			LOGDEBUG("Market Api Create Failed!...");
			continue;
		}

		market_api->sigOnTick.connect(boost::bind(&DataStoreFace::UpdateTick,m_pDataStoreFace,boost::lambda::_1));
		//market_api->sigOnUserLogin.connect(boost::bind(&DataStoreFace::OnMarketInited, m_pDataStoreFace));
		//启动行情登录;
		if (!market_api->Login(uiter->second))
		{
			continue;
		}
		//加入到管理器里;
		uam.GetMarkets().insert(std::make_pair(market_api->Id(),market_api));
		//所有交易所进行分类;
		for (size_t i = 0; i < exchg_container.size(); i++)
		{
			std::vector<std::string> vec_instruments;
			std::string exchange_id=ExchangeContainer::get_instance()[i]->get_exchange_id();
			if (insts_container.get_names_by_exchange(vec_instruments, exchange_id))
			{
				market_api->SubscribeMarketData(vec_instruments, exchange_id);
			}
		}
	}
#ifndef PY_CTP_MARKET_EXPORTS
	for (size_t i = 0; i < DataStoreFacePtrList.size(); ++i)
	{
		DataStoreFacePtrList[i]->join();
	}

	LOGUNINIT();
#endif
// 	HttpServerStrategy http_server("http_server");
// 	http_server.Start();
// 
// 	try {
// 		websocket_server server_instance;
// 
// 		// Start a thread to run the processing loop
// 		//thread t(bind(&websocket_server::process_messages, &server_instance));
// 
// 		// Run the asio loop with the main thread
// 		server_instance.run(9002);
// 
// 		//t.join();
// 		http_server.Join();
// 	}
// 	catch (websocketpp::exception const & e) {
// 		std::cout << e.what() << std::endl;
// 	}
    return 0;
}

#ifdef PY_CTP_MARKET_EXPORTS

void market_exit()
{
	LOGUNINIT();
	OnExit(SIGINT);
}

BOOST_PYTHON_MODULE(PyCtpMarket)
{
	using namespace boost::python;
	def("market_main", market_main);
	def("market_exit", market_exit);
}



#endif
