#ifndef _FACILITY_BASE_EXCHANGE_CONTAINER_H_
#define _FACILITY_BASE_EXCHANGE_CONTAINER_H_
#include "FacilityBaseLib.h"
#include "Exchange.h"
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include "ExchangeBase.h"

//交易所类;
class FACILITY_API ExchangeContainer:public std::vector<boost::shared_ptr<ExchangeBase> >,public boost::noncopyable
{
public:
	//默认构造函数;
	ExchangeContainer();
	static ExchangeContainer& get_instance();

	boost::shared_ptr<ExchangeBase> find_exchange_by_id(const std::string& exchangeId);

	int get_market_id(const std::string& exchangeid);

	bool update(boost::shared_ptr<exchange_t> exchange_ptr);
	//获取离当前时间最近的开盘时间;
	boost::posix_time::ptime get_next_open_time(const std::string& exchange_id);

	//设置今年的节假日时间;
	void set_holiday_list(const std::vector<boost::gregorian::date>& days );
	//设置交易日;
	void set_trading_day(boost::gregorian::date& trading_day);
	//获取交易日;
	boost::gregorian::date get_trading_day();
	//默认交易所;
	std::string get_default_exchange();
	void set_default_exchange(const std::string& exchange_id);
protected:
	//节假日;
	std::map<std::string, std::vector<boost::gregorian::date> > holidays;
	std::string default_exchange_id;
};
#endif
