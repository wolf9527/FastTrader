﻿#ifndef _FACILITY_INSTRUMENT_H_
#define _FACILITY_INSTRUMENT_H_
#include "InstrumentInfo.h"
#include "KData.h"
#include "BaseData.h"
#include "Report.h"
#include "Minute.h"
//#include "DateTime.h"
#include "DataTypes.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/shared_ptr.hpp>
class IDataStore;

#define STOCK_CODE_HS300		"000300"    //沪深300;
#define	STOCK_CODE_SZZS			"1A0001"	// 上证指数;
#define	STOCK_CODE_SZYBL		"1A0009"	// 上证180指数;
#define	STOCK_CODE_SZAG			"1A0002"	// 上证A股;
#define	STOCK_CODE_SZBG			"1A0003"	// 上证B股;
#define	STOCK_CODE_SZNCZ		 "399001"	// 深证成指;
#define	STOCK_CODE_SZNZZ		 "399106"	// 深证综指;
#define	STOCK_CODE_SZNCFAZ		 "399002"	// 深证成份A指;
#define	STOCK_CODE_SZNCFBZ		 "399003"	// 深证成份B指;
#define	STOCK_CODE_SZNFIRST		"000001"	// 深证第一支股票;
#define	STOCK_CODE_SHAFIRST		"600000"	// 上证第一支股票;
#define	STOCK_CODE_ZLDD			"000000"	// 主力大单;

enum enKtypeDefaultLoadCount
{
	DEFAULT_LOAD_TICK_COUNT = 2400,
	DEFAULT_LOAD_MIN1_COUNT = 4 * 60 * 2,
};

//单个的合约信息,包括合约信息，K线数据;
class FACILITY_API InstrumentData
{
public:
	enum DataType {
		dataUnknown		=	0x00,		// 未知数据;
		dataInfo		=	0x01,		// 合约信息;
		dataK			=	0x02,		// K线数据;
		dataDR			=	0x03,		// 权息资料;
		dataBasetable	=	0x04,		// 财务数据;
		dataBasetext	=	0x05,		// F10资料;
		dataNews		=	0x06,		// 新闻资讯;
		dataReport		=	0x07,		// 行情刷新数据;
		dataMinute		=	0x08,		// 行情分时数据;
		dataOutline		=	0x09,		// 行情额外数据;
		dataCode		=	0x0A,		// 股票代码数组;
		dataMultisort	=	0x0B,		// 综合排名;
		dataDetail		=	0x0C,		// 成交明细;
		dataExchange	=   0x0D,		//交易所信息;
	};
	enum ReportType	{
		reportUnknown	=	0x00,	//	未知;
		reportQuarter	=	0x01,	//	第一季度季报;
		reportMid		=	0x02,	//	中报;
		reportQuarter3	=	0x03,	//	第三季度季报;
		reportAnnals	=	0x04,	//	年报;
	};
	//交易所类型;
	enum StockMarket {
		marketUnknown	=	'UKNW',	// 未知;
		marketSHSE		=	'ESHS',	// 上海证券交易所;
		marketSZSE		=	'ESZS',	// 深圳证券交易所;
		marketCYSE		=	'ESYC',	// 中国大陆创业板市场;
		marketCHNA		=	'ANHC',	// 中国大陆上海或深圳或创业板证券交易所;
		marketHKEX		=	'XEKH',	// 香港联合交易所;
		marketTBSE		=	'ESBT',	// 台北证券交易所;
		marketTKSE		=	'EST',	// 东京证券交易所;
		marketLSE		=	'ESL',	// 伦敦证券交易所;
		marketFLKFSE	=	'FKLF',	// 法兰克福证券交易所;
		marketNYSE		=	'ESYN',	// 纽约证券交易所;
		marketNASDAQ	=	'QDSN',	// 纳斯达克证券交易所;
	};
	enum MultisortType {
		classRise		=	0x01,	// 涨幅;
		classFall		=	0x02,	// 跌幅;
		claddRiseMin5	=	0x04,	// 5分钟涨幅;
		classFallMin5	=	0x08,	// 5分钟跌幅;
		classBSRatioAsc	=	0x10,	// 委比前六;
		classBSRatioDesc=	0x20,	// 委比后六;
		classDiff		=	0x40,	// 震幅;
		classVolRatio	=	0x80,	// 量比;
		classAmount		=	0x100,	// 总金额;
	};
	enum InstrumentType
	{
		//中金;
		typeNone		=	0x00,
		typeIF			=	0x01,//IF指数;
		typeIC			=   0x03,//IC指数;
		typeIH			=   0x04,//IH指数;
		typeTF			=   0x02,//5年国债;
		typeT			=   0x03,//10年国债;
		//上期所;
		typeCU			=	0x04,//铜;
		typeAL			=	0x05,//铝;
		typeZN			=	0x06,//锌;
		typePB			=   0x07,//铅;
		typeRU			=   0x08,//橡胶;
		typeFU			=	0x09,//燃油;
		typeRB			=	0x0A,//螺纹;
		typeWR			=	0x0B,//线材;
		typeAU,
		typeAG,
		typeBU,
		typeHC,
		typeNI,
		typeSN,
		//大连;
		typeA,
		typeB,
		typeC,
		typeM,
		typeP,
		typeL,
		typeV,
		typeJ,
		typeJM,
		typeI,
		typeJD,
		typeFB,
		typeBB,
		typePP,
		typeCS,
		//郑州;
		typePM,
		typeWH,
		typeSR,
		typeCF,
		typeTA,
		typeOL,
		typeRI,
		typeMA,
		typeFG,
		typeRS,
		typeRM,
		typeTC,
		typeJR,
		typeLR,
		typeSF,
		typeSM,
	};
public:
	InstrumentData();
	InstrumentData(const char* insID, const char* exID = nullptr);

	virtual ~InstrumentData();
	static double GetReportFactor(int nReportType);
	void SetInstrumentID(const char* szExID,const char* szID);
	//设置数据操作对象;
	//void SetDataStore(boost::shared_ptr<IDataStore> pDataStore);
	//设置合约信息类;
	bool SetInstrumentInfo(InstrumentInfo* pInfo);
	//获取当月主力合约,交割日之后的合约为下月的了;
	std::string GetMainCode();

	//获取注册的K线数据;
	std::vector<int> GetKTypes();

	const char* GetInstrumentID();
	DWORD GetLatestDate();
	InstrumentInfo& GetInstrumentInfo();
	KdataContainer& GetKData(int ktype);
	KdataContainer& GetKDataMonth();
	KdataContainer& GetKDataWeek();
	KdataContainer& GetKDataDay();
	KdataContainer& GetKDataMin60();
	KdataContainer&	GetKDataMin30( );
	KdataContainer&	GetKDataMin15( );
	KdataContainer&	GetKDataMin5( );
	KdataContainer&    GetKDataMin1();
	ReportContainer& GetReport( );
	MinuteContainer& GetMinute( );
	char* GetBaseTextPtr( );
	int	GetBaseTextLength( );

	// Operations
	void	Clear();
	int		ExtractKData( int nKType, bool bForced );
	int		MergeBaseText( InstrumentData &instrument );
	int		MergeKData(InstrumentData &instrument, int nKType);
	int		MergeDRData(InstrumentData &instrument);
	int		StoreDRData();
	//当月合约，以月份为合约名;
	std::string GetCurMonthCode();

	bool update(const Tick* pTick);
	
	boost::shared_ptr<Tick> GetLastTick();
protected:
	//boost::shared_ptr<IDataStore> m_pDataStoreOperator;
	boost::shared_ptr<InstrumentInfo> m_pInstrumentInfo;
	//各种其他周期的K线数据;
	std::map<int,boost::shared_ptr<KdataContainer> > ktyped_data_containers;
	boost::shared_ptr<ReportContainer>		m_report;
	boost::shared_ptr<MinuteContainer>		m_minute;
	std::string		m_pBaseText;
public:
	//时间区间;
	//DateTime m_rptTimeBegin;
	//DateTime m_rptTimeEnd;
	//时间区间;
	boost::posix_time::ptime m_ptBegin;
	boost::posix_time::ptime m_ptEnd;
};
#endif
