#ifndef _BASE_DATA_H_
#define _BASE_DATA_H_
#include <PlatformStruct.h>
#include "FacilityBaseLib.h"
#include <vector>
#include <boost/shared_ptr.hpp>

class FACILITY_API  BasedataContainer:public std::vector<boost::shared_ptr<basedata_t> >
{
public:
	//通过合约ID取得基本资料;
	int		insert_sort( boost::shared_ptr<BASEDATA> newElement );
	void	sort();
};

#endif
