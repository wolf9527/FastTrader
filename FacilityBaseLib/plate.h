#ifndef _FACILITY_PLATE_H_
#define _FACILITY_PLATE_H_
#include "FacilityBaseLib.h"
#include <string>
#include <vector>
#include <set>
//#include "array_container.h"
/***
	板块类，包含板块名称、板块中的合约代码;
*/
class InstrumentContainer;

//目前板块是通交易所的名称进行分类的,板块;
class FACILITY_API Plate
{
public:
	Plate( );
	Plate( const Plate &src );
	Plate(const std::vector<std::string>& astr);
	virtual	~Plate( );
	void    set_id(const std::string& plate_id);
	std::string get_id() const;
	bool    add(const std::string& id);
	bool	add(const char* szId );
	bool	add_sort(const char* szId );
	bool	remove(const char* szId );
	Plate	& operator = ( const Plate &src );
	void copy(const std::vector<std::string>& instruments);
	int     find(const char* szId);
	std::size_t size();
	void    clear();
	std::string operator [](std::size_t index);
	std::vector<std::string>& get();
	void remove(std::size_t index);
protected:
	std::vector<std::string> m_code_list;
public:
	std::string	m_strName;//板块名字;
	std::string m_strId;//板块ID;
};


//template class FACILITY_API array_container<plate>;
/***
	板块数组类;
*/
class FACILITY_API plate_container:public std::vector<Plate>
{
public:
	plate_container( );
    plate_container( const plate_container & src );
	virtual ~plate_container( );

    plate_container & operator = (const plate_container &src );

	int		add( const char* lpszePlateName );
	int		add( Plate & newPlate );
	int		replace( Plate & newPlate );
	int		replace( plate_container & newdomains );
	bool	remove( const char* lpszDomainName );
	bool	add_code(  const char*  lpszDomainName,  const char*  lpszStockCode );
	bool	add_code(  const char* lpszDomainName, std::vector<std::string> & astr );

	bool	remove( const char* lpszDomainName,  const char*  lpszStockCode );
	bool	remove_plate(  const char*  lpszDomainName );
	bool	get_codes(  const char*  lpszDomain, std::vector<std::string> &astr );
	bool	get_plates( std::vector<std::string> & astr );

	bool	get_all_plate_info( InstrumentContainer * pContainer, DWORD dwDate );

	bool	load(  const char*  lpszFileName );
	bool	save(  const char*  lpszFileName );

public:
	//所有板块列表;
	static plate_container& get_domain_container();
	//自选股板块;
	static plate_container& get_group_container();
};
#endif
