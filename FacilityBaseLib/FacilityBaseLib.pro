TARGET = FacilityBaseLib
TEMPLATE = lib
INCLUDEPATH += ../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += ../spdlog/include
DEFINES += _USE_SPDLOG
CONFIG -= qt

CONFIG += debug_and_release

DEFINES += FACILITYLIB_EXPORTS

linux-g++{
    QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
    QMAKE_CFLAGS = -arch x86_64 -lpthread -lstdc++11

    INCLUDEPATH += /home/rmb338/boost_1_64_0
    LIBS += -L/home/rmb338/boost_1_64_0/stage/lib
}

win32{
    INCLUDEPATH += F:/boost_1_63_0
    LIBS += -LF:/boost_1_63_0/lib32-msvc-12.0
}

CONFIG(debug, debug|release) {
        DESTDIR = ../build/v120/debug
} else {
        DESTDIR = ../build/v120/release
}

include(./FacilityBaseLib.pri)

