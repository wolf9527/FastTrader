#ifndef _MINUTE_CONTAINER_H_
#define _MINUTE_CONTAINER_H_
#include "FacilityBaseLib.h"
//#include <PlatformStruct.h>
//#include "array_container.h"
class KdataContainer;

//分时行情数组类;
//template class FACILITY_API array_container<minute_t>;

inline bool operator < (const MINUTE& pTemp1, const MINUTE& pTemp2)
{
	if (pTemp1.TradingTime < pTemp2.TradingTime)
		return true;
	else if (pTemp1.TradingTime > pTemp2.TradingTime)
		return false;
	else
	{
		if (pTemp1.TradingMillisec < pTemp2.TradingMillisec)
		{
			return true;
		}
		else if (pTemp1.TradingMillisec > pTemp2.TradingMillisec)
		{
			return false;
		}
		else
		{
			return false;
		}
	}
}


class FACILITY_API MinuteContainer:public std::vector<minute_t>
{
public:
	int		GetUpperBound() const;
	int		InsertMinuteSort(MINUTE& newElement );
	void	RemoveDirty();
	bool	StatVolumeInfo( double *pdVolNow, double *pdVolOuter, double *pdVolInner );	// 统计内盘外盘
	bool	StatDealInfo( std::vector<DWORD> & adwPrice, std::vector<DWORD> & adwVolume, double * pdMaxVolume ); // 统计价量关系，返回值中的价格需乘以0.001
	bool	GetDiffPercentMin5( double * pValue );
	bool	GetLBDKMinMaxInfo( double dVolAverage, double *pdMin, double *pdMax );	// 取得量比多空指标最大值最小值
	bool	GetIndexWave( double *pdWave, size_t nIndex );
	int		ToKData( KdataContainer & kdata );
	void	sort();
	bool	update(const Tick* pReport);
};

#endif
