#ifndef _INSTRUMENT_TECH_LIB_H_
#define _INSTRUMENT_TECH_LIB_H_
//定义开发平台
//定义生成的DLL文件的名称


//定义导出标记
#if defined(_MSC_VER)
	#ifdef FACILITYLIB_EXPORTS
		#define FACILITY_API __declspec(dllexport)
	#else
		#define FACILITY_API __declspec(dllimport)
	#endif
#else 
	#define	FACILITY_API
#endif


#include <complier.h>


#include <PlatformStruct.h>
#ifdef _WIN32
#include <WinSock.h>
#else
#define sprintf_s sprintf
#endif



#endif


