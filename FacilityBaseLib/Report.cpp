#include "Report.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include "KData.h"
//#include "DateTime.h"
#include "../Log/logging.h"
/////////////////////////////////////////////////////////////////////////////

int ReportContainer::InsertReportSort(const Tick& newElement )
{
// 	boost::mutex::scoped_lock lck(m_mutex);
	if( newElement.UpdateTime <= 0 )
		return -1;
	if( newElement.LastPrice < 1e-4 )
		return -1;
	if( newElement.Volume < 1e-4 )
		return -1;
// 	if( !CSPTime::InTradeTime( newElement.m_time, 60 ) )
// 		return -1;
// 
	for( int i=size()-1; i>=0; i-- )
	{
		Tick & temp = at(i);
		if ( (newElement.UpdateTime == temp.UpdateTime) && (newElement.UpdateMillisec == temp.UpdateMillisec) )
		{
			at(i)=newElement;
			return i;
		}
		if( llabs(newElement.Volume - temp.Volume) < 1e-4 )
		{
			at(i)=(newElement);
			return i;
		}
		if ((newElement.UpdateTime > temp.UpdateTime || (newElement.UpdateTime > temp.UpdateTime && newElement.UpdateMillisec > newElement.UpdateMillisec))
			&& newElement.Volume > temp.Volume )
		{
			insert(i+1+begin(),newElement);
			return i+1;
		}
// 		if( newElement.UpdateTime > temp.UpdateTime && newElement.Volume < temp.Volume )
// 			return -1;
// 		if( newElement.UpdateTime < temp.UpdateTime && newElement.Volume > temp.Volume )
// 			return -1;
	}
	
	insert( begin(), newElement );
	return 0;
}


void ReportContainer::sort( )
{
// 	if( m_pData )
// 		qsort( m_pData, size(), sizeof(Tick), SortReport );
	std::sort(begin(), end(), [](const Tick& p1,const Tick& p2)
	{
		if (p1.UpdateTime<p2.UpdateTime)
		{
			return true;
		}
		else if (p1.UpdateTime ==  p2.UpdateTime && p1.UpdateMillisec < p2.UpdateMillisec)
		{
			return true;
		}
		return false;
	});
}

void ReportContainer::RemoveDirty( )
{
// 	for( auto i=rbegin(); i!=rend(); ++i )
// 	{
// 		if( i->UpdateTime <= 0 )
// 			i=erase(i);
// 		else if( i->LastPrice < 1e-4 )
// 			i = erase(i);
// 		else if( i->Volume < 1e-4 )
// 			i = erase(i);
// // 		else if( !CSPTime::InTradeTime( ElementAt(i).m_time, 60 ) )
// // 			RemoveAt(i);
// 		else if( i>0 && i->Volume - std::prev(i)->Volume < 1e-4 )
// 			i = erase(i);
// 	}
}



bool ReportContainer::GetMMLD( std::size_t nIndex, double *pdVolBuy, double *pdVolSell, double * pdVolDiff )
{
	assert( nIndex >= 0 && nIndex < size() );
	if( nIndex < 0 || nIndex > size()-1 )
		return false;

	double	dVolBuy = 0;
	double	dVolSell = 0;
	Tick & rpt = at(nIndex);
	for( int k=0; k<sizeof(rpt.BidVolume)/sizeof(rpt.BidVolume[0]); k++ )
		dVolBuy		+=	rpt.BidVolume[k];
	for( int k=0; k<sizeof(rpt.AskVolume)/sizeof(rpt.AskVolume[0]); k++ )
		dVolSell	+=	rpt.AskVolume[k];

	if( pdVolBuy )	*pdVolBuy	=	dVolBuy;
	if( pdVolSell )	*pdVolSell	=	dVolSell;
	if( pdVolDiff )	*pdVolDiff	=	(dVolBuy-dVolSell);
	return true;
}

bool ReportContainer::GetMMLDMinMaxInfo( double *pdMin, double *pdMax )
{
	double	dMin = 0, dMax = 1;
	for( size_t i=0; i<size(); i++ )
	{
		double	dVolBuy = 0;
		double	dVolSell = 0;
		GetMMLD( i, &dVolBuy, &dVolSell, NULL );

		if( 0 == i )
		{
			dMin	=	dVolBuy;
			dMax	=	dVolBuy;
		}
		if( dVolBuy < dMin )	dMin	=	dVolBuy;
		if( dVolBuy > dMax )	dMax	=	dVolBuy;
		if( dVolSell < dMin )	dMin	=	dVolSell;
		if( dVolSell > dMax )	dMax	=	dVolSell;
	}

	if( dMax < 500 )
		dMax	=	500;	// 最小为5手

	if( pdMin )	*pdMin	=	dMin;
	if( pdMax )	*pdMax	=	dMax;
	return true;
}

bool ReportContainer::StatBuySellEx(double * fSellPrice, double * fSellVolume, double * fBuyPrice, double * fBuyVolume, size_t nSize)
{
	size_t k;
	for( k=0; k<nSize; k++ )
	{
		fSellPrice[k]	=	0.;
		fSellVolume[k]	=	0.;
		fBuyPrice[k]	=	0.;
		fBuyVolume[k]	=	0.;
	}
	std::vector<int>	adwSellPrice, adwSellVolume, adwBuyPrice, adwBuyVolume;
	Tick	rpt;
	memset( &rpt, 0, sizeof(rpt) );
	int	nBSCount	=	sizeof(rpt.BidPrice)/sizeof(float);

	for( k=0; k<size(); k++ )
	{
		Tick	& report	=	at(k);

		// Insert
		for( int i=nBSCount-1; i>=0; i-- )
		{
			if( report.AskPrice[i] > 1e-4 )
			{
				adwSellPrice.insert(adwSellPrice.begin(), DWORD(report.AskPrice[i] * 1000));
				adwSellVolume.insert(adwSellPrice.begin(), DWORD(report.AskVolume[i]));
			}
			if( report.BidPrice[i] > 1e-4 )
			{
				adwBuyPrice.insert(adwSellPrice.begin(), DWORD(report.BidPrice[i] * 1000));
				adwBuyVolume.insert(adwSellPrice.begin(), DWORD(report.BidVolume[i]));
			}
		}

		// Remove 
		DWORD	dwSellMin = 0;
		for (auto iter = adwSellPrice.begin(); iter != adwSellPrice.end(); /*i++*/)
		{

			if( *iter < dwSellMin+1e-4 )
			{
				int idx = iter - adwSellPrice.begin();
				iter = adwSellPrice.erase(iter);

				adwSellVolume.erase(adwSellVolume.begin()+idx);
				
			}
			else
			{
				dwSellMin	=	/*adwSellPrice[i]*/*iter;
				++iter;
			}
		}

		DWORD	dwBuyMax = 0;
		for (auto iter = adwBuyPrice.begin(); iter != adwBuyPrice.end(); /*i++*/)
		{
			if( 0 != dwBuyMax && *iter > dwBuyMax-1e-4 )
			{
				int idx = iter - adwBuyPrice.begin();
				iter = adwBuyPrice.erase(iter);
				adwBuyVolume.erase(adwBuyVolume.begin()+idx);
				
			}
			else
			{
				dwBuyMax	=	*iter;
				++iter;
			}
		}
	}

	// Store
	for( k=0; k<nSize && k<adwSellPrice.size(); k++ )
	{
		fSellPrice[k]	=	float(0.001 * adwSellPrice[k]);
		fSellVolume[k]	=	float(adwSellVolume[k]);
	}
	for( k=0; k<nSize && k<adwBuyPrice.size(); k++ )
	{
		fBuyPrice[k]	=	float(0.001 * adwBuyPrice[k]);
		fBuyVolume[k]	=	float(adwBuyVolume[k]);
	}
	return true;
}


bool ReportContainer::GetIndexWave( double *pdWave, size_t nIndex )
{
	double dWave = 0;
	for( std::size_t k=nIndex-12; k<=nIndex; k++ )
	{
		if( k > 0 && k < size() )
			dWave	+=	at(k).LastPrice - at(k-1).LastPrice;
	}
	if( pdWave )
		*pdWave	=	dWave;
	return true;
}

void ReportContainer::clear()
{
	std::vector<Tick>::clear();
}

int ReportContainer::ToKData( KdataContainer & kdata )
{
	kdata.Clear();
	if( size() <= 0 )
		return 0;
	DWORD dwType = kdata.GetKType();
	//转换成分钟K线数据;
	if( ktypeNone < dwType && dwType <= ktypeMin60 )
	{
		//分钟;
		kdata.reserve( size()/dwType + 1 );

		KDATA	kd;
		memset( &kd, 0, sizeof(kd) );
		Tick & report = at(0);
		
		strcpy(kd.szExchange, report.szExchange);
		time_t kTradingTime = dwType*(report.UpdateTime / dwType+1);
		kd.TradingTime=kTradingTime;
		strcpy( kd.szCode, report.szCode);

		kd.OpenPrice=report.LastPrice;
		kd.HighestPrice=report.LastPrice;
		kd.LowestPrice=report.LastPrice;
		kd.ClosePrice = report.LastPrice;

		kd.Volume = report.Volume;
		kd.Turnover = report.Turnover;
		kd.OpenInterest = report.OpenInterest;
		kd.PreSettlementPrice = report.PreSettlementPrice;
		//交易日;
		strcpy(kd.TradingDay, report.TradingDay);

		for( size_t i=1; i<size(); i++ )
		{
			report = at(i);
			kTradingTime= dwType*(report.UpdateTime/dwType+1);
			if (kTradingTime != kd.TradingTime)
			{
				//新的K线数据!!!
				kd.ClosePrice=at(i-1).LastPrice;
				kd.OpenInterest = at(i - 1).OpenInterest;
				kd.PreSettlementPrice = at(i-1).PreSettlementPrice;
				kdata.push_back(kd);
				kd.TradingTime = kTradingTime;
				kd.OpenPrice = at(i).LastPrice;
				kd.ClosePrice = at(i).LastPrice;
				kd.HighestPrice = at(i).LastPrice;
				kd.LowestPrice = at(i).LastPrice;
				if (std::string(report.TradingDay) != at(i - 1).TradingDay)
				{
					//交易日有所变化;
					kd.Volume = report.Volume;
					kd.Turnover = report.Turnover;
				}
				else
				{
					kd.Volume = report.Volume - at(i - 1).Volume;
					kd.Turnover = report.Turnover - at(i - 1).Turnover;
				}
				strcpy(kd.TradingDay, report.TradingDay);
			}
			else
			{
				if (report.LastPrice>kd.HighestPrice)
				{
					kd.HighestPrice=report.LastPrice;
				}
				if (report.LastPrice<kd.LowestPrice)
				{
					kd.LowestPrice=report.LastPrice;
				}
				kd.Volume += report.Volume - at(i - 1).Volume;
				kd.Turnover += report.Turnover - at(i - 1).Turnover;
			}
			
		}
		LOGDEBUG("conv {} T:{}.{}->K:{}"
			,report.TradingDay,
			report.UpdateTimeStr, report.UpdateMillisec,
			boost::posix_time::to_simple_string(boost::posix_time::from_time_t(kd.TradingTime).time_of_day()));
		kdata.push_back(kd);
		return kdata.size();
	}
	return 0;
}


//基于TICK计算的价差;
ReportContainer operator -(const ReportContainer& l1, const ReportContainer& l2)
{
	ReportContainer result;
	if (l1.size()==0 || l2.size()==0)
	{
		return result;
	}
	size_t l1Index = 0,l2Index=0;
	size_t tNewIndex = 0;
	auto fmt = boost::format("%1%-%2%");
	std::string InstrumentID = boost::str(fmt%l1[l1Index].InstrumentID() % l2[l2Index].InstrumentID());
	Tick tNew;
	memset(&tNew, 0, sizeof(tNew));
	tNew.InstrumentID(InstrumentID);
	std::string szName = boost::str(fmt%l1[l1Index].szName % l2[l2Index].szName);
	strcpy(tNew.szName, szName.c_str());
	while (true)
	{
		if ( (l1Index >= l1.size()-1) || (l2Index >= l2.size()-1))
		{
			break;
		}
		if (l1[l1Index].UpdateTime < l2[l2Index].UpdateTime)
		{
			++l1Index;
			continue;
		}
		else if (l1[l1Index].UpdateTime > l2[l2Index].UpdateTime)
		{
			++l2Index;
			continue;
		}
		else
		{
			//秒时间相等,比较毫秒;
			if (l1[l1Index].UpdateMillisec == l2[l2Index].UpdateMillisec)
			{
				//毫秒相等;
				
				strcpy(tNew.szExchange, l1[l1Index].szExchange);
				tNew.TickIndex = tNewIndex++;
				strcpy(tNew.TradingDay, l1[l1Index].TradingDay);
				tNew.LastPrice = l1[l1Index].LastPrice - l2[l2Index].LastPrice;
				tNew.UpdateTime = l1[l1Index].UpdateTime;
				tNew.UpdateMillisec = l1[l1Index].UpdateMillisec;
				strcpy(tNew.UpdateTimeStr, l1[l1Index].UpdateTimeStr);
				tNew.Volume = l1[l1Index].Volume - l2[l2Index].Volume;
				tNew.Turnover = l1[l1Index].Turnover - l2[l2Index].Volume;
				result.push_back(tNew);

			}
			else
			{
				//毫秒不相等;

			}
			++l1Index;
			++l2Index;
		}
	}
	return result;
}
