#ifndef __SIM_MARKET_H__
#define __SIM_MARKET_H__
#include "ServerInfo.h"
#include "UserInfo.h"
#include "DataTypes.h"
#include <vector>
#include <map>
#include "../ServiceLib/Market.h"

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/thread.hpp>
#include <boost/enable_shared_from_this.hpp>

#ifdef _WIN32
#if _MSC_VER >=1700 
#include <mutex>
#include <condition_variable>
using namespace std;
#else
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
using namespace boost;
#endif
#else
#include <mutex>
#include <condition_variable>
using namespace std;
#endif


class IDataStore;
class InstrumentData;
//行情;
class SimMarket : public Market, public boost::enable_shared_from_this<SimMarket>
{
public:
	//构造函数;
	SimMarket();
	~SimMarket();
	//初始化;
	bool Init(const ServerInfo& s);
	//登录;
	bool Login(const UserLoginParam&  u);
	//设置交易日;
	void SetTradingDay(const std::string& tradingDay);
public:
	virtual void OnFrontConnected();
	virtual void OnRspUserLogin( int nRequestID, bool bIsLast);
	bool ReqUserLogin();
	//确保这2个函数是线程安全的;
	bool SubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg="");
	bool UnSubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg="");
	
	void OnRspSubMarketData(const std::string& subInst, int nRequestID, bool bIsLast);
	void OnRspUnSubMarketData(const std::string& subInst, int nRequestID, bool bIsLast);
	void OnRtnDepthMarketData(boost::shared_ptr<Tick> tick);
protected:
	void OnTakeTickTimer(const boost::system::error_code& ec);
	void TakeTick();
protected:
	SimMarket(const SimMarket& other);
	SimMarket& operator=(const SimMarket& other);
	//行情接口;
protected:
	boost::shared_ptr<IDataStore> m_DataStorePtr;
	std::map<std::string, boost::shared_ptr<InstrumentData> > m_InstrumentDataMap;
	std::map<std::string, size_t > m_TheSameTimeTickIndexMap;

	int m_Ktype; //回测数据周期类型;
	int m_TakeTickTime;//定时多少毫秒去取下一个Tick;
	std::string m_StartTradingDay;
	std::string m_EndTradingDay;
protected:
	//异步模拟器;
	boost::shared_ptr<boost::asio::io_service> m_ioService;
	boost::shared_ptr<boost::asio::io_service::work> m_ioWork;
	boost::shared_ptr<boost::thread> m_ioThread;
	//;定时取行情;
	boost::shared_ptr<boost::asio::steady_timer> m_TickTimer;
};



#endif
