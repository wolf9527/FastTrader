#include "stdafx.h"
#include "SimHub.h"
#include "SimTrader.h"
#include "SimMarket.h"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

SimHub& SimHub::get_instance()
{
	static SimHub g_SimHub;
	return g_SimHub;
}

void SimHub::Register(boost::shared_ptr<SimTrader> pTrader)
{
	m_Trader = pTrader;
}

void SimHub::Register(boost::shared_ptr<SimMarket> pMarket)
{
	m_Market = pMarket;
}

void SimHub::OnRtnDepthMarket(boost::shared_ptr<Tick> tick)
{
	//发送合约交易状态?;
	if (m_TradingDay.empty())
	{
		m_TradingDay = tick->TradingDay;
		//推送连续交易;
		std::map<std::string, std::vector<std::string> > AllSubInstrumentsMap;
		m_Market->GetSubInstruments(AllSubInstrumentsMap);
		std::map<std::string, std::vector<std::string> >::iterator asiIter = AllSubInstrumentsMap.begin();
		for (; asiIter != AllSubInstrumentsMap.end(); ++asiIter)
		{
			std::vector<std::string>& AllSubInstruments = asiIter->second;
			for (size_t i = 0; i < AllSubInstruments.size(); ++i)
			{
				boost::shared_ptr<InstStatus> pInstStatus = boost::make_shared<InstStatus>();
				pInstStatus->InstrumentID = AllSubInstruments[i];
				pInstStatus->InstrumentStatus = IS_Continous;
				m_Trader->OnRtnInstrumentStatus(pInstStatus);
			}
		}
	}
	else
	{
		if (m_TradingDay != tick->TradingDay)
		{
			//交易日切换了;
			//收盘;
			std::map<std::string, std::vector<std::string> > AllSubInstrumentsMap;
			m_Market->GetSubInstruments(AllSubInstrumentsMap);
			std::map<std::string, std::vector<std::string> >::iterator asiIter = AllSubInstrumentsMap.begin();
			for (; asiIter != AllSubInstrumentsMap.end(); ++asiIter)
			{
				std::vector<std::string>& AllSubInstruments = asiIter->second;
				for (size_t i = 0; i < AllSubInstruments.size(); ++i)
				{
					boost::shared_ptr<InstStatus> pInstStatus = boost::make_shared<InstStatus>();
					pInstStatus->InstrumentID = AllSubInstruments[i];
					pInstStatus->InstrumentStatus = IS_Closed;
					m_Trader->OnRtnInstrumentStatus(pInstStatus);
				}
			}
			//进行结算,todo;
			//每日结算的结算价取自真实市场的结算价，在最后交易日时现金交割（以合约当天结算价了结剩余仓位）。

			//更新交易日;
			m_TradingDay = tick->TradingDay;

			m_Trader->SetTradingDay(m_TradingDay);
			m_Market->SetTradingDay(m_TradingDay);

			m_Ticks.erase(tick->InstrumentID());
			//收盘之后立即又开盘,模拟就这样喽;
			for (; asiIter != AllSubInstrumentsMap.end(); ++asiIter)
			{
				std::vector<std::string>& AllSubInstruments = asiIter->second;
				for (size_t i = 0; i < AllSubInstruments.size(); ++i)
				{
					boost::shared_ptr<InstStatus> pInstStatus = boost::make_shared<InstStatus>();
					pInstStatus->InstrumentID = AllSubInstruments[i];
					pInstStatus->InstrumentStatus = IS_Continous;
					m_Trader->OnRtnInstrumentStatus(pInstStatus);
				}
			}
		}
	}


	m_Ticks[tick->InstrumentID()] = tick;
	//遍历报单队列;
	std::map<std::string, boost::shared_ptr<Order> >::iterator oIter = m_Orders.begin();
	for (; oIter != m_Orders.end();++oIter)
	{
		//判断能不能成交;
		boost::shared_ptr<Order> pOrder = oIter->second;
		if (pOrder->InstrumentID == tick->InstrumentID())
		{
			OnRtnOrder(pOrder);
		}
	}
}

boost::shared_ptr<Trade> SimHub::OnRtnOrder(boost::shared_ptr<Order> pOrder)
{
	std::map<std::string, boost::shared_ptr<Tick> >::iterator tIter = m_Ticks.find(pOrder->InstrumentID);
	if (tIter == m_Ticks.end())
	{
		//这个合约现在还没有行情呢,返回未成交还在队列中;
		pOrder->OrderStatus = OST_NoTradeNotQueueing;

		//存到队列里去;
		m_Orders.insert(std::make_pair(pOrder->OrderRef, pOrder));
		return nullptr;
	}
	else
	{
		//有行情的了,判断能不能成交;
		pOrder->InsertTime = tIter->second->UpdateTimeStr;
		if (pOrder->OrderPriceType == OPT_LimitPrice)
		{
			//限价单;
			if (pOrder->Direction == D_Buy)
			{
				//看卖一价;
				if (pOrder->LimitPrice >= tIter->second->AskPrice[0])
				{
					//成交;
					pOrder->OrderStatus = OST_AllTraded;
				}
				else
				{
					//排队;
					pOrder->OrderStatus = OST_NoTradeQueueing;
					m_Orders.insert(std::make_pair(pOrder->OrderRef, pOrder));
				}
			}
			else
			{
				//看买一价;
				if (pOrder->LimitPrice <= tIter->second->BidPrice[0])
				{
					//成交;
					pOrder->OrderStatus = OST_AllTraded;
				}
				else
				{
					//排队;
					pOrder->OrderStatus = OST_NoTradeQueueing;
					m_Orders.insert(std::make_pair(pOrder->OrderRef, pOrder));
				}
			}
		}
		else
		{
			//市价单,直接成交;
			pOrder->OrderStatus = OST_AllTraded;
		}

		//如果全部成交;
		if (pOrder->OrderStatus==OST_AllTraded)
		{
			//通知接口成交;
			boost::shared_ptr<Trade> pTrade = boost::make_shared<Trade>();
			if (pOrder->Direction == D_Buy)
			{
				//成交价为委托价、卖一价、最新价三价取中;
				boost::accumulators::accumulator_set < double,
					boost::accumulators::features<boost::accumulators::tag::median> > media_acc;
				media_acc(pOrder->LimitPrice);
				media_acc(tIter->second->AskPrice[0]);
				media_acc(tIter->second->LastPrice);
				
				pTrade->Price = boost::accumulators::median(media_acc);
			}
			else
			{
				//成交价为委托价、买一价、最新价三价取中;
				boost::accumulators::accumulator_set < double,
					boost::accumulators::features<boost::accumulators::tag::median> > media_acc;
				media_acc(pOrder->LimitPrice);
				media_acc(tIter->second->BidPrice[0]);
				media_acc(tIter->second->LastPrice);

				pTrade->Price = boost::accumulators::median(media_acc);
			}

			if (pOrder->OrderSysID.empty())
			{
				//生成OrderSysID,为当前时间戳;
				pOrder->OrderSysID=
					boost::to_string(boost::posix_time::to_time_t(boost::posix_time::second_clock::local_time()));
			}

			pOrder->VolumeTraded = pOrder->VolumeTotalOriginal;
			pOrder->VolumeTotal = pOrder->VolumeTotalOriginal - pOrder->VolumeTraded;
			pOrder->StatusMsg = "全部成交";

			pTrade->BrokerID = pOrder->BrokerID;
			pTrade->BusinessUnit = pOrder->BusinessUnit;
			pTrade->ClearingPartID = pOrder->ClearingPartID;
			pTrade->ClientID = pOrder->ClientID;
			pTrade->Direction = pOrder->Direction;
			pTrade->TradeDate = pOrder->TradingDay;
			pTrade->ExchangeID = pOrder->ExchangeID;
			pTrade->HedgeFlag = pOrder->CombHedgeFlag[0];
			pTrade->InstrumentID = pOrder->InstrumentID;
			pTrade->InvestorID = pOrder->InvestorID;
			pTrade->OffsetFlag = pOrder->CombOffsetFlag[0];
			pTrade->OrderRef = pOrder->OrderRef;
			pTrade->TradeTime = tIter->second->UpdateTimeStr;
			pTrade->UserID = pOrder->UserID;
			pTrade->TradingDay = pOrder->TradingDay;
			pTrade->TradeType = TRDT_Common;
			pTrade->Volume = pOrder->VolumeTotalOriginal;
			pTrade->OrderSysID = pOrder->OrderSysID;
			//将报单信息从队列中删除;
			std::map<std::string, boost::shared_ptr<Order> >::iterator oIter = m_Orders.find(pOrder->OrderRef);
			if (oIter !=m_Orders.end())
			{
				m_Orders.erase(oIter);
			}
			
			return pTrade;
		}
	}
	return nullptr;
}

std::string SimHub::GetTradingDay()
{
	return m_TradingDay;
}

