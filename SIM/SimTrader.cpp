#include "platform.h"
#include <iostream>
#include <sstream>

using namespace std;
#include <string.h>

#include "SimTrader.h"
#include "DataTypes.h"
#include "file_helper.h"
#include "../Log/logging.h"
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <boost/make_shared.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
//#include "../FacilityBaseLib/ExchangeContainer.h"
#include "../sdk/include/PlatformStruct.h"
#include "../DataStoreLib/IDataStore.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/ExchangeContainer.h"
#include "SimHub.h"

#include <boost/serialization/serialization.hpp>
#include <boost/archive/tmpdir.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
// void SimTrader::OnFrontConnected()
// {
// 	LOGDEBUG("交易前置连接响应...");
// 	ReqUserLogin();
// }
// 
// 
// void SimTrader::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,
// 							CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
// {
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		SetLastError(pRspInfo);
// 		LOGDEBUG("交易前置登录失败:{}",LastError().ErrorMsg);
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	LOGDEBUG("-----------用户 交易登录验证通过----------{}",pRspUserLogin->UserID);
// 	LOGDEBUG("----------------交易日:{}",m_pTraderApi->GetTradingDay());
// 	LOGDEBUG("--------------登录时间:{}",pRspUserLogin->LoginTime);
// 	//LOGDEBUG("--------------本地时间:"<<DateTime::Now().GetAsString());
// 	LOGDEBUG("------------交易所时间:{}",pRspUserLogin->SHFETime);
// 	LOGDEBUG("----------------回话ID:{}",pRspUserLogin->SessionID);
// 	LOGDEBUG("----------------前置ID:{}",pRspUserLogin->FrontID);
// 	LOGDEBUG("--------------报单引用:{}",pRspUserLogin->MaxOrderRef);
// 
// 	m_UserInfo.TradingDay=pRspUserLogin->TradingDay;
// 	m_UserInfo.LoginTime=pRspUserLogin->LoginTime;
// 	m_UserInfo.SystemName=pRspUserLogin->SystemName;
// 	m_UserInfo.FrontID=pRspUserLogin->FrontID;
// 	m_UserInfo.SessionID=pRspUserLogin->SessionID;
// 	m_UserInfo.MaxOrderRef=pRspUserLogin->MaxOrderRef;
// 	m_UserInfo.ExhangeTime=pRspUserLogin->SHFETime;
// 	if (!QryTradingParam())
// 	{
// 		m_UserInfo.IsTraderLogined = true;
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 	}
// }


bool SimTrader::Init( const ServerInfo& info)
{
	if (m_bInited)
	{
		return false;
	}
	LOGDEBUG("Init ServerInfo {}",info.name);

	m_ServerInfo=info;

	m_DataStorePtr = IDataStore::CreateStore(
		m_ServerInfo.trade_server_front[0].address.c_str(), 
		IDataStore::dbtypeSqlite3
		);

	if (!m_ioService)
	{
		m_ioService = boost::make_shared<boost::asio::io_service>();
	}

	if (!m_ioWork)
	{
		m_ioWork = boost::make_shared<boost::asio::io_service::work>(boost::ref(*m_ioService));
	}
 	m_bInited=true;
	return m_bInited;
}


SimTrader::SimTrader( )
	:Trader()
{
	LOGINIT("sim_trader");
	LOGDEBUG("Trader对象构造...");
	//m_pTraderApi=NULL;
	m_bInited=false;
	m_bFrontDisconnected=false;
	m_bIsLogined = false;

	{
		//加载手续费率;
		std::ifstream ifs("CTP_CommisionRate.bin", std::ios::binary);

		if (ifs.is_open())
		{
			boost::archive::binary_iarchive oa(ifs);

			oa >> m_InstCommisionRates;

			ifs.close();
		}
	}

	{
		std::ifstream ifs("CTP_MarginRate.bin", std::ios::binary);

		if (ifs.is_open())
		{
			boost::archive::binary_iarchive oa(ifs);

			oa >> m_InstMarginRates;

			ifs.close();
		}
	}
}

void SimTrader::Release()
{
	//释放成员指针...;
	m_bInited=false;
	//释放对象，参照文档;
	//综合交易平台API开发FAQ.pdf/综合交易平台API开发常见问题列表;
// 	if (NULL != m_pTraderApi)
// 	{
// 		m_pTraderApi->RegisterSpi(NULL);
// 		m_pTraderApi->Release();
// 		m_pTraderApi = NULL;
// 	}
}

bool SimTrader::Login( const UserLoginParam& u )
{
	if (!m_bInited || u.UserID.empty() || u.Password.empty())
	{
		return false;
	}
	m_UserInfo=u;
	if (!m_InvestorAccount)
	{
		m_InvestorAccount = boost::make_shared<InvestorAccount>(m_UserInfo.UserID,m_ServerInfo.id);
	}
	//m_pTraderApi->Init();
	//等待登录完成;
	LOGDEBUG("----------user begin login :{}",u.UserID);
	

	if (!m_ioThread)
	{
		m_ioThread = boost::make_shared<boost::thread>(boost::bind(&boost::asio::io_service::run,m_ioService));
	}

	m_ioService->post(boost::bind(&SimTrader::OnFrontConnected,shared_from_this()));
	boost::unique_lock<boost::mutex> lck(m_LoginMutex);
	int login_timeout = m_ServerInfo.trader_login_timeout;
	if (login_timeout <= 0)
	{
		login_timeout = 300;
	}
	if (m_LoginCondVar.wait_for(lck, boost::chrono::seconds(login_timeout)) == boost::cv_status::timeout)
	{
		LOGDEBUG("交易登录超时...");
		return false;
	}
	LOGDEBUG("交易登录结束...");
	SimHub::get_instance().Register(shared_from_this());
	return m_UserInfo.IsTraderLogined;
}


// void SimTrader::NotifyUserLogin(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
// {
// 	if (m_UserInfo.IsTraderLogined)
// 	{
// 		sigUserLogin(m_UserInfo);
// 		m_LoginCondVar.notify_one();
// 	}
// }


// void SimTrader::OnRspQrySettlementInfoConfirm( CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return;
// 	}
// 	bool bNeedSettlementConfirm=true;
// 	if (pSettlementInfoConfirm)
// 	{
// 		m_SettlementInfoConfirm.BrokerID=pSettlementInfoConfirm->BrokerID;
// 		m_SettlementInfoConfirm.ConfirmDate=pSettlementInfoConfirm->ConfirmDate;
// 		m_SettlementInfoConfirm.ConfirmTime=pSettlementInfoConfirm->ConfirmTime;
// 		m_SettlementInfoConfirm.InvestorID=pSettlementInfoConfirm->InvestorID;
// 		NotifyApiMgr(RspQrySettlementConfirm,LPARAM(&m_SettlementInfoConfirm));
// 
// 		//当前时间;
// 		boost::posix_time::ptime::date_type dNow = boost::gregorian::date_from_iso_string(GetTradingDay());
// 		boost::posix_time::ptime nowTime(dNow,boost::posix_time::second_clock::local_time().time_of_day());
// 
// 		//确认时间;
// 		boost::posix_time::ptime::date_type dConfirm = boost::gregorian::date_from_iso_string(m_SettlementInfoConfirm.ConfirmDate);
// 		boost::posix_time::ptime::time_duration_type tdConfirm = boost::posix_time::duration_from_string(m_SettlementInfoConfirm.ConfirmTime);
// 
// 		boost::posix_time::ptime confirmTime(dConfirm, tdConfirm);
// 		
// 		LOGDEBUG("上次结算时间:{},{}",m_SettlementInfoConfirm.ConfirmDate,m_SettlementInfoConfirm.ConfirmTime);
// 		//今天结算过了没有?;
// 		if (confirmTime.date()<nowTime.date())
// 		{
// 			bNeedSettlementConfirm = true;
// 		}
// 		else
// 		{
// 			if (confirmTime <= nowTime)
// 			{
// 				bNeedSettlementConfirm = false;
// 			}
// 		}
// 	}
// 	else
// 	{
// 		bNeedSettlementConfirm=true;
// 	}
// 	LOGDEBUG("查询结算信息确认响应...");
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		//如果没有登录;
// 		if (bNeedSettlementConfirm)
// 		{
// 			//查询结算信息;
// 			if (!QrySettlementInfo())
// 			{
// 				NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 			}
// 		}
// 		else
// 		{
// 			LOGDEBUG("查询结算信息确认响应--登录完成...");
// 			m_UserInfo.IsTraderLogined=true;
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// 	LOGDEBUG("查询结算信息确认响应--完成...");
// }
// 
// void SimTrader::OnRspSettlementInfoConfirm( CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	if (pSettlementInfoConfirm)
// 	{
// 		m_SettlementInfoConfirm.BrokerID=pSettlementInfoConfirm->BrokerID;
// 		m_SettlementInfoConfirm.ConfirmDate=pSettlementInfoConfirm->ConfirmDate;
// 		m_SettlementInfoConfirm.ConfirmTime=pSettlementInfoConfirm->ConfirmTime;
// 		m_SettlementInfoConfirm.InvestorID=pSettlementInfoConfirm->InvestorID;
// 		NotifyApiMgr(RspSettlementInfoConfirm,LPARAM(&m_SettlementInfoConfirm));
// 	}
// 	LOGDEBUG("结算信息确认响应...");
// 	if (bIsLast && !m_UserInfo.IsTraderLogined)
// 	{
// 		m_UserInfo.IsTraderLogined=true;
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 	}
// }
// 
// void SimTrader::OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
// {
// 	
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		///请求查询投资者持仓;
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	if (pTradingAccount)
// 	{
// 		TradingAccount m_TradingAccount;
// 		m_TradingAccount.AccountID=pTradingAccount->AccountID;
// 		m_TradingAccount.Available=pTradingAccount->Available;
// 		m_TradingAccount.Balance=pTradingAccount->Balance;
// 		m_TradingAccount.BrokerID=pTradingAccount->BrokerID;
// 		m_TradingAccount.CashIn=pTradingAccount->CashIn;
// 		m_TradingAccount.CloseProfit=pTradingAccount->CloseProfit;
// 		m_TradingAccount.Commission=pTradingAccount->Commission;
// 		m_TradingAccount.Credit=pTradingAccount->Credit;
// 		m_TradingAccount.CurrencyID=pTradingAccount->CurrencyID;
// 		m_TradingAccount.CurrMargin=pTradingAccount->CurrMargin;
// 		m_TradingAccount.DeliveryMargin=pTradingAccount->DeliveryMargin;
// 		m_TradingAccount.Deposit=pTradingAccount->Deposit;
// 		m_TradingAccount.ExchangeDeliveryMargin=pTradingAccount->ExchangeDeliveryMargin;
// 		m_TradingAccount.ExchangeMargin=pTradingAccount->ExchangeDeliveryMargin;
// 		m_TradingAccount.FrozenCash=pTradingAccount->FrozenCash;
// 		m_TradingAccount.FrozenCommission=pTradingAccount->FrozenCommission;
// 		m_TradingAccount.FrozenMargin=pTradingAccount->FrozenMargin;
// 		m_TradingAccount.FundMortgageAvailable=pTradingAccount->FundMortgageAvailable;
// 		m_TradingAccount.FundMortgageIn=pTradingAccount->FundMortgageIn;
// 		m_TradingAccount.FundMortgageOut=pTradingAccount->FundMortgageOut;
// 		m_TradingAccount.Interest=pTradingAccount->Interest;
// 		m_TradingAccount.InterestBase=pTradingAccount->InterestBase;
// 		m_TradingAccount.Mortgage=pTradingAccount->Mortgage;
// 		m_TradingAccount.MortgageableFund=pTradingAccount->MortgageableFund;
// 		m_TradingAccount.PositionProfit=pTradingAccount->PositionProfit;
// 		m_TradingAccount.PreBalance=pTradingAccount->PreBalance;
// 		m_TradingAccount.PreCredit=pTradingAccount->PreCredit;
// 		m_TradingAccount.PreDeposit=pTradingAccount->PreDeposit;
// 		m_TradingAccount.PreFundMortgageIn=pTradingAccount->PreFundMortgageIn;
// 		m_TradingAccount.PreFundMortgageOut=pTradingAccount->PreFundMortgageOut;
// 		m_TradingAccount.PreMargin=pTradingAccount->PreMargin;
// 		m_TradingAccount.PreMortgage=pTradingAccount->PreMargin;
// 		m_TradingAccount.Reserve=pTradingAccount->Reserve;
// 		m_TradingAccount.ReserveBalance=pTradingAccount->ReserveBalance;
// 		m_TradingAccount.SettlementID=pTradingAccount->SettlementID;
// 		m_TradingAccount.SpecProductCloseProfit=pTradingAccount->SpecProductCloseProfit;
// 		m_TradingAccount.SpecProductCommission=pTradingAccount->SpecProductCommission;
// 		m_TradingAccount.SpecProductExchangeMargin=pTradingAccount->SpecProductExchangeMargin;
// 		m_TradingAccount.SpecProductFrozenCommission=pTradingAccount->SpecProductFrozenCommission;
// 		m_TradingAccount.SpecProductFrozenMargin=pTradingAccount->SpecProductFrozenMargin;
// 		m_TradingAccount.SpecProductMargin=pTradingAccount->SpecProductMargin;
// 		m_TradingAccount.SpecProductPositionProfit=pTradingAccount->SpecProductPositionProfit;
// 		m_TradingAccount.SpecProductPositionProfitByAlg=pTradingAccount->SpecProductPositionProfitByAlg;
// 		m_TradingAccount.TradingDay=pTradingAccount->TradingDay;
// 		m_TradingAccount.Withdraw=pTradingAccount->Withdraw;
// 		m_TradingAccount.WithdrawQuota=pTradingAccount->WithdrawQuota;
// 
// 		m_InvestorAccount->SafeUpdate(m_TradingAccount);
// 
// 		NotifyApiMgr(RspQryTradingAccount,LPARAM(&m_TradingAccount));
// 	}
// 	LOGDEBUG("查询账户资金响应...");
// 	if (bIsLast && !m_UserInfo.IsTraderLogined)
// 	{
// 		if (!QryPosition(""))
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// void SimTrader::OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
// {
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	if (pInstrument!=NULL)
// 	{
// 		auto instrument_data=boost::make_shared<Instrument>();
// 		instrument_data->InstrumentID=pInstrument->InstrumentID;
// 		instrument_data->ExchangeID=pInstrument->ExchangeID;
// 		instrument_data->InstrumentName=pInstrument->InstrumentName;
// 		instrument_data->ProductClass=pInstrument->ProductClass;
// 		instrument_data->ProductID=pInstrument->ProductID;
// 		instrument_data->CreateDate=pInstrument->CreateDate;
// 		instrument_data->OpenDate=pInstrument->OpenDate;
// 		instrument_data->ExpireDate=pInstrument->ExpireDate;
// 		instrument_data->IsTrading=pInstrument->IsTrading;
// 		instrument_data->PositionType=pInstrument->PositionDateType;
// 		instrument_data->LongMarginRatio=pInstrument->LongMarginRatio;
// 		instrument_data->ShortMarginRatio=pInstrument->ShortMarginRatio;
// 		instrument_data->IsTrading=pInstrument->IsTrading;
// 		instrument_data->VolumeMultiple=pInstrument->VolumeMultiple;
// 		instrument_data->PriceTick=pInstrument->PriceTick;
// 		//TraderSharedData::GetInstance().instruments[pInstrument->InstrumentID]=(instrument_data);
// 		//InstrumentCommisionRate icr;
// 		//icr.IsInitied = false;
// 		//m_InstCommisionRates.insert(
// 		//	std::pair<std::string, InstrumentCommisionRate>(instrument_data.ProductID,icr));
// 		//LOGDEBUG("查询合约:{}" , instrument_data.InstrumentID );
// 		//boost::shared_ptr<BASEDATA> pBasedata = boost::make_shared<BASEDATA>();
// 		//strncpy(
// 		//	pBasedata->szCode,
// 		//	pInstrument->InstrumentID,
// 		//	sizeof(pInstrument->InstrumentID)
// 		//);
// 		//strncpy(
// 		//	pBasedata->szExchange,
// 		//	pInstrument->ExchangeID,
// 		//	sizeof(pInstrument->ExchangeID)
// 		//);
// 		//pBasedata->IsTrading = pInstrument->IsTrading;
// 		//pBasedata->InstLifePhase = pInstrument->InstLifePhase;
// 		//pBasedata->PriceTick = pInstrument->PriceTick;
// 		//strncpy(pBasedata->ProductID, pInstrument->ProductID, sizeof(pInstrument->ProductID));
// 		//pBasedata->DeliveryMonth = pInstrument->DeliveryMonth;
// 		//pBasedata->DeliveryYear = pInstrument->DeliveryYear;
// 		//strncpy(pBasedata->StartDelivDate, pInstrument->StartDelivDate, sizeof(pInstrument->StartDelivDate));
// 		//strncpy(pBasedata->EndDelivDate, pInstrument->EndDelivDate, sizeof(pInstrument->EndDelivDate));
// 		//pBasedata->VolumeMultiple = pInstrument->VolumeMultiple;
// 		//pBasedata->ShortMarginRatio = pInstrument->ShortMarginRatio;
// 		//pBasedata->LongMarginRatio = pInstrument->LongMarginRatio;
// 		//pBasedata->ProductClass = pInstrument->ProductClass;
// 		//pBasedata->PriceTick = pInstrument->PriceTick;
// 		//pBasedata->PositionType = pInstrument->PositionType;
// 		//pBasedata->PositionDateType = pInstrument->PositionDateType;
// 		//pBasedata->MinMarketOrderVolume = pInstrument->MinMarketOrderVolume;
// 		//pBasedata->MaxMarketOrderVolume = pInstrument->MaxMarketOrderVolume;
// 		//pBasedata->MinLimitOrderVolume = pInstrument->MinLimitOrderVolume;
// 		//pBasedata->MaxLimitOrderVolume = pInstrument->MaxLimitOrderVolume;
// 		//strncpy(pBasedata->CreateDate, pInstrument->CreateDate, sizeof(pInstrument->CreateDate));
// 		//strncpy(pBasedata->szName, pInstrument->InstrumentName, sizeof(pInstrument->InstrumentName));
// 		//strncpy(pBasedata->OpenDate, pInstrument->OpenDate, sizeof(pInstrument->OpenDate));
// 		//strncpy(pBasedata->ExpireDate, pInstrument->ExpireDate, sizeof(pInstrument->ExpireDate));
// 		sigInstrumentInfo(instrument_data);
// 	}
// 	
// 	if (bIsLast && !m_UserInfo.IsTraderLogined)
// 	{
// 		if (!QryAccount())
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
// {
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
// 		return;
// 	}
// 	if (pInvestorPosition)
// 	{
// 		LOGDEBUG("ID={},Position={},YdPosition={},TodayPosition={},PosiDirection={}",
// 			pInvestorPosition->InstrumentID,pInvestorPosition->Position,
// 			pInvestorPosition->YdPosition,
// 			pInvestorPosition->TodayPosition,
// 			pInvestorPosition->PosiDirection);
// 		boost::shared_ptr<InvestorPosition> ip = boost::make_shared<InvestorPosition>();
// 		ip->InvestorID = pInvestorPosition->InvestorID;
// 		ip->BrokerID = pInvestorPosition->BrokerID;
// 		//ip->AbandonFrozen = pInvestorPosition->AbandonFrozen;
// 		ip->CashIn = pInvestorPosition->CashIn;
// 		ip->CloseAmount = pInvestorPosition->CloseAmount;
// 		ip->CloseProfit = pInvestorPosition->CloseProfit;
// 		ip->CloseProfitByDate = pInvestorPosition->CloseProfitByDate;
// 		ip->CloseProfitByTrade = pInvestorPosition->CloseProfitByTrade;
// 		ip->CloseVolume = pInvestorPosition->CloseVolume;
// 		ip->CombLongFrozen = pInvestorPosition->CombLongFrozen;
// 		ip->CombPosition = pInvestorPosition->CombPosition;
// 		ip->CombShortFrozen = pInvestorPosition->CombShortFrozen;
// 		ip->Commission = pInvestorPosition->Commission;
// 		ip->ExchangeMargin = pInvestorPosition->ExchangeMargin;
// 		ip->FrozenCash = pInvestorPosition->FrozenCash;
// 		ip->FrozenCommission = pInvestorPosition->FrozenCommission;
// 		ip->FrozenMargin = pInvestorPosition->FrozenMargin;
// 		ip->HedgeFlag = (EnumHedgeFlag)pInvestorPosition->HedgeFlag;
// 		ip->InstrumentID = pInvestorPosition->InstrumentID;
// 		ip->LongFrozen = pInvestorPosition->LongFrozen;
// 		ip->LongFrozenAmount = pInvestorPosition->LongFrozenAmount;
// 		ip->MarginRateByMoney = pInvestorPosition->MarginRateByMoney;
// 		ip->MarginRateByVolume = pInvestorPosition->MarginRateByVolume;
// 		ip->OpenAmount = pInvestorPosition->OpenAmount;
// 		ip->OpenCost = pInvestorPosition->OpenCost;
// 		ip->OpenVolume = pInvestorPosition->OpenVolume;
// 		ip->PosiDirection = (EnumPosiDirection)pInvestorPosition->PosiDirection;
// 		ip->Position = pInvestorPosition->Position;
// 		ip->PositionCost = pInvestorPosition->PositionCost;
// 		ip->PositionDate = (EnumPositionDate)pInvestorPosition->PositionDate;
// 		ip->PositionProfit = pInvestorPosition->PositionProfit;
// 		ip->PreMargin = pInvestorPosition->PreMargin;
// 		ip->PreSettlementPrice = pInvestorPosition->PreSettlementPrice;
// 		ip->SettlementPrice = pInvestorPosition->SettlementPrice;
// 		ip->ShortFrozen = pInvestorPosition->ShortFrozen;
// 		ip->ShortFrozenAmount = pInvestorPosition->ShortFrozenAmount;
// 		//ip->StrikeFrozen = pInvestorPosition->StrikeFrozen;
// 		//ip->StrikeFrozenAmount = pInvestorPosition->StrikeFrozenAmount;
// 		ip->TodayPosition = pInvestorPosition->TodayPosition;
// 		ip->TradingDay = pInvestorPosition->TradingDay;
// 		ip->UseMargin = pInvestorPosition->UseMargin;
// 		ip->YdPosition = pInvestorPosition->YdPosition;
// 		m_InvestorAccount->SafeUpdate(ip);
// 		NotifyApiMgr(RspQryPosition, LPARAM(&ip));
// 	}
// 	LOGDEBUG("查询投资者持仓响应...");
// 	if (bIsLast && !m_UserInfo.IsTraderLogined)
// 	{
// 		if (!QryPositionDetail(""))
// 		{
// 			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
// 		}
// 	}
// }
// 
// 
void SimTrader::OnRspOrderInsert(boost::shared_ptr<InputOrder> pInputOrder, int nRequestID, bool bIsLast)
{
	if (pInputOrder)
	{
		boost::shared_ptr<Order> pOrder = boost::make_shared<Order>();
		//*pOrder = *pInputOrder;
		sigOnOrder(pOrder);
		if (m_UserInfo.IsTraderLogined)
		{


		}
	}
}

// void CTraderSpi::ReqOrderAction(CThostFtdcOrderField *pOrder)
// {
// 	static bool ORDER_ACTION_SENT = false;		//是否发送了报单
// 	if (ORDER_ACTION_SENT)
// 		return;
// 
// 	CThostFtdcInputOrderActionField req;
// 	memset(&req, 0, sizeof(req));
// 	///经纪公司代码
// 	strcpy(req.BrokerID, pOrder->BrokerID);
// 	///投资者代码
// 	strcpy(req.InvestorID, pOrder->InvestorID);
// 	///报单操作引用
// 	//	TThostFtdcOrderActionRefType	OrderActionRef;
// 	///报单引用
// 	strcpy(req.OrderRef, pOrder->OrderRef);
// 	///请求编号
// 	//	TThostFtdcRequestIDType	RequestID;
// 	///前置编号
// 	req.FrontID = FRONT_ID;
// 	///会话编号
// 	req.SessionID = SESSION_ID;
// 	///交易所代码
// 	//	TThostFtdcExchangeIDType	ExchangeID;
// 	///报单编号
// 	//	TThostFtdcOrderSysIDType	OrderSysID;
// 	///操作标志
// 	req.ActionFlag = THOST_FTDC_AF_Delete;
// 	///价格
// 	//	TThostFtdcPriceType	LimitPrice;
// 	///数量变化
// 	//	TThostFtdcVolumeType	VolumeChange;
// 	///用户代码
// 	//	TThostFtdcUserIDType	UserID;
// 	///合约代码
// 	strcpy(req.InstrumentID, pOrder->InstrumentID);
// 
// 	int iResult = pUserApi->ReqOrderAction(&req, ++iRequestID);
// 	cerr << "--->>> 报单操作请求: " << ((iResult == 0) ? "成功" : "失败") << endl;
// 	ORDER_ACTION_SENT = true;
//}

void SimTrader::OnRspOrderAction(boost::shared_ptr<InputOrderAction> inputOrderAction, int nRequestID, bool bIsLast)
{
	if (inputOrderAction)
	{
		sigOnOrderAction(inputOrderAction);
	}
}

///报单通知;
// void SimTrader::OnRtnOrder(CThostFtdcOrderField *pOrder)
// {
// 	//报单回报;
// 	if (pOrder)
// 	{
// 		boost::shared_ptr<Order> order=boost::make_shared<Order>();
// 		order->InvestorID=pOrder->InvestorID;
// 		order->InstrumentID=pOrder->InstrumentID;
// 		order->ActiveTime=pOrder->ActiveTime;
// 		order->ActiveTraderID=pOrder->ActiveTraderID;
// 		order->ActiveUserID=pOrder->ActiveTraderID;
// 		order->BrokerID=pOrder->BrokerID;
// 		order->BrokerOrderSeq=pOrder->BrokerOrderSeq;
// 		order->BusinessUnit=pOrder->BusinessUnit;
// 		order->CancelTime=pOrder->CancelTime;
// 		order->ClearingPartID=pOrder->ClearingPartID;
// 		order->ClientID=pOrder->ClientID;
// 		strncpy((char*)order->CombHedgeFlag, pOrder->CombHedgeFlag, sizeof(pOrder->CombHedgeFlag));
// 		strcpy((char*)order->CombOffsetFlag,pOrder->CombOffsetFlag);
// 		order->ContingentCondition=(EnumContingentCondition)pOrder->ContingentCondition;
// 		order->Direction=(EnumDirection)pOrder->Direction;
// 		order->ExchangeID=pOrder->ExchangeID;
// 		order->ExchangeInstID=pOrder->ExchangeInstID;
// 		order->ForceCloseReason=(EnumForceCloseReason)pOrder->ForceCloseReason;
// 		order->FrontID=pOrder->FrontID;
// 		order->GTDDate=pOrder->GTDDate;
// 		order->InsertDate=pOrder->InsertDate;
// 		order->InsertTime=pOrder->InsertTime;
// 		order->InstallID=pOrder->InstallID;
// 		order->InstrumentID=pOrder->InstrumentID;
// 		order->InvestorID=pOrder->InvestorID;
// 		order->IsAutoSuspend=pOrder->IsAutoSuspend;
// 		order->IsSwapOrder=pOrder->IsSwapOrder;
// 		order->LimitPrice=pOrder->LimitPrice;
// 		order->MinVolume=pOrder->MinVolume;
// 		order->NotifySequence=pOrder->NotifySequence;
// 		order->OrderLocalID=pOrder->OrderLocalID;
// 		order->OrderPriceType=(EnumOrderPriceType)pOrder->OrderPriceType;
// 		order->OrderRef=pOrder->OrderRef;
// 		order->OrderSource=(EnumOrderSource)pOrder->OrderSource;
// 		order->OrderStatus=(EnumOrderStatus)pOrder->OrderStatus;
// 		order->OrderSubmitStatus=(EnumOrderSubmitStatus)pOrder->OrderSubmitStatus;
// 		order->OrderSysID=pOrder->OrderSysID;
// 		order->OrderType=(EnumOrderType)pOrder->OrderType;
// 		order->ParticipantID=pOrder->ParticipantID;
// 		order->RelativeOrderSysID=pOrder->RelativeOrderSysID;
// 		order->RequestID=pOrder->RequestID;
// 		order->SequenceNo=pOrder->SequenceNo;
// 		order->SessionID=pOrder->SessionID;
// 		order->SettlementID=pOrder->SettlementID;
// 		order->StatusMsg=pOrder->StatusMsg;
// 		order->StopPrice=pOrder->StopPrice;
// 		order->SuspendTime=pOrder->SuspendTime;
// 		order->TimeCondition=(EnumTimeCondition)pOrder->TimeCondition;
// 		order->TraderID=pOrder->TraderID;
// 		order->TradingDay=pOrder->TradingDay;
// 		order->UpdateTime=pOrder->UpdateTime;
// 		order->UserForceClose=pOrder->UserForceClose;
// 		order->UserID=pOrder->UserID;
// 		order->UserProductInfo=pOrder->UserProductInfo;
// 		order->VolumeCondition=(EnumVolumeCondition)pOrder->VolumeCondition;
// 		order->VolumeTotal=pOrder->VolumeTotal;
// 		order->VolumeTotalOriginal=pOrder->VolumeTotalOriginal;
// 		order->VolumeTraded=pOrder->VolumeTraded;
// 		order->ZCETotalTradedVolume=pOrder->ZCETotalTradedVolume;
// 		m_InvestorAccount->SafeUpdate(order);
// 		sigOnOrder(order);
// 		//DEBUG_PRINTF("报单回报:%s:sysid:%s,ref:%s",order.InstrumentID.c_str(),order.OrderSysID.c_str(),order.OrderRef.c_str());
// 		//NotifyApiMgr(RtnOrder,LPARAM(&order));
// 	}
// }

///成交通知;
// void SimTrader::OnRtnTrade(CThostFtdcTradeField *pTrade)
// {
// 	if (pTrade)
// 	{
// 		LOGDEBUG("成交回报:{}" , pTrade->InstrumentID);
// 		boost::shared_ptr<Trade> td=boost::make_shared<Trade>();
// 		td->BrokerID=pTrade->BrokerID;
// 		td->BrokerOrderSeq=pTrade->BrokerOrderSeq;
// 		td->BusinessUnit=pTrade->BusinessUnit;
// 		td->ClearingPartID=pTrade->ClearingPartID;
// 		td->ClientID=pTrade->ClientID;
// 		td->Direction=(EnumDirection)pTrade->Direction;
// 		td->ExchangeID=pTrade->ExchangeID;
// 		td->ExchangeInstID=pTrade->ExchangeInstID;
// 		td->HedgeFlag=(EnumHedgeFlag)pTrade->HedgeFlag;
// 		td->InstrumentID=pTrade->InstrumentID;
// 		td->InvestorID=pTrade->InvestorID;
// 		td->OffsetFlag=(EnumOffsetFlag)pTrade->OffsetFlag;
// 		td->OrderLocalID=pTrade->OrderLocalID;
// 		td->OrderRef=pTrade->OrderRef;
// 		td->OrderSysID=pTrade->OrderSysID;
// 		td->ParticipantID=pTrade->ParticipantID;
// 		td->Price=pTrade->Price;
// 		td->PriceSource=(EnumPriceSource)pTrade->PriceSource;
// 		td->SequenceNo=pTrade->SequenceNo;
// 		td->SettlementID=pTrade->SettlementID;
// 		td->TradeDate=pTrade->TradeDate;
// 		td->TradeID=pTrade->TradeID;
// 		td->TraderID=pTrade->TraderID;
// 		td->TradeSource=(EnumTradeSource)pTrade->TradeSource;
// 		td->TradeTime=pTrade->TradeTime;
// 		td->TradeType=(EnumTradeType)pTrade->TradeType;
// 		td->TradingRole=(EnumTradingRole)pTrade->TradingRole;
// 		td->TradingDay=pTrade->TradingDay;
// 		td->UserID=pTrade->UserID;
// 		td->Volume=pTrade->Volume;
// 		m_InvestorAccount->SafeUpdate(td);
// 		sigOnTrade(td);
// 	}
// }
// 
// void SimTrader:: OnFrontDisconnected(int nReason)
// {
// 	
// 	LOGDEBUG("交易前置断开连接:{}",nReason);
// 	m_bFrontDisconnected=true;
// }
// 
// void SimTrader::OnHeartBeatWarning(int nTimeLapse)
// {
// 	cerr << "--->>> " << "OnHeartBeatWarning" << endl;
// 	cerr << "--->>> nTimerLapse = " << nTimeLapse << endl;
// }
// 
// void SimTrader::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
// {
// 	LOGDEBUG("OnRspError:{},{}",nRequestID, bIsLast);
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		if (!m_UserInfo.IsTraderLogined)
// 		{
// 			//可能是查询还未就绪;
// 			QryTradingParam();
// 		}
// 	}
// }

// bool SimTrader::IsErrorRspInfo( CThostFtdcRspInfoField *pRspInfo )
// {
// 	bool bResult = ((pRspInfo!=NULL) && (pRspInfo->ErrorID != 0));
// 	if (bResult)
// 	{
// 		LOGDEBUG("--->>> ErrorID={}, ErrorMsg={}",  pRspInfo->ErrorID , pRspInfo->ErrorMsg);
// 	}
// 	return bResult;
// }

// CThostFtdcTraderApi* SimTrader::GetTraderApi()
// {
// 	return m_pTraderApi;
// }

// void SimTrader::OnRspQryExchange( CThostFtdcExchangeField *pExchange, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	//保存;
// 	if (pExchange)
// 	{
// 		boost::shared_ptr<exchange_t> exchange=boost::make_shared<exchange_t>();
// 		strcpy(exchange->ExchangeID,pExchange->ExchangeID);
// 		strcpy(exchange->ExchangeName,pExchange->ExchangeName);
// 		exchange->ExchangeProperty=(EnumExchangeProperty)pExchange->ExchangeProperty;
// 		sigExchangeInfo(exchange);
// 	}
// 	LOGDEBUG("查询交易所响应...");
// 	if (bIsLast && !m_UserInfo.IsTraderLogined)
// 	{
// 		if (!QryInstrument("",""))
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }

// void SimTrader::SetLastError(CThostFtdcRspInfoField *pRspInfo)
// {
// 	if (NULL==pRspInfo)
// 	{
// 		m_ErrorMessage.ErrorMsg="未知的错误";
// 		return ;
// 	}
// 	m_ErrorMessage.ErrorMsg=pRspInfo->ErrorMsg;
// 	m_ErrorMessage.ErrorID=pRspInfo->ErrorID;
// }

// void SimTrader::OnRspQryInstrumentCommissionRate( CThostFtdcInstrumentCommissionRateField *pInstrumentCommissionRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//查手续费;
// 	
// 
// 	//保存;
// 	if (pInstrumentCommissionRate)
// 	{
// 		InstrumentCommisionRate& icr = m_InstCommisionRates[pInstrumentCommissionRate->InstrumentID];
// 		icr.BrokerID = pInstrumentCommissionRate->BrokerID;
//  		icr.CloseRatioByMoney = pInstrumentCommissionRate->CloseRatioByMoney;
// 		icr.CloseRatioByVolume = pInstrumentCommissionRate->CloseRatioByVolume;
// 		icr.CloseTodayRatioByMoney = pInstrumentCommissionRate->CloseTodayRatioByMoney;
// 		icr.CloseTodayRatioByVolume = pInstrumentCommissionRate->CloseTodayRatioByVolume;
// 		icr.InstrumentID = pInstrumentCommissionRate->InstrumentID;
// 		icr.InvestorID = pInstrumentCommissionRate->InvestorID;
// 		icr.InvestorRange = (EnumInvestorRange)pInstrumentCommissionRate->InvestorRange;
// 		icr.OpenRatioByMoney = pInstrumentCommissionRate->OpenRatioByMoney;
// 		icr.OpenRatioByVolume = pInstrumentCommissionRate->OpenRatioByVolume;
// 		icr.IsInitied = true;
// 		LOGDEBUG("查询合约手续费...{}",icr.InstrumentID);
// 		
// 	}
// 	m_CV4QryInstCommisonRate.notify_one();
// }

bool SimTrader::OrderInsert( boost::shared_ptr<InputOrder> inputOrder )
{
	auto& param = *inputOrder;
	auto oiIter=m_OrderBook.find(param.OrderRef);
	if (oiIter!=m_OrderBook.end())
	{
		//重复报单了;
		m_ioService->post(boost::bind(&SimTrader::OnRspOrderInsert, shared_from_this(), oiIter->second.pInputOrder,++RequestID(),true));
		return false;
	}
	InputOrder_Order_InputOrderAction_Trade orderInfo;
// 	CThostFtdcInputOrderField req;
// 	memset(&req, 0, sizeof(req));
	boost::shared_ptr<Order> pOrder = boost::make_shared<Order>();
	orderInfo.pOrder = pOrder;
	///经纪公司代码;
	if (param.BrokerID.empty())
	{
		param.BrokerID=m_ServerInfo.id;
	}
	pOrder->BrokerID = param.BrokerID;
	///投资者代码;
	if (param.InvestorID.empty())
	{
		if (!m_UserInfo.InvestorID.empty())
		{
			param.InvestorID = m_UserInfo.InvestorID;
		}
		else
		{
			param.InvestorID = m_UserInfo.UserID;
		}
	}
	pOrder->InvestorID = param.InvestorID;
	///合约代码;
	pOrder->InstrumentID=param.InstrumentID;
	///用户代码;
	if (!param.UserID.empty())
	{
		pOrder->UserID = param.UserID;
	}
	else
	{
		param.UserID = m_UserInfo.UserID;
		pOrder->UserID = m_UserInfo.UserID;
	}
	///买卖方向;
	pOrder->Direction = param.Direction;

	pOrder->LimitPrice = param.LimitPrice;
	///组合开平标志;
	memset(pOrder->CombOffsetFlag, 0, sizeof(pOrder->CombOffsetFlag));
	pOrder->CombOffsetFlag[0] = param.CombOffsetFlag[0];
	pOrder->CombOffsetFlag[1] = param.CombOffsetFlag[1];
	///数量: 1;
	pOrder->VolumeTotalOriginal = param.VolumeTotalOriginal;
	///强平原因: 非强平;
	pOrder->ForceCloseReason = FCC_NotForceClose;
	///自动挂起标志: 否;
	pOrder->IsAutoSuspend = 0;
	///用户强评标志: 否;
	pOrder->UserForceClose = 0;
	//最小数量;
	if (param.MinVolume<=0 || param.MinVolume>param.VolumeTotalOriginal)
	{
		pOrder->MinVolume = param.VolumeTotalOriginal;
	}
	else
	{
		pOrder->MinVolume = param.MinVolume;
	}
	///组合投机套保标志:投机;
	if (param.CombHedgeFlag[0] != HF_Speculation
		&& param.CombHedgeFlag[0] != HF_Arbitrage
		&& param.CombHedgeFlag[0] != HF_Hedge)
	{
		//默认情况下使用投机;
		pOrder->CombHedgeFlag[0] = HF_Speculation;
		param.CombHedgeFlag[0] = HF_Speculation;
	}
	else
	{
		memcpy(pOrder->CombHedgeFlag, param.CombHedgeFlag, sizeof(pOrder->CombHedgeFlag));
	}
	
	///报单引用:每次报单是要自增1;
	int cur_order_ref=atoi(m_UserInfo.MaxOrderRef.c_str());
	cur_order_ref++;
	pOrder->OrderRef=std::to_string(cur_order_ref);
	param.OrderRef = pOrder->OrderRef;
	m_UserInfo.MaxOrderRef = pOrder->OrderRef;

	if (param.OrderPriceType==OPT_LimitPrice)
	{
		//限价单;
		//限价单:立即触发,最小成交量为1;
		pOrder->ContingentCondition=CC_Immediately; //立即触发;
		if (pOrder->LimitPrice <= 0.0001f)
		{
			//认为是市价单;
			pOrder->VolumeCondition = VC_AV;//小于最小量则不成交;
			pOrder->OrderPriceType = OPT_AnyPrice;
			pOrder->TimeCondition = TC_IOC;//立即完成1，当日有效3;
			pOrder->LimitPrice = 0.0f;//价格;
		}
		else
		{
			pOrder->VolumeCondition = VC_AV;//小于最小量则不成交;
			pOrder->TimeCondition = TC_GFD;//立即完成，当日有效;
			pOrder->OrderPriceType = OPT_LimitPrice;
		}
	}
	else if (param.OrderPriceType==OPT_AnyPrice)
	{
		//市价;
		pOrder->VolumeCondition = VC_AV;//任意数量1,全部成交
		pOrder->ContingentCondition = CC_Immediately;//立即触发;
		pOrder->OrderPriceType = OPT_AnyPrice;
		pOrder->TimeCondition = TC_IOC;//立即完成1，当日有效3;
		pOrder->LimitPrice = 0.0f;//价格;
	}
	else
	{
		//不支持的类型;
		return false;
	}
	param.FrontID = m_UserInfo.FrontID;
	param.SessionID = m_UserInfo.SessionID;
	pOrder->FrontID = param.FrontID;
	pOrder->SessionID = param.SessionID;

	auto insertRes=m_OrderBook.insert(std::make_pair(param.OrderRef, orderInfo));
	oiIter = insertRes.first;
	oiIter->second.pOrder->OrderStatus = OST_Unknown;
	m_ioService->post(boost::bind(&SimTrader::OnRtnOrder, shared_from_this(), pOrder));
// 	int iResult = m_pTraderApi->ReqOrderInsert(&req, ++RequestID());
// 	return iResult==0;

	return true;
}

bool SimTrader::OrderAction(boost::shared_ptr<InputOrderAction> inputOrderAction)
{
	auto& action = *inputOrderAction;
	std::map<std::string, InputOrder_Order_InputOrderAction_Trade>::iterator oiIter = m_OrderBook.find(action.OrderRef);
	if (oiIter != m_OrderBook.end())
	{
		//报单不存在不能撤单;
		return false;
	}
	if (oiIter->second.pOrder)
	{
		if (oiIter->second.pOrder->OrderStatus != OST_AllTraded)
		{
			oiIter->second.pOrder->OrderStatus = OST_Canceled;
			m_ioService->post(boost::bind(&SimTrader::OnRtnOrder, shared_from_this(), oiIter->second.pOrder));
		}
		else
		{
			//已经成交,不能撤单;
			if (!oiIter->second.pOrderAction)
			{

				oiIter->second.pOrderAction = boost::make_shared<InputOrderAction>();

			}
			m_ioService->post(boost::bind(&SimTrader::OnRspOrderAction, shared_from_this(),
				oiIter->second.pOrderAction,++RequestID(),true));
		}
	}
	else
	{
		if (!oiIter->second.pOrderAction)
		{

			oiIter->second.pOrderAction = boost::make_shared<InputOrderAction>();

		}
		m_ioService->post(boost::bind(&SimTrader::OnRspOrderAction, shared_from_this(),
			oiIter->second.pOrderAction,++RequestID(),true));
	}
	
	return true;
}

SimTrader::~SimTrader()
{
	//LOGDEBUG("Trader对象析构...");
	Release();
	LOGUNINIT();
}

bool SimTrader::ReqUserLogin()
{
	if (m_ioService)
	{
		m_ioService->post(boost::bind(&SimTrader::OnRspUserLogin, shared_from_this(),++RequestID(),true));
	}
	return true;
}

//结算确认;
bool SimTrader::SettlementConfirm()
{
// 	if (m_pTraderApi==NULL)
// 	{
// 		return false;
// 	}
// 	CThostFtdcSettlementInfoConfirmField req;
// 	memset(&req,0,sizeof(CThostFtdcQrySettlementInfoConfirmField));
// 	strcpy(req.BrokerID, m_ServerInfo.id.c_str());
// 	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
// 	//DateTime nowTime=DateTime::Now();
// 	//注意，这里要用交易日确认;
// 	//strncpy(req.ConfirmTime,nowTime.GetLongTimeString().c_str(),sizeof(req.ConfirmTime));
// 	//strncpy(req.ConfirmDate,nowTime.GetShortDateString().c_str(),sizeof(req.ConfirmDate));
// 	int iResult=m_pTraderApi->ReqSettlementInfoConfirm(&req,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult=m_pTraderApi->ReqSettlementInfoConfirm(&req,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

bool SimTrader::QryTradingParam()
{
	if (m_ioService)
	{
		m_ioService->post(boost::bind(&SimTrader::OnRspQryBrokerTradingParams, shared_from_this(),++RequestID(),true));
	}
	return true;
}

bool SimTrader::QryTransBank()
{
// 	if (NULL==m_pTraderApi)
// 	{
// 		//接口没有初始化;
// 		return false;
// 	}
// 	CThostFtdcQryTransferBankField req={0};
// 	int iResult=m_pTraderApi->ReqQryTransferBank(&req,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult=m_pTraderApi->ReqQryTransferBank(&req,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

bool SimTrader::QryNotice()
{
// 	if (NULL==m_pTraderApi)
// 	{
// 		//接口没有初始化;
// 		return false;
// 	}
// 	CThostFtdcQryNoticeField req={0};
// 	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
// 	int iResult=m_pTraderApi->ReqQryNotice(&req,++RequestID());
// 	if(iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult=m_pTraderApi->ReqQryNotice(&req,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

bool SimTrader::QryTradingNotice()
{
// 	if (NULL==m_pTraderApi)
// 	{
// 		//接口没有初始化;
// 		return false;
// 	}
// 	CThostFtdcQryTradingNoticeField req={0};
// 	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
// 	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
// 	int iResult=m_pTraderApi->ReqQryTradingNotice(&req,++RequestID());
// 	if(iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult=m_pTraderApi->ReqQryTradingNotice(&req,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

bool SimTrader::QryInvestor()
{
	if (m_ioService)
	{
		
		m_ioService->post(boost::bind(&SimTrader::OnRspQryInvestor,
			shared_from_this(), ++RequestID(), true));
	}
	return true;
}

bool SimTrader::QryExchange( const std::string& exchgId )
{
	if (m_ioService)
	{
		ExchangeContainer container;
		m_DataStorePtr->LoadExchange(container);
		if (exchgId.empty())
		{
			for (size_t i = 0; i < container.size();++i)
			{
				boost::shared_ptr<ExchangeBase> pExchange = container[i];
				m_ioService->post(boost::bind(&SimTrader::OnRspQryExchange,
					shared_from_this(), pExchange->toExchange(), ++RequestID(), 
					i==container.size()-1));
			}
		}
		else
		{
			boost::shared_ptr<ExchangeBase> pExchange = container.find_exchange_by_id(exchgId);
			boost::shared_ptr<exchange_t> exchange = pExchange->toExchange();
			m_ioService->post(boost::bind(&SimTrader::OnRspQryExchange,
				shared_from_this(), exchange, ++RequestID(), true));
		}
		
	}
	return true;
}

bool SimTrader::QryInstrument( const std::string& instId,const std::string& exchgId )
{
	LOGDEBUG("qry instrument!");
	if (!m_DataStorePtr)
	{
		return false;
	}
	
	if (m_ioService)
	{
		InstrumentContainer container;
		m_DataStorePtr->LoadBasetable(container);
		if (!instId.empty())
		{
			InstrumentInfo info;
			container.GetInstrumentInfo(instId.c_str(), &info);
			m_ioService->post(boost::bind(&SimTrader::OnRspQryInstrument,
				shared_from_this(), info.toInstrument(), ++RequestID(), true));
		}
		else
		{
			for (size_t i = 0; i < container.size();++i)
			{
				m_ioService->post(boost::bind(&SimTrader::OnRspQryInstrument,
					shared_from_this(), container[i].toInstrument(), ++RequestID(), 
					i==container.size()-1));
			}
		}
	}
	return true;
}

bool SimTrader::QryAccount()
{
	//读取配置文件;
	try
	{
		boost::property_tree::ptree pt;
		boost::property_tree::ini_parser::read_ini("TradingAccount.ini", pt);
		std::string sectionName = m_UserInfo.UserID;
		boost::property_tree::ptree ptTradingAccount = pt.get_child(sectionName);

		m_TradingAccount.AccountID = ptTradingAccount.get<std::string>("AccountID", m_UserInfo.InvestorID);
		m_TradingAccount.Available = ptTradingAccount.get<double>("Available", 100000);
		m_TradingAccount.Balance = 0;
		m_TradingAccount.BrokerID = m_UserInfo.BrokerID;
		m_TradingAccount.CashIn = 0;
		m_TradingAccount.CloseProfit = ptTradingAccount.get<double>("CloseProfit", 0);
		m_TradingAccount.Commission = ptTradingAccount.get<double>("Commission", 0);
		m_TradingAccount.Credit = 0;
		m_TradingAccount.CurrencyID = ptTradingAccount.get<std::string>("CurrencyID", "CNY");
		m_TradingAccount.CurrMargin = ptTradingAccount.get<double>("CurrMargin", 0);
		m_TradingAccount.DeliveryMargin = 0;
		m_TradingAccount.Deposit = ptTradingAccount.get<double>("Deposit", 0);
		m_TradingAccount.ExchangeDeliveryMargin = 0;
		m_TradingAccount.ExchangeMargin = 0;
		m_TradingAccount.FrozenCash = 0;
		m_TradingAccount.FrozenCommission = 0;
		m_TradingAccount.FrozenMargin = 0;
		m_TradingAccount.FundMortgageAvailable = 0;
		m_TradingAccount.FundMortgageIn = 0;
		m_TradingAccount.FundMortgageOut = 0;
		m_TradingAccount.Interest = 0;
		m_TradingAccount.InterestBase = 0;
		m_TradingAccount.Mortgage = 0;
		m_TradingAccount.MortgageableFund = ptTradingAccount.get<double>("MortgageableFund", 0);
		m_TradingAccount.PositionProfit = ptTradingAccount.get<double>("PositionProfit", 0);
		m_TradingAccount.PreBalance = ptTradingAccount.get<double>("PreBalance", 0);
		m_TradingAccount.PreCredit = ptTradingAccount.get<double>("PreCredit", 0);
		m_TradingAccount.PreDeposit = ptTradingAccount.get<double>("PreDeposit", 0);
		m_TradingAccount.PreFundMortgageIn = ptTradingAccount.get<double>("PreFundMortgageIn", 0);
		m_TradingAccount.PreFundMortgageOut = ptTradingAccount.get<double>("PreFundMortgageOut", 0);
		m_TradingAccount.PreMargin = ptTradingAccount.get<double>("PreMargin", 0);
		m_TradingAccount.PreMortgage = ptTradingAccount.get<double>("PreMortgage", 0);
		m_TradingAccount.Reserve = ptTradingAccount.get<double>("Reserve", 0);
		m_TradingAccount.ReserveBalance = ptTradingAccount.get<double>("ReserveBalance", 0);
		m_TradingAccount.SettlementID = ptTradingAccount.get<int>("SettlementID", 0);
		m_TradingAccount.SpecProductCloseProfit = ptTradingAccount.get<double>("SpecProductCloseProfit", 0);
		m_TradingAccount.SpecProductCommission = ptTradingAccount.get<double>("SpecProductCommission", 0);
		m_TradingAccount.SpecProductExchangeMargin = ptTradingAccount.get<double>("SpecProductExchangeMargin", 0);
		m_TradingAccount.SpecProductFrozenCommission = ptTradingAccount.get<double>("SpecProductFrozenCommission", 0);
		m_TradingAccount.SpecProductFrozenMargin = ptTradingAccount.get<double>("SpecProductFrozenMargin", 0);
		m_TradingAccount.SpecProductMargin = ptTradingAccount.get<double>("SpecProductMargin", 0);
		m_TradingAccount.SpecProductPositionProfit = ptTradingAccount.get<double>("SpecProductPositionProfit", 0);
		m_TradingAccount.SpecProductPositionProfitByAlg = ptTradingAccount.get<double>("Deposit", 0);
		m_TradingAccount.TradingDay = ptTradingAccount.get<std::string>("TradingDay", "");
		m_TradingAccount.Withdraw = ptTradingAccount.get<double>("Withdraw", 0);
		m_TradingAccount.WithdrawQuota = ptTradingAccount.get<double>("WithdrawQuota", 0);
		m_TradingAccount.TradingDay = m_UserInfo.TradingDay;
	}
	catch (boost::property_tree::ini_parser_error& e1)
	{
		LOGDEBUG("TradingAccount.ini parser error:{}", e1.what());
	}
	catch (std::exception& e1)
	{
		LOGDEBUG("TradingAccount.ini read error:{}", e1.what());
	}
	if (m_ioService)
	{
		m_ioService->post(boost::bind(&SimTrader::OnRspQryTradingAccount,shared_from_this(),
			boost::make_shared<TradingAccount>(m_TradingAccount),
			++RequestID(), true));
	}
	return true;
}

bool SimTrader::QryPosition(const std::string& instrument_id)
{
	//请求查询投资者持仓;
// 	CThostFtdcQryInvestorPositionField req;
// 	memset(&req, 0, sizeof(req));
// 	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
// 	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
// 	strcpy(req.InstrumentID, instrument_id.c_str());
// 	int iResult = m_pTraderApi->ReqQryInvestorPosition(&req,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult = m_pTraderApi->ReqQryInvestorPosition(&req,++RequestID());
// 	}
// 	return iResult==0;
	if (m_ioService)
	{

	}
	return true;
}

bool SimTrader::QryPositionDetail(const std::string& instrument_id)
{
	//查询投资者持仓明细;
// 	struct CThostFtdcQryInvestorPositionDetailField req;
// 	memset(&req, 0, sizeof(req));
// 	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
// 	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
// 	strcpy(req.InstrumentID, instrument_id.c_str());
// 	int iResult = m_pTraderApi->ReqQryInvestorPositionDetail(&req,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult=m_pTraderApi->ReqQryInvestorPositionDetail(&req,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

bool SimTrader::QryCombPosDetail(const std::string& instrument_id)
{
// 	CThostFtdcQryInvestorPositionCombineDetailField req;
// 	memset(&req, 0, sizeof(req));
// 	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
// 	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
// 	strcpy(req.CombInstrumentID, instrument_id.c_str());
// 	int iResult = m_pTraderApi->ReqQryInvestorPositionCombineDetail(&req,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		m_pTraderApi->ReqQryInvestorPositionCombineDetail(&req,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

bool SimTrader::QrySettlementConfirm()
{
	//查询结算信息确认;
// 	if (m_pTraderApi==NULL)
// 	{
// 		return false;
// 	}
// 	//查询用户是否已登录;
// 	//请求查询结算结果确认;
// 	CThostFtdcQrySettlementInfoConfirmField req={0};
// 	//memset(&req,0,sizeof(CThostFtdcQrySettlementInfoConfirmField));
// 	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
// 	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
// 	int iResult=m_pTraderApi->ReqQrySettlementInfoConfirm(&req,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult=m_pTraderApi->ReqQrySettlementInfoConfirm(&req,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

// void SimTrader::OnRspUserLogout( CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspUserPasswordUpdate( CThostFtdcUserPasswordUpdateField *pUserPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspTradingAccountPasswordUpdate( CThostFtdcTradingAccountPasswordUpdateField *pTradingAccountPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspParkedOrderInsert( CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspParkedOrderAction( CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQueryMaxOrderVolume( CThostFtdcQueryMaxOrderVolumeField *pQueryMaxOrderVolume, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspRemoveParkedOrder( CThostFtdcRemoveParkedOrderField *pRemoveParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspRemoveParkedOrderAction( CThostFtdcRemoveParkedOrderActionField *pRemoveParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// //void trader::OnRspExecOrderInsert( CThostFtdcInputExecOrderField *pInputExecOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// //{
// //    //throw std::exception("The method or operation is not implemented.");
// //}
// 
// //void trader::OnRspExecOrderAction( CThostFtdcInputExecOrderActionField *pInputExecOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// //{
// //    //throw std::exception("The method or operation is not implemented.");
// //}
// 
// //void trader::OnRspForQuoteInsert( CThostFtdcInputForQuoteField *pInputForQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// //{
// //    //throw std::exception("The method or operation is not implemented.");
// //}
// 
// //void trader::OnRspQuoteInsert( CThostFtdcInputQuoteField *pInputQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// //{
// //    //throw std::exception("The method or operation is not implemented.");
// //}
// 
// //void trader::OnRspQuoteAction( CThostFtdcInputQuoteActionField *pInputQuoteAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// //{
// //    //throw std::exception("The method or operation is not implemented.");
// //}
// 
// //void trader::OnRspCombActionInsert( CThostFtdcInputCombActionField *pInputCombAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// //{
// //    //throw std::exception("The method or operation is not implemented.");
// //}
// 
// void SimTrader::OnRspQryOrder( CThostFtdcOrderField *pOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQryTrade( CThostFtdcTradeField *pTrade, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQryInvestor( CThostFtdcInvestorField *pInvestor, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return;
// 	}
// 	if (pInvestor)
// 	{
// 		Investor m_Investor;
// 		m_Investor.Address=pInvestor->Address;
// 		m_Investor.BrokerID=pInvestor->BrokerID;
// 		m_Investor.CommModelID=pInvestor->CommModelID;
// 		m_Investor.IdentifiedCardNo=pInvestor->IdentifiedCardNo;
// 		m_Investor.IdentifiedCardType=
// 			(EnumIdCardType)pInvestor->IdentifiedCardType;
// 		m_Investor.InvestorGroupID=pInvestor->InvestorGroupID;
// 		m_Investor.InvestorID=pInvestor->InvestorID;
// 		m_Investor.InvestorName=pInvestor->InvestorName;
// 		m_Investor.IsActive=pInvestor->IsActive;
// 		m_Investor.MarginModelID=pInvestor->MarginModelID;
// 		m_Investor.Mobile=pInvestor->Mobile;
// 		m_Investor.OpenDate=pInvestor->OpenDate;
// 		m_Investor.Telephone=pInvestor->Telephone;
// 
// 		m_UserInfo.InvestorID = m_Investor.InvestorID;
// 
// 		m_InvestorAccount->SafeUpdate(m_Investor);
// 		NotifyApiMgr(RspQryInvestor,LPARAM(&m_Investor));
// 	}
// 	LOGDEBUG("查询投资者响应...");
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		if (!QryExchange(""))
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryTradingCode( CThostFtdcTradingCodeField *pTradingCode, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQryInstrumentMarginRate( CThostFtdcInstrumentMarginRateField *pInstrumentMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//查手续费;
// 	
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
// 		return;
// 	}
// 	//保存;
// 	if (pInstrumentMarginRate)
// 	{
// 		InstrumentMarginRate imr;
// 		imr.BrokerID = pInstrumentMarginRate->BrokerID;
// 		imr.HedgeFlag = (EnumHedgeFlag)pInstrumentMarginRate->HedgeFlag;
// 		imr.InstrumentID = pInstrumentMarginRate->InstrumentID;
// 		imr.InvestorID = pInstrumentMarginRate->InvestorID;
// 		imr.InvestorRange = (EnumInvestorRange)pInstrumentMarginRate->InvestorRange;
// 		imr.IsRelative = pInstrumentMarginRate->IsRelative;
// 		imr.LongMarginRatioByMoney = pInstrumentMarginRate->LongMarginRatioByMoney;
// 		imr.LongMarginRatioByVolume = pInstrumentMarginRate->LongMarginRatioByVolume;
// 		imr.ShortMarginRatioByMoney = pInstrumentMarginRate->ShortMarginRatioByMoney;
// 		imr.ShortMarginRatioByVolume = pInstrumentMarginRate->ShortMarginRatioByVolume;
// 		NotifyApiMgr(RspQryInstMarginRate, LPARAM(&imr));
// 	}
// 	if (bIsLast && !m_UserInfo.IsTraderLogined)
// 	{
// 		if (!QryAccount())
// 		{
// 			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryProduct( CThostFtdcProductField *pProduct, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQryDepthMarketData( CThostFtdcDepthMarketDataField *pDepthMarketData, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQrySettlementInfo( CThostFtdcSettlementInfoField *pSettlementInfo, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return;
// 	}
// 	if (pSettlementInfo)
// 	{
// 		SettlementInfo m_SettlementInfo;
// 		m_SettlementInfo.BrokerID=pSettlementInfo->BrokerID;
// 		m_SettlementInfo.Content+=pSettlementInfo->Content;
// 		m_SettlementInfo.InvestorID=pSettlementInfo->InvestorID;
// 		m_SettlementInfo.SequenceNo=pSettlementInfo->SequenceNo;
// 		m_SettlementInfo.SettlementID=pSettlementInfo->SettlementID;
// 		m_SettlementInfo.TradingDay=pSettlementInfo->TradingDay;
// 		//std::shared_ptr<InvestorAccount> m_ptrInvestor=
// 		//	InvestorAccountMgr::GetInstance().GetUserAccount(m_SettlementInfo.InvestorID,m_SettlementInfo.BrokerID);
// 		//if (m_ptrInvestor)
// 		//{
// 		//	m_ptrInvestor->SafeUpdate(m_SettlementInfo);
// 		//}
// 		NotifyApiMgr(QuerySettlementInfo,LPARAM(&m_SettlementInfo));
// 	}
// 	LOGDEBUG("查询结算信息响应...");
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		//结算信息确认;
// 		if (!SettlementConfirm())
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryTransferBank( CThostFtdcTransferBankField *pTransferBank, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if ((bIsLast && IsErrorRspInfo(pRspInfo)) )
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return;
// 	}
// 	if (pTransferBank)
// 	{
// 		static TransferBank tb;
// 		tb.BankID=pTransferBank->BankID;
// 		tb.BankBrchID=pTransferBank->BankBrchID;
// 		tb.BankName=pTransferBank->BankName;
// 		tb.IsActive=pTransferBank->IsActive;
// 		//TraderSharedData::GetInstance().transferBanks.push_back(tb);
// 		NotifyApiMgr(RspQryTransferBank,LPARAM(&tb));
// 		if (bIsLast)
// 		{
// 			LOGDEBUG("查询转账银行响应...");
// 		}
// 	}
// 
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		if (!QryNotice())
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryInvestorPositionDetail( CThostFtdcInvestorPositionDetailField *pInvestorPositionDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return;
// 	}
// 	if (pInvestorPositionDetail)
// 	{
// 		InvestorPositionDetail ipd;
// 		ipd.BrokerID=pInvestorPositionDetail->BrokerID;
// 		ipd.CloseAmount=pInvestorPositionDetail->CloseAmount;
// 		ipd.CloseProfitByDate=pInvestorPositionDetail->CloseProfitByDate;
// 		ipd.CloseProfitByTrade=pInvestorPositionDetail->CloseProfitByTrade;
// 		ipd.CloseVolume=pInvestorPositionDetail->CloseVolume;
// 		ipd.CombInstrumentID=pInvestorPositionDetail->CombInstrumentID;
// 		LOGDEBUG("查询投资者持仓响应:{}",pInvestorPositionDetail->InstrumentID);
// 		m_InvestorAccount->SafeUpdate(ipd);
// 		NotifyApiMgr(RspQryPositionDetail,LPARAM(&ipd));
// 	}
// 
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		if (!QryCombPosDetail(""))
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryNotice( CThostFtdcNoticeField *pNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//查询通知;
// 	
// 	if (IsErrorRspInfo(pRspInfo) && bIsLast)
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	if (pNotice)
// 	{
// 		m_Notice.Content+=pNotice->Content;
// 		m_Notice.SequenceLabel.push_back(pNotice->SequenceLabel);
// 		if (bIsLast)
// 		{
// 			NotifyApiMgr(RspQryNotice,LPARAM(&m_Notice));
// 		}
// 	}
// 	LOGDEBUG("查询通知响应...");
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		//继续登录;
// 		if (!QryTradingNotice())
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryInvestorPositionCombineDetail( CThostFtdcInvestorPositionCombineDetailField *pInvestorPositionCombineDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if (bIsLast && IsErrorRspInfo(pRspInfo))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	if (pInvestorPositionCombineDetail)
// 	{
// 		InvestorPositionCombineDetail ipcd;
// 		ipcd.BrokerID=pInvestorPositionCombineDetail->BrokerID;
// 		ipcd.CombInstrumentID=pInvestorPositionCombineDetail->CombInstrumentID;
// 		ipcd.ComTradeID=pInvestorPositionCombineDetail->ComTradeID;
// 		ipcd.Direction=(EnumDirection)pInvestorPositionCombineDetail->Direction;
// 		ipcd.ExchangeID=pInvestorPositionCombineDetail->ExchangeID;
// 		ipcd.ExchMargin=pInvestorPositionCombineDetail->ExchMargin;
// 		ipcd.HedgeFlag=(EnumHedgeFlag)pInvestorPositionCombineDetail->HedgeFlag;
// 		ipcd.InstrumentID=pInvestorPositionCombineDetail->InstrumentID;
// 		ipcd.InvestorID=pInvestorPositionCombineDetail->InvestorID;
// 		ipcd.LegID=pInvestorPositionCombineDetail->LegID;
// 		ipcd.LegMultiple=pInvestorPositionCombineDetail->LegMultiple;
// 		ipcd.Margin=pInvestorPositionCombineDetail->Margin;
// 		ipcd.MarginRateByMoney=pInvestorPositionCombineDetail->MarginRateByMoney;
// 		ipcd.MarginRateByVolume=pInvestorPositionCombineDetail->MarginRateByVolume;
// 		ipcd.OpenDate=pInvestorPositionCombineDetail->OpenDate;
// 		ipcd.SettlementID=pInvestorPositionCombineDetail->SettlementID;
// 		ipcd.TotalAmt=pInvestorPositionCombineDetail->TotalAmt;
// 		ipcd.TradeGroupID=pInvestorPositionCombineDetail->TradeGroupID;
// 		ipcd.TradeID=pInvestorPositionCombineDetail->TradeID;
// 		ipcd.TradingDay=pInvestorPositionCombineDetail->TradingDay;
// 		m_InvestorAccount->SafeUpdate(ipcd);
// 		NotifyApiMgr(RspQryCombPosition,LPARAM(&ipcd));
// 	}
// 	LOGDEBUG("查询投资者组合持仓响应...");
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		if (!QrySettlementConfirm())
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryExchangeMarginRate( CThostFtdcExchangeMarginRateField *pExchangeMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQryExchangeRate( CThostFtdcExchangeRateField *pExchangeRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnErrRtnOrderInsert( CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo )
// {
// 	
// 	LOGDEBUG("OnErrRtnOrderInsert响应...");
// 	if (pInputOrder)
// 	{
// 		Order inputOrder;
// 		inputOrder.BrokerID=pInputOrder->BrokerID;
// 		strcpy((char*)inputOrder.CombHedgeFlag,pInputOrder->CombHedgeFlag);
// 		strcpy((char*)inputOrder.CombOffsetFlag,pInputOrder->CombOffsetFlag);
// 		inputOrder.ContingentCondition=pInputOrder->ContingentCondition;
// 		inputOrder.Direction=(EnumDirection)pInputOrder->Direction;
// 		inputOrder.GTDDate=pInputOrder->Direction;
// 		inputOrder.InstrumentID=pInputOrder->InstrumentID;
// 		inputOrder.InvestorID=pInputOrder->InvestorID;
// 		inputOrder.LimitPrice=pInputOrder->LimitPrice;
// 		inputOrder.MinVolume=pInputOrder->MinVolume;
// 		inputOrder.OrderPriceType=(EnumOrderPriceType)pInputOrder->OrderPriceType;
// 		inputOrder.OrderRef=pInputOrder->OrderRef;
// 		inputOrder.StopPrice=pInputOrder->StopPrice;
// 		inputOrder.TimeCondition=pInputOrder->TimeCondition;
// 		inputOrder.UserID=pInputOrder->UserID;
// 		inputOrder.VolumeCondition=pInputOrder->VolumeCondition;
// 		inputOrder.VolumeTotalOriginal=pInputOrder->VolumeTotalOriginal;
// 		inputOrder.RequestID = pInputOrder->RequestID;
// 		if (IsErrorRspInfo(pRspInfo) && m_UserInfo.IsTraderLogined)
// 		{
// 			inputOrder.ErrorID = pRspInfo->ErrorID;
// 			inputOrder.ErrorMsg = pRspInfo->ErrorMsg;
// 			NotifyApiMgr(RspOrderInsert, LPARAM(&inputOrder));
// 		}
// 	}
// }
// 
// void SimTrader::OnRtnInstrumentStatus( CThostFtdcInstrumentStatusField *pInstrumentStatus )
// {
// 	if (pInstrumentStatus)
// 	{
// 		//InstStatus is;
// 		//is.ExchangeID=pInstrumentStatus->ExchangeID;
// 		//is.InstrumentID=pInstrumentStatus->InstrumentID;
// 		//is.SettlementGroupID=pInstrumentStatus->SettlementGroupID;
// 		//is.InstrumentStatus=(EnumInstStatus)pInstrumentStatus->InstrumentStatus;
// 		//is.TradingSegmentSN=pInstrumentStatus->TradingSegmentSN;
// 		//is.EnterTime=pInstrumentStatus->EnterTime;
// 		//is.EnterReason=(EnumInstStatusEnterReason)pInstrumentStatus->EnterReason;
// 		//std::pair<std::map<std::string,InstStatus>::iterator,bool> inst_iter_b=
// 		//	TraderSharedData::GetInstance().instStatus.insert(
// 		//	std::map<std::string,InstStatus>::value_type(is.InstrumentID,is));
// 		//if (!inst_iter_b.second)
// 		//{
// 		//	inst_iter_b.first->second=is;
// 		//}
// 		//NotifyApiMgr(RtnInstrumentStatus,LPARAM(&is));
// 	}
// }
// 
// void SimTrader::OnRtnTradingNotice( CThostFtdcTradingNoticeInfoField *pTradingNoticeInfo )
// {
// 	if (pTradingNoticeInfo)
// 	{
// 
// 	}
// }
// 
// //void trader::OnRtnQuote( CThostFtdcQuoteField *pQuote )
// //{
// //    //throw std::exception("The method or operation is not implemented.");
// //}
// 
// void SimTrader::OnRspQryParkedOrder( CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQryParkedOrderAction( CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }
// 
// void SimTrader::OnRspQryTradingNotice( CThostFtdcTradingNoticeField *pTradingNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if ((bIsLast && IsErrorRspInfo(pRspInfo)))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	if (pTradingNotice)
// 	{
// 		static TradingNotice tn;
// 		tn.BrokerID=pTradingNotice->BrokerID;
// 		tn.InvestorID=pTradingNotice->InvestorID;
// 		tn.InvestorRange=(EnumInvestorRange)pTradingNotice->InvestorRange;
// 		tn.SendTime=pTradingNotice->SendTime;
// 		tn.SequenceNo=pTradingNotice->SequenceNo;
// 		tn.SequenceSeries=pTradingNotice->SequenceSeries;
// 		NotifyApiMgr(RspQryTradingNotice,LPARAM(&tn));
// 	}
// 	LOGDEBUG("查询交易通知响应...");
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		if (!QryInvestor())
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryBrokerTradingParams( CThostFtdcBrokerTradingParamsField *pBrokerTradingParams, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	
// 	if ((bIsLast && IsErrorRspInfo(pRspInfo)))
// 	{
// 		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		return ;
// 	}
// 	LOGDEBUG("查询交易参数响应...");
// 	if (pBrokerTradingParams)
// 	{
// 		m_BrokerTradingParams.Algorithm=(EnumAlgorithm)pBrokerTradingParams->Algorithm;
// 		m_BrokerTradingParams.AvailIncludeCloseProfit=(EnumIncludeCloseProfit)pBrokerTradingParams->AvailIncludeCloseProfit;
// 		m_BrokerTradingParams.CurrencyID=pBrokerTradingParams->CurrencyID;
// 		m_BrokerTradingParams.MarginPriceType=(EnumMarginPrice)pBrokerTradingParams->MarginPriceType;
// 		//m_BrokerTradingParams.OptionRoyaltyPriceType=(EnumOptionRoyaltyPrice)pBrokerTradingParams->OptionRoyaltyPriceType;
// 		NotifyApiMgr(RspQryBrokerTradingParams,LPARAM(&m_BrokerTradingParams));
// 	}
// 
// 	if (!m_UserInfo.IsTraderLogined && bIsLast)
// 	{
// 		if(!QryTransBank())
// 		{
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
// 	}
// }
// 
// void SimTrader::OnRspQryBrokerTradingAlgos( CThostFtdcBrokerTradingAlgosField *pBrokerTradingAlgos, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
// {
// 	//throw std::exception("The method or operation is not implemented.");
// }


bool SimTrader::QrySettlementInfo()
{
// 	if (!m_pTraderApi)
// 	{
// 		return false;
// 	}
// 	//没有结算过:查结算信息;
// 	CThostFtdcQrySettlementInfoField qrySettlementInfo={0};
// 	strcpy(qrySettlementInfo.BrokerID,m_UserInfo.BrokerID.c_str());
// 	strcpy(qrySettlementInfo.InvestorID,m_UserInfo.UserID.c_str());
// 	//strcpy_s(qrySettlementInfo.TradingDay,curTime.FormatStd("%Y%m%d").c_str());
// 	int iResult=m_pTraderApi->ReqQrySettlementInfo(&qrySettlementInfo,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult=m_pTraderApi->ReqQrySettlementInfo(&qrySettlementInfo,++RequestID());
// 	}
// 	return iResult==0;
	return true;
}

bool SimTrader::QryDepthMarket(const std::string& instId)
{
	return false;
}

bool SimTrader::ParkedOrderAction()
{
	return false;
}

bool SimTrader::QryMaxOrderVolume(const std::string& instId)
{
	return false;
}

bool SimTrader::QryMarginRate(const std::string& instId)
{
// 	if (!m_pTraderApi)
// 	{
// 		return false;
// 	}
// 	//查询合约保证金率;
// 	CThostFtdcQryInstrumentMarginRateField req = { 0 };
// 	strcpy(req.BrokerID, m_UserInfo.BrokerID.c_str());
// 	strcpy(req.InvestorID, m_UserInfo.InvestorID.c_str());
// 	strcpy(req.InstrumentID, instId.c_str());
// 	int iResult=m_pTraderApi->ReqQryInstrumentMarginRate(&req,++RequestID());
// 	if (iResult!=0)
// 	{
// 		Sleep(1000);
// 		iResult = m_pTraderApi->ReqQryInstrumentMarginRate(&req, ++RequestID());
// 	}
// 	return iResult == 0;
	return true;
}

bool SimTrader::ParkedOrderInsert()
{
	return false;
}

std::string SimTrader::GetApiVersion()
{
	//if (!m_pTraderApi)
	//{
	//	return "0.0.0.0";
	//}
	//return m_pTraderApi->GetApiVersion();
	return "6.3.5.1";
}

std::string SimTrader::GetTradingDay()
{
	if (m_TradingAccount.TradingDay.empty())
	{
		//取当前时间;
		boost::posix_time::ptime dtLocal = boost::posix_time::second_clock::local_time();
		return boost::gregorian::to_iso_string(dtLocal.date());
	}
	return m_TradingAccount.TradingDay;
}

void SimTrader::OnFrontConnected()
{
	ReqUserLogin();
}

void SimTrader::OnRspUserLogin(int nRequestID, bool bIsLast)
{
	try
	{
		boost::property_tree::ptree ptSim;
		boost::property_tree::ini_parser::read_ini("SIM.ini", ptSim);
		//默认为今天;
		auto current_time = boost::posix_time::second_clock::local_time();
		m_UserInfo.TradingDay = ptSim.get<std::string>("Mode.TradingDay", boost::gregorian::to_iso_string(current_time.date()));

		m_UserInfo.LoginTime = ptSim.get<std::string>("Mode.LoginTime", boost::posix_time::to_simple_string(current_time.time_of_day()));

		m_UserInfo.ExchangeTime = m_UserInfo.LoginTime;
	}
	catch (boost::property_tree::ini_parser_error& e1)
	{
		LOGDEBUG("SIM.ini parser error:{}", e1.what());
	}
	catch (std::exception& e1)
	{
		LOGDEBUG("SIM.ini read error:{}", e1.what());
	}
	m_UserInfo.SystemName = "SIM";
	m_UserInfo.FrontID = 1;
	m_UserInfo.SessionID = time(nullptr);
	m_UserInfo.MaxOrderRef = "2";
	//读取配置文件;
	if (!QryTradingParam())
	{
		m_UserInfo.IsTraderLogined = true;
		NotifyUserLogin(nRequestID,bIsLast);
	}
}

void SimTrader::NotifyUserLogin(int nRequestID, bool bIsLast)
{
	if (m_UserInfo.IsTraderLogined)
	{
		sigUserLogin(m_UserInfo);
		m_LoginCondVar.notify_one();
	}
}

void SimTrader::OnRspQryBrokerTradingParams(int nRequestID, bool bIsLast)
{
	m_BrokerTradingParams.Algorithm = AG_All;
	m_BrokerTradingParams.AvailIncludeCloseProfit = ICP_Include;
	m_BrokerTradingParams.CurrencyID = "CNY";
	m_BrokerTradingParams.MarginPriceType = MPT_PreSettlementPrice;
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryInvestor())
		{
			NotifyUserLogin(nRequestID, bIsLast);
		}
	}
}

void SimTrader::OnRspQryInvestor(int nRequestID, bool bIsLast)
{

	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryExchange(""))
		{
			NotifyUserLogin(nRequestID, bIsLast);
		}
	}
}

void SimTrader::OnRspError(int nRequestID, bool bIsLast)
{

}

void SimTrader::OnRspQryExchange(boost::shared_ptr<exchange_t>, int nRequestID, bool bIsLast)
{

	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		//能源交易所;
		std::string INE_EXCHANGE_ID("INE");
		boost::shared_ptr<exchange_t> exchange = boost::make_shared<exchange_t>();
		strcpy(exchange->ExchangeID, INE_EXCHANGE_ID.c_str());
		strcpy(exchange->ExchangeName, "上海能源交易所");
		//exchange->ExchangeProperty = (EnumExchangeProperty)pExchange->ExchangeProperty;
		sigExchangeInfo(exchange);
		if (!QryInstrument("", ""))
		{
			NotifyUserLogin( nRequestID, bIsLast);
		}
	}
}

void SimTrader::OnRspQryInstrument(boost::shared_ptr<Instrument> pInstruemnt, int nRequestID, bool bIsLast)
{
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		if (!QryAccount())
		{
			NotifyUserLogin(/*pRspInfo,*/ nRequestID, bIsLast);
		}
	}
}

void SimTrader::OnRspQryTradingAccount(boost::shared_ptr<TradingAccount> pTA,int nRequestID, bool bIsLast)
{
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		//if (!QryPosition(""))
		{
			m_UserInfo.IsTraderLogined = true;
			NotifyUserLogin( nRequestID, bIsLast);
		}
	}
}

void SimTrader::OnRspQryInvestorPosition(int nRequestID, bool bIsLast)
{
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		m_UserInfo.IsTraderLogined = true;
		NotifyUserLogin(nRequestID, bIsLast);
	}
}

void SimTrader::OnRtnOrder(boost::shared_ptr<Order> pOrder/*CThostFtdcOrderField *pOrder*/)
{
	if (pOrder)
	{
		boost::shared_ptr<Trade> pTrade=SimHub::get_instance().OnRtnOrder(pOrder);
		sigOnOrder(boost::make_shared<Order>(*pOrder));
		if (pTrade)
		{
			OnRtnTrade(pTrade);
		}
	}
}

void SimTrader::OnRtnTrade(boost::shared_ptr<Trade> pTrade/*CThostFtdcTradeField *pTrade*/)
{
	if (pTrade)
	{
		sigOnTrade(boost::make_shared<Trade>(*pTrade));
	}
}

void SimTrader::OnRtnInstrumentStatus(boost::shared_ptr<InstStatus> pInstrumentStatus)
{
	if (pInstrumentStatus)
	{
		sigInstrumentStatus(pInstrumentStatus);
	}
}

void SimTrader::SetTradingDay(const std::string& tradingDay)
{
	m_TradingAccount.TradingDay = tradingDay;
	m_UserInfo.TradingDay = tradingDay;
}

boost::shared_ptr<InstrumentCommisionRate> SimTrader::GetInstCommissionRate(const std::string& instId)
{
	auto commIter = m_InstCommisionRates.find(instId);
	if (commIter == m_InstCommisionRates.end())
	{
		return nullptr;
	}
	return commIter->second;
}

boost::shared_ptr<InstrumentMarginRate> SimTrader::GetInstMarginRate(const std::string& instId)
{
	auto marginIter = m_InstMarginRates.find(instId);
	if (marginIter == m_InstMarginRates.end())
	{
		return nullptr;
	}
	return marginIter->second;
}

boost::shared_ptr<BrokerTradingParams> SimTrader::GetBrokerTradingParams()
{
	return boost::make_shared<BrokerTradingParams>(m_BrokerTradingParams);
}

bool SimTrader::QryInstCommissionRate(const std::string& instId)
{
	return true;
}

bool SimTrader::QryInstMarginRate(const std::string& instId)
{
	return true;
}








