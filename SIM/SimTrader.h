#ifndef _SIM_TRADER_H_
#define _SIM_TRADER_H_
#include "DataTypes.h"
#include "ServerInfo.h"
#include "UserInfo.h"
#include "../ServiceLib/Trader.h"

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/enable_shared_from_this.hpp>

class IDataStore;
//交易接口;
class SimTrader : public Trader,public boost::enable_shared_from_this<SimTrader>
{
public:
	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected();
	///登录请求响应
	virtual void OnRspUserLogin(int nRequestID, bool bIsLast);

	void OnRspOrderInsert(boost::shared_ptr<InputOrder> pInputOrder,int nRequestID, bool bIsLast);
	///报单操作请求响应
	virtual void OnRspOrderAction(boost::shared_ptr<InputOrderAction> inputOrderAction, int nRequestID, bool bIsLast);

	///请求查询投资者持仓响应
	virtual void OnRspQryInvestorPosition(int nRequestID, bool bIsLast);

	///请求查询资金账户响应
	//virtual void OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);


	///请求查询经纪公司交易参数响应
	virtual void OnRspQryBrokerTradingParams(int nRequestID, bool bIsLast);

	///请求查询投资者响应
	virtual void OnRspQryInvestor(int nRequestID, bool bIsLast);

	void OnRspQryTradingAccount(boost::shared_ptr<TradingAccount> pTA,int nRequestID, bool bIsLast);
	///请求查询合约保证金率响应
	//virtual void OnRspQryInstrumentMarginRate(CThostFtdcInstrumentMarginRateField *pInstrumentMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///请求查询合约手续费率响应
	//virtual void OnRspQryInstrumentCommissionRate(CThostFtdcInstrumentCommissionRateField *pInstrumentCommissionRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///请求查询交易所响应
	virtual void OnRspQryExchange(boost::shared_ptr<exchange_t>,int nRequestID, bool bIsLast);

	///请求查询产品响应
	//virtual void OnRspQryProduct(CThostFtdcProductField *pProduct, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);

	///请求查询合约响应
	virtual void OnRspQryInstrument(boost::shared_ptr<Instrument> pInstruemnt, int nRequestID, bool bIsLast);
	///错误应答
	virtual void OnRspError(int nRequestID, bool bIsLast);

	///报单通知
	virtual void OnRtnOrder(boost::shared_ptr<Order> pOrder/*CThostFtdcOrderField *pOrder*/);

	///成交通知
	virtual void OnRtnTrade(boost::shared_ptr<Trade> pTrade/*CThostFtdcTradeField *pTrade*/);

	///合约交易状态通知
	virtual void OnRtnInstrumentStatus(boost::shared_ptr<InstStatus> pInstrumentStatus);
protected:
	void NotifyUserLogin(int nRequestID,bool bIsLast);
public:
	//功能函数;
	//请求登录;
	bool ReqUserLogin();
	//结算信息确认;
	bool SettlementConfirm();
	//查询交易参数;
	bool QryTradingParam();
	//查询转帐银行;
	bool QryTransBank();
	//查询通知;
	bool QryNotice();
	//查询交易通知;
	bool QryTradingNotice();
	//查询投资者;
	bool QryInvestor();
	//查询交易所;
	bool QryExchange(const std::string& exchangeId);
	//查询合约;
	bool QryInstrument(const std::string& instId,const std::string& exchgId);
	//查询帐户资金;
	bool QryAccount();
	//查询报单;

	//查询持仓;
	bool QryPosition(const std::string& instId);
	//查询持仓明细;
	bool QryPositionDetail(const std::string& instId);
	//查询组合持仓;
	bool QryCombPosDetail(const std::string& instId);
	//查询结算信息确认;
	bool QrySettlementConfirm();
	//查询结算信息;
	bool QrySettlementInfo();
	//查询保证金;
	bool QryMarginRate(const std::string& instId);
	//查询最大下单量;
	bool QryMaxOrderVolume(const std::string& instId);
	//报单;
	bool OrderInsert(boost::shared_ptr<InputOrder> inputOrder);
	//撤单;
	bool OrderAction(boost::shared_ptr<InputOrderAction> inputOrderAction);
	//预埋单;
	bool ParkedOrderInsert();
	//预埋撤单;
	bool ParkedOrderAction();
	//查询行情;
	bool QryDepthMarket(const std::string& instId);

	//查询手续费;
	bool QryInstCommissionRate(const std::string& instId);
	bool QryInstMarginRate(const std::string& instId);

	//此函数要登录后使用;
	boost::shared_ptr<InstrumentCommisionRate> GetInstCommissionRate(const std::string& instId);
	boost::shared_ptr<InstrumentMarginRate> GetInstMarginRate(const std::string& instId);
	//交易计算参数;
	virtual boost::shared_ptr<BrokerTradingParams> GetBrokerTradingParams();
public:
	std::string GetApiVersion();
	std::string GetTradingDay();

public:
	//交易参数;
	BrokerTradingParams m_BrokerTradingParams;
	//用户通知;
	Notice m_Notice;
	//结算确认;
	SettlementInfoConfirm m_SettlementInfoConfirm;
	TradingAccount m_TradingAccount;
	//合约手续费;
	std::map<std::string,boost::shared_ptr<InstrumentCommisionRate> > m_InstCommisionRates;
	//保证金率;
	std::map<std::string, boost::shared_ptr<InstrumentMarginRate> > m_InstMarginRates;
public:
	//构造函数;
	SimTrader();
	virtual ~SimTrader();
	//初始化;
	bool Init(const ServerInfo& s);
	//登录;
	bool Login(const UserLoginParam& u);
	//释放接口对象;
	void Release();

	void SetTradingDay(const std::string& tradingDay);
protected:
	SimTrader(const SimTrader& other);
	SimTrader& operator =(const SimTrader& other);
protected:
	boost::shared_ptr<IDataStore> m_DataStorePtr;
	mutex m_Mutex4QryInstCommisonRate;
	condition_variable m_CV4QryInstCommisonRate;
	std::shared_ptr<thread> thrd4QryCommisionRate;
protected:
	//异步模拟器;
	boost::shared_ptr<boost::asio::io_service> m_ioService;
	boost::shared_ptr<boost::asio::io_service::work> m_ioWork;
	boost::shared_ptr<boost::thread> m_ioThread;

	struct InputOrder_Order_InputOrderAction_Trade
	{
		boost::shared_ptr<InputOrder> pInputOrder;
		boost::shared_ptr<Order> pOrder;
		boost::shared_ptr<InputOrderAction> pOrderAction;
		boost::shared_ptr<Trade> pTrade;
	};
	//报单关联项;
	std::map<std::string, InputOrder_Order_InputOrderAction_Trade> m_OrderBook;
};
#endif
