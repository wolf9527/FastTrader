#include "simple_simulation.h"
#include "../DataStoreLib/DataStoreManager.h"

simple_simulation::simple_simulation(void)
	:m_stop_and_reset(false)
{
	m_startegy=NULL;
	m_thread=NULL;
}


simple_simulation::~simple_simulation(void)
{
	if (NULL!=m_startegy)
	{
		delete m_startegy;
		m_startegy=NULL;
	}
}

void simple_simulation::set_strategy( simple_strategy* pStrategy )
{
	m_startegy=new SimulatorStrategy();
	m_startegy->set_strategy(pStrategy);
	m_startegy->ClearCache();

}

bool simple_simulation::download_data_if_need()
{
	if( NULL == m_startegy )
		return false;

	DateTime	tmInitial, tmPioneer, tmLatest;
	DateTime	tmBegin	=	m_startegy->GetOpParam().GetBeginTime();
	DateTime	tmEnd	=	m_startegy->GetOpParam().GetEndTime();
	DateTime	tmDLBegin = tmBegin, tmDLEnd = tmEnd;
	bool	bNeedDownload	=	false;

	for( int i=0; i<m_startegy->GetInstruments().size(); i++ )
	{
		DateTime	tmDLBeginLocal, tmDLEndLocal;
		instrument_info	info;
		if( instrument_container::get_instance().GetInstrumentInfo( m_startegy->GetInstruments().at(i).c_str(), &info )
		&& DataStoreManager::GetInstance()->GetDefault()->GetNeedDownloadRange( info, tmBegin, tmEnd, tmDLBeginLocal, tmDLEndLocal ) )
		{
			if( !bNeedDownload )
			{
				tmDLBegin	=	tmDLBeginLocal;
				tmDLEnd		=	tmDLEndLocal;
			}
			bNeedDownload	=	true;
			if( tmDLBeginLocal < tmDLBegin )
				tmDLBegin	=	tmDLBeginLocal;
			if( tmDLEndLocal > tmDLEnd )
				tmDLEnd		=	tmDLEndLocal;
		}
	}
	return true;
}

void simple_simulation::restart()
{
	DEBUG_METHOD();
	DEBUG_PRINTF("simple_simulation:restart");
	assert( m_startegy);
	m_stop_and_reset		=	false;

	if( NULL == m_startegy )
		return;

	if( !download_data_if_need())
		return;

	m_startegy->ClearCache();
	m_startegy->SimuReset();
	m_startegy->SimuSetStatusRunning();
	m_thread=new tthread::thread(thread_callback,this);
}

void simple_simulation::pause()
{
	assert( m_startegy );
	m_stop_and_reset		=	false;

	if( m_startegy )
		m_startegy->SimuSetStatusPaused();
}

void simple_simulation::stop()
{
	DEBUG_METHOD();
	DEBUG_PRINTF("simple_simulation::stop-->Enter");
	assert(m_startegy);
	m_stop_and_reset=true;
	//�����ź�;
	if (m_startegy && m_startegy->SimuIsStatusPaused())
	{
		on_end(false);
	}
	DEBUG_PRINTF("simple_simulation::stop-->Leave");
}

void simple_simulation::on_end( bool bFinished )
{
	assert(m_startegy);

	if( bFinished && m_startegy )
	{
		m_startegy->SimuSetStatusFinished();
	}
	if( m_stop_and_reset && m_startegy )
	{
		m_startegy->SimuReset();
		m_startegy->SimuSetStatusInit();
	}
}

void simple_simulation::continus()
{
	assert( m_startegy );
	m_stop_and_reset		=	false;

	if( m_startegy )
	{
		m_startegy->SimuSetStatusRunning();
	}
}

SimulatorStrategy* simple_simulation::get_strategy()
{
	return m_startegy;
}

void simple_simulation::set_monitor( simulator_monitor* pMonitor )
{
	m_monitor=pMonitor;
}


void thread_callback( void* thread_arg )
{
	DEBUG_METHOD();
	DEBUG_PRINTF("simple_simulation:thread_callback-->Enter");
	simple_simulation* pSimulator=
		reinterpret_cast<simple_simulation*>(thread_arg);
	if (pSimulator)
	{
		pSimulator->m_startegy->ClearCache();
		pSimulator->m_startegy->SimuRun(pSimulator->m_monitor,pSimulator);
	}
	DEBUG_PRINTF("simple_simulation:thread_callback-->Leave");
}
