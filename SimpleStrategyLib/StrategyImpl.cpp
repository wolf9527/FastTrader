#include "StrategyImpl.h"
#include "../Log/logging.h"
#include "../ServiceLib/UserApiMgr.h"
#include "../ServiceLib/Trader.h"
#include "../ServiceLib/Market.h"
#include "../ServiceLib/InvestorAccount.h"
#include "../DataStoreLib/DataStoreFace.h"
#include "../FacilityBaseLib/Container.h"
#include "UserInfo.h"
#include "csv_output.h"
#include <tuple>

#ifdef REDIS_STORE_MONITOR
#include "../RedisStore/RedisStore.h"
#endif

#include <boost/lambda/lambda.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/archive/tmpdir.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <fstream>

StrategyImpl::StrategyImpl(const std::string& id)
	:Portfolio(id)
{
	is_run = false;
	LOGINIT("StrategyImpl");
	//默认1年前的数据;
	auto start_date = boost::gregorian::day_clock::local_day() - boost::gregorian::years(1);
	m_StartDay = boost::gregorian::to_iso_string(start_date);
}


StrategyImpl::~StrategyImpl(void)
{
}

bool StrategyImpl::IsRun() const
{
	return is_run;
}

void StrategyImpl::Start()
{
	if (m_Thread)
	{
		return ;
	}
	//记录时间;
	//RedisStore::get_instance().pub_timeline("strategy start","the strategy:"+m_Id+"is ready to run");
	//RedisStore::get_instance().pub_timeline("strategy running", std::string("instrument:") + "aaaaa" + " last price is:" + "1111");
// 	if (!m_SelfSlots.OnStatus.slot_function())
// 	{
// 		m_SelfSlots.OnStatus = boost::bind(&StrategyImpl::OnStatus, shared_from_this(), boost::lambda::_1);
// 	}
// 	
// 	if (!m_SelfSlots.OnOrder.slot_function())
// 	{
// 		m_SelfSlots.OnOrder = boost::bind(&StrategyImpl::OnOrder, shared_from_this(), boost::lambda::_1);
// 	}
// 	
// 	if (!m_SelfSlots.OnTrade.slot_function())
// 	{
// 		m_SelfSlots.OnTrade = boost::bind(&StrategyImpl::OnTrade, shared_from_this(), boost::lambda::_1);
// 	}
// 
// 	if (!m_SelfSlots.OnTrade.slot_function())
// 	{
// 		m_SelfSlots.OnTrade = boost::bind(&StrategyImpl::OnTrade, shared_from_this(), boost::lambda::_1);
// 	}
// 	
// 	if (!m_SelfSlots.OnOrderAction.slot_function())
// 	{
// 		m_SelfSlots.OnOrderAction = boost::bind(&StrategyImpl::OnOrderAction, shared_from_this(), boost::lambda::_1);
// 	}
// 	
// 	if (!m_SelfSlots.OnTick.slot_function())
// 	{
// 		m_SelfSlots.OnTick = boost::bind(&StrategyImpl::OnTick, shared_from_this(), boost::lambda::_1);
// 	}
// 	
// 	if (!m_SelfSlots.OnInitialze.slot_function())
// 	{
// 		m_SelfSlots.OnInitialze = boost::bind(&StrategyImpl::Initialize, shared_from_this());
// 	}
// 	
// 	if (!m_SelfSlots.OnFinalize.slot_function())
// 	{
// 		m_SelfSlots.OnFinalize = boost::bind(&StrategyImpl::Finalize, shared_from_this());
// 	}
//	m_SelfSlots = boost::shared_ptr<StrategySlot>(new StrategySlot{ boost::signals2::slot<void(boost::shared_ptr<Tick>)>(boost::bind(&StrategyImpl::OnTick, shared_from_this(), boost::lambda::_1)) });
	is_run = true;
	if (!m_Thread)
	{
		m_Thread = boost::make_shared<boost::thread>(&StrategyImpl::ThreadFun, shared_from_this());
	}
}

void StrategyImpl::DoStop()
{
	LOGDEBUG("StrategyImpl DoStop");
	is_run=false;
	
	//RedisStore::get_instance().pub_timeline("strategy stop", "the strategy:" + m_Id + "is ready to stop");
}


void StrategyImpl::Stop()
{
	if (is_run)
	{
		auto evt = std::bind(&StrategyImpl::DoStop, shared_from_this());
		PostEvent(evt);
	}
	if (m_Thread && m_Thread->joinable())
	{
		m_Thread->join();
	}
// 	unique_lock<mutex> lck(m_Mutex4Event);
// 	while (!m_EventQueue.empty())
// 	{
// 		m_EventQueue.pop();
// 	}
}

void StrategyImpl::SetCallback( StrategyCallback& callback )
{
	m_StrategyCallback=callback;
}

const std::string& StrategyImpl::GetId() const
{
	return m_Id;
}


boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > StrategyImpl::GetTrade(boost::shared_ptr<Trader> ptrTrader)
{
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > trades = boost::make_shared<std::map<std::string, boost::shared_ptr<Trade> > >();
	if (ptrTrader)
	{
		for (auto i = m_MyTrades->begin(); i != m_MyTrades->end(); ++i)
		{
			if (i->second->UserID == ptrTrader->GetUser().UserID
				&& i->second->BrokerID == ptrTrader->GetUser().BrokerID)
			{
				trades->insert(*i);
			}
		}
	}
	return trades;
}

boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > StrategyImpl::GetOrder(boost::shared_ptr<Trader> ptrTrader)
{
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > orders = boost::make_shared<std::map<std::string, boost::shared_ptr<Order> > >();
	if (ptrTrader)
	{
		for (auto i = m_MyOrders->begin(); i != m_MyOrders->end(); ++i)
		{
			if (i->second->FrontID == ptrTrader->GetUser().FrontID
				&& i->second->SessionID == ptrTrader->GetUser().SessionID)
			{
				orders->insert(*i);
			}
		}
	}
	return orders;
}



boost::shared_ptr<InputOrder> StrategyImpl::OrderInsert(boost::shared_ptr<InputOrder> inputOrder, boost::shared_ptr<Trader> trader)
{
	if (!IsEnableTrading(trader))
	{
		return nullptr;
	}
	do
	{
		unique_lock<mutex> lck(m_Mutext4OrderInsert);
		if (!trader)
		{
			break;
		}
		inputOrder->InsertDate = trader->GetTradingDay();
		auto inst_info = GetInstrumentInfo(inputOrder->InstrumentID);
		if (inst_info)
		{
			inputOrder->InsertTime = inst_info->m_reportLatest.UpdateTimeStr;
		}

		if (!trader->OrderInsert(inputOrder))
		{
			break;
		}
		Update(inputOrder);
		
		return inputOrder;
	} while (0);
	return nullptr;
}

boost::shared_ptr<InputOrder> StrategyImpl::OrderInsert(const std::string& instId, int Volume, double Price,
								  EnumOffsetFlag OpenOrClose,EnumDirection BuyOrSell,
	boost::shared_ptr<Trader> trader)
{
	boost::shared_ptr<InputOrder> inputOrder = boost::make_shared<InputOrder>();
	inputOrder->InstrumentID = instId;
	inputOrder->VolumeTotalOriginal = Volume;
	inputOrder->LimitPrice = Price;
	inputOrder->MinVolume = 1;
	inputOrder->CombOffsetFlag[0] = OpenOrClose;
	inputOrder->CombOffsetFlag[1] = OF_None;
	inputOrder->CombOffsetFlag[2] = OF_None;
	inputOrder->CombOffsetFlag[3] = OF_None;
	inputOrder->CombOffsetFlag[4] = OF_None;
	inputOrder->Direction = BuyOrSell;
	inputOrder->OrderPriceType = OPT_LimitPrice;
	inputOrder->CombHedgeFlag[0] = HF_Speculation;
	inputOrder->CombHedgeFlag[1] = HF_None;
	inputOrder->CombHedgeFlag[2] = HF_None;
	inputOrder->CombHedgeFlag[3] = HF_None;
	inputOrder->CombHedgeFlag[4] = HF_None;

	return OrderInsert(inputOrder, trader);
}

boost::shared_ptr<InputOrderAction> StrategyImpl::OrderCancel(int frontId, int sessionId, const std::string& orderRef)
{
	boost::shared_ptr<InputOrderAction> inputOrderAction = boost::make_shared<InputOrderAction>();
	inputOrderAction->FrontID=frontId;
	inputOrderAction->SessionID=sessionId;
	inputOrderAction->OrderRef=orderRef;
	//遍历所有接口;
	boost::shared_ptr<Trader> traderid=GetTraderIDByFronIDSessionID(frontId,sessionId);
	if (!traderid)
	{
		return inputOrderAction;
	}
	if (OrderCancel(inputOrderAction,traderid))
	{
		return inputOrderAction;
	}
	return inputOrderAction;
}


boost::shared_ptr<InputOrderAction> StrategyImpl::OrderCancel(const std::string& ExchangeID, const std::string& OrderSysId, const std::string& instId)
{
	boost::shared_ptr<InputOrderAction> inputOrderAction = boost::make_shared<InputOrderAction>();
	inputOrderAction->ExchangeID = ExchangeID;
	inputOrderAction->OrderSysID = OrderSysId;
	inputOrderAction->InstrumentID = instId;

	auto oiter = m_MyOrders->find(OrderSysId);


	if (oiter == m_MyOrders->end())
	{
		return nullptr;
	}
	else
	{
		boost::shared_ptr<Trader> traderid = IStrategy::GetTraderIDByFronIDSessionID(oiter->second->FrontID, oiter->second->SessionID);
		if (!traderid)
		{
			inputOrderAction->FrontID = oiter->second->FrontID;
			inputOrderAction->SessionID = oiter->second->SessionID;
			return OrderCancel(inputOrderAction, traderid);
		}
	}
	return nullptr;
}

boost::shared_ptr<InputOrderAction> StrategyImpl::OrderCancel(boost::shared_ptr<InputOrderAction> inputOrderAction, boost::shared_ptr<Trader> trader)
{
	bool bCancel=false;
	do 
	{
		if (!trader)
		{
			break;
		}
		inputOrderAction->ActionFlag=AF_Delete;
		inputOrderAction->InsertDate = trader->GetTradingDay();

		//填写ExchangeID;
		boost::shared_ptr<InstrumentInfo> pInfo = GetInstrumentInfo(inputOrderAction->InstrumentID);
		if (inputOrderAction->ExchangeID.empty())
		{
			inputOrderAction->ExchangeID = pInfo->GetExchangeID();
		}
		inputOrderAction->InsertTime = pInfo->m_reportLatest.UpdateTimeStr;
		if(!trader->OrderAction(inputOrderAction))
		{
			break;
		}
		bCancel=true;
	} while (0);
	if (bCancel)
	{
		m_MyInputOrderActions->insert(std::make_pair(inputOrderAction->OrderRef, inputOrderAction));
		return inputOrderAction;
	}
	return nullptr;
}

boost::shared_ptr<Trader> StrategyImpl::GetMainTrader()
{
	return m_MainTrader;
}

//默认的交易对象ID;
void StrategyImpl::SetMainMarket(const boost::shared_ptr<Market> market)
{
	if (market != m_MainMarket)
	{
// 		auto ontick = boost::bind(&StrategyImpl::OnTick, shared_from_this(), boost::lambda::_1);
// 		if (m_MainMarket)
// 		{
// 			m_MainMarket->sigOnTick.disconnect(ontick);
// 		}
// 		market->sigOnTick.connect(ontick);
		m_MainMarket = market;
		
	}
}
//其他交易对象的ID;
void StrategyImpl::SetSubMarkets(const std::vector<boost::shared_ptr<Market> >& subMarkets)
{
// 	auto ontick = boost::bind(&StrategyImpl::OnTick, shared_from_this(), boost::lambda::_1);
// 	for (size_t i = 0; i < m_SubMarkets.size(); i++)
// 	{
// 		if (m_SubMarkets[i])
// 		{
// 			m_SubMarkets[i]->sigOnTick.disconnect(ontick);
// 		}
// 	}
	m_SubMarkets.clear();
	for (size_t i = 0; i < subMarkets.size(); i++)
	{
		//subMarkets[i]->sigOnTick.connect(ontick);
		m_SubMarkets.push_back(subMarkets[i]);
	}
}


void StrategyImpl::SetMainTrader(boost::shared_ptr<Trader> trader, double Available /*= 0*/)
{
	if (trader!=m_MainTrader)
	{
		auto onstatus = boost::bind(&StrategyImpl::OnStatusHandler, shared_from_this(), boost::lambda::_1);
		auto onorder = boost::bind(&StrategyImpl::OnOrderHandler, shared_from_this(), boost::lambda::_1);
		auto ontrade = boost::bind(&StrategyImpl::OnTradeHandler, shared_from_this(), boost::lambda::_1);
		auto onorderaction = boost::bind(&StrategyImpl::OnOrderActionHandler, shared_from_this(), boost::lambda::_1);
		
		if (m_MainTrader)
		{
			m_MainTrader->sigInstrumentStatus.disconnect(onstatus);
			m_MainTrader->sigOnOrder.disconnect(onorder);
			m_MainTrader->sigOnTrade.disconnect(ontrade);
			m_MainTrader->sigOnOrderAction.disconnect(onorderaction);
		}
		trader->sigInstrumentStatus.connect(onstatus);
		trader->sigOnOrder.connect(onorder);
		trader->sigOnTrade.connect(ontrade);
		trader->sigOnOrderAction.connect(onorderaction);
		m_MainTrader = trader;

		m_MaxOrderRef = m_MainTrader->GetUser().MaxOrderRef;
		//实际可用资金;
		m_TradingAccount->TradingDay = m_MainTrader->GetTradingDay();
		if (m_TradingAccount->PreBalance == 0)
		{
			//如果该策略没有分配过资金;
			m_TradingAccount->PreBalance = Available;
			m_TradingAccount->Available = Available;
		}
	}
}


void StrategyImpl::PostEvent(const OnEventFun& evt)
{
	if (is_run)
	{
		unique_lock<mutex> lck(m_Mutex4Event);
		m_EventQueue.push(evt);
		m_CondVar4Event.notify_one();
	}
}


void StrategyImpl::SetInstruments( const std::map<std::string,int>& instruments )
{
	unique_lock<mutex> lcki(m_Mutex4SubInstruments);
	m_Instruments=instruments;
}

void StrategyImpl::OnTick(boost::shared_ptr<Tick> tick)
{
	if (!is_run)
	{
		return;
	}
	//记录时间;
// 	auto pStatus = boost::make_shared<InstStatus>();
// 	pStatus->InstrumentID = "ni";
// 	pStatus->InstrumentStatus = IS_Closed;
// 	OnStatus(pStatus);
// 	std::map<std::string, boost::shared_ptr<InstStatus> >::iterator isIter 
// 		= m_MyInstStatus.find(tick->InstrumentID());
// 	if (isIter !=m_MyInstStatus.end())
// 	{
// 		if (isIter->second->InstrumentStatus == IS_NoTrading || 
// 			isIter->second->InstrumentStatus == IS_Closed)
// 		{
// 			return;
// 		}
// 	}
	Update(tick);
	//这个行情不是我订阅的;
	if (tick)
	{
		unique_lock<mutex> lcki(m_Mutex4SubInstruments);
		//RedisStore::get_instance().pub_timeline("strategy running", "instrument:" + tick->InstrumentID() + " last price is:" + std::to_string(tick->LastPrice));
		auto iiter = std::find(m_RealSubInstruments.begin(), m_RealSubInstruments.end(), tick->InstrumentID());
		if (m_RealSubInstruments.end() == iiter)
		{
			return;
		}
		m_StrategyCallback.OnTick(tick);
	}
}

void StrategyImpl::ThreadFun()
{
	Initialize();
// 	while(IsRun())
// 	{
// 		OnEventFun evtFun;
// 		{
// 			unique_lock<mutex> lck(m_Mutex4Event);
// 			while (m_EventQueue.empty())
// 			{
// 				m_CondVar4Event.wait(lck);
// 			}
// 			if (m_EventQueue.empty())
// 			{
// 				continue;
// 			}
// 			evtFun = m_EventQueue.front();
// 			m_EventQueue.pop();
// 		}
// 		if (evtFun)
// 		{
// 			evtFun();
// 		}
// 		
// // 		m_EventQueue.pop();
// // 		std::queue<OnEventFun> eventQueue;
// // 		m_EventQueue.swap(eventQueue);
// // 		lck.unlock();
// // 		while (!eventQueue.empty())
// // 		{
// // 			OnEventFun& evtFun= eventQueue.front();
// // 			if (evtFun)
// // 			{
// // 				evtFun();
// // 				eventQueue.pop();
// // 			}
// // 		}
// 	}
	while (IsRun())
	{
		std::queue<OnEventFun> eventQueue;
		{
			/*boost::*/unique_lock</*boost::*/mutex> lck(m_Mutex4Event);
			if (m_EventQueue.empty())
			{
				m_CondVar4Event.wait(lck);
			}
			eventQueue.swap(m_EventQueue);
		}
		
		while (!eventQueue.empty())
		{
			OnEventFun evtFun = eventQueue.front();
			eventQueue.pop();
			if (evtFun)
			{
				evtFun();
			}
		}
	}
	Finalize();
}

void StrategyImpl::Finalize()
{
	LOGDEBUG("----Finalize----");
	Resave();
	LOGDEBUG("----ReSave Finished!----");
	if (m_StrategyCallback.OnFinalize)
	{
		m_StrategyCallback.OnFinalize();
	}
}


// void StrategyImpl::UpdateInstStatus(const std::string& InstrumentID, boost::shared_ptr<InstStatus> pInstStatus)
// {
// 	//合约里的状态有可能是品种的状态,所以要把它发散到具体的合约上去;
// 	auto cIter = m_MyInstStatus.find(InstrumentID);
// 	if (cIter == m_MyInstStatus.end())
// 	{
// 		cIter = std::find_if(m_MyInstStatus.begin(), m_MyInstStatus.end(),
// 			[InstrumentID](std::pair<std::string, boost::shared_ptr<InstStatus> >  InstStatusPair)
// 		{
// 			std::vector<std::string> ProductYM;
// 			boost::split(ProductYM, InstStatusPair.first, boost::is_any_of("1234567890"));
// 			if (ProductYM.size() > 0 && ProductYM[0] == InstrumentID)
// 			{
// 				return true;
// 			}
// 			return false;
// 		});
// 		if (cIter != m_MyInstStatus.end())
// 		{
// 			m_MyInstStatus[InstrumentID] = cIter->second;
// 			LOGDEBUG("update status 1:{}->{}", InstrumentID, cIter->second->InstrumentStatus);
// 		}
// 	}
// 	else
// 	{
// 		if (cIter->second && pInstStatus)
// 		{
// 			*(cIter->second) = *pInstStatus;
// 			cIter->second->InstrumentID = InstrumentID;
// 			LOGDEBUG("update status 2:{}->{}", InstrumentID, cIter->second->InstrumentStatus);
// 		}
// 	}
// }


void StrategyImpl::Initialize()
{
	LOGDEBUG("----Initialize----");
	//订阅合约;
	std::vector<std::string> instruments;
	std::map<std::string, int>::iterator instIter = m_Instruments.begin();
	while (instIter != m_Instruments.end())
	{
		std::string InstrumentID = instIter->first;
		instruments.push_back(InstrumentID);
		//合约里的状态有可能是品种的状态,所以要把它发散到具体的合约上去;
		InstrumentInfo info;
		auto bFindA = InstrumentContainer::get_instance().GetInstrumentInfo(InstrumentID.c_str(), &info);
// 		if (bFindA)
// 		{
// 			m_InstrumentInfoMap[InstrumentID] = info;
// 		}
		++instIter;
	}
	//读取以前的数据;
	Restore();

	//合约状态;
// 	if (m_MainTrader)
// 	{
// 		m_MainTrader->GetInstStatus(m_MyInstStatus);
// 	}
	
	LOGDEBUG(GetId()+"-->Initialize");

	

	if (m_StrategyCallback.OnInitialze)
	{
		m_StrategyCallback.OnInitialze();
	}

	//订阅行情;
	SubInstruments(instruments, "");
}


void StrategyImpl::OnOrderAction(boost::shared_ptr<InputOrderAction> orderAction)
{
	if (!is_run)
	{
		return;
	}
	//保存Order;
	if (Update(orderAction))
	{
		m_StrategyCallback.OnOrderAction(orderAction);
	}
}


void StrategyImpl::OnOrder( boost::shared_ptr<Order> order )
{
	if (!is_run)
	{
		return;
	}
	//保存Order;
	if (Update(order))
	{
		m_StrategyCallback.OnOrder(order);
	}
}

void StrategyImpl::OnTrade( boost::shared_ptr<Trade> trade )
{
	if (!is_run)
	{
		return;
	}
	//保存自己的成交单;
	if (Update(trade))
	{
		//如果当前状态为全部平仓状态,继续平仓;
		if (!m_CloseAllFirstPosition.InstrumentID.empty())
		{
			CloseAllPositions(m_CloseAllInstrumentPattern);
		}

		LOGDEBUG("Strategy {} OnTrade {} {} {}", trade->UserID,
			trade->OrderSysID, trade->Volume, trade->TradeID);

		m_StrategyCallback.OnTrade(trade);
	}
	else
	{
		LOGDEBUG("Strategy {} Update Trade Failed! {} {} {}", trade->UserID, 
			trade->OrderSysID, trade->Volume, trade->TradeID);
	}
}

void StrategyImpl::OnStatus( boost::shared_ptr<InstStatus> status )
{
	if (!is_run)
	{
		return;
	}
	InstrumentContainer& container = InstrumentContainer::get_instance();
	for (auto i = m_RealSubInstruments.begin(); i != m_RealSubInstruments.end(); ++i)
	{
		std::string InstID = *i;
		InstrumentInfo& info = container.get_info_by_id(InstID, status->ExchangeID);
		if (!info.is_valid())
		{
			continue;
		}
		if (info.ProductClass == PC_Combination)
		{
			//如果合约是组合合约,则只要其中一个合约状态发生变化则通知;
			std::vector<std::string> subInsts;//子合约品种;
			boost::split(subInsts, InstID, boost::is_any_of(" &"));
			if (subInsts.size()>=3)
			{
				auto& info1 = container.get_info_by_id(subInsts[1], status->ExchangeID);
				auto& info2 = container.get_info_by_id(subInsts[2], status->ExchangeID);

				//LOGDEBUG("inst split {}:{},{},{}", i->first, subInsts[0], subInsts[1], subInsts[2]);

				//LOGDEBUG("infos {}:{},{},{}", status->InstrumentID, info1.get_id(), info2.get_id(), status->ExchangeID);

				bool bNotify = false;
				if (info1.get_id() == status->InstrumentID || info1.ProductID == status->InstrumentID)
				{
					bNotify = true;
					//第1腿状态发生了改变;
					info1.m_status = boost::make_shared<InstStatus>(*status);
					//LOGDEBUG("update status 1:{}->{}", subInsts[1], info1.m_status->InstrumentStatus);
				}
				if (info2.get_id() == status->InstrumentID || info2.ProductID == status->InstrumentID)
				{
					bNotify = true;
					//第2腿状态发生了改变;
					info2.m_status = boost::make_shared<InstStatus>(*status);
					//LOGDEBUG("update status 2:{}->{}", subInsts[2], info2.m_status->InstrumentStatus);
				}
				if (bNotify && info1.m_status && info2.m_status)
				{
					info.m_status = boost::make_shared<InstStatus>(*status);
					info.m_status->InstrumentID = info.get_id();
					if (m_StrategyCallback.OnStatus)
					{
						//LOGDEBUG("update status 4:{}->{}", i->first, info.m_status->InstrumentStatus);
						m_StrategyCallback.OnStatus(info.m_status);
					}
				}
			}
		}
		else
		{
			if (info.ProductID == status->InstrumentID || info.get_id() == status->InstrumentID)
			{
				info.m_status = boost::make_shared<InstStatus>(*status);
				info.m_status->InstrumentID = InstID;

				if (m_StrategyCallback.OnStatus)
				{
					LOGDEBUG("update status 3:{}->{}", InstID, info.m_status->InstrumentStatus);
					Resave();
					m_StrategyCallback.OnStatus(info.m_status);
				}
			}
		}
	}

	bool isNight = !GetDataStoreFace()->IsDayOrNight();
	bool AllIsClosed = 
	std::all_of(m_RealSubInstruments.begin(), m_RealSubInstruments.end(), 
	[this, &container, status, isNight](const std::string& InstrumetnID)
	{
		InstrumentInfo& info = container.get_info_by_id(InstrumetnID, status->ExchangeID);
		if (info.is_valid() && info.m_status
			&& ((info.m_status->InstrumentStatus == IS_Closed)
			|| (info.m_status->InstrumentStatus == IS_NoTrading && isNight))) //夜盘下进入非交易状态后,也要停止程序;
		{
			//收盘;
			LOGDEBUG(" {}:is closed!", info.m_status->InstrumentID);
			return true;
		}
		return false;
	});
	//合约已经收盘了;
	if (AllIsClosed)
	{
		DoStop();
	}
}

bool StrategyImpl::IsMyOrder( boost::shared_ptr<Order> order )
{
	if (order->FrontID==m_MainTrader->GetUser().FrontID
		&& order->SessionID==m_MainTrader->GetUser().SessionID)
	{
		return true;
	}
	for (std::size_t i=0;i<m_SubTraders.size();++i)
	{
		boost::shared_ptr<Trader> ptrTrader=m_SubTraders[i];
		if (ptrTrader)
		{
			UserInfo& u=ptrTrader->GetUser();
			if ((u.FrontID==order->FrontID) && (u.SessionID== order->SessionID))
			{
				return true;
			}
		}
	}
	return false;
}

bool StrategyImpl::Join()
{
//	DEBUG_METHOD();
//	DEBUG_MESSAGE("进入Join");
	if (m_Thread)
	{
//		DEBUG_MESSAGE("进入Joinable");
		if (m_Thread->joinable())
		{
//			DEBUG_MESSAGE("执行Join");
			m_Thread->join();
//			DEBUG_MESSAGE("离开Join-->true");
			return true;
		}
	}
//	DEBUG_MESSAGE("离开Join-->false");
	return false;
}

void StrategyImpl::SetSubTraders( const std::vector<boost::shared_ptr<Trader> >& subTraders )
{
	auto onstatus = boost::bind(&StrategyImpl::OnStatusHandler, shared_from_this(), boost::lambda::_1);
	auto onorder = boost::bind(&StrategyImpl::OnOrderHandler, shared_from_this(), boost::lambda::_1);
	auto ontrade = boost::bind(&StrategyImpl::OnTradeHandler, shared_from_this(), boost::lambda::_1);
	auto onorderaction = boost::bind(&StrategyImpl::OnOrderActionHandler, shared_from_this(), boost::lambda::_1);
	for (size_t i = 0; i < m_SubTraders.size(); i++)
	{
		if (m_SubTraders[i])
		{
			m_SubTraders[i]->sigInstrumentStatus.disconnect(onstatus);
			m_SubTraders[i]->sigOnOrder.disconnect(onorder);
			m_SubTraders[i]->sigOnTrade.disconnect(ontrade);
			m_SubTraders[i]->sigOnOrderAction.disconnect(onorderaction);
		}
	}
	m_SubTraders.clear();
	for (size_t i = 0; i < subTraders.size(); i++)
	{
		subTraders[i]->sigInstrumentStatus.connect(onstatus);
		subTraders[i]->sigOnOrder.connect(onorder);
		subTraders[i]->sigOnTrade.connect(ontrade);
		subTraders[i]->sigOnOrderAction.connect(onorderaction);
		if (subTraders[i]->GetUser().MaxOrderRef > m_MaxOrderRef)
		{
			m_MaxOrderRef = subTraders[i]->GetUser().MaxOrderRef;
		}
		m_SubTraders.push_back(subTraders[i]);
	}
}

std::map<std::string,int>& StrategyImpl::GetInstruments() 
{
	return m_Instruments;
}


bool StrategyImpl::IsMyTrade( boost::shared_ptr<Trade> trade )
{
	if (!trade)
	{
		return false;
	}
	//查找所有的Orders,匹配OrderSysID
	auto oditer=m_MyOrders->find(trade->OrderSysID);
// 	while (oditer!=m_MyOrders->end())
// 	{
// 		if (trade.OrderSysID==oditer->OrderSysID && trade.ExchangeID==oditer->ExchangeID)
// 		{
// 			return true;
// 		}
// 		++oditer;
// 	}
// 	return false;
	return oditer != m_MyOrders->end();
}

void StrategyImpl::SubInstruments( const std::vector<std::string>& instruments, const std::string& exchange)
{
	//订阅合约;
	//通过合约查找交易所;
	auto markets = UserApiMgr::GetInstance().GetMarkets();
	for (auto i = markets.begin(); i!=markets.end(); ++i)
	{
		auto td=m_MainTrader;
		if (td && i->second->UserID()==td->UserID())
		{
			if (i->second->SubscribeMarketData(instruments,exchange))
			{
				//添加;
				unique_lock<mutex> lck(m_Mutex4SubInstruments);
				for (std::size_t i = 0;i<instruments.size();++i)
				{
					auto iiter = std::find(m_RealSubInstruments.begin(), m_RealSubInstruments.end(), instruments[i]);
					if (iiter == m_RealSubInstruments.end())
					{
						m_RealSubInstruments.push_back(instruments[i]);
					}
				}
			}
			break;
		}
	}
}

void StrategyImpl::UnSubInstruments( const std::vector<std::string>& instruments, const std::string& exchange)
{
	auto markets =
		UserApiMgr::GetInstance().GetMarkets();
	for (auto i = markets.begin(); i != markets.end(); ++i)
	{
		auto td = m_MainTrader;
		if (td && i->second->UserID() == td->UserID())
		{
			if (i->second->UnSubscribeMarketData(instruments,exchange))
			{
				//去掉;
				unique_lock<mutex> lck(m_Mutex4SubInstruments);
				for (std::size_t i = 0;i < instruments.size();++i)
				{
					auto iiter = std::find(m_RealSubInstruments.begin(), m_RealSubInstruments.end(), instruments[i]);
					if (iiter != m_RealSubInstruments.end())
					{
						m_RealSubInstruments.erase(iiter);
					}
				}
			}
		}
	}
}


struct TimerInfo
{
	int timerId;
	OnEventFun callback;
	unsigned int interval;
	shared_ptr<boost::thread> thrd;
	condition_variable cond;
	static mutex mutex4Timer;
	bool run;
	bool interrupt;
	TimerInfo(OnEventFun& fn, int mseconds = 1000)
	{
		run = false;
		interrupt = false;
		interval = mseconds;
		callback = fn;
		thrd = make_shared<boost::thread>(bind(&TimerInfo::execute,this));
	}
	~TimerInfo()
	{
		run = false;
		interrupt = true;
	}
	void execute()
	{
		unique_lock<mutex> lck(mutex4Timer);
		cond.wait_for(lck,chrono::milliseconds(interval),[this]() { return interrupt; });
		callback();
	}
};

mutex TimerInfo::mutex4Timer;

int StrategyImpl::StartTimer(OnEventFun& evt, int elapsed/*=1000*/)
{
	int timerId = m_Timers.size() + 1;
	auto tIter = m_Timers.find(timerId);
	while (tIter!=m_Timers.end())
	{
		++timerId;
		tIter = m_Timers.find(timerId);
	}
	OnEventFun timerCallback = bind(&StrategyImpl::PostEvent,this,evt);
	m_Timers.insert(std::make_pair(timerId,make_shared<TimerInfo>(timerCallback,elapsed)));
	return timerId;
}

bool StrategyImpl::StopTimer(int timerId)
{
	auto timerIter = m_Timers.find(timerId);
	if (timerIter==m_Timers.end())
	{
		return false;
	}
	m_Timers.erase(timerIter);
	return true;
}


void StrategyImpl::Restore()
{
	//1.加载报单信息;
	//LoadOrders();

	//2.加载持仓信息;
	LoadPositions();

	//3.加载成交信息;
	//LoadTrades();

	LoadTradingAccounts();
}

void StrategyImpl::Resave()
{
	//保存数据到磁盘;
	LOGDEBUG("begin save input orders");
	//报单;
	SaveInputOrders();

	LOGDEBUG("begin save input order actions");
	//撤单;
	SaveInputOrderActions();

	LOGDEBUG("begin save orders");
	//1.保存报单信息;
	SaveOrders();

	LOGDEBUG("begin save positions");
	//2.保存持仓信息;
	SavePositions();

	LOGDEBUG("begin save trades");
	//3.保存成交信息;
	SaveTrades();

	LOGDEBUG("begin save trading account!");
	SaveTradingAccounts();
}


void StrategyImpl::SetDataStoreFace(boost::shared_ptr<DataStoreFace> pDataStore)
{
	if (m_pDataStoreFace != pDataStore)
	{
		m_pDataStoreFace = pDataStore;
		auto ontick = boost::bind(&StrategyImpl::OnTickHandler, shared_from_this(), boost::lambda::_1);
		m_pDataStoreFace->sigOnTick.connect(ontick);
	}
}

boost::shared_ptr<DataStoreFace> StrategyImpl::GetDataStoreFace()
{
	return m_pDataStoreFace;
}

bool StrategyImpl::CloseAllPositions(const std::string& InstrumentID /*= false*/)
{
	if (m_MyPositions->size() <= 0)
	{
		return true;
	}
	//暂停用户报单;
	if (!SetEnableTrading(m_MainTrader))
	{
		return false;
	}

	if (m_MyPositions->size() == 0)
	{
		//仓位全部平完了;
		m_CloseAllInstrumentPattern = "";
		m_CloseAllFirstPosition = InvestorPosition();
		return true;
	}
	//构造报单列表;
	if (m_CloseAllInstrumentPattern.empty())
	{
		m_CloseAllInstrumentPattern = InstrumentID;

		for (size_t i = 0; i < m_MyPositions->size(); ++i)
		{
			if (InstrumentID.empty() && (*m_MyPositions)[i]->InstrumentID == InstrumentID)
			{
				m_CloseAllFirstPosition = *(*m_MyPositions)[i];
				break;
			}
			else
			{
				m_CloseAllFirstPosition = *(*m_MyPositions)[i];
			}
		}

		auto closeInputOrder = ClosePosition(m_CloseAllFirstPosition);
		if (!closeInputOrder)
		{
			//仓位全部平完了;
			m_CloseAllInstrumentPattern = "";
			m_CloseAllFirstPosition = InvestorPosition();
		}
	}
	return true;
}


boost::shared_ptr<InputOrder> StrategyImpl::ClosePosition(const InvestorPosition& FirstPosition)
{
	if (FirstPosition.InstrumentID.empty())
	{
		return nullptr;
	}
	if (FirstPosition.Position == 0)
	{
		return nullptr;
	}
	auto info = GetInstrumentInfo(FirstPosition.InstrumentID);
	if (!info)
	{
		return nullptr;
	}
	auto& tick = info->m_reportLatest;
	
	//构造报单,获取对手价;
	EnumDirection directionFlag = D_Buy;
	double Price = tick.AskPrice[0];
	if (FirstPosition.PosiDirection == PD_Long)
	{
		directionFlag = D_Sell;
		Price = tick.BidPrice[0];
	}
	EnumOffsetFlag offsetFlag = OF_CloseToday;
	if (FirstPosition.PositionDate == PSD_History)
	{
		offsetFlag = OF_CloseYesterday;
	}
	//报单平仓;
	return OrderInsert(FirstPosition.InstrumentID, FirstPosition.Position, Price, offsetFlag, directionFlag, m_MainTrader);
}


bool StrategyImpl::IsEnableTrading(boost::shared_ptr<Trader> traderId)
{
	return true;
}

bool StrategyImpl::SetEnableTrading(boost::shared_ptr<Trader> traderId, bool bEnableTrading/*=true*/)
{
	return true;
}

boost::shared_ptr<InvestorPosition> StrategyImpl::GetPosition(const std::string& InstrumentID)
{
	auto posiIter = std::find_if(m_MyPositions->begin(), m_MyPositions->end(), 
	[&InstrumentID](const boost::shared_ptr<InvestorPosition>& ip)
	{
		return ip->InstrumentID == InstrumentID;
	});
	if (posiIter!=m_MyPositions->end())
	{
		return *posiIter;
	}
	return nullptr;
}

int StrategyImpl::GetSignedPosition(const std::string& InstrumentID)
{
	boost::shared_ptr<InvestorPosition> ptrInvestorPosition = GetPosition(InstrumentID);
	if (!ptrInvestorPosition)
	{
		return 0;
	}
	if (ptrInvestorPosition->PosiDirection == PD_Long)
	{
		return abs(ptrInvestorPosition->Position);
	}
	else
	{
		return -abs(ptrInvestorPosition->Position);
	}
}

bool StrategyImpl::GetPositionCost(const std::string& InstrumentID, double& PositionAvgPrice)
{
	boost::shared_ptr<InvestorPosition> ptrInvestorPosition = GetPosition(InstrumentID);
	if (!ptrInvestorPosition)
	{
		return false;
	}
	if (ptrInvestorPosition->PositionCost <= 0)
	{
		return false;
	}
	PositionAvgPrice = ptrInvestorPosition->PositionCost;
	return true;
}

boost::shared_ptr<InputOrderAction> StrategyImpl::SafeCancelOrder(boost::shared_ptr<Order> order, boost::shared_ptr<InputOrder> inputOrder)
{
	LOGDEBUG("发送撤单指令->Order...");
	if (!order)
	{
		return SafeCancelOrder(inputOrder);
	}
	//DEBUG_METHOD();
	int frontId = order->FrontID, sessionId = order->SessionID;
	auto traderid = GetTraderIDByFronIDSessionID(frontId, sessionId);
	if (!traderid)
	{
		LOGDEBUG("发送撤单指令->没有找到traderid");
        return nullptr;
	}
	boost::shared_ptr<InputOrderAction> inputOrderAction = boost::make_shared<InputOrderAction>();
	inputOrderAction->FrontID = frontId;
	inputOrderAction->SessionID = sessionId;
	inputOrderAction->OrderRef = order->OrderRef;
	inputOrderAction->InvestorID = order->InvestorID;
	inputOrderAction->BrokerID = order->BrokerID;
	inputOrderAction->InstrumentID = order->InstrumentID;
	inputOrderAction->ExchangeID = order->ExchangeID;
	inputOrderAction->OrderSysID = order->OrderSysID;
	LOGDEBUG("发送撤单指令Order:{} {} {}",order->InstrumentID, order->ExchangeID, order->OrderSysID);
	return OrderCancel(inputOrderAction, traderid);
}



boost::shared_ptr<InputOrderAction> StrategyImpl::SafeCancelOrder(boost::shared_ptr<InputOrder> order)
{
	LOGDEBUG("发送撤单指令->InputOrder...");
	if (!order)
	{
        return nullptr;
	}
	//DEBUG_METHOD();
	int frontId = order->FrontID, sessionId = order->SessionID;
	auto traderid = GetTraderIDByFronIDSessionID(frontId, sessionId);
	if (!traderid)
	{
		LOGDEBUG("发送撤单指令->没有找到traderid");
        return nullptr;
	}
	boost::shared_ptr<InputOrderAction> inputOrderAction = boost::make_shared<InputOrderAction>();
	inputOrderAction->FrontID = frontId;
	inputOrderAction->SessionID = sessionId;
	inputOrderAction->OrderRef = order->OrderRef;
	inputOrderAction->InvestorID = order->InvestorID;
	inputOrderAction->BrokerID = order->BrokerID;
	inputOrderAction->InstrumentID = order->InstrumentID;
//	inputOrderAction->ExchangeID = order->ExchangeID;
//	inputOrderAction->OrderSysID = order->OrderSysID;
	LOGDEBUG("发送撤单指令InputOrder:{} {} {} {}", order->InstrumentID, frontId, sessionId, order->OrderRef);
	return OrderCancel(inputOrderAction, traderid);
}

boost::shared_ptr<Trader> StrategyImpl::GetTraderIDByFronIDSessionID(int frontId, int SessionId)
{
	std::map<std::string, boost::shared_ptr<Trader> >& traders = UserApiMgr::GetInstance().GetTraders();
	auto titer = traders.begin();
	while (titer != traders.end())
	{
		if (titer->second)
		{
			if (titer->second->GetUser().FrontID == frontId && titer->second->GetUser().SessionID == SessionId)
			{
				return titer->second;
			}
		}
		++titer;
	}
	return nullptr;
}



boost::shared_ptr<Trader> StrategyImpl::GetTraderByInvestorID(const std::string& InvestorID)
{
	if (m_MainTrader)
	{
		auto pInvestor = m_MainTrader->GetInvestorAccount()->GetInvestor();
		if (pInvestor && pInvestor->InvestorID == InvestorID)
		{
			return m_MainTrader;
		}
		else
		{
			if (m_MainTrader->GetUser().InvestorID == InvestorID)
			{
				return m_MainTrader;
			}
		}
	}
	for (auto tIter = m_SubTraders.begin(); tIter != m_SubTraders.end();++tIter)
	{
		auto pInvestor = (*tIter)->GetInvestorAccount()->GetInvestor();
		if (pInvestor && pInvestor->InvestorID == InvestorID)
		{
			return (*tIter);
		}
		else
		{
			if ((*tIter)->GetUser().InvestorID ==  InvestorID)
			{
				return (*tIter);
			}
		}
	}
	return nullptr;
}

boost::shared_ptr<InputOrder> StrategyImpl::OpenPosition(const std::string& InstrumentID, EnumDirection BuyOrSell, int Volume, boost::shared_ptr<Trader> pTrader)
{
	auto tIter = GetInstrumentInfo(InstrumentID);
	if (!tIter)
	{
		return nullptr;
	}
	boost::shared_ptr<InputOrder> inputOrder = boost::make_shared<InputOrder>();
	inputOrder->InstrumentID = InstrumentID;
	inputOrder->VolumeTotalOriginal = Volume;
	if (BuyOrSell == D_Buy )
	{
		inputOrder->LimitPrice = tIter->m_reportLatest.AskPrice[0];
	}
	else
	{
		inputOrder->LimitPrice = tIter->m_reportLatest.BidPrice[0];
	}
	
	inputOrder->MinVolume = Volume;
	inputOrder->CombOffsetFlag[0] = OF_Open;
	inputOrder->CombOffsetFlag[1] = OF_None;
	inputOrder->CombOffsetFlag[2] = OF_None;
	inputOrder->CombOffsetFlag[3] = OF_None;
	inputOrder->CombOffsetFlag[4] = OF_None;

	inputOrder->Direction = BuyOrSell;
	inputOrder->OrderPriceType = OPT_LimitPrice;
	inputOrder->CombHedgeFlag[0] = HF_Speculation;
	inputOrder->CombHedgeFlag[1] = HF_None;
	inputOrder->CombHedgeFlag[2] = HF_None;
	inputOrder->CombHedgeFlag[3] = HF_None;
	inputOrder->CombHedgeFlag[4] = HF_None;
	return  OrderInsert(inputOrder, GetMainTrader());
}


boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > StrategyImpl::GetInputOrder(boost::shared_ptr<Trader> ptrTrader)
{
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > orders = boost::make_shared<std::map<std::string, boost::shared_ptr<InputOrder> > >();
	if (ptrTrader)
	{
		for (auto i = m_MyInputOrders->begin(); i != m_MyInputOrders->end(); ++i)
		{
			if (i->second->FrontID == ptrTrader->GetUser().FrontID 
				&& i->second->SessionID == ptrTrader->GetUser().SessionID)
			{
				orders->insert(*i); 
			}
		}
	}
	return orders;
}


boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > StrategyImpl::GetInputOrderAction(boost::shared_ptr<Trader> ptrTrader)
{
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > orders = boost::make_shared<std::map<std::string, boost::shared_ptr<InputOrderAction> > >();
	if (ptrTrader)
	{
		for (auto i = m_MyInputOrderActions->begin(); i != m_MyInputOrderActions->end(); ++i)
		{
			if (i->second->FrontID == ptrTrader->GetUser().FrontID
				&& i->second->SessionID == ptrTrader->GetUser().SessionID)
			{
				orders->insert(*i);
			}
		}
	}
	return orders;
}

void StrategyImpl::SetStartDay(const std::string& startDay)
{
	//m_TradingAccount.back().TradingDay=startDay;
	if (!startDay.empty())
	{
		m_StartDay = startDay;
	}
}

std::string StrategyImpl::GetStartDay()
{
	return m_StartDay;
}


void StrategyImpl::OnTickHandler(boost::shared_ptr<Tick> tick)
{
	PostEvent(boost::bind(&StrategyImpl::OnTick, shared_from_this(),tick));
}

void StrategyImpl::OnStatusHandler(boost::shared_ptr<InstStatus> status)
{
	PostEvent(boost::bind(&StrategyImpl::OnStatus, shared_from_this(), status));
}

void StrategyImpl::OnOrderHandler(boost::shared_ptr<Order> order)
{
	PostEvent(boost::bind(&StrategyImpl::OnOrder, shared_from_this(), order));
}

void StrategyImpl::OnTradeHandler(boost::shared_ptr<Trade> trade)
{
	PostEvent(boost::bind(&StrategyImpl::OnTrade, shared_from_this(), trade));
}

void StrategyImpl::OnOrderActionHandler(boost::shared_ptr<InputOrderAction> orderAction)
{
	PostEvent(boost::bind(&StrategyImpl::OnOrderAction, shared_from_this(), orderAction));
}

boost::shared_ptr<InstrumentCommisionRate> StrategyImpl::GetInstCommissionRate(const std::string& instId)
{
	return m_MainTrader->GetInstCommissionRate(instId);
}

boost::shared_ptr<InstrumentMarginRate> StrategyImpl::GetInstMarginRate(const std::string& instId)
{
	return m_MainTrader->GetInstMarginRate(instId);
}

boost::shared_ptr<BrokerTradingParams> StrategyImpl::GetBrokerTradingParams()
{
	return m_MainTrader->GetBrokerTradingParams();
}



