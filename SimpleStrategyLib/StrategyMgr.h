#ifndef _STRATEGY_MGR_H_
#define _STRATEGY_MGR_H_

#include <vector>
#include <string>
#include "UserInfo.h"
#include <memory>
#include "simplestrategylib_global.h"
#include "IStrategy.h"
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <boost/dll.hpp>


class IStrategy;

struct StrategyLibParam
{
	boost::shared_ptr<boost::dll::shared_library> strategy_lib_ptr;
	boost::function<strategy_create_t > strategy_creator;
	std::vector<boost::shared_ptr<IStrategy> > strategies;
};

class SIMPLESTRATEGYLIB_EXPORT StrategyMgr:public boost::noncopyable
{
public:
	~StrategyMgr(void);
	StrategyMgr();
	//从一个DLL加载一个策略;
	bool LoadStrategy(const std::string& strategy_path);
	//卸载一个策略;
	//bool UnLoadStrategy(const std::string& strategy_path);
	//维护策略指针;
	boost::shared_ptr<IStrategy> AddStrategyPtr(const std::string& id,const std::string& strategy_path);
	boost::shared_ptr<IStrategy> GetStrategyPtr(const std::string& id);
	std::map<std::string, boost::shared_ptr<IStrategy> >& GetStrategies();

	bool RemoveStrategyPtr(const std::string& id);
	bool RemoveStrategyPtr(boost::shared_ptr<IStrategy>&  ptrStrtegy);
	bool Join();
	void Stop();
protected:
	//维护目前加载的策略;
	std::map<std::string, boost::shared_ptr<IStrategy> > m_StrategiesMap;
	//库管理;
	std::map<std::string, StrategyLibParam > m_StrategyLibs;


public:
	static StrategyMgr& GetInstance();
};


#endif

