#ifndef _ISTRATEGY_H_
#define _ISTRATEGY_H_

#include "DataTypes.h"
#include "UserInfo.h"
#include "simplestrategylib_global.h"
#include "../ServiceLib/Trader.h"
#include "../ServiceLib/Market.h"
#include "../DataStoreLib/DataStoreFace.h"
#include <vector>
#include <queue>
#include <list>
#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>

//回调定义;
struct StrategyCallback 
{
	std::function<void(boost::shared_ptr<Tick>)> OnTick;
	std::function<void(boost::shared_ptr<Trade>)> OnTrade;
	std::function<void(boost::shared_ptr<Order>)> OnOrder;
	std::function<void(boost::shared_ptr<InstStatus>)> OnStatus;
	std::function<void(boost::shared_ptr<InputOrderAction>)> OnOrderAction;
	std::function<void(void)> OnInitialze;
	std::function<void(void)> OnFinalize;
};


#define STRATEGY_CREATE_FUNCTION_NAME "create_strategy"

class StrategyImpl;

class SIMPLESTRATEGYLIB_EXPORT  IStrategy:public boost::noncopyable
{
public:
	IStrategy(const std::string& id);
	virtual ~IStrategy(void);

	virtual bool IsRun();
	//这个函数必须从主线程调用;
	virtual void Start();
	virtual void Stop();
	virtual const std::string& GetId() const;

	const std::string& GetVersion();

	const std::string& GetFilePath() ;

	const std::string& GetName() ;

	void SetFilePath(const std::string& filePath);

	void SetName(const std::string& name);

	void SetVersion(const std::string& version);

	void SetStartDay(const std::string& startDay);

	std::string GetStartDay();

	boost::shared_ptr<DataStoreFace> GetDataStoreFace();

	void SetDataStore(boost::shared_ptr<DataStoreFace> pDataStoreFace);
	//报单操作接口;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > GetOrders();
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > GetOrder(boost::shared_ptr<Trader> traderID);
	//成交;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > GetTrades();
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > GetTrade(boost::shared_ptr<Trader> traderID);

	//获取所有的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > GetInputOrders();

	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > GetInputOrder(boost::shared_ptr<Trader> traderID);

	//获取所有的撤单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > GetInputOrderActions();
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > GetInputOrderAction(boost::shared_ptr<Trader> traderID);

	//查找投资者;
	static std::string GetInvestorIDByTraderID(const std::string& traderId);
	static boost::shared_ptr<Trader> GetTraderIDByFronIDSessionID(int frontId,int SessionId);
	//报单,限价,主要是投机;
	boost::shared_ptr<InputOrder> OrderInsert(const std::string& instId,int Volume,double Price,
		EnumOffsetFlag OpenOrClose,EnumDirection BuyOrSell, boost::shared_ptr<Trader> traderId);

	boost::shared_ptr<InputOrder> OrderInsert(boost::shared_ptr<InputOrder> order, boost::shared_ptr<Trader> traderId);

	bool SetEnableTrading(boost::shared_ptr<Trader> traderId, bool bEnableTrading = true);
	bool IsEnableTrading(boost::shared_ptr<Trader> traderId);
	//撤单;
	boost::shared_ptr<InputOrderAction> OrderCancel(int frontId,int sessionId,const std::string& orderRef);
	boost::shared_ptr<InputOrderAction> OrderCancel(const std::string& ExchangeID,
		const std::string& OrderSysId,const std::string& instId);
	boost::shared_ptr<InputOrderAction> OrderCancel(boost::shared_ptr<InputOrderAction> inputOrderAction, boost::shared_ptr<Trader> traderId);
	boost::shared_ptr<InputOrderAction> SafeCancelOrder(boost::shared_ptr<Order> order, boost::shared_ptr<InputOrder> inputOrder = nullptr);
	boost::shared_ptr<InputOrderAction> SafeCancelOrder(boost::shared_ptr<InputOrder> order);
	
	//订阅合约;
	void SubInstruments(const std::vector<std::string>& instruments,const std::string& exchange);
	void SubInstrument(const std::string& instrument,const std::string& exchange);
	void UnSubInstruments(const std::vector<std::string>& instruments, const std::string& exchange);
	void UnSubInstrument(const std::string& instrument, const std::string& exchange);
	//平掉本策略所有的仓位;
	bool CloseAllPositions(const std::string& InstrumentID="");
	//开仓或者加仓;
	boost::shared_ptr<InputOrder> OpenPosition(const std::string& InstrumentID, EnumDirection BuyOrSell, int volume);
	//平仓;
	boost::shared_ptr<InputOrder> ClosePosition(const InvestorPosition& ip);

	boost::shared_ptr<InvestorPosition> GetPosition(const std::string& InstrumentID);

	boost::shared_ptr < std::vector<boost::shared_ptr<InvestorPosition> > > GetPositions();

	boost::shared_ptr < std::vector<boost::shared_ptr<InvestorPosition> > > GetClosedPositions();

	//历史资金信息;
	boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > GetTradingAccounts();
	//当日的资金信息数据;
	boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > GetTradingAccountsToday();
	//持仓量;
	int GetSignedPosition(const std::string& InstrumentID);

	bool GetPositionCost(const std::string& InstrumentID, double& PositionAvgPrice);

	//默认的交易对象ID;
	void SetMainTrader(boost::shared_ptr<Trader> ptrTrader,double Avaiable=0);
	boost::shared_ptr<Trader> GetMainTrader();
	//行情;
	void SetMainMarket(boost::shared_ptr<Market> ptrMarket);
	//其他交易对象的ID;
	void SetSubTraders(const std::vector<boost::shared_ptr<Trader> >& subId);
	void SetSubMarkets(const std::vector<boost::shared_ptr<Market> >& subId);
	//设置合约;
	void SetInstruments(const std::map<std::string,int>& instruments);
	//获取合约;
	std::map<std::string,int>& GetInstruments();
	//行情;
	virtual void OnTick(boost::shared_ptr<Tick> tick);
	//状态;
	virtual void OnStatus(boost::shared_ptr<InstStatus> status);
	//报单;
	virtual void OnOrder(boost::shared_ptr<Order> order);
	//成交;
	virtual void OnTrade(boost::shared_ptr<Trade> trade);
	//撤单失败;
	virtual void OnOrderAction(boost::shared_ptr<InputOrderAction> orderAction);
	//重新载入;
	virtual void Restore();

	//把函数放到策略的线程里面去调用;
	void PostEvent(const OnEventFun& evt);

	//开启一个定时器;
	int StartTimer(OnEventFun& evt, int elapsed=1000);
	//关闭一个定时器;
	bool StopTimer(int timerId);
	bool Join();
	int GetCancelCount(const std::string& InstrumentID);
public:
	//线程启动是运行;
	virtual void Initialize();
	//线程即将结束时运行;
	virtual void Finalize();
public:
	virtual bool IsMyOrder(boost::shared_ptr<Order> order);
	virtual bool IsMyTrade(boost::shared_ptr<Trade> trade);
protected:
	std::shared_ptr<StrategyImpl> m_pImpl;
	std::string m_FilePath;
	std::string m_Name;
	std::string m_Version;
};

typedef boost::shared_ptr<IStrategy>(strategy_create_t)(const std::string&);

//获取浮点数精度;
inline int GetAccuracy(double PriceTick)
{
	//取出小数部份;
	double intPriceTick;
	double dPriceTick = modf(PriceTick, &intPriceTick);
	if (dPriceTick <= 0)
	{
		return 0;
	}
	int accuracy = 0;
	while (dPriceTick < 1.0)
	{
		dPriceTick *= 10;
		accuracy++;
	}
	return accuracy;
}



#endif

