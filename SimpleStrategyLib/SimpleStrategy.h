#ifndef _SIMPLE_STRATEGY_H_
#define _SIMPLE_STRATEGY_H_

#include "simplestrategylib_global.h"
#include "../ServiceLib/Service/StkReceiver.h"
#include "../ServiceLib/observerwiththread.h"
#include "../FacilityBaseLib/Container.h"
#include "../TechLib/Strategy.h"
#include "Enums.h"
#include <queue>
#include <vector>


//CtpMdSpi是CTP接口里MdSpi的子类，或者自己封装一个MdSpi以便能注册多个SPI实际上是在CTP的MdSpi里向每个CtpMdSpi对象发一次数据;
class SIMPLESTRATEGYLIB_EXPORT simple_strategy :public IObserverWithThread
{
public:
	simple_strategy(void);
	virtual ~simple_strategy(void);
protected:
	simple_strategy(const simple_strategy& other){}
	simple_strategy& operator=(const simple_strategy& other){return *this;}
public:
	//用户登录;
	virtual void on_login(user_info&  user);
	//用户登出;
	virtual void on_logout(user_info  user);
	//报单回报事件;
	virtual void on_order(OrderField& order);
	//成交回报事件;
	virtual void on_trade(Trade& trade);
	//网络断开事件;
	virtual void on_broken();
	//行情数据接收;
	virtual void on_report(const Tick&);
	//状态发生改变;
	virtual void on_status(InstrumentStatus& status);
	//收到合约信息;
	virtual void on_info(basedata_t& info);
	//可以修改这个函数的参数以添加一些自己要的参数;
	//如果不想这么做（因为参数太多），可以调用主程序的一些接口供使用;
	//例如，主程序要提供获取CTP api的接口，以便在buy和sell里可以调用;
	virtual bool add_user(user_info& user,server_info& server);
	virtual bool del_user(const std::string& investor_id);
	virtual user_info get_user();
	//仓位>0表示多头,<0表示空头;
	int position(const std::string& inst_id);
protected:
	virtual int post_event(WPARAM wParam,LPARAM lParam);
	virtual int handler(WPARAM wParam);
	virtual void on_start();
	virtual void on_stop();
	friend void login_thread_callback(void* strategy);
public:
	virtual Order make_order(const std::string& uid, const std::string& instrument, EnumDirectionType direct,
		EnumOffsetFlagType posiDirect,double price,int volume);
	std::vector<std::string>& get_reg_markets();
	virtual int cancel_order(OrderField& order);
	//平仓(强平所有);
	virtual int close_all_position(const std::string& uid);
	//平仓;
	virtual int close_one_position(const std::string& uid,const std::string& iid,
		double price,int volume =0);
	//合约的最新价;
	virtual bool last_tick(Tick& rpt);
	virtual bool is_my_order(OrderField& order);
	virtual bool is_my_trade(Trade& trade);
	//监控策略的运行结果;
	void set_monitor(strategy_monitor* pMonitor);
	//资产序列;
	CAssetSerialContainer& get_asset_serial();
	//操作记录;
	COpRecordContainer& get_op_record();
	//持仓;
	CInstrumentOwnContainer& get_instrument_own();
	//下单记录;
	std::map<std::string,OrderField> get_orders();
	//策略名称;
	virtual std::string name() const;
	//报单存储模块;
// 	virtual void store_order();
// 	virtual void store_trade();
// 	virtual bool load_order();
// 	virtual bool load_trade();
	//显示当前策略的状态;
	virtual std::string show();
protected:
	strategy_monitor* m_monitor;
	//当前用户;
	user_info  m_current_user;
	//当前登录服务器信息;
	server_info m_server;
	// 当前拥有合约;
	CInstrumentOwnContainer	m_InstrumentsOwn;
	// 记录;
	COpRecordContainer		m_OpRecord;
	// 资产值序列;
	CAssetSerialContainer	m_AssetSerial;
	//下单记录;
	std::map<std::string,OrderField> m_Orders;
	//合约信息;
	instrument_container m_InstrumentContainer;
	
	//锁;
	tthread::mutex m_mutex;
private:
	//数据队列;
	tthread::mutex m_dataQueueMutex;
	std::queue<Tick> m_qRealtime;
	std::queue<OrderField> m_qOrders;
	std::queue<Trade> m_qTrades;
	std::queue<user_info> m_qUsers;
	std::queue<InstrumentStatus> m_qStatus;
	//合约信息数据;
	std::queue<basedata_t> m_qBaseData;
};
#endif

