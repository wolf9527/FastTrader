#ifndef _INVESTOR_ACCOUNT_H_
#define _INVESTOR_ACCOUNT_H_
#include "DataTypes.h"
#include "UserInfo.h"
#include "ServiceLib.h"

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include "Portfolio.h"

//多点登录的用户只算一个;
class SERVICELIB_API InvestorAccount:public Portfolio
{
public:
	InvestorAccount(const std::string& id);
	InvestorAccount(const std::string& InvestorID,const std::string& BrokerID);
	~InvestorAccount(void);
public:
	std::string GetField(int field);
	void setField(int field, const std::string& value);

	std::pair<std::string,std::string> GetInvestorIDBrokerID();

	void SafeUpdate(const Investor& );
	void SafeUpdate(boost::shared_ptr<TradingAccount> ta );
	void SafeUpdate(const SettlementInfo& s,bool bIsLast=false );
	void SafeUpdate(boost::shared_ptr<InvestorPosition> ptrPosition,bool bUserIsLogin = false);
	void SafeUpdate(boost::shared_ptr<InvestorPositionDetail>,bool bUserIsLogin=false );
	void SafeUpdate(boost::shared_ptr<InvestorPositionCombineDetail>,bool bUserIsLogin=false );

	void SafeUpdate(boost::shared_ptr<Order>, bool bUserIsLogin = false);
	void SafeUpdate(boost::shared_ptr<Trade>, bool bUserIsLogin = false);
	void SafeUpdate(boost::shared_ptr<Tick> tick);
	void SafeUpdate(boost::shared_ptr<InputOrder> pInputOrder);
	void SafeUpdate(boost::shared_ptr<InputOrderAction> pInputOrder);
	void SafeUpdate(boost::shared_ptr<BrokerTradingParams> borkerTradingParam);

	boost::shared_ptr<Investor> GetInvestor();
	boost::shared_ptr<SettlementInfo> GetSettlementInfo();
	//获取持仓;
	std::vector<boost::shared_ptr<InvestorPositionDetail> >& GetInvestorPositionDetail();
	std::vector<boost::shared_ptr<InvestorPositionCombineDetail> >& GetInvestorComboPositionsDetail();
protected:
	void Update(const Investor&);
	void Update(const SettlementInfo& s,bool bIsLast=false);
	void Update(boost::shared_ptr<InvestorPositionDetail>,bool IsUserLogin=false);
	void Update(boost::shared_ptr<InvestorPositionCombineDetail>,bool IsUserLogin=false);
	void Update(boost::shared_ptr<InvestorPosition> ptrPosition);

	virtual boost::shared_ptr<BrokerTradingParams> GetBrokerTradingParams();

protected:
	boost::mutex m_Mutex4Investor;
	boost::mutex m_Mutex4Account;
	boost::mutex m_Mutex4SettlementInfo;
	boost::mutex m_Mutex4Order;
	boost::mutex m_Mutex4Trade;
public:
	boost::mutex m_Mutex4PosDetail;
	boost::mutex m_Mutex4CmbPosDetail;
protected:
	//持仓明细;
	std::vector<boost::shared_ptr<InvestorPositionDetail> > m_InvestorPositionDetail;
	//组合持仓明细;
	std::vector<boost::shared_ptr<InvestorPositionCombineDetail> > m_InvestorPositionCombineDetail;
protected:
	//投资者;
	boost::shared_ptr<Investor> m_Investor;
	//结算信息;
	boost::shared_ptr<SettlementInfo> m_SettlementInfo;

	boost::shared_ptr<BrokerTradingParams> m_BrokerTradingParams;

};


#endif

