#ifndef __MARKET_BASE_H__
#define __MARKET_BASE_H__
#include "ServerInfo.h"
#include "UserInfo.h"
#include "DataTypes.h"
#include "ServiceLib.h"
#include <vector>


#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/signals2.hpp>
#include <boost/noncopyable.hpp>

//行情;
class SERVICELIB_API Market:public boost::noncopyable
{
public:
	//构造函数;
	Market();
	virtual ~Market();
	//初始化;
	virtual bool Init(const ServerInfo& s);
	//登录;
	virtual bool Login(const UserLoginParam&  u);
	//登出;
	virtual bool Logout();
public:
	//收到深度行情;
	boost::signals2::signal<void(boost::shared_ptr<Tick>)> sigOnTick;
	//行情登录;
	boost::signals2::signal<void()> sigOnUserLogin;
	//基础函数;
	virtual std::string Id() const;
	virtual std::string UserID() const;
	virtual std::string BrokerID() const;
	virtual std::string GetTradingDay();
	virtual std::string GetApiVersion();
	//请求登录;
	virtual bool ReqUserLogin();
	//确保这2个函数是线程安全的;
	virtual bool SubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg="");
	virtual bool UnSubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg="");
	//获取已经订阅的合约;
	virtual void GetSubInstruments(std::map<std::string,std::vector<std::string> >& instruments);
protected:
	//已经订阅的合约;
	mutex m_MutInst;
	std::map<std::string,std::vector<std::string> > m_SubInstruments;
	UserLoginParam m_UserInfo;
	//当前请求号;
	int& RequestID();
	const ErrorMessage& LastError() const;
protected:
	void UpdateSubInst(const std::string& inst,const std::string& exchg,bool bAddOrDel);
	bool SubFilter(const std::vector<std::string>& instruments,const std::string& exchg,std::vector<std::string>& subs);
	bool UnSubFilter(const std::vector<std::string>& instruments,const std::string& exchg,std::vector<std::string>& unsubs);
protected:
	//前置信息;
	ServerInfo m_ServerInfo;
	//登录同步变量;
	boost::mutex m_LoginMutex;
	boost::condition_variable m_LoginCondVar;
	//请求编号;
	int m_nRequestID;
	//是否断线了;
	bool m_bFrontDisconnected;
	//是否登录了;
	bool m_bIsLogined;
	//错误信息;
	ErrorMessage m_ErrorMessage;
};

typedef boost::shared_ptr<Market> (market_create_t)(const std::string& protocol);


#endif
