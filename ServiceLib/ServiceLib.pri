
HEADERS += \
    UserApiMgr.h \
    Trader.h \
    Portfolio.h \
    Market.h \
    InvestorAccount.h \
    ServiceLib.h

SOURCES += \
    UserApiMgr.cpp \
    Trader.cpp \
    Portfolio.cpp \
    #pyexports.cpp \
    Market.cpp \
    InvestorAccount.cpp \
    ServiceLib.cpp


