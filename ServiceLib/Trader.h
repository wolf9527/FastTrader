#ifndef _TRADER_BASE_H_
#define _TRADER_BASE_H_
#include "DataTypes.h"
#include "ServerInfo.h"
#include "UserInfo.h"
#include "ServiceLib.h"
#include "InvestorAccount.h"

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/signals2.hpp>


struct TraderSignals
{
	boost::signals2::signal<void(boost::shared_ptr<Instrument>)> sigInstrumentInfo;
	boost::signals2::signal<void(boost::shared_ptr<exchange_t>)> sigExchangeInfo;
	boost::signals2::signal<void(boost::shared_ptr<InstStatus>)> sigInstrumentStatus;
	boost::signals2::signal<void(boost::shared_ptr<Order>)> sigOnOrder;
	boost::signals2::signal<void(boost::shared_ptr<Trade>)> sigOnTrade;
	boost::signals2::signal<void(boost::shared_ptr<InputOrderAction>)> sigOnOrderAction;
};

//交易接口;
class SERVICELIB_API Trader
{
public:
	//请求登录;
	virtual bool ReqUserLogin()=0;
	//结算信息确认;
	virtual bool SettlementConfirm();
	//查询交易参数;
	virtual bool QryTradingParam();
	//查询转帐银行;
	virtual bool QryTransBank();
	//查询通知;
	virtual bool QryNotice();
	//查询交易通知;
	virtual bool QryTradingNotice();
	//查询投资者;
	virtual bool QryInvestor();
	//查询交易所;
	virtual bool QryExchange(const std::string& exchangeId);
	//查询合约;
	virtual bool QryInstrument(const std::string& instId,const std::string& exchgId)=0;
	//查询帐户资金;
	virtual bool QryAccount();
	//查询持仓;
	virtual bool QryPosition(const std::string& instId);
	//查询持仓明细;
	bool QryPositionDetail(const std::string& instId);
	//查询组合持仓;
	bool QryCombPosDetail(const std::string& instId);
	//查询结算信息确认;
	bool QrySettlementConfirm();
	//查询结算信息;
	bool QrySettlementInfo();
	//查询手续费;
	virtual bool QryInstCommissionRate(const std::string& instId);
	virtual boost::shared_ptr<InstrumentCommisionRate> GetInstCommissionRate(const std::string& instId);
	virtual boost::shared_ptr<InstrumentMarginRate> GetInstMarginRate(const std::string& instId);
	//交易计算参数;
	virtual boost::shared_ptr<BrokerTradingParams> GetBrokerTradingParams();
	//查询保证金;
	virtual bool QryExchgMarginRate(const std::string& instId);
	//查询最大下单量;
	virtual bool QryMaxOrderVolume(const std::string& instId);
	//报单;
	virtual bool OrderInsert(boost::shared_ptr<InputOrder> param);
	//撤单;
	virtual bool OrderAction(boost::shared_ptr<InputOrderAction> param);
	//预埋单;
	virtual bool ParkedOrderInsert(boost::shared_ptr<ParkedOrder> param);
	//预埋撤单;
	virtual bool ParkedOrderAction();
	//查询行情;
	virtual bool QryDepthMarket(const std::string& instId);
	//查询报单;
	virtual bool QryOrder(const std::string&, const std::string& instId);
	//查询成交;
	virtual bool QryTrade(const std::string&,const std::string& instId);

	virtual void OnTick(boost::shared_ptr<Tick> tick);
public:
	//信号;
	boost::signals2::signal<void(const UserInfo& )> sigUserLogin;
	boost::signals2::signal<void(const UserInfo& )> sigUserLogout;
	boost::signals2::signal<void(const UserInfo& )> sigDisconnected;

	boost::signals2::signal<void(boost::shared_ptr<Instrument>)> sigInstrumentInfo;
	boost::signals2::signal<void(boost::shared_ptr<exchange_t>)> sigExchangeInfo;
	boost::signals2::signal<void(boost::shared_ptr<InstStatus>)> sigInstrumentStatus;
	boost::signals2::signal<void(boost::shared_ptr<Order>)> sigOnOrder;
	boost::signals2::signal<void(boost::shared_ptr<Trade>)> sigOnTrade;
	boost::signals2::signal<void(boost::shared_ptr<InputOrderAction>)> sigOnOrderAction;

	virtual std::string GetTradingDay();

	virtual std::string UserID() const;

	virtual std::string BrokerID() const;

	virtual std::string Id() const;

	boost::shared_ptr<InvestorAccount> GetInvestorAccount();
protected:
	bool m_bInited;

	UserInfo m_UserInfo;
public:
	//交易参数;
	BrokerTradingParams m_BrokerTradingParams;
	//用户通知;
	Notice m_Notice;
	//结算确认;
	SettlementInfoConfirm m_SettlementInfoConfirm;
public:
	//构造函数;
	Trader();
	virtual ~Trader();
	//初始化;
	virtual bool Init(const ServerInfo& s);
	//登录;
	virtual bool Login(const UserLoginParam& u);
	//登出;
	virtual bool Logout();
	//等待线程退出;
	virtual int Join();
	UserInfo& GetUser();
	ServerInfo& GetServer();
	//释放接口对象;
	void Release();
	//当前请求号;
	int& RequestID();

	const ErrorMessage& LastError() const;
protected:
	Trader(const Trader& other);
	Trader& operator =(const Trader& other);
protected:
	//前置信息;
	ServerInfo m_ServerInfo;
	//登录同步变量;
	boost::mutex m_LoginMutex;
	boost::condition_variable m_LoginCondVar;
	//请求编号;
	int m_nRequestID;
	//是否断线了;
	bool m_bFrontDisconnected;
	//是否登录了;
	bool m_bIsLogined;
	//错误信息;
	ErrorMessage m_ErrorMessage;
	//交易的账户信息;
	boost::shared_ptr<InvestorAccount> m_InvestorAccount;
};

typedef boost::shared_ptr<Trader> (trader_create_t)();
#endif
