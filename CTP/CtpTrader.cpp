#include "platform.h"
#include <iostream>
#include <sstream>

using namespace std;
#include <string.h>

#include "ThostFtdcTraderApi.h"
#include "CtpTrader.h"
#include "DataTypes.h"
#include "file_helper.h"
#include "../Log/logging.h"
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "../sdk/include/PlatformStruct.h"
#include <boost/serialization/serialization.hpp>
#include <boost/archive/tmpdir.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <fstream>
#ifdef _MSC_VER
#pragma warning(disable : 4996)
#pragma comment(lib,"thosttraderapi.lib")
#endif

void CtpTrader::OnFrontConnected()
{
	LOGDEBUG("交易前置连接响应...");
	ReqUserLogin();
}


void CtpTrader::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,
							CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		SetLastError(pRspInfo);
		LOGDEBUG("交易前置登录失败:{}",LastError().ErrorMsg);
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	LOGDEBUG("-----------用户 交易登录验证通过----------{}",pRspUserLogin->UserID);
	LOGDEBUG("----------------交易日:{}",m_pTraderApi->GetTradingDay());
	LOGDEBUG("--------------登录时间:{}",pRspUserLogin->LoginTime);
	//LOGDEBUG("--------------本地时间:"<<DateTime::Now().GetAsString());
	LOGDEBUG("------------交易所时间:{}",pRspUserLogin->SHFETime);
	LOGDEBUG("----------------回话ID:{}",pRspUserLogin->SessionID);
	LOGDEBUG("----------------前置ID:{}",pRspUserLogin->FrontID);
	LOGDEBUG("--------------报单引用:{}",pRspUserLogin->MaxOrderRef);

	m_UserInfo.TradingDay=pRspUserLogin->TradingDay;
	m_UserInfo.LoginTime=pRspUserLogin->LoginTime;
	m_UserInfo.SystemName=pRspUserLogin->SystemName;
	m_UserInfo.FrontID=pRspUserLogin->FrontID;
	m_UserInfo.SessionID=pRspUserLogin->SessionID;
	m_UserInfo.MaxOrderRef=pRspUserLogin->MaxOrderRef;
	m_UserInfo.ExchangeTime=pRspUserLogin->SHFETime;
	if (!QryTradingParam())
	{
		m_UserInfo.IsTraderLogined = true;
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
	}
}


bool CtpTrader::Init( const ServerInfo& info)
{
	if (m_bInited)
	{
		return false;
	}
	LOGDEBUG("Init ServerInfo {}",info.name);

	m_ServerInfo=info;
	if (NULL==m_pTraderApi)
	{
		std::string bid=m_ServerInfo.id,wdir=get_work_dir();
		create_dir(bid,wdir);
		m_pTraderApi=CThostFtdcTraderApi::CreateFtdcTraderApi((m_ServerInfo.id+"/").c_str());
		m_pTraderApi->SubscribePublicTopic(/*THOST_TERT_RESTART*/(THOST_TE_RESUME_TYPE)m_ServerInfo.public_topic_resume_type);  // 注册公有流;
		m_pTraderApi->SubscribePrivateTopic(/*THOST_TERT_RESTART*/(THOST_TE_RESUME_TYPE)m_ServerInfo.private_topic_resume_type);// 注册私有流;
	}
	m_pTraderApi->RegisterSpi(this);						// 注册事件类;
	char front_addr[256]={0};
	for (std::size_t i=0;i<m_ServerInfo.trade_server_front.size();++i)
	{
		strcpy(front_addr,m_ServerInfo.trade_server_front[i].to_string().c_str());
		m_pTraderApi->RegisterFront(front_addr);
	}
	m_bInited=true;
	return m_bInited;
}


CtpTrader::CtpTrader( )
	:Trader()
{
	LOGINIT("ctp_trader");
	LOGDEBUG("Trader对象构造...");
	m_pTraderApi=NULL;
	m_bInited=false;
	m_bFrontDisconnected=false;
	m_bIsLogined=false;
}

void CtpTrader::Release()
{
	//释放成员指针...;
	m_bInited=false;
	//释放对象，参照文档;
	//综合交易平台API开发FAQ.pdf/综合交易平台API开发常见问题列表;
	if (NULL != m_pTraderApi)
	{
		m_pTraderApi->RegisterSpi(NULL);
		m_pTraderApi->Release();
		m_pTraderApi = NULL;
	}
}

bool CtpTrader::Logout()
{
	if (m_UserInfo.IsTraderLogined && m_pTraderApi)
	{
		CThostFtdcUserLogoutField req;
		memset(&req, 0, sizeof(CThostFtdcUserLogoutField));
		strcpy(req.BrokerID, m_UserInfo.BrokerID.c_str());
		strcpy(req.UserID, m_UserInfo.UserID.c_str());
		return m_pTraderApi->ReqUserLogout(&req, ++RequestID())==0;
	}
	return true;
}

bool CtpTrader::Login( const UserLoginParam& u )
{
	if (!m_bInited || u.UserID.empty() || u.Password.empty())
	{
		return false;
	}
	m_UserInfo=u;
	if (!m_InvestorAccount)
	{
		m_InvestorAccount = boost::make_shared<InvestorAccount>(m_UserInfo.UserID,m_ServerInfo.id);
	}
	m_pTraderApi->Init();
	//等待登录完成;
	LOGDEBUG("----------user begin login :{}",u.UserID);
	boost::unique_lock<boost::mutex> lck(m_LoginMutex);
	int login_timeout = m_ServerInfo.trader_login_timeout;
	if (login_timeout <= 0)
	{
		login_timeout = 300;
	}
	if (m_LoginCondVar.wait_for(lck, boost::chrono::seconds(login_timeout)) == boost::cv_status::timeout)
	{
		LOGDEBUG("交易登录超时...");
		return false;
	}

	LoadInstCommisionRate();
	LoadInstMarginRate();
	//查询合约手续费;
	for (auto i = m_FuturesInstruments.begin(); i != m_FuturesInstruments.end(); ++i)
	{
		auto cIter = m_InstCommisionRates.find(*i);
		if (cIter ==  m_InstCommisionRates.end())
		{
			//组合合约;
			boost::shared_ptr<InstrumentCommisionRate> icr = boost::make_shared<InstrumentCommisionRate>();
			m_InstCommisionRates.insert(std::make_pair(*i, icr));
			if (!QryInstCommissionRate(*i))
			{
				//查询手续费出错;
				LOGDEBUG("查询手续费出错...{}", *i);
			}
		}
		auto mIter = m_InstMarginRates.find(*i);
		if (mIter==m_InstMarginRates.end())
		{
			if (!QryInstMarginRate(*i))
			{
				LOGDEBUG("查询保证金率出错...:{}", *i);
			}
		}
	}
	SaveInstCommisionRate();
	SaveInstMarginRate();
	LOGDEBUG("交易登录完成...");
	return m_UserInfo.IsTraderLogined;
}


void CtpTrader::NotifyUserLogin(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (m_UserInfo.IsTraderLogined)
	{
		sigUserLogin(m_UserInfo);
		m_LoginCondVar.notify_one();
	}
}


void CtpTrader::OnRspQrySettlementInfoConfirm( CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return;
	}
	bool bNeedSettlementConfirm=true;
	if (pSettlementInfoConfirm)
	{
		m_SettlementInfoConfirm.BrokerID=pSettlementInfoConfirm->BrokerID;
		m_SettlementInfoConfirm.ConfirmDate=pSettlementInfoConfirm->ConfirmDate;
		m_SettlementInfoConfirm.ConfirmTime=pSettlementInfoConfirm->ConfirmTime;
		m_SettlementInfoConfirm.InvestorID=pSettlementInfoConfirm->InvestorID;
		//NotifyApiMgr(RspQrySettlementConfirm,LPARAM(&m_SettlementInfoConfirm));

		//当前时间;
		boost::posix_time::ptime::date_type dNow = boost::gregorian::date_from_iso_string(GetTradingDay());
		boost::posix_time::ptime nowTime(dNow,boost::posix_time::second_clock::local_time().time_of_day());

		//确认时间;
		boost::posix_time::ptime::date_type dConfirm = boost::gregorian::date_from_iso_string(m_SettlementInfoConfirm.ConfirmDate);
		boost::posix_time::ptime::time_duration_type tdConfirm = boost::posix_time::duration_from_string(m_SettlementInfoConfirm.ConfirmTime);

		boost::posix_time::ptime confirmTime(dConfirm, tdConfirm);
		
		LOGDEBUG("上次结算时间:{},{}",m_SettlementInfoConfirm.ConfirmDate,m_SettlementInfoConfirm.ConfirmTime);
		//今天结算过了没有?;
		if (confirmTime.date()<nowTime.date())
		{
			bNeedSettlementConfirm = true;
		}
		else
		{
			if (confirmTime <= nowTime)
			{
				bNeedSettlementConfirm = false;
			}
		}
	}
	else
	{
		bNeedSettlementConfirm=true;
	}
	LOGDEBUG("查询结算信息确认响应...");
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		//如果没有登录;
// 		if (bNeedSettlementConfirm)
// 		{
		//现在是总是要查询一次结算信息;
			//查询结算信息;
			if (!QrySettlementInfo())
			{
				NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
			}
// 		}
// 		else
// 		{
// 			LOGDEBUG("查询结算信息确认响应--登录完成...");
// 			m_UserInfo.IsTraderLogined=true;
// 			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
// 		}
	}
	LOGDEBUG("查询结算信息确认响应--完成...");
}

void CtpTrader::OnRspSettlementInfoConfirm( CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	if (pSettlementInfoConfirm)
	{
		m_SettlementInfoConfirm.BrokerID=pSettlementInfoConfirm->BrokerID;
		m_SettlementInfoConfirm.ConfirmDate=pSettlementInfoConfirm->ConfirmDate;
		m_SettlementInfoConfirm.ConfirmTime=pSettlementInfoConfirm->ConfirmTime;
		m_SettlementInfoConfirm.InvestorID=pSettlementInfoConfirm->InvestorID;
		//NotifyApiMgr(RspSettlementInfoConfirm,LPARAM(&m_SettlementInfoConfirm));
	}
	LOGDEBUG("结算信息确认响应...");
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		m_UserInfo.IsTraderLogined=true;
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
	}
}

void CtpTrader::OnRspQryTradingAccount(CThostFtdcTradingAccountField *pTradingAccount, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		///请求查询投资者持仓;
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	if (pTradingAccount)
	{
		boost::shared_ptr<TradingAccount> m_TradingAccount=boost::make_shared<TradingAccount>();
		m_TradingAccount->AccountID=pTradingAccount->AccountID;
		m_TradingAccount->Available=pTradingAccount->Available;
		m_TradingAccount->Balance=pTradingAccount->Balance;
		m_TradingAccount->BrokerID=pTradingAccount->BrokerID;
		m_TradingAccount->CashIn=pTradingAccount->CashIn;
		m_TradingAccount->CloseProfit=pTradingAccount->CloseProfit;
		m_TradingAccount->Commission=pTradingAccount->Commission;
		m_TradingAccount->Credit=pTradingAccount->Credit;
		m_TradingAccount->CurrencyID=pTradingAccount->CurrencyID;
		m_TradingAccount->CurrMargin=pTradingAccount->CurrMargin;
		m_TradingAccount->DeliveryMargin=pTradingAccount->DeliveryMargin;
		m_TradingAccount->Deposit=pTradingAccount->Deposit;
		m_TradingAccount->ExchangeDeliveryMargin=pTradingAccount->ExchangeDeliveryMargin;
		m_TradingAccount->ExchangeMargin=pTradingAccount->ExchangeDeliveryMargin;
		m_TradingAccount->FrozenCash=pTradingAccount->FrozenCash;
		m_TradingAccount->FrozenCommission=pTradingAccount->FrozenCommission;
		m_TradingAccount->FrozenMargin=pTradingAccount->FrozenMargin;
		m_TradingAccount->FundMortgageAvailable=pTradingAccount->FundMortgageAvailable;
		m_TradingAccount->FundMortgageIn=pTradingAccount->FundMortgageIn;
		m_TradingAccount->FundMortgageOut=pTradingAccount->FundMortgageOut;
		m_TradingAccount->Interest=pTradingAccount->Interest;
		m_TradingAccount->InterestBase=pTradingAccount->InterestBase;
		m_TradingAccount->Mortgage=pTradingAccount->Mortgage;
		m_TradingAccount->MortgageableFund=pTradingAccount->MortgageableFund;
		m_TradingAccount->PositionProfit=pTradingAccount->PositionProfit;
		m_TradingAccount->PreBalance=pTradingAccount->PreBalance;
		m_TradingAccount->PreCredit=pTradingAccount->PreCredit;
		m_TradingAccount->PreDeposit=pTradingAccount->PreDeposit;
		m_TradingAccount->PreFundMortgageIn=pTradingAccount->PreFundMortgageIn;
		m_TradingAccount->PreFundMortgageOut=pTradingAccount->PreFundMortgageOut;
		m_TradingAccount->PreMargin=pTradingAccount->PreMargin;
		m_TradingAccount->PreMortgage=pTradingAccount->PreMargin;
		m_TradingAccount->Reserve=pTradingAccount->Reserve;
		m_TradingAccount->ReserveBalance=pTradingAccount->ReserveBalance;
		m_TradingAccount->SettlementID=pTradingAccount->SettlementID;
		m_TradingAccount->SpecProductCloseProfit=pTradingAccount->SpecProductCloseProfit;
		m_TradingAccount->SpecProductCommission=pTradingAccount->SpecProductCommission;
		m_TradingAccount->SpecProductExchangeMargin=pTradingAccount->SpecProductExchangeMargin;
		m_TradingAccount->SpecProductFrozenCommission=pTradingAccount->SpecProductFrozenCommission;
		m_TradingAccount->SpecProductFrozenMargin=pTradingAccount->SpecProductFrozenMargin;
		m_TradingAccount->SpecProductMargin=pTradingAccount->SpecProductMargin;
		m_TradingAccount->SpecProductPositionProfit=pTradingAccount->SpecProductPositionProfit;
		m_TradingAccount->SpecProductPositionProfitByAlg=pTradingAccount->SpecProductPositionProfitByAlg;
		m_TradingAccount->TradingDay=pTradingAccount->TradingDay;
		m_TradingAccount->Withdraw=pTradingAccount->Withdraw;
		m_TradingAccount->WithdrawQuota=pTradingAccount->WithdrawQuota;

		SaveTradingAccount(*m_TradingAccount);
		//读取配置文件;
		m_InvestorAccount->SafeUpdate(m_TradingAccount);

		//NotifyApiMgr(RspQryTradingAccount,LPARAM(&m_TradingAccount));
	}
	LOGDEBUG("查询账户资金响应...");
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		if (!QryPosition(""))
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}
void CtpTrader::OnRspQryInstrument(CThostFtdcInstrumentField *pInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	if (pInstrument!=NULL)
	{
		auto instrument_data=boost::make_shared<Instrument>();
		instrument_data->InstrumentID=pInstrument->InstrumentID;
		instrument_data->ExchangeID=pInstrument->ExchangeID;
		instrument_data->InstrumentName=pInstrument->InstrumentName;
		instrument_data->ProductClass=(EnumProductClass)pInstrument->ProductClass;
		instrument_data->ProductID=pInstrument->ProductID;
		instrument_data->CreateDate=pInstrument->CreateDate;
		instrument_data->OpenDate=pInstrument->OpenDate;
		instrument_data->ExpireDate=pInstrument->ExpireDate;
		instrument_data->IsTrading=pInstrument->IsTrading;
		instrument_data->PositionType=(EnumPositionType)pInstrument->PositionDateType;
		instrument_data->LongMarginRatio=pInstrument->LongMarginRatio;
		instrument_data->ShortMarginRatio=pInstrument->ShortMarginRatio;
		instrument_data->IsTrading=pInstrument->IsTrading;
		instrument_data->VolumeMultiple=pInstrument->VolumeMultiple;
		instrument_data->PriceTick=pInstrument->PriceTick;
		instrument_data->StartDelivDate = pInstrument->StartDelivDate;
		instrument_data->EndDelivDate = pInstrument->EndDelivDate;
		if ( instrument_data->ProductClass == PC_Futures )
		{
			m_FuturesInstruments.insert(instrument_data->InstrumentID);
		}
		//LOGDEBUG("合约:{},类型:{},品种:{}", pInstrument->InstrumentID,pInstrument->ProductID, pInstrument->ProductClass);
		sigInstrumentInfo(instrument_data);
	}
	
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		if (!QryAccount())
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		return;
	}
	if (pInvestorPosition)
	{
		LOGDEBUG("ID={},Position={},YdPosition={},TodayPosition={},PosiDirection={},PositionDate={}",
			pInvestorPosition->InstrumentID,pInvestorPosition->Position,
			pInvestorPosition->YdPosition,
			pInvestorPosition->TodayPosition,
			pInvestorPosition->PosiDirection,
			pInvestorPosition->PositionDate);

		boost::shared_ptr<InvestorPosition> ip = boost::make_shared<InvestorPosition>();
		ip->InvestorID = pInvestorPosition->InvestorID;
		ip->BrokerID = pInvestorPosition->BrokerID;
		//ip->AbandonFrozen = pInvestorPosition->AbandonFrozen;
		ip->CashIn = pInvestorPosition->CashIn;
		ip->CloseAmount = pInvestorPosition->CloseAmount;
		ip->CloseProfit = pInvestorPosition->CloseProfit;
		ip->CloseProfitByDate = pInvestorPosition->CloseProfitByDate;
		ip->CloseProfitByTrade = pInvestorPosition->CloseProfitByTrade;
		ip->CloseVolume = pInvestorPosition->CloseVolume;
		ip->CombLongFrozen = pInvestorPosition->CombLongFrozen;
		ip->CombPosition = pInvestorPosition->CombPosition;
		ip->CombShortFrozen = pInvestorPosition->CombShortFrozen;
		ip->Commission = pInvestorPosition->Commission;
		ip->ExchangeMargin = pInvestorPosition->ExchangeMargin;
		ip->FrozenCash = pInvestorPosition->FrozenCash;
		ip->FrozenCommission = pInvestorPosition->FrozenCommission;
		ip->FrozenMargin = pInvestorPosition->FrozenMargin;
		ip->HedgeFlag = (EnumHedgeFlag)pInvestorPosition->HedgeFlag;
		ip->InstrumentID = pInvestorPosition->InstrumentID;
		ip->LongFrozen = pInvestorPosition->LongFrozen;
		ip->LongFrozenAmount = pInvestorPosition->LongFrozenAmount;
		ip->MarginRateByMoney = pInvestorPosition->MarginRateByMoney;
		ip->MarginRateByVolume = pInvestorPosition->MarginRateByVolume;
		ip->OpenAmount = pInvestorPosition->OpenAmount;
		ip->OpenCost = pInvestorPosition->OpenCost;
		ip->OpenVolume = pInvestorPosition->OpenVolume;
		ip->PosiDirection = (EnumPosiDirection)pInvestorPosition->PosiDirection;
		ip->Position = pInvestorPosition->Position;
		ip->PositionCost = pInvestorPosition->PositionCost;
		ip->PositionDate = (EnumPositionDateType)pInvestorPosition->PositionDate;
		ip->PositionProfit = pInvestorPosition->PositionProfit;
		ip->PreMargin = pInvestorPosition->PreMargin;
		ip->PreSettlementPrice = pInvestorPosition->PreSettlementPrice;
		ip->SettlementPrice = pInvestorPosition->SettlementPrice;
		ip->ShortFrozen = pInvestorPosition->ShortFrozen;
		ip->ShortFrozenAmount = pInvestorPosition->ShortFrozenAmount;
		//ip->StrikeFrozen = pInvestorPosition->StrikeFrozen;
		//ip->StrikeFrozenAmount = pInvestorPosition->StrikeFrozenAmount;
		ip->TodayPosition = pInvestorPosition->TodayPosition;
		ip->TradingDay = pInvestorPosition->TradingDay;
		ip->UseMargin = pInvestorPosition->UseMargin;
		ip->YdPosition = pInvestorPosition->YdPosition;
		m_InvestorAccount->SafeUpdate(ip);
		//NotifyApiMgr(RspQryPosition, LPARAM(&ip));
	}
	LOGDEBUG("查询投资者持仓响应...");
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		if (!QryPositionDetail(""))
		{
			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		}
	}
}


void CtpTrader::OnRspOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pInputOrder)
	{
		boost::shared_ptr<Order> inOrder = boost::make_shared<Order>();
		Order& inputOrder = *inOrder;
		inputOrder.BrokerID=pInputOrder->BrokerID;
		strncpy((char*)inputOrder.CombHedgeFlag, pInputOrder->CombHedgeFlag, sizeof(pInputOrder->CombHedgeFlag));
		strcpy((char*)inputOrder.CombOffsetFlag,pInputOrder->CombOffsetFlag);
		inputOrder.ContingentCondition=pInputOrder->ContingentCondition;
		inputOrder.Direction=(EnumDirection)pInputOrder->Direction;
		inputOrder.GTDDate=pInputOrder->Direction;
		inputOrder.InstrumentID=pInputOrder->InstrumentID;
		inputOrder.InvestorID=pInputOrder->InvestorID;
		inputOrder.LimitPrice=pInputOrder->LimitPrice;
		inputOrder.MinVolume=pInputOrder->MinVolume;
		inputOrder.OrderPriceType=(EnumOrderPriceType)pInputOrder->OrderPriceType;
		inputOrder.OrderRef=pInputOrder->OrderRef;
		inputOrder.StopPrice=pInputOrder->StopPrice;
		inputOrder.TimeCondition=pInputOrder->TimeCondition;
		inputOrder.UserID=pInputOrder->UserID;
		inputOrder.VolumeCondition=pInputOrder->VolumeCondition;
		inputOrder.VolumeTotalOriginal=pInputOrder->VolumeTotalOriginal;
		inputOrder.RequestID = pInputOrder->RequestID;

		if (IsErrorRspInfo(pRspInfo) && m_UserInfo.IsTraderLogined)
		{
			inputOrder.ErrorID = pRspInfo->ErrorID;
			inputOrder.ErrorMsg = pRspInfo->ErrorMsg;
		}
		
		sigOnOrder(inOrder);
		LOGDEBUG("OnRspOrderInsert响应...");
	}
}

// void CTraderSpi::ReqOrderAction(CThostFtdcOrderField *pOrder)
// {
// 	static bool ORDER_ACTION_SENT = false;		//是否发送了报单
// 	if (ORDER_ACTION_SENT)
// 		return;
// 
// 	CThostFtdcInputOrderActionField req;
// 	memset(&req, 0, sizeof(req));
// 	///经纪公司代码
// 	strcpy(req.BrokerID, pOrder->BrokerID);
// 	///投资者代码
// 	strcpy(req.InvestorID, pOrder->InvestorID);
// 	///报单操作引用
// 	//	TThostFtdcOrderActionRefType	OrderActionRef;
// 	///报单引用
// 	strcpy(req.OrderRef, pOrder->OrderRef);
// 	///请求编号
// 	//	TThostFtdcRequestIDType	RequestID;
// 	///前置编号
// 	req.FrontID = FRONT_ID;
// 	///会话编号
// 	req.SessionID = SESSION_ID;
// 	///交易所代码
// 	//	TThostFtdcExchangeIDType	ExchangeID;
// 	///报单编号
// 	//	TThostFtdcOrderSysIDType	OrderSysID;
// 	///操作标志
// 	req.ActionFlag = THOST_FTDC_AF_Delete;
// 	///价格
// 	//	TThostFtdcPriceType	LimitPrice;
// 	///数量变化
// 	//	TThostFtdcVolumeType	VolumeChange;
// 	///用户代码
// 	//	TThostFtdcUserIDType	UserID;
// 	///合约代码
// 	strcpy(req.InstrumentID, pOrder->InstrumentID);
// 
// 	int iResult = pUserApi->ReqOrderAction(&req, ++iRequestID);
// 	cerr << "--->>> 报单操作请求: " << ((iResult == 0) ? "成功" : "失败") << endl;
// 	ORDER_ACTION_SENT = true;
//}

void CtpTrader::OnRspOrderAction(CThostFtdcInputOrderActionField *pInputOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		if (pRspInfo)
		{
			LOGDEBUG("撤单有误:{},{}", pRspInfo->ErrorID, pRspInfo->ErrorMsg);
		}
	}
	if (pInputOrderAction)
	{
		LOGDEBUG("---撤单失败---:FrontID={},SessionID={},ExchangeID={},OrderSysID={},OrderRef={},InstrumentID={}",
			pInputOrderAction->FrontID, pInputOrderAction->SessionID, pInputOrderAction->ExchangeID, 
			pInputOrderAction->OrderSysID, pInputOrderAction->OrderRef,pInputOrderAction->InstrumentID);
		boost::shared_ptr<InputOrderAction> inputOrderAction = boost::make_shared<InputOrderAction>();
		inputOrderAction->ActionFlag = (EnumActionFlag)pInputOrderAction->ActionFlag;
		inputOrderAction->BrokerID = pInputOrderAction->BrokerID;
		inputOrderAction->ExchangeID = pInputOrderAction->ExchangeID;
		inputOrderAction->FrontID = pInputOrderAction->FrontID;
		inputOrderAction->InstrumentID = pInputOrderAction->InstrumentID;
		inputOrderAction->InvestorID = pInputOrderAction->InvestorID;
		inputOrderAction->OrderActionRef = pInputOrderAction->OrderActionRef;
		inputOrderAction->OrderRef = pInputOrderAction->OrderRef;
		inputOrderAction->OrderSysID = pInputOrderAction->OrderSysID;
		inputOrderAction->RequestID = pInputOrderAction->RequestID;
		inputOrderAction->SessionID = pInputOrderAction->SessionID;
		sigOnOrderAction(inputOrderAction);
	}
}

boost::shared_ptr<Order> CtpTrader::Convert(CThostFtdcOrderField* pOrder)
{
	if (pOrder)
	{
		boost::shared_ptr<Order> order = boost::make_shared<Order>();
		order->InvestorID = pOrder->InvestorID;
		order->InstrumentID = pOrder->InstrumentID;
		order->ActiveTime = pOrder->ActiveTime;
		order->ActiveTraderID = pOrder->ActiveTraderID;
		order->ActiveUserID = pOrder->ActiveTraderID;
		order->BrokerID = pOrder->BrokerID;
		order->BrokerOrderSeq = pOrder->BrokerOrderSeq;
		order->BusinessUnit = pOrder->BusinessUnit;
		order->CancelTime = pOrder->CancelTime;
		order->ClearingPartID = pOrder->ClearingPartID;
		order->ClientID = pOrder->ClientID;
		strncpy((char*)order->CombHedgeFlag, pOrder->CombHedgeFlag, sizeof(pOrder->CombHedgeFlag));
		strcpy((char*)order->CombOffsetFlag, pOrder->CombOffsetFlag);
		order->ContingentCondition = (EnumContingentCondition)pOrder->ContingentCondition;
		order->Direction = (EnumDirection)pOrder->Direction;
		order->ExchangeID = pOrder->ExchangeID;
		order->ExchangeInstID = pOrder->ExchangeInstID;
		order->ForceCloseReason = (EnumForceCloseReason)pOrder->ForceCloseReason;
		order->FrontID = pOrder->FrontID;
		order->GTDDate = pOrder->GTDDate;
		order->InsertDate = pOrder->InsertDate;
		order->InsertTime = pOrder->InsertTime;
		order->InstallID = pOrder->InstallID;
		order->InstrumentID = pOrder->InstrumentID;
		order->InvestorID = pOrder->InvestorID;
		order->IsAutoSuspend = pOrder->IsAutoSuspend;
		order->IsSwapOrder = pOrder->IsSwapOrder;
		order->LimitPrice = pOrder->LimitPrice;
		order->MinVolume = pOrder->MinVolume;
		order->NotifySequence = pOrder->NotifySequence;
		order->OrderLocalID = pOrder->OrderLocalID;
		order->OrderPriceType = (EnumOrderPriceType)pOrder->OrderPriceType;
		order->OrderRef = pOrder->OrderRef;
		order->OrderSource = (EnumOrderSource)pOrder->OrderSource;
		order->OrderStatus = (EnumOrderStatus)pOrder->OrderStatus;
		order->OrderSubmitStatus = (EnumOrderSubmitStatus)pOrder->OrderSubmitStatus;
		order->OrderSysID = pOrder->OrderSysID;
		order->OrderType = (EnumOrderType)pOrder->OrderType;
		order->ParticipantID = pOrder->ParticipantID;
		order->RelativeOrderSysID = pOrder->RelativeOrderSysID;
		order->RequestID = pOrder->RequestID;
		order->SequenceNo = pOrder->SequenceNo;
		order->SessionID = pOrder->SessionID;
		order->SettlementID = pOrder->SettlementID;
		order->StatusMsg = pOrder->StatusMsg;
		order->StopPrice = pOrder->StopPrice;
		order->SuspendTime = pOrder->SuspendTime;
		order->TimeCondition = (EnumTimeCondition)pOrder->TimeCondition;
		order->TraderID = pOrder->TraderID;
		order->TradingDay = pOrder->TradingDay;
		order->UpdateTime = pOrder->UpdateTime;
		order->UserForceClose = pOrder->UserForceClose;
		order->UserID = pOrder->UserID;
		order->UserProductInfo = pOrder->UserProductInfo;
		order->VolumeCondition = (EnumVolumeCondition)pOrder->VolumeCondition;
		order->VolumeTotal = pOrder->VolumeTotal;
		order->VolumeTotalOriginal = pOrder->VolumeTotalOriginal;
		order->VolumeTraded = pOrder->VolumeTraded;
		order->ZCETotalTradedVolume = pOrder->ZCETotalTradedVolume;
		return order;
	}
	return nullptr;
}

boost::shared_ptr<Trade> CtpTrader::Convert(CThostFtdcTradeField* pTrade)
{
	boost::shared_ptr<Trade> td = boost::make_shared<Trade>();
	td->BrokerID = pTrade->BrokerID;
	td->BrokerOrderSeq = pTrade->BrokerOrderSeq;
	td->BusinessUnit = pTrade->BusinessUnit;
	td->ClearingPartID = pTrade->ClearingPartID;
	td->ClientID = pTrade->ClientID;
	td->Direction = (EnumDirection)pTrade->Direction;
	td->ExchangeID = pTrade->ExchangeID;
	td->ExchangeInstID = pTrade->ExchangeInstID;
	td->HedgeFlag = (EnumHedgeFlag)pTrade->HedgeFlag;
	td->InstrumentID = pTrade->InstrumentID;
	td->InvestorID = pTrade->InvestorID;
	td->OffsetFlag = (EnumOffsetFlag)pTrade->OffsetFlag;
	td->OrderLocalID = pTrade->OrderLocalID;
	td->OrderRef = pTrade->OrderRef;
	td->OrderSysID = pTrade->OrderSysID;
	td->ParticipantID = pTrade->ParticipantID;
	td->Price = pTrade->Price;
	td->PriceSource = (EnumPriceSource)pTrade->PriceSource;
	td->SequenceNo = pTrade->SequenceNo;
	td->SettlementID = pTrade->SettlementID;
	td->TradeDate = pTrade->TradeDate;
	td->TradeID = pTrade->TradeID;
	td->TraderID = pTrade->TraderID;
	td->TradeSource = (EnumTradeSource)pTrade->TradeSource;
	td->TradeTime = pTrade->TradeTime;
	td->TradeType = (EnumTradeType)pTrade->TradeType;
	td->TradingRole = (EnumTradingRole)pTrade->TradingRole;
	td->TradingDay = pTrade->TradingDay;
	td->UserID = pTrade->UserID;
	td->Volume = pTrade->Volume;
	return td;
}

boost::shared_ptr<ParkedOrder> CtpTrader::Convert(CThostFtdcParkedOrderField *pOrder)
{
	if (pOrder)
	{
		boost::shared_ptr<ParkedOrder> order = boost::make_shared<ParkedOrder>();
		order->InvestorID = pOrder->InvestorID;
		order->InstrumentID = pOrder->InstrumentID;
// 		order->ActiveTime = pOrder->ActiveTime;
// 		order->ActiveTraderID = pOrder->ActiveTraderID;
// 		order->ActiveUserID = pOrder->ActiveTraderID;
		order->BrokerID = pOrder->BrokerID;
//		order->BrokerOrderSeq = pOrder->BrokerOrderSeq;
		order->BusinessUnit = pOrder->BusinessUnit;
// 		order->CancelTime = pOrder->CancelTime;
// 		order->ClearingPartID = pOrder->ClearingPartID;
// 		order->ClientID = pOrder->ClientID;
		strncpy((char*)order->CombHedgeFlag, pOrder->CombHedgeFlag, sizeof(pOrder->CombHedgeFlag));
		strcpy((char*)order->CombOffsetFlag, pOrder->CombOffsetFlag);
		order->ContingentCondition = (EnumContingentCondition)pOrder->ContingentCondition;
		order->Direction = (EnumDirection)pOrder->Direction;
		order->ExchangeID = pOrder->ExchangeID;
//		order->ExchangeInstID = pOrder->ExchangeInstID;
		order->ForceCloseReason = (EnumForceCloseReason)pOrder->ForceCloseReason;
//		order->FrontID = pOrder->FrontID;
		order->GTDDate = pOrder->GTDDate;
// 		order->InsertDate = pOrder->InsertDate;
// 		order->InsertTime = pOrder->InsertTime;
// 		order->InstallID = pOrder->InstallID;
		order->InstrumentID = pOrder->InstrumentID;
		order->InvestorID = pOrder->InvestorID;
		order->IsAutoSuspend = pOrder->IsAutoSuspend;
		order->IsSwapOrder = pOrder->IsSwapOrder;
		order->LimitPrice = pOrder->LimitPrice;
		order->MinVolume = pOrder->MinVolume;
// 		order->NotifySequence = pOrder->NotifySequence;
// 		order->OrderLocalID = pOrder->OrderLocalID;
		order->OrderPriceType = (EnumOrderPriceType)pOrder->OrderPriceType;
		order->OrderRef = pOrder->OrderRef;
// 		order->OrderSource = (EnumOrderSource)pOrder->OrderSource;
// 		order->OrderStatus = (EnumOrderStatus)pOrder->OrderStatus;
// 		order->OrderSubmitStatus = (EnumOrderSubmitStatus)pOrder->OrderSubmitStatus;
// 		order->OrderSysID = pOrder->OrderSysID;
// 		order->OrderType = (EnumOrderType)pOrder->OrderType;
// 		order->ParticipantID = pOrder->ParticipantID;
// 		order->RelativeOrderSysID = pOrder->RelativeOrderSysID;
		order->RequestID = pOrder->RequestID;
// 		order->SequenceNo = pOrder->SequenceNo;
// 		order->SessionID = pOrder->SessionID;
// 		order->SettlementID = pOrder->SettlementID;
// 		order->StatusMsg = pOrder->StatusMsg;
		order->StopPrice = pOrder->StopPrice;
//		order->SuspendTime = pOrder->SuspendTime;
		order->TimeCondition = (EnumTimeCondition)pOrder->TimeCondition;
// 		order->TraderID = pOrder->TraderID;
// 		order->TradingDay = pOrder->TradingDay;
// 		order->UpdateTime = pOrder->UpdateTime;
		order->UserForceClose = pOrder->UserForceClose;
		order->UserID = pOrder->UserID;
//		order->UserProductInfo = pOrder->UserProductInfo;
		order->VolumeCondition = (EnumVolumeCondition)pOrder->VolumeCondition;
//		order->VolumeTotal = pOrder->VolumeTotal;
		order->VolumeTotalOriginal = pOrder->VolumeTotalOriginal;
// 		order->VolumeTraded = pOrder->VolumeTraded;
// 		order->ZCETotalTradedVolume = pOrder->ZCETotalTradedVolume;
		return order;
	}
	return nullptr;
}

///报单通知;
void CtpTrader::OnRtnOrder(CThostFtdcOrderField *pOrder)
{
	//报单回报;
	if (pOrder)
	{
		LOGDEBUG("CTP报单回报:{} {}", pOrder->OrderRef,pOrder->OrderStatus);
		boost::shared_ptr<Order> order = Convert(pOrder);
		m_InvestorAccount->SafeUpdate(order,m_UserInfo.IsTraderLogined);
		sigOnOrder(order);
		//DEBUG_PRINTF("报单回报:%s:sysid:%s,ref:%s",order.InstrumentID.c_str(),order.OrderSysID.c_str(),order.OrderRef.c_str());
		//NotifyApiMgr(RtnOrder,LPARAM(&order));
	}
}

///成交通知;
void CtpTrader::OnRtnTrade(CThostFtdcTradeField *pTrade)
{
	if (pTrade)
	{
		LOGDEBUG("CTP成交回报:{} {} {} {}",pTrade->UserID, pTrade->OrderRef,pTrade->Volume,pTrade->TradeID);
		boost::shared_ptr<Trade> td = Convert(pTrade);
		m_InvestorAccount->SafeUpdate(td, m_UserInfo.IsTraderLogined);
		sigOnTrade(td);
	}
}

void CtpTrader:: OnFrontDisconnected(int nReason)
{
	LOGDEBUG("交易前置断开连接:{}",nReason);
	m_bFrontDisconnected=true;
}

void CtpTrader::OnHeartBeatWarning(int nTimeLapse)
{
	LOGDEBUG( "--->>>  OnHeartBeatWarning" );
	LOGDEBUG("--->>> nTimerLapse = {}" , nTimeLapse );
}

void CtpTrader::OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	LOGDEBUG("OnRspError:{},{}",nRequestID, bIsLast);
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		if (!m_UserInfo.IsTraderLogined)
		{
			//可能是查询还未就绪;
			QryTradingParam();
		}
	}
}

bool CtpTrader::IsErrorRspInfo( CThostFtdcRspInfoField *pRspInfo )
{
	bool bResult = ((pRspInfo!=NULL) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		LOGDEBUG("--->>> ErrorID={}, ErrorMsg={}",  pRspInfo->ErrorID , pRspInfo->ErrorMsg);
	}
	return bResult;
}

CThostFtdcTraderApi* CtpTrader::GetTraderApi()
{
	return m_pTraderApi;
}

void CtpTrader::OnRspQryExchange( CThostFtdcExchangeField *pExchange, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	//保存;
	if (pExchange)
	{
		boost::shared_ptr<exchange_t> exchange=boost::make_shared<exchange_t>();
		strcpy(exchange->ExchangeID,pExchange->ExchangeID);
		strcpy(exchange->ExchangeName,pExchange->ExchangeName);
		exchange->ExchangeProperty=(EnumExchangeProperty)pExchange->ExchangeProperty;
		sigExchangeInfo(exchange);
	}
	LOGDEBUG("查询交易所响应...");
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		//能源交易所;
		std::string INE_EXCHANGE_ID("INE");
		boost::shared_ptr<exchange_t> exchange = boost::make_shared<exchange_t>();
		strcpy(exchange->ExchangeID, INE_EXCHANGE_ID.c_str());
		strcpy(exchange->ExchangeName,"上海能源交易所");
		exchange->ExchangeProperty = (EnumExchangeProperty)pExchange->ExchangeProperty;
		sigExchangeInfo(exchange);
		if (!QryProduct("",0))
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::SetLastError(CThostFtdcRspInfoField *pRspInfo)
{
	if (NULL==pRspInfo)
	{
		m_ErrorMessage.ErrorMsg="未知的错误";
		return ;
	}
	m_ErrorMessage.ErrorMsg=pRspInfo->ErrorMsg;
	m_ErrorMessage.ErrorID=pRspInfo->ErrorID;
}

void CtpTrader::OnRspQryInstrumentCommissionRate( CThostFtdcInstrumentCommissionRateField *pInstrumentCommissionRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//查手续费;
	if (IsErrorRspInfo(pRspInfo))
	{
		LOGDEBUG("查询合约手续费...出错");
	}
	else
	{
		//保存;
		if (pInstrumentCommissionRate)
		{
			auto UpdateCommisionRateFunc = [](boost::shared_ptr<InstrumentCommisionRate> icr,
				CThostFtdcInstrumentCommissionRateField* pInstrumentCommissionRate)
			{
				icr->BrokerID = pInstrumentCommissionRate->BrokerID;
				icr->CloseRatioByMoney = pInstrumentCommissionRate->CloseRatioByMoney;
				icr->CloseRatioByVolume = pInstrumentCommissionRate->CloseRatioByVolume;
				icr->CloseTodayRatioByMoney = pInstrumentCommissionRate->CloseTodayRatioByMoney;
				icr->CloseTodayRatioByVolume = pInstrumentCommissionRate->CloseTodayRatioByVolume;
				icr->InvestorID = pInstrumentCommissionRate->InvestorID;
				icr->InvestorRange = (EnumInvestorRange)pInstrumentCommissionRate->InvestorRange;
				icr->OpenRatioByMoney = pInstrumentCommissionRate->OpenRatioByMoney;
				icr->OpenRatioByVolume = pInstrumentCommissionRate->OpenRatioByVolume;
				icr->IsInitied = true;
			};
			auto cIter = m_InstCommisionRates.find(pInstrumentCommissionRate->InstrumentID);
			if (cIter == m_InstCommisionRates.end())
			{
				auto curIter = m_InstCommisionRates.begin();
				while (true)
				{
					cIter = std::find_if(curIter, m_InstCommisionRates.end(),
						[pInstrumentCommissionRate](std::pair<std::string, boost::shared_ptr<InstrumentCommisionRate> > CommisionPair)
					{
						std::vector<std::string> ProductYM;
						boost::split(ProductYM, CommisionPair.first, boost::is_any_of("1234567890"));
						if (ProductYM.size() > 0 && ProductYM[0] == pInstrumentCommissionRate->InstrumentID)
						{
							return true;
						}
						return false;
					});
					if (cIter == m_InstCommisionRates.end())
					{
						break;
					}
					auto icr = cIter->second;
					icr->InstrumentID = cIter->first;
					UpdateCommisionRateFunc(icr, pInstrumentCommissionRate);
					curIter = cIter;
					curIter++;
					LOGDEBUG("查询合约手续费...{}", icr->InstrumentID);
				};
			}
			else
			{
				auto icr = cIter->second;
				icr->InstrumentID = cIter->first;
				UpdateCommisionRateFunc(icr, pInstrumentCommissionRate);
				LOGDEBUG("查询合约手续费...{}", icr->InstrumentID);
			}
		}
	}
	m_CV4QryInstCommisonRate.notify_one();
}

bool CtpTrader::OrderInsert( boost::shared_ptr<InputOrder> pInputOrder )
{
	InputOrder& param = *pInputOrder;
	CThostFtdcInputOrderField req;
	memset(&req, 0, sizeof(req));
	///经纪公司代码;
	if (param.BrokerID.empty())
	{
		param.BrokerID=this->m_ServerInfo.id;
	}
	strcpy(req.BrokerID, param.BrokerID.c_str());
	///投资者代码;
	if (param.InvestorID.empty())
	{
		param.InvestorID=this->m_UserInfo.UserID;
	}
	strcpy(req.InvestorID, param.InvestorID.c_str());
	///合约代码;
	strcpy(req.InstrumentID, param.InstrumentID.c_str());
	///用户代码;
	strcpy(req.UserID,param.InvestorID.c_str());
	///买卖方向;
	req.Direction = param.Direction;
	req.LimitPrice=param.LimitPrice;
	///组合开平标志:;
	req.CombOffsetFlag[0] = param.CombOffsetFlag[0];
	req.CombOffsetFlag[1] = param.CombOffsetFlag[1];
	///数量: 1;
	req.VolumeTotalOriginal = param.VolumeTotalOriginal;
	///强平原因: 非强平;
	req.ForceCloseReason = FCC_NotForceClose;
	///自动挂起标志: 否;
	req.IsAutoSuspend = 0;
	///用户强评标志: 否;
	req.UserForceClose = 0;
	//最小数量;
	if (param.MinVolume<=0 || param.MinVolume>param.VolumeTotalOriginal)
	{
		req.MinVolume = param.VolumeTotalOriginal;
	}
	else
	{
		req.MinVolume = param.MinVolume;
	}
	///组合投机套保标志:投机;
	if (param.CombHedgeFlag[0] != HF_Speculation
		&& param.CombHedgeFlag[0] != HF_Arbitrage
		&& param.CombHedgeFlag[0] != HF_Hedge)
	{
		//默认情况下使用投机;
		req.CombHedgeFlag[0] = HF_Speculation;
		param.CombHedgeFlag[0] = HF_Speculation;
		///组合投机套保标志:投机;
		if (param.CombHedgeFlag[1] != HF_None)
		{
			//默认情况下使用投机;
			req.CombHedgeFlag[1] = HF_Speculation;
			param.CombHedgeFlag[1] = HF_Speculation;
		}
	}
	else
	{
		strcpy(req.CombHedgeFlag, (char*)param.CombHedgeFlag);
	}
	///报单引用:每次报单是要自增1;
	if (param.OrderRef.empty())
	{
		unique_lock<mutex> lck(m_Mutex4OrderRef);
		int cur_order_ref = atoi(m_UserInfo.MaxOrderRef.c_str());
		cur_order_ref++;
		sprintf(req.OrderRef, "%d", cur_order_ref);
		param.OrderRef = req.OrderRef;
		m_UserInfo.MaxOrderRef = req.OrderRef;
	}
	else
	{
		strcpy(req.OrderRef, param.OrderRef.c_str());
		m_UserInfo.MaxOrderRef = param.OrderRef;
	}


	if (param.OrderPriceType==OPT_LimitPrice)
	{
		//限价单;
		//限价单:立即触发,最小成交量为1;
		req.ContingentCondition=THOST_FTDC_CC_Immediately; //立即触发;
		req.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
// 		if (req.LimitPrice<=0.0001f)  //组合合约价格是有可能小于0的;
// 		{
// 			//认为是市价单;
// 			req.VolumeCondition = THOST_FTDC_VC_AV;//小于最小量则不成交;
// 			req.OrderPriceType = THOST_FTDC_OPT_AnyPrice;
// 			req.TimeCondition = THOST_FTDC_TC_IOC;//立即完成1，当日有效3;
// 			req.LimitPrice = 0.0f;//价格;
// 		}
// 		else
		if (param.VolumeCondition == VC_MV)
		{
			req.VolumeCondition = THOST_FTDC_VC_MV;//小于最小量则不成交;
		}
		else if (param.VolumeCondition == VC_CV)
		{
			req.VolumeCondition = THOST_FTDC_VC_CV;//小于全部数量则不成交;
		}
		else
		{
			req.VolumeCondition = THOST_FTDC_VC_AV;//任意数量;
		}
		if (param.TimeCondition == TC_IOC)
		{
			///立即完成，否则撤销;
			req.TimeCondition = THOST_FTDC_TC_IOC;
		}
		else if (param.TimeCondition == TC_GFS)
		{
			///本节有效;
			req.TimeCondition = THOST_FTDC_TC_GFS;
		}
		else if (param.TimeCondition ==  TC_GTD)
		{
			///指定日期前有效;
			req.TimeCondition = THOST_FTDC_TC_GTD;
		}
		else if (param.TimeCondition == TC_GTC)
		{
			///撤销前有效;
			req.TimeCondition = THOST_FTDC_TC_GTC;
		}
		else if (param.TimeCondition == TC_GFA)
		{
			///集合竞价有效;
			req.TimeCondition = THOST_FTDC_TC_GFA;
		}
		else
		{
			req.TimeCondition = THOST_FTDC_TC_GFD;//立即完成，当日有效;
		}
	}
	else if (param.OrderPriceType==OPT_AnyPrice)
	{
		//市价;
		req.VolumeCondition=THOST_FTDC_VC_AV;//任意数量1,全部成交
		req.ContingentCondition=THOST_FTDC_CC_Immediately;//立即触发;
		req.OrderPriceType=THOST_FTDC_OPT_AnyPrice;
		req.TimeCondition=THOST_FTDC_TC_IOC;//立即完成1，当日有效3;
		req.LimitPrice = 0.0f;//价格;
	}
	else
	{
		//不支持的类型;
		return false;
	}
	param.FrontID = m_UserInfo.FrontID;
	param.SessionID = m_UserInfo.SessionID;
	
	GetInvestorAccount()->SafeUpdate(pInputOrder);
	int iResult = m_pTraderApi->ReqOrderInsert(&req, ++RequestID());
	if (iResult!=0)
	{
		Sleep(500);
		int iResult = m_pTraderApi->ReqOrderInsert(&req, ++RequestID());
		return iResult == 0;
	}
	else
	{
		return true;
	}
}

bool CtpTrader::OrderAction( boost::shared_ptr<InputOrderAction> pInputOrderAction)
{
	InputOrderAction& action = *pInputOrderAction;
	LOGDEBUG("撤单:FrontID={},SessionID={},ExchangeID={},OrderSysID={},OrderRef={}",
		action.FrontID,action.SessionID,action.ExchangeID,action.OrderSysID,action.OrderRef);
	CThostFtdcInputOrderActionField ctpOrderAction={0};
	ctpOrderAction.ActionFlag=action.ActionFlag;
	if (action.BrokerID.empty())
	{
		action.BrokerID=m_ServerInfo.id;
	}
	strcpy(ctpOrderAction.BrokerID,action.BrokerID.c_str());
	if (action.InvestorID.empty())
	{
		action.InvestorID = UserID();
	}
	strcpy(ctpOrderAction.InvestorID,action.InvestorID.c_str());
	if (!action.InstrumentID.empty())
	{
		strcpy(ctpOrderAction.InstrumentID,action.InstrumentID.c_str());
	}
	if (!action.ExchangeID.empty())
	{
		strcpy(ctpOrderAction.ExchangeID,action.ExchangeID.c_str());
	}

	if (action.FrontID==0)
	{
		action.FrontID = m_UserInfo.FrontID;
	}
	ctpOrderAction.FrontID = m_UserInfo.FrontID;

	if (action.SessionID==0)
	{
		action.SessionID=m_UserInfo.SessionID;
	}
	ctpOrderAction.SessionID = action.SessionID;
	if (!action.OrderSysID.empty())
	{
		std::string new_sys_id=string(sizeof(ctpOrderAction.OrderSysID)-(action.OrderSysID.length()),'\0')+action.OrderSysID;
		strcpy(ctpOrderAction.OrderSysID,new_sys_id.c_str());
	}
	if (action.OrderRef!="0")
	{
		strcpy(ctpOrderAction.OrderRef,action.OrderRef.c_str());
	}
	
	int iResult = m_pTraderApi->ReqOrderAction(&ctpOrderAction,++RequestID());
	if (iResult!=0)
	{
		Sleep(500);
		GetInvestorAccount()->SafeUpdate(pInputOrderAction);
		iResult = m_pTraderApi->ReqOrderAction(&ctpOrderAction, ++RequestID());
	}
	else
	{
		GetInvestorAccount()->SafeUpdate(pInputOrderAction);
	}
	return iResult == 0;
}

CtpTrader::~CtpTrader()
{
	//LOGDEBUG("Trader对象析构...");
	Release();
	LOGUNINIT();
}

bool CtpTrader::ReqUserLogin()
{
	if (!m_pTraderApi)
	{
		return false;
	}
	CThostFtdcReqUserLoginField login_field;
	memset(&login_field,0,sizeof(login_field));
	strcpy(login_field.BrokerID,m_UserInfo.BrokerID.c_str());
	strcpy(login_field.UserID,m_UserInfo.UserID.c_str());
	strcpy(login_field.Password,m_UserInfo.Password.c_str());
	int iResult=m_pTraderApi->ReqUserLogin(&login_field,++RequestID());
	if (iResult!=0)
	{
		//1,2,3：的意义参考文档;
		Sleep(1000);
		iResult=m_pTraderApi->ReqUserLogin(&login_field,++RequestID());
	}
	return iResult==0;
}

//结算确认;
bool CtpTrader::SettlementConfirm()
{
	if (m_pTraderApi==NULL)
	{
		return false;
	}
	CThostFtdcSettlementInfoConfirmField req = {0};
	memset(&req,0,sizeof(CThostFtdcQrySettlementInfoConfirmField));
	strcpy(req.BrokerID, m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	//DateTime nowTime=DateTime::Now();
	//注意，这里要用交易日确认;
	//strncpy(req.ConfirmTime,nowTime.GetLongTimeString().c_str(),sizeof(req.ConfirmTime));
	//strncpy(req.ConfirmDate,nowTime.GetShortDateString().c_str(),sizeof(req.ConfirmDate));
	int iResult=m_pTraderApi->ReqSettlementInfoConfirm(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqSettlementInfoConfirm(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryTradingParam()
{
	if (!m_pTraderApi)
	{
		//接口没有初始化;
		return false;
	}
	CThostFtdcQryBrokerTradingParamsField req={0};
	strcpy(req.CurrencyID,"RMB");
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	int iResult=m_pTraderApi->ReqQryBrokerTradingParams(&req,++RequestID());
	if(iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryBrokerTradingParams(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryTransBank()
{
	if (NULL==m_pTraderApi)
	{
		//接口没有初始化;
		return false;
	}
	CThostFtdcQryTransferBankField req={0};
	int iResult=m_pTraderApi->ReqQryTransferBank(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryTransferBank(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryNotice()
{
	if (NULL==m_pTraderApi)
	{
		//接口没有初始化;
		return false;
	}
	CThostFtdcQryNoticeField req={0};
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	int iResult=m_pTraderApi->ReqQryNotice(&req,++RequestID());
	if(iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryNotice(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryTradingNotice()
{
	if (NULL==m_pTraderApi)
	{
		//接口没有初始化;
		return false;
	}
	CThostFtdcQryTradingNoticeField req={0};
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	int iResult=m_pTraderApi->ReqQryTradingNotice(&req,++RequestID());
	if(iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryTradingNotice(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryInvestor()
{
	if (NULL==m_pTraderApi)
	{
		return false;
	}
	//这2个参数可以不填;
	CThostFtdcQryInvestorField qry={0};
	strcpy(qry.BrokerID,m_ServerInfo.id.c_str());
	strcpy(qry.InvestorID,m_UserInfo.UserID.c_str());
	int iResult=m_pTraderApi->ReqQryInvestor(&qry,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryInvestor(&qry,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryOrder(const std::string& OrderSysID, const std::string& instId)
{
	if (NULL == m_pTraderApi)
	{
		return false;
	}
	//这2个参数可以不填;
	CThostFtdcQryOrderField qry = { 0 };
	strcpy(qry.BrokerID, m_ServerInfo.id.c_str());
	strcpy(qry.InvestorID, m_UserInfo.UserID.c_str());
	strcpy(qry.OrderSysID, OrderSysID.c_str());
	strcpy(qry.InstrumentID, instId.c_str());
	int iResult = m_pTraderApi->ReqQryOrder(&qry, ++RequestID());
	if (iResult != 0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryOrder(&qry, ++RequestID());
	}
	return iResult == 0;
}

bool CtpTrader::QryTrade(const std::string& tradeid, const std::string& instId)
{
	if (NULL == m_pTraderApi)
	{
		return false;
	}
	//这2个参数可以不填;
	CThostFtdcQryTradeField qry = { 0 };
	strcpy(qry.BrokerID, m_ServerInfo.id.c_str());
	strcpy(qry.InvestorID, m_UserInfo.UserID.c_str());
	strcpy(qry.TradeID, tradeid.c_str());
	strcpy(qry.InstrumentID, instId.c_str());
	int iResult = m_pTraderApi->ReqQryTrade(&qry, ++RequestID());
	if (iResult != 0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryTrade(&qry, ++RequestID());
	}
	return iResult == 0;
}

bool CtpTrader::QryExchange( const std::string& exchgId )
{
	if (!m_pTraderApi)
	{
		return false;
	}
	CThostFtdcQryExchangeField reqExchange={0};
	strncpy(reqExchange.ExchangeID,exchgId.c_str(),sizeof(reqExchange.ExchangeID));
	int iResult=m_pTraderApi->ReqQryExchange(&reqExchange,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryExchange(&reqExchange,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryInstrument( const std::string& instId,const std::string& exchgId )
{
	if (NULL==m_pTraderApi)
	{
		return false;
	}
	CThostFtdcQryInstrumentField req={0};
	if (!instId.empty())
	{
		strncpy(req.InstrumentID,instId.c_str(),sizeof(req.InstrumentID));
	}
	
	if (!exchgId.empty())
	{
		strncpy(req.ExchangeID,exchgId.c_str(),sizeof(req.ExchangeID));
	}
	int iResult= m_pTraderApi->ReqQryInstrument(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult= m_pTraderApi->ReqQryInstrument(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryAccount()
{
	if (NULL==m_pTraderApi)
	{
		//接口没有初始化;
		return false;
	}
	CThostFtdcQryTradingAccountField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, m_ServerInfo.id.c_str());
	strcpy(req.InvestorID, m_UserInfo.UserID.c_str());
	int iResult = m_pTraderApi->ReqQryTradingAccount(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryTradingAccount(&req,++RequestID());
	}
	return iResult==0;
}


bool CtpTrader::QryBrokerTradingAlgos(const std::string& instId, const std::string& ExchangeID /* = ""*/)
{
	CThostFtdcQryBrokerTradingAlgosField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, m_ServerInfo.id.c_str());
	strcpy(req.ExchangeID, ExchangeID.c_str());
	strcpy(req.InstrumentID, instId.c_str());
	int iResult = m_pTraderApi->ReqQryBrokerTradingAlgos(&req, ++RequestID());
	if (iResult != 0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryBrokerTradingAlgos(&req, ++RequestID());
	}
	return iResult == 0;
}


bool CtpTrader::QryPosition(const std::string& instrument_id)
{
	//请求查询投资者持仓;
	CThostFtdcQryInvestorPositionField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	strcpy(req.InstrumentID, instrument_id.c_str());
	int iResult = m_pTraderApi->ReqQryInvestorPosition(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryInvestorPosition(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryPositionDetail(const std::string& instrument_id)
{
	//查询投资者持仓明细;
	struct CThostFtdcQryInvestorPositionDetailField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	strcpy(req.InstrumentID, instrument_id.c_str());
	int iResult = m_pTraderApi->ReqQryInvestorPositionDetail(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryInvestorPositionDetail(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryCombPosDetail(const std::string& instrument_id)
{
	CThostFtdcQryInvestorPositionCombineDetailField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	strcpy(req.CombInstrumentID, instrument_id.c_str());
	int iResult = m_pTraderApi->ReqQryInvestorPositionCombineDetail(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		m_pTraderApi->ReqQryInvestorPositionCombineDetail(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QrySettlementConfirm()
{
	//查询结算信息确认;
	if (m_pTraderApi==NULL)
	{
		return false;
	}
	//查询用户是否已登录;
	//请求查询结算结果确认;
	CThostFtdcQrySettlementInfoConfirmField req={0};
	//memset(&req,0,sizeof(CThostFtdcQrySettlementInfoConfirmField));
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	int iResult=m_pTraderApi->ReqQrySettlementInfoConfirm(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQrySettlementInfoConfirm(&req,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryInstCommissionRate( const std::string& instrument_id)
{
	if (NULL==m_pTraderApi)
	{
		return false;
	}
	
	CThostFtdcQryInstrumentCommissionRateField req={0};
	strcpy(req.BrokerID,m_ServerInfo.id.c_str());
	strcpy(req.InvestorID,m_UserInfo.UserID.c_str());
	strcpy(req.InstrumentID,instrument_id.c_str());
	int iResult=m_pTraderApi->ReqQryInstrumentCommissionRate(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQryInstrumentCommissionRate(&req,++RequestID());
	}
	if (iResult != 0)
	{
		return false;
	}
	
	int login_timeout = m_ServerInfo.market_login_timeout;
	if (login_timeout <= 0)
	{
		login_timeout = 30;
	}
	unique_lock<mutex> lck(m_Mutex4QryInstCommisonRate);
	if (m_CV4QryInstCommisonRate.wait_for(lck, chrono::seconds(login_timeout)) == cv_status::timeout)
	{
		return false;
	}
	return true;
}

void CtpTrader::OnRspUserLogout( CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspUserPasswordUpdate( CThostFtdcUserPasswordUpdateField *pUserPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspTradingAccountPasswordUpdate( CThostFtdcTradingAccountPasswordUpdateField *pTradingAccountPasswordUpdate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspParkedOrderInsert( CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspParkedOrderAction( CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspQueryMaxOrderVolume( CThostFtdcQueryMaxOrderVolumeField *pQueryMaxOrderVolume, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspRemoveParkedOrder( CThostFtdcRemoveParkedOrderField *pRemoveParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspRemoveParkedOrderAction( CThostFtdcRemoveParkedOrderActionField *pRemoveParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

//void trader::OnRspExecOrderInsert( CThostFtdcInputExecOrderField *pInputExecOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
//{
//    //throw std::exception("The method or operation is not implemented.");
//}

//void trader::OnRspExecOrderAction( CThostFtdcInputExecOrderActionField *pInputExecOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
//{
//    //throw std::exception("The method or operation is not implemented.");
//}

//void trader::OnRspForQuoteInsert( CThostFtdcInputForQuoteField *pInputForQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
//{
//    //throw std::exception("The method or operation is not implemented.");
//}

//void trader::OnRspQuoteInsert( CThostFtdcInputQuoteField *pInputQuote, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
//{
//    //throw std::exception("The method or operation is not implemented.");
//}

//void trader::OnRspQuoteAction( CThostFtdcInputQuoteActionField *pInputQuoteAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
//{
//    //throw std::exception("The method or operation is not implemented.");
//}

//void trader::OnRspCombActionInsert( CThostFtdcInputCombActionField *pInputCombAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
//{
//    //throw std::exception("The method or operation is not implemented.");
//}

void CtpTrader::OnRspQryOrder( CThostFtdcOrderField *pOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		return;
	}
	if (pOrder)
	{
		boost::shared_ptr<Order> order =Convert(pOrder);
		m_InvestorAccount->SafeUpdate(order, m_UserInfo.IsTraderLogined);
	}
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryTrade("",""))
		{
			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		}
	}
}

void CtpTrader::OnRspQryTrade( CThostFtdcTradeField *pTrade, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		return;
	}
	if (pTrade)
	{
		boost::shared_ptr<Trade> trade = Convert(pTrade);
		m_InvestorAccount->SafeUpdate(trade, m_UserInfo.IsTraderLogined);
	}
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QrySettlementConfirm())
		{
			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		}
	}
}

void CtpTrader::OnRspQryInvestor( CThostFtdcInvestorField *pInvestor, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return;
	}
	if (pInvestor)
	{
		Investor m_Investor;
		m_Investor.Address=pInvestor->Address;
		m_Investor.BrokerID=pInvestor->BrokerID;
		m_Investor.CommModelID=pInvestor->CommModelID;
		m_Investor.IdentifiedCardNo=pInvestor->IdentifiedCardNo;
		m_Investor.IdentifiedCardType=
			(EnumIdCardType)pInvestor->IdentifiedCardType;
		m_Investor.InvestorGroupID=pInvestor->InvestorGroupID;
		m_Investor.InvestorID=pInvestor->InvestorID;
		m_Investor.InvestorName=pInvestor->InvestorName;
		m_Investor.IsActive=pInvestor->IsActive;
		m_Investor.MarginModelID=pInvestor->MarginModelID;
		m_Investor.Mobile=pInvestor->Mobile;
		m_Investor.OpenDate=pInvestor->OpenDate;
		m_Investor.Telephone=pInvestor->Telephone;

		m_UserInfo.InvestorID = m_Investor.InvestorID;

		m_InvestorAccount->SafeUpdate(m_Investor);
		//NotifyApiMgr(RspQryInvestor,LPARAM(&m_Investor));
	}
	LOGDEBUG("查询投资者响应...");
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryExchange(""))
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryTradingCode( CThostFtdcTradingCodeField *pTradingCode, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspQryInstrumentMarginRate( CThostFtdcInstrumentMarginRateField *pInstrumentMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//查手续费;
	if (IsErrorRspInfo(pRspInfo))
	{
		LOGDEBUG("查询合约保证金...出错");
	}
	else
	{
		//保存;
		if (pInstrumentMarginRate)
		{
			boost::shared_ptr<InstrumentMarginRate> imr = boost::make_shared<InstrumentMarginRate>();
			imr->BrokerID = pInstrumentMarginRate->BrokerID;
			imr->HedgeFlag = (EnumHedgeFlag)pInstrumentMarginRate->HedgeFlag;
			imr->InstrumentID = pInstrumentMarginRate->InstrumentID;
			imr->InvestorID = pInstrumentMarginRate->InvestorID;
			imr->InvestorRange = (EnumInvestorRange)pInstrumentMarginRate->InvestorRange;
			imr->IsRelative = pInstrumentMarginRate->IsRelative;
			imr->LongMarginRatioByMoney = pInstrumentMarginRate->LongMarginRatioByMoney;
			imr->LongMarginRatioByVolume = pInstrumentMarginRate->LongMarginRatioByVolume;
			imr->ShortMarginRatioByMoney = pInstrumentMarginRate->ShortMarginRatioByMoney;
			imr->ShortMarginRatioByVolume = pInstrumentMarginRate->ShortMarginRatioByVolume;
			//NotifyApiMgr(RspQryInstMarginRate, LPARAM(&imr));
			LOGDEBUG("查询合约保证金率:{}", pInstrumentMarginRate->InstrumentID);
			m_InstMarginRates[pInstrumentMarginRate->InstrumentID] = imr;
		}
	}
	m_CV4QryInstMarginRate.notify_one();
}

void CtpTrader::OnRspQryProduct( CThostFtdcProductField *pProduct, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		return;
	}
	if (pProduct != NULL)
	{
		auto instrument_data = boost::make_shared<Product>();
		instrument_data->ExchangeID = pProduct->ExchangeID;
		instrument_data->ProductClass = (EnumProductClass)pProduct->ProductClass;
		instrument_data->ProductID = pProduct->ProductID;
		instrument_data->PositionType = (EnumPositionType)pProduct->PositionDateType;
		instrument_data->VolumeMultiple = pProduct->VolumeMultiple;
		instrument_data->PriceTick = pProduct->PriceTick;
// 		if (instId.find("SP ") == std::string::npos
// 			&& instId.find("IPS ") == std::string::npos
// 			&& instId.find("SPD ") == std::string::npos
// 			&& instId.find("SPC ") == std::string::npos)
// 		{
// 			//组合合约;
// 			boost::shared_ptr<InstrumentCommisionRate> icr = boost::make_shared<InstrumentCommisionRate>();
// 
// 			m_InstCommisionRates.insert(std::make_pair(instrument_data->InstrumentID, icr));
// 		}


		LOGDEBUG("品种:{},类型:{}", pProduct->ProductID, pProduct->ProductClass);
//		sigInstrumentInfo(instrument_data);
	}

	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		if (!QryInstrument("",""))
		{
			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		}
	}
}

void CtpTrader::OnRspQryDepthMarketData( CThostFtdcDepthMarketDataField *pDepthMarketData, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspQrySettlementInfo( CThostFtdcSettlementInfoField *pSettlementInfo, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return;
	}
	if (pSettlementInfo)
	{
		m_SettlementInfo.BrokerID=pSettlementInfo->BrokerID;
		m_SettlementInfo.Content+=pSettlementInfo->Content;
		m_SettlementInfo.InvestorID=pSettlementInfo->InvestorID;
		m_SettlementInfo.SequenceNo=pSettlementInfo->SequenceNo;
		m_SettlementInfo.SettlementID=pSettlementInfo->SettlementID;
		m_SettlementInfo.TradingDay=pSettlementInfo->TradingDay;
		if (bIsLast && m_InvestorAccount)
		{
			m_InvestorAccount->SafeUpdate(m_SettlementInfo);
			m_SettlementInfo.Content = "";
		}
		//NotifyApiMgr(QuerySettlementInfo,LPARAM(&m_SettlementInfo));
	}
	LOGDEBUG("查询结算信息响应...");
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		//结算信息确认;
		if (!SettlementConfirm())
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryTransferBank( CThostFtdcTransferBankField *pTransferBank, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if ((bIsLast && IsErrorRspInfo(pRspInfo)) )
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return;
	}
	if (pTransferBank)
	{
		static TransferBank tb;
		tb.BankID=pTransferBank->BankID;
		tb.BankBrchID=pTransferBank->BankBrchID;
		tb.BankName=pTransferBank->BankName;
		tb.IsActive=pTransferBank->IsActive;
		//TraderSharedData::GetInstance().transferBanks.push_back(tb);
		//NotifyApiMgr(RspQryTransferBank,LPARAM(&tb));
		if (bIsLast)
		{
			LOGDEBUG("查询转账银行响应...");
		}
	}

	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryNotice())
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryInvestorPositionDetail( CThostFtdcInvestorPositionDetailField *pInvestorPositionDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return;
	}
	if (pInvestorPositionDetail)
	{
		boost::shared_ptr<InvestorPositionDetail> ipd=boost::make_shared<InvestorPositionDetail>();
		ipd->InstrumentID = pInvestorPositionDetail->InstrumentID;
		ipd->BrokerID=pInvestorPositionDetail->BrokerID;
		ipd->InvestorID = pInvestorPositionDetail->InvestorID;
		ipd->HedgeFlag = (EnumHedgeFlag)pInvestorPositionDetail->HedgeFlag;
		ipd->Direction = (EnumDirection)pInvestorPositionDetail->Direction;
		ipd->OpenDate = pInvestorPositionDetail->OpenDate;
		ipd->TradeID = pInvestorPositionDetail->TradeID;
		ipd->Volume = pInvestorPositionDetail->Volume;
		ipd->OpenPrice = pInvestorPositionDetail->OpenPrice;
		ipd->TradingDay = pInvestorPositionDetail->TradingDay;
		ipd->SettlementID = pInvestorPositionDetail->SettlementID;
		ipd->TradeType = (EnumTradeType)pInvestorPositionDetail->TradeType;
		ipd->CombInstrumentID = pInvestorPositionDetail->CombInstrumentID;
		ipd->ExchangeID = pInvestorPositionDetail->ExchangeID;
		ipd->CloseProfitByDate = pInvestorPositionDetail->CloseProfitByDate;
		ipd->CloseProfitByTrade = pInvestorPositionDetail->CloseProfitByTrade;
		ipd->PositionProfitByTrade = pInvestorPositionDetail->PositionProfitByDate;
		ipd->PositionProfitByDate = pInvestorPositionDetail->PositionProfitByDate;
		ipd->Margin = pInvestorPositionDetail->Margin;
		ipd->ExchMargin = pInvestorPositionDetail->ExchMargin;
		ipd->MarginRateByMoney = pInvestorPositionDetail->MarginRateByMoney;
		ipd->MarginRateByVolume = pInvestorPositionDetail->MarginRateByVolume;
		ipd->LastSettlementPrice = pInvestorPositionDetail->LastSettlementPrice;
		ipd->SettlementPrice = pInvestorPositionDetail->SettlementPrice;
		ipd->CloseAmount=pInvestorPositionDetail->CloseAmount;
		ipd->CloseVolume=pInvestorPositionDetail->CloseVolume;
		LOGDEBUG("持仓明细:{} {} {}",pInvestorPositionDetail->InstrumentID,ipd->Volume,ipd->Direction);
		m_InvestorAccount->SafeUpdate(ipd);
		//NotifyApiMgr(RspQryPositionDetail,LPARAM(&ipd));
	}

	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryCombPosDetail(""))
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryNotice( CThostFtdcNoticeField *pNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//查询通知;
	if (IsErrorRspInfo(pRspInfo) && bIsLast)
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	if (pNotice)
	{
		m_Notice.Content+=pNotice->Content;
		m_Notice.SequenceLabel.push_back(pNotice->SequenceLabel);
		//if (bIsLast)
		//{
		//	 NotifyApiMgr(RspQryNotice,LPARAM(&m_Notice));
		//}
	}
	LOGDEBUG("查询通知响应...");
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		//继续登录;
		if (!QryTradingNotice())
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryInvestorPositionCombineDetail( CThostFtdcInvestorPositionCombineDetailField *pInvestorPositionCombineDetail, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	if (pInvestorPositionCombineDetail)
	{
		boost::shared_ptr<InvestorPositionCombineDetail> ipcd = boost::make_shared<InvestorPositionCombineDetail>();
		ipcd->BrokerID=pInvestorPositionCombineDetail->BrokerID;
		ipcd->CombInstrumentID=pInvestorPositionCombineDetail->CombInstrumentID;
		ipcd->ComTradeID=pInvestorPositionCombineDetail->ComTradeID;
		ipcd->Direction=(EnumDirection)pInvestorPositionCombineDetail->Direction;
		ipcd->ExchangeID=pInvestorPositionCombineDetail->ExchangeID;
		ipcd->ExchMargin=pInvestorPositionCombineDetail->ExchMargin;
		ipcd->HedgeFlag=(EnumHedgeFlag)pInvestorPositionCombineDetail->HedgeFlag;
		ipcd->InstrumentID=pInvestorPositionCombineDetail->InstrumentID;
		ipcd->InvestorID=pInvestorPositionCombineDetail->InvestorID;
		ipcd->LegID=pInvestorPositionCombineDetail->LegID;
		ipcd->LegMultiple=pInvestorPositionCombineDetail->LegMultiple;
		ipcd->Margin=pInvestorPositionCombineDetail->Margin;
		ipcd->MarginRateByMoney=pInvestorPositionCombineDetail->MarginRateByMoney;
		ipcd->MarginRateByVolume=pInvestorPositionCombineDetail->MarginRateByVolume;
		ipcd->OpenDate=pInvestorPositionCombineDetail->OpenDate;
		ipcd->SettlementID=pInvestorPositionCombineDetail->SettlementID;
		ipcd->TotalAmt=pInvestorPositionCombineDetail->TotalAmt;
		ipcd->TradeGroupID=pInvestorPositionCombineDetail->TradeGroupID;
		ipcd->TradeID=pInvestorPositionCombineDetail->TradeID;
		ipcd->TradingDay=pInvestorPositionCombineDetail->TradingDay;
		m_InvestorAccount->SafeUpdate(ipcd,m_UserInfo.IsTraderLogined);
		//NotifyApiMgr(RspQryCombPosition,LPARAM(&ipcd));
	}
	LOGDEBUG("查询投资者组合持仓响应...");
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryOrder("",""))
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryExchangeMarginRate( CThostFtdcExchangeMarginRateField *pExchangeMarginRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		return;
	}
	//保存;
	if (pExchangeMarginRate)
	{
		InstrumentMarginRate imr;
		imr.BrokerID = pExchangeMarginRate->BrokerID;
		imr.HedgeFlag = (EnumHedgeFlag)pExchangeMarginRate->HedgeFlag;
		imr.InstrumentID = pExchangeMarginRate->InstrumentID;
// 		imr.InvestorID = pExchangeMarginRate->InvestorID;
// 		imr.InvestorRange = (EnumInvestorRange)pExchangeMarginRate->InvestorRange;
// 		imr.IsRelative = pExchangeMarginRate->IsRelative;
		imr.LongMarginRatioByMoney = pExchangeMarginRate->LongMarginRatioByMoney;
		imr.LongMarginRatioByVolume = pExchangeMarginRate->LongMarginRatioByVolume;
		imr.ShortMarginRatioByMoney = pExchangeMarginRate->ShortMarginRatioByMoney;
		imr.ShortMarginRatioByVolume = pExchangeMarginRate->ShortMarginRatioByVolume;
		//NotifyApiMgr(RspQryInstMarginRate, LPARAM(&imr));
	}
	if (bIsLast && !m_UserInfo.IsTraderLogined)
	{
		if (!QryAccount())
		{
			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		}
	}
}

void CtpTrader::OnRspQryExchangeRate( CThostFtdcExchangeRateField *pExchangeRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnErrRtnOrderInsert( CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo )
{
	
	LOGDEBUG("OnErrRtnOrderInsert响应...");
	if (pInputOrder)
	{
		boost::shared_ptr<Order> inOrder = boost::make_shared<Order>();
		Order& inputOrder=*inOrder;
		inputOrder.BrokerID=pInputOrder->BrokerID;
		strcpy((char*)inputOrder.CombHedgeFlag,pInputOrder->CombHedgeFlag);
		strcpy((char*)inputOrder.CombOffsetFlag,pInputOrder->CombOffsetFlag);
		inputOrder.ContingentCondition=pInputOrder->ContingentCondition;
		inputOrder.Direction=(EnumDirection)pInputOrder->Direction;
		inputOrder.GTDDate=pInputOrder->Direction;
		inputOrder.InstrumentID=pInputOrder->InstrumentID;
		inputOrder.InvestorID=pInputOrder->InvestorID;
		inputOrder.LimitPrice=pInputOrder->LimitPrice;
		inputOrder.MinVolume=pInputOrder->MinVolume;
		inputOrder.OrderPriceType=(EnumOrderPriceType)pInputOrder->OrderPriceType;
		inputOrder.OrderRef=pInputOrder->OrderRef;
		inputOrder.StopPrice=pInputOrder->StopPrice;
		inputOrder.TimeCondition=pInputOrder->TimeCondition;
		inputOrder.UserID=pInputOrder->UserID;
		inputOrder.VolumeCondition=pInputOrder->VolumeCondition;
		inputOrder.VolumeTotalOriginal=pInputOrder->VolumeTotalOriginal;
		inputOrder.RequestID = pInputOrder->RequestID;
		if (IsErrorRspInfo(pRspInfo) && m_UserInfo.IsTraderLogined)
		{
			inputOrder.ErrorID = pRspInfo->ErrorID;
			inputOrder.ErrorMsg = pRspInfo->ErrorMsg;
			//NotifyApiMgr(RspOrderInsert, LPARAM(&inputOrder));
		}
		sigOnOrder(inOrder);
	}
}

void CtpTrader::OnRtnInstrumentStatus( CThostFtdcInstrumentStatusField *pInstrumentStatus )
{
	if (pInstrumentStatus)
	{
		boost::shared_ptr<InstStatus> is=boost::make_shared<InstStatus>();
		is->ExchangeID=pInstrumentStatus->ExchangeID;
		is->InstrumentID=pInstrumentStatus->InstrumentID;
		is->SettlementGroupID=pInstrumentStatus->SettlementGroupID;
		is->InstrumentStatus=(EnumInstStatus)pInstrumentStatus->InstrumentStatus;
		is->TradingSegmentSN=pInstrumentStatus->TradingSegmentSN;
		is->EnterTime=pInstrumentStatus->EnterTime;
		is->EnterReason=(EnumInstStatusEnterReason)pInstrumentStatus->EnterReason;

		LOGDEBUG("合约状态:{},{}",is->InstrumentID,(char)is->InstrumentStatus);
		m_InstStatus[is->InstrumentID] = is;
		sigInstrumentStatus(is);
	}
}

void CtpTrader::OnRtnTradingNotice( CThostFtdcTradingNoticeInfoField *pTradingNoticeInfo )
{
	if (pTradingNoticeInfo)
	{

	}
}

//void trader::OnRtnQuote( CThostFtdcQuoteField *pQuote )
//{
//    //throw std::exception("The method or operation is not implemented.");
//}

void CtpTrader::OnRspQryParkedOrder( CThostFtdcParkedOrderField *pParkedOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
	if (bIsLast && IsErrorRspInfo(pRspInfo))
	{
		NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		return;
	}
	if (pParkedOrder)
	{
		boost::shared_ptr<ParkedOrder> order = Convert(pParkedOrder);
		//m_InvestorAccount->SafeUpdate(order, m_UserInfo.IsTraderLogined);
	}
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryTrade("", ""))
		{
			NotifyUserLogin(pRspInfo, nRequestID, bIsLast);
		}
	}
}

void CtpTrader::OnRspQryParkedOrderAction( CThostFtdcParkedOrderActionField *pParkedOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}

void CtpTrader::OnRspQryTradingNotice( CThostFtdcTradingNoticeField *pTradingNotice, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if ((bIsLast && IsErrorRspInfo(pRspInfo)))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	if (pTradingNotice)
	{
		TradingNotice tn;
		tn.BrokerID=pTradingNotice->BrokerID;
		tn.InvestorID=pTradingNotice->InvestorID;
		tn.InvestorRange=(EnumInvestorRange)pTradingNotice->InvestorRange;
		tn.SendTime=pTradingNotice->SendTime;
		tn.SequenceNo=pTradingNotice->SequenceNo;
		tn.SequenceSeries=pTradingNotice->SequenceSeries;
		//NotifyApiMgr(RspQryTradingNotice,LPARAM(&tn));
	}
	LOGDEBUG("查询交易通知响应...");
	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if (!QryInvestor())
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryBrokerTradingParams( CThostFtdcBrokerTradingParamsField *pBrokerTradingParams, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	if ((bIsLast && IsErrorRspInfo(pRspInfo)))
	{
		NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		return ;
	}
	LOGDEBUG("查询交易参数响应...");
	if (pBrokerTradingParams)
	{
		m_BrokerTradingParams.Algorithm=(EnumAlgorithm)pBrokerTradingParams->Algorithm;
		m_BrokerTradingParams.AvailIncludeCloseProfit=(EnumIncludeCloseProfit)pBrokerTradingParams->AvailIncludeCloseProfit;
		m_BrokerTradingParams.CurrencyID=pBrokerTradingParams->CurrencyID;
		m_BrokerTradingParams.MarginPriceType=(EnumMarginPrice)pBrokerTradingParams->MarginPriceType;
		//m_BrokerTradingParams.OptionRoyaltyPriceType=(EnumOptionRoyaltyPrice)pBrokerTradingParams->OptionRoyaltyPriceType;
		//NotifyApiMgr(RspQryBrokerTradingParams,LPARAM(&m_BrokerTradingParams));
		if (m_InvestorAccount)
		{
			m_InvestorAccount->SafeUpdate(boost::make_shared<BrokerTradingParams>(m_BrokerTradingParams));
		}
	}

	if (!m_UserInfo.IsTraderLogined && bIsLast)
	{
		if(!QryTransBank())
		{
			NotifyUserLogin(pRspInfo,nRequestID,bIsLast);
		}
	}
}

void CtpTrader::OnRspQryBrokerTradingAlgos( CThostFtdcBrokerTradingAlgosField *pBrokerTradingAlgos, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast )
{
	//throw std::exception("The method or operation is not implemented.");
}


bool CtpTrader::QrySettlementInfo()
{
	if (!m_pTraderApi)
	{
		return false;
	}
	//没有结算过:查结算信息;
	CThostFtdcQrySettlementInfoField qrySettlementInfo={0};
	strcpy(qrySettlementInfo.BrokerID,m_UserInfo.BrokerID.c_str());
	strcpy(qrySettlementInfo.InvestorID,m_UserInfo.UserID.c_str());
	//strcpy_s(qrySettlementInfo.TradingDay,curTime.FormatStd("%Y%m%d").c_str());
	int iResult=m_pTraderApi->ReqQrySettlementInfo(&qrySettlementInfo,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult=m_pTraderApi->ReqQrySettlementInfo(&qrySettlementInfo,++RequestID());
	}
	return iResult==0;
}

bool CtpTrader::QryDepthMarket(const std::string& instId)
{
	return false;
}

bool CtpTrader::ParkedOrderAction()
{
	return false;
}

bool CtpTrader::QryMaxOrderVolume(const std::string& instId)
{
	return false;
}

bool CtpTrader::QryExchgMarginRate(const std::string& instId)
{
	if (!m_pTraderApi)
	{
		return false;
	}
	//查询合约保证金率;
	CThostFtdcQryExchangeMarginRateField req = { 0 };
	strcpy(req.BrokerID, m_UserInfo.BrokerID.c_str());
	//strcpy(req.InvestorID, m_UserInfo.InvestorID.c_str());
	strcpy(req.InstrumentID, instId.c_str());
	req.HedgeFlag = THOST_FTDC_HF_Speculation;
	int iResult=m_pTraderApi->ReqQryExchangeMarginRate(&req,++RequestID());
	if (iResult!=0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryExchangeMarginRate(&req, ++RequestID());
	}
	return iResult == 0;
}

bool CtpTrader::ParkedOrderInsert(boost::shared_ptr<ParkedOrder> pParkedOrder)
{
	CThostFtdcParkedOrderField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, pParkedOrder->BrokerID.c_str());
	strcpy(req.InvestorID, pParkedOrder->InvestorID.c_str());
	strcpy(req.InstrumentID, pParkedOrder->InstrumentID.c_str());
	strcpy(req.BusinessUnit,pParkedOrder->BusinessUnit.c_str());
	strcpy(req.CombOffsetFlag,(char*)pParkedOrder->CombOffsetFlag);
	strcpy(req.CombHedgeFlag, (char*)pParkedOrder->CombHedgeFlag);
	req.ContingentCondition = pParkedOrder->ContingentCondition;
	req.Direction = pParkedOrder->Direction;
	strcpy(req.ExchangeID, pParkedOrder->ExchangeID.c_str());
	strcpy(req.GTDDate, pParkedOrder->GTDDate.c_str());
	strcpy(req.OrderRef, pParkedOrder->OrderRef.c_str());
	req.LimitPrice = pParkedOrder->LimitPrice;
	req.MinVolume = pParkedOrder->MinVolume;
	req.StopPrice = pParkedOrder->StopPrice;
	req.VolumeCondition = pParkedOrder->VolumeCondition;
	req.TimeCondition = pParkedOrder->TimeCondition;
	strcpy(req.UserID, pParkedOrder->UserID.c_str());
	req.VolumeTotalOriginal = pParkedOrder->VolumeTotalOriginal;
	req.OrderPriceType = pParkedOrder->OrderPriceType;
	int iResult = m_pTraderApi->ReqParkedOrderInsert(&req, ++RequestID());
	return iResult == 0;
}

std::string CtpTrader::GetApiVersion()
{
	//if (!m_pTraderApi)
	//{
	//	return "0.0.0.0";
	//}
	//return m_pTraderApi->GetApiVersion();
	return "6.3.5.1";
}

std::string CtpTrader::GetTradingDay()
{
	if (!m_pTraderApi)
	{
		//取当前时间;
		boost::posix_time::ptime dtLocal = boost::posix_time::second_clock::local_time();
		return boost::gregorian::to_iso_string(dtLocal.date());
	}
	return m_pTraderApi->GetTradingDay();
}

void CtpTrader::OnRspQryInstrumentOrderCommRate(CThostFtdcInstrumentOrderCommRateField *pInstrumentOrderCommRate, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (pInstrumentOrderCommRate)
	{
		LOGDEBUG(pInstrumentOrderCommRate->InstrumentID);
	}
}

bool CtpTrader::QryInstOrderCommRate(const std::string& instId)
{
	if (!m_pTraderApi)
	{
		return false;
	}
	//查询合约保证金率;
	CThostFtdcQryInstrumentOrderCommRateField req = { 0 };
	strcpy(req.BrokerID, m_UserInfo.BrokerID.c_str());
	strcpy(req.InvestorID, m_UserInfo.InvestorID.c_str());
	strcpy(req.InstrumentID, instId.c_str());

	int iResult = m_pTraderApi->ReqQryInstrumentOrderCommRate(&req, ++RequestID());
	if (iResult != 0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryInstrumentOrderCommRate(&req, ++RequestID());
	}
	return iResult == 0;
}

bool CtpTrader::QryInstMarginRate(const std::string& instId)
{
	if (!m_pTraderApi)
	{
		return false;
	}
	if (instId.find("SP ") ==0 || instId.find("IPS ")==0 || instId.find("SPD ") == 0 || instId.find("SPC ")==0)
	{
		return true;
	}
	//查询合约保证金率;
	CThostFtdcQryInstrumentMarginRateField req = { 0 };
	strcpy(req.BrokerID, m_UserInfo.BrokerID.c_str());
	strcpy(req.InvestorID, m_UserInfo.InvestorID.c_str());
	strcpy(req.InstrumentID, instId.c_str());
	req.HedgeFlag = THOST_FTDC_HF_Speculation;
	int iResult = m_pTraderApi->ReqQryInstrumentMarginRate(&req, ++RequestID());
	if (iResult != 0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryInstrumentMarginRate(&req, ++RequestID());
	}
	if (iResult != 0)
	{
		return false;
	}
	int login_timeout = m_ServerInfo.trader_login_timeout;
	if (login_timeout <= 0)
	{
		login_timeout = 30;
	}
	unique_lock<mutex> lck(m_Mutex4QryInstMarginRate);
	if (m_CV4QryInstMarginRate.wait_for(lck, chrono::seconds(login_timeout)) == cv_status::timeout)
	{
		return false;
	}
	return true;
}

bool CtpTrader::QryProduct(const std::string& productID, char productClass)
{
	if (NULL == m_pTraderApi)
	{
		return false;
	}
	CThostFtdcQryProductField req = { 0 };
	if (!productID.empty())
	{
		strncpy(req.ProductID, productID.c_str(), sizeof(req.ProductID));
	}
	req.ProductClass = productClass;
	int iResult = m_pTraderApi->ReqQryProduct(&req, ++RequestID());
	if (iResult != 0)
	{
		Sleep(1000);
		iResult = m_pTraderApi->ReqQryProduct(&req, ++RequestID());
	}
	return iResult == 0;
}

void CtpTrader::LoadInstCommisionRate()
{
	//加载手续费率;
	std::ifstream ifs("CTP_CommisionRate.bin", std::ios::binary);

	if (ifs.is_open())
	{
		boost::archive::binary_iarchive oa(ifs);

		oa >> m_InstCommisionRates;

		ifs.close();
	}
}

void CtpTrader::SaveInstCommisionRate()
{
	if (m_InstCommisionRates.size() == 0)
	{
		return;
	}
	//保存报单信息;
	std::ofstream ofs("CTP_CommisionRate.bin", std::ios::binary);

	if (ofs.is_open())
	{
		boost::archive::binary_oarchive oa(ofs);

		oa << m_InstCommisionRates;

		ofs.close();
	}
}

void CtpTrader::LoadInstMarginRate()
{
	std::ifstream ifs("CTP_MarginRate.bin", std::ios::binary);

	if (ifs.is_open())
	{
		boost::archive::binary_iarchive oa(ifs);

		oa >> m_InstMarginRates;

		ifs.close();
	}
}

void CtpTrader::SaveInstMarginRate()
{
	if (m_InstMarginRates.size() == 0)
	{
		return;
	}
	//保存报单信息;
	std::ofstream ofs("CTP_MarginRate.bin", std::ios::binary);

	if (ofs.is_open())
	{
		boost::archive::binary_oarchive oa(ofs);

		oa << m_InstMarginRates;

		ofs.close();
	}
}


void CtpTrader::SaveTradingAccount(const TradingAccount& ta)
{
	boost::property_tree::ptree ptTradingAccount;
	ptTradingAccount.put("AccountID", ta.AccountID);
	ptTradingAccount.put("Available", ta.Available);
	ptTradingAccount.put("Balance", ta.Balance);
	ptTradingAccount.put("BrokerID", ta.BrokerID);
	ptTradingAccount.put("CashIn", ta.CashIn);
	ptTradingAccount.put("CloseProfit", ta.CloseProfit);
	ptTradingAccount.put("Commission", ta.Commission );
	ptTradingAccount.put("Credit", ta.Credit );
	ptTradingAccount.put("CurrencyID", ta.CurrencyID );
	ptTradingAccount.put("CurrMargin", ta.CurrMargin );
	ptTradingAccount.put("DeliveryMargin", ta.DeliveryMargin);
	ptTradingAccount.put("Deposit", ta.Deposit);
	ptTradingAccount.put("ExchangeDeliveryMargin", ta.ExchangeDeliveryMargin );
	ptTradingAccount.put("ExchangeMargin", ta.ExchangeMargin );
	ptTradingAccount.put("FrozenCash", ta.FrozenCash);
	ptTradingAccount.put("FrozenCommission", ta.FrozenCommission );
	ptTradingAccount.put("FrozenMargin", ta.FrozenMargin);
	ptTradingAccount.put("FundMortgageAvailable", ta.FundMortgageAvailable );
	ptTradingAccount.put("FundMortgageIn", ta.FundMortgageIn );
	ptTradingAccount.put("FundMortgageOut", ta.FundMortgageOut );
	ptTradingAccount.put("Interest", ta.Interest);
	ptTradingAccount.put("InterestBase", ta.InterestBase);
	ptTradingAccount.put("Mortgage", ta.Mortgage);
	ptTradingAccount.put("MortgageableFund", ta.MortgageableFund);
	ptTradingAccount.put("PositionProfit", ta.PositionProfit);
	ptTradingAccount.put("PreBalance", ta.PreBalance);
	ptTradingAccount.put("PreCredit", ta.PreCredit);
	ptTradingAccount.put("PreDeposit", ta.PreDeposit);
	ptTradingAccount.put("PreFundMortgageIn", ta.PreFundMortgageIn);
	ptTradingAccount.put("PreFundMortgageOut", ta.PreFundMortgageOut);
	ptTradingAccount.put("PreMargin", ta.PreMargin);
	ptTradingAccount.put("PreMortgage", ta.PreMortgage);
	ptTradingAccount.put("Reserve", ta.Reserve);
	ptTradingAccount.put("ReserveBalance", ta.ReserveBalance);
	ptTradingAccount.put("SettlementID", ta.SettlementID);
	ptTradingAccount.put("SpecProductCloseProfit", ta.SpecProductCloseProfit);
	ptTradingAccount.put("SpecProductCommission", ta.SpecProductCommission);
	ptTradingAccount.put("SpecProductExchangeMargin", ta.SpecProductExchangeMargin);
	ptTradingAccount.put("SpecProductFrozenCommission", ta.SpecProductFrozenCommission);
	ptTradingAccount.put("SpecProductFrozenMargin", ta.SpecProductFrozenMargin);
	ptTradingAccount.put("SpecProductMargin", ta.SpecProductMargin);
	ptTradingAccount.put("SpecProductPositionProfit", ta.SpecProductPositionProfit);
	ptTradingAccount.put("SpecProductPositionProfitByAlg", ta.SpecProductPositionProfitByAlg);
	ptTradingAccount.put("TradingDay", ta.TradingDay);
	ptTradingAccount.put("Withdraw", ta.Withdraw);
	ptTradingAccount.put("WithdrawQuota", ta.WithdrawQuota);

	boost::property_tree::ptree pt;
	std::string sectionName = m_UserInfo.UserID;
	pt.put_child(sectionName, ptTradingAccount);

	boost::property_tree::ini_parser::write_ini("TradingAccount.ini", pt);
}

boost::shared_ptr<InstrumentCommisionRate> CtpTrader::GetInstCommissionRate(const std::string& instId)
{
	auto commIter = m_InstCommisionRates.find(instId);
	if (commIter == m_InstCommisionRates.end())
	{
		return nullptr;
	}
	return commIter->second;
}

boost::shared_ptr<InstrumentMarginRate> CtpTrader::GetInstMarginRate(const std::string& instId)
{
	auto marginIter = m_InstMarginRates.find(instId);
	if (marginIter == m_InstMarginRates.end())
	{
		return nullptr;
	}
	return marginIter->second;
}

boost::shared_ptr<BrokerTradingParams> CtpTrader::GetBrokerTradingParams()
{
	return boost::make_shared<BrokerTradingParams>(m_BrokerTradingParams);
}














