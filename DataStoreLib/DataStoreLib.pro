TARGET = DataStoreLib
TEMPLATE = lib
CONFIG -= qt
INCLUDEPATH += $$PWD/../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += /home/rmb338/boost_1_64_0
INCLUDEPATH += ../spdlog/include
DEFINES *= DATASTORELIB_EXPORTS
DEFINES += _USE_SPDLOG
CONFIG += debug_and_release
linux-g++|macx-g++{
    QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
    QMAKE_CFLAGS = -arch x86_64 -lpthread

    INCLUDEPATH += /home/rmb338/boost_1_64_0
    LIBS += -L/home/rmb338/boost_1_64_0/stage/lib
}

win32{
    INCLUDEPATH += F:/boost_1_63_0
    LIBS += -LF:/boost_1_63_0/lib32-msvc-12.0
}

CONFIG(debug, debug|release) {
        DESTDIR = ../build/v120/debug
        LIBS  += -L$$PWD/../build/v120/debug/ -lsqlite3 -lFacilityBaseLib -lTechLib
} else {
        DESTDIR = ../build/release
        LIBS = -L$$PWD/../build/v120/release/ -lsqlite3 -lFacilityBaseLib -lTechLib
}

include(./DataStoreLib.pri)
