#pragma once
#include "IDataStore.h"
#include "../sqlite3/sqlite3.h"

class instrument_data;

class BOOST_SYMBOL_EXPORT Sqlite3DataStore :
	public IDataStore
{
public:
	Sqlite3DataStore();
	virtual ~Sqlite3DataStore();

	virtual bool Init(const std::string& path);

	virtual enDBType GetType() ;

	virtual bool Load(boost::shared_ptr<kdata_container> pKData,int kPeriod) ;

	virtual bool Load(boost::shared_ptr<instrument_container> pContainer) ;

	virtual bool Load(const std::string& InstrumentID,boost::shared_ptr<tick_container> pTick,int nLength=0) ;

	virtual bool Load(boost::shared_ptr<minute_container> pMiniute) ;

	virtual bool Store(const std::string& InstrumentID,int kPeriod,boost::shared_ptr<kdata_container> pKData) ;

	virtual bool Store(boost::shared_ptr<instrument_container> pContainer) ;

	//返回值表示插入了多少条数据;
	virtual int Store(boost::shared_ptr<tick_container> pTick);

	virtual bool Store(boost::shared_ptr<minute_container> pMiniute);

	virtual bool PrepareData(boost::shared_ptr<instrument_data> pInstrument, int dType, int dPeriod, int dLength) override;
protected:
	//创建数据库并保存该指针;
	bool CreateDB(const std::string& dbname,sqlite3*& db);
protected:
	sqlite3* m_InstDB;//用来存合约信息;
	sqlite3* m_TickDB;//用来存Tick数据;
	sqlite3* m_KDataDB;//用来存K线数据;
	sqlite3* m_MinuteDB;//存储分时数据;
	std::map<std::string, sqlite3*> m_DBMap;//某类数据;
};

