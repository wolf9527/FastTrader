#include "stdafx.h"
#include "Sqlite3DataStore.h"
#include "Sqlite3Sql.h"
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/make_shared.hpp>
#include "../common/DataTypes.h"
#include "../FacilityBaseLib/instrument_data.h"


Sqlite3DataStore::Sqlite3DataStore()
{
	m_InstDB = nullptr;
	m_TickDB = nullptr;
	m_MinuteDB = nullptr;
	m_KDataDB = nullptr;
}


Sqlite3DataStore::~Sqlite3DataStore()
{
	//关闭数据库;
	for (auto dIter = m_DBMap.begin();dIter!=m_DBMap.end();++dIter)
	{
		if (dIter->second)
		{
			sqlite3_close(dIter->second);
			dIter->second = nullptr;
		}
	}
	m_DBMap.clear();
}

bool Sqlite3DataStore::Init(const std::string& path)
{
	CreateDB("instruments.db", m_InstDB);

	CreateDB("Tick.db", m_TickDB); 

	CreateDB("KData.db", m_KDataDB);

	CreateDB("Minute.db", m_MinuteDB);

	//建表;
	char* errmsg = nullptr;
	sqlite3_exec(m_InstDB,sql_create_table_instruments,nullptr,nullptr,&errmsg );

	sqlite3_exec(m_InstDB, sql_create_exchange, nullptr, nullptr, &errmsg);

	sqlite3_exec(m_MinuteDB, sql_create_minute, nullptr, nullptr, &errmsg);

	sqlite3_exec(m_InstDB, sql_create_minute, nullptr, nullptr, &errmsg);

	return true;
}

IDataStore::enDBType Sqlite3DataStore::GetType()
{
	return eSqlite;
}

bool Sqlite3DataStore::Load(boost::shared_ptr<kdata_container> pKData, int kPeriod)
{
	//"rb1810_min1";
	std::string InstrumentID = (*pKData)[0].szCode;
	std::string table_name = InstrumentID + "_" + std::to_string(kPeriod);
	if (/*exists table_name*/true)
	{
		std::string sql_select;
		//读表;
	}
	else
	{
		//读周期比kPeriod小的周期表;
		if (kPeriod == ktypeMin)
		{
			//读取Tick表;
			boost::shared_ptr<tick_container> tc=boost::make_shared<tick_container>();
			Load("",tc);
			tc->ToKData(*pKData);
		}
		else if (kPeriod == ktypeMin5)
		{
			boost::shared_ptr<kdata_container> kc;
			Load(kc,ktypeMin);
			kdata_container::Convert(*kc,*pKData);
		}
		else if (kPeriod == ktypeMin15)
		{
			boost::shared_ptr<kdata_container> kc;
			Load(kc, ktypeMin5);
			kdata_container::Convert(*kc, *pKData);
		}
		else if (kPeriod == ktypeDay)
		{
			boost::shared_ptr<kdata_container> kc;
			Load(kc, ktypeMin15);
			kdata_container::Convert(*kc, *pKData);
		}
	}
	return true;
}

bool Sqlite3DataStore::Load(boost::shared_ptr<instrument_container> pContainer)
{
	return true;
}

bool Sqlite3DataStore::Load(const std::string& InstrumentID, boost::shared_ptr<tick_container> pTick, int nLength /*= 0*/)
{
	if (!pTick)
	{
		return false;
	}
	std::string table_name=InstrumentID;
	std::string sql_select = boost::str(boost::format("select * from %1% ORDER BY TradingTime %2%,TradingMillisec %2% LIMIT %3%") 
		% table_name % "DESC" % nLength);
	sqlite3_stmt* statement = nullptr;
	if (SQLITE_OK!=sqlite3_prepare(m_TickDB,sql_select.c_str(),-1,&statement,nullptr))
	{
		return false;
	}
	int res = SQLITE_OK;
	while (true)
	{
		res = sqlite3_step(statement);
		if (res== SQLITE_ROW)
		{
			//获取数据;
			Tick tick;
			memset(&tick, 0, sizeof(Tick));
			strcpy(tick.szCode,(const char*)sqlite3_column_text(statement, 0));
			strcpy(tick.szExchange, (const char*)sqlite3_column_text(statement, 1));
			strcpy(tick.TradingDay, (const char*)sqlite3_column_text(statement, 2));

			//...;
			tick.UpdateTime = sqlite3_column_int64(statement, 3);
			tick.UpdateMillisec = sqlite3_column_int(statement, 4);

			tick.PreClosePrice = sqlite3_column_double(statement, 5);

			//...;
			pTick->push_back(tick);
		}
		else if (res  =SQLITE_DONE)
		{
			//遍历完成;
			break;
		}
	}
	return true;
}

bool Sqlite3DataStore::Load(boost::shared_ptr<minute_container> pMiniute)
{
	return true;
}

bool Sqlite3DataStore::Store(const std::string& InstrumentID, int kPeriod, boost::shared_ptr<kdata_container> pKData)
{
	std::string table_name = InstrumentID + "_" + std::to_string(kPeriod);

	return true;
}

bool Sqlite3DataStore::Store(boost::shared_ptr<instrument_container> pContainer)
{
	return true;
}

int Sqlite3DataStore::Store(boost::shared_ptr<tick_container> pTick)
{
	//tick container保证是时间排序的;
	//tick_container只包含一个合约的数据;
	if (pTick || pTick->size()==0)
	{
		return -1;
	}
	//建表,每个合约一个表;
	std::string table_name = (*pTick)[0].szCode;
	std::string sql_create_table = boost::str(boost::format(sql_create_table_report) % table_name);
	char* errmsg = nullptr;
	if (SQLITE_OK != sqlite3_exec(m_InstDB, sql_create_table.c_str(), nullptr, nullptr, &errmsg))
	{
		sqlite3_free(errmsg);
		return -1;
	}
	int nCount = 0;
	sqlite3_exec(m_InstDB,sql_begin_transaction,nullptr,nullptr,&errmsg);
	for (size_t i = 0; i < pTick->size();++i)
	{
		//拼接insert语句;
		auto& tick = (*pTick)[i];
		std::string sql_insert = 
			boost::str(boost::format(
			"insert into %1%  values('%2%','%3%','%4%',%5%,%6%,%7%,"
			"%8%,%9%,%10%,%11%,%12%,%13%,%14%,%15%,%16%,%17%,%18%,%19%,%20%,%21%,%22%,%23%,%24%,%25%)") 
			% table_name 
			%tick.szCode 
			% tick.szExchange 
			% tick.TradingDay 
			% tick.UpdateTime 
			% tick.UpdateMillisec
			% tick.PreSettlementPrice
			% tick.OpenPrice
			% tick.ClosePrice
			% tick.HighestPrice
			% tick.LowestPrice
			% tick.LastPrice
			% tick.AveragePrice
			% tick.CurrDelta
			% tick.PreDelta
			% tick.PreSettlementPrice
			% tick.SettlementPrice
			% tick.UpperLimitPrice
			% tick.LowerLimitPrice
			% tick.OpenInterest
			% tick.Volume
			% tick.Turnover
			% tick.BidPrice
			% tick.BidVolume
			% tick.AskPrice
			% tick.AskVolume
			);
		if (SQLITE_OK!=sqlite3_exec(m_InstDB,sql_insert.c_str(),nullptr,nullptr,&errmsg))
		{
			continue;
		}
		nCount++;
	}
	sqlite3_exec(m_InstDB, sql_commit_transaction, nullptr, nullptr, &errmsg);
	if (errmsg)
	{
		sqlite3_free(errmsg);
		errmsg = nullptr;
	}
	return nCount;
}

bool Sqlite3DataStore::Store(boost::shared_ptr<minute_container> pMiniute)
{
	return false;
}

bool Sqlite3DataStore::PrepareData(boost::shared_ptr<instrument_data> pInstrument, int dType, int dPeriod, int dLength)
{
	//加载数据;
	if (dType == instrument_data::dataReport)
	{
		//加载Tick数据;
		return Load(pInstrument->GetInstrumentInfo()->InstrumentID,pInstrument->GetTick());
	}
	else if (dType == instrument_data::dataInfo)
	{
		//加载合约数据;
		return instrument_container::GetInstance().get_info_by_id("", pInstrument->GetInstrumentInfo().get());
	}
	else if (dType == instrument_data::dataK)
	{
		//加载K线数据;
		return Load(pInstrument->GetKData(dPeriod),dPeriod);
	}
	return false;
}

bool Sqlite3DataStore::CreateDB(const std::string& dbname, sqlite3*& db)
{
	if (SQLITE_OK == sqlite3_open(dbname.c_str(), &db))
	{
		m_DBMap[dbname] = db;
	}
	return true;
}
