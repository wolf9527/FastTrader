

//#include "Database.h"
#include "QianLong.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/DateTime.h"
#include "../FacilityBaseLib/Instrument.h"
#include <cassert>
#include <fstream>
#include <cmath>
#include <sys/stat.h>
#ifdef WIN32
#include <direct.h>
#include <io.h>
#endif
using namespace std;

char CQianlong::m_szDataType[]	=	"钱龙";


bool convert_QL_Data_5min_to_KDATA( const char* dwMarket, const char *szCode, struct QL_Data_5min * pqlkd, KDATA *pkd )
{
	assert( pqlkd && pkd );
	if( NULL == pqlkd || NULL == pkd )
		return false;
	memset( pkd, 0, sizeof(KDATA) );

	strcpy(pkd->szExchange,dwMarket);
	if( szCode )
		strncpy( pkd->szCode, szCode, min(sizeof(pkd->szCode)-1,strlen(szCode)) );

	pkd->TradingDate		= pqlkd->min_off;
	DateTime	sptime;
	if( sptime.FromInstrumentTimeMin( pkd->TradingDate) )
		pkd->TradingTime	= sptime.GetTime();

	pkd->OpenPrice	= (double)fabs( pqlkd->open_price * 0.001 );
	pkd->HighestPrice	= (double)fabs( pqlkd->high_price * 0.001 );
	pkd->LowestPrice		= (double)fabs( pqlkd->low_price * 0.001 );
	pkd->ClosePrice	= (double)fabs( pqlkd->close_price * 0.001 );
	pkd->Volume	= fabs( pqlkd->min_volume * 100.);
	pkd->Turnover	= (double)fabs( pqlkd->min_amount * 100. );
	return true;
}

bool convert_QL_Data_day_to_KDATA( const char* dwMarket, const char *szCode, struct QL_Data_day * pqlkd, KDATA *pkd )
{
	assert( pqlkd && pkd );
	if( NULL == pqlkd || NULL == pkd )
		return false;
	memset( pkd, 0, sizeof(KDATA) );

	strcpy(pkd->szExchange,dwMarket);
	if( szCode )
		strncpy( pkd->szCode, szCode, min(sizeof(pkd->szCode)-1,strlen(szCode)) );

	pkd->TradingDate		= pqlkd->day_date;
	DateTime	sptime;
	if( sptime.FromInstrumentTimeDay(pkd->TradingDate) )
		pkd->TradingTime	= sptime.GetTime();

	pkd->OpenPrice	= (float)fabs( pqlkd->open_price * 0.001 );
	pkd->HighestPrice	= (float)fabs( pqlkd->high_price * 0.001 );
	pkd->LowestPrice		= (float)fabs( pqlkd->low_price * 0.001 );
	pkd->ClosePrice	= (float)fabs( pqlkd->close_price * 0.001 );
	pkd->Volume	= (float)fabs( pqlkd->day_volume * 100. );
	pkd->Turnover	= (float)fabs( pqlkd->day_amount * 1000. );
	return true;
}

bool convert_KDATA_to_QL_Data_day( KDATA * pkd, struct QL_Data_day * pqlkd )
{
	assert( pqlkd && pkd );
	if( NULL == pqlkd || NULL == pkd )
		return false;
	memset( pqlkd, 0, sizeof(struct QL_Data_day) );

	pqlkd->day_date		= (DWORD)( pkd->TradingDate );
	pqlkd->open_price	= (DWORD)( pkd->OpenPrice * 1000 );
	pqlkd->high_price	= (DWORD)( pkd->HighestPrice * 1000 );
	pqlkd->low_price	= (DWORD)( pkd->LowestPrice * 1000 );
	pqlkd->close_price	= (DWORD)( pkd->ClosePrice * 1000 );
	pqlkd->day_volume	= (DWORD)( pkd->Volume * 0.01 );
	pqlkd->day_amount	= (DWORD)( pkd->Turnover * 0.001 );
	return true;
}

bool convert_KDATA_to_QL_Data_5min( KDATA * pkd, struct QL_Data_5min * pqlkd )
{
	assert( pqlkd && pkd );
	if( NULL == pqlkd || NULL == pkd )
		return false;
	memset( pqlkd, 0, sizeof(struct QL_Data_day) );

	pqlkd->min_off		= (DWORD)( pkd->TradingTime );
	pqlkd->open_price	= (DWORD)( pkd->OpenPrice * 1000 );
	pqlkd->high_price	= (DWORD)( pkd->HighestPrice * 1000 );
	pqlkd->low_price	= (DWORD)( pkd->LowestPrice * 1000 );
	pqlkd->close_price	= (DWORD)( pkd->ClosePrice * 1000 );
	pqlkd->min_volume	= (DWORD)( pkd->Volume * 0.01 );
	pqlkd->min_amount	= (DWORD)( pkd->Turnover * 0.01 );
	return true;
}

void ConvertQLStockInfo( DWORD dwMarket, QL_Stock_info_V302 & block, instrument_info *pInfo )
{
	pInfo->clear();

	char	code[sizeof(block.stock_code)+2];
	memset(code,0,sizeof(code));
	strncpy(code,(const char *)block.stock_code,sizeof(block.stock_code));

	char	name[sizeof(block.stock_name)+2];
	memset(name,0,sizeof(name));
	strncpy(name,(const char *)block.stock_name,sizeof(block.stock_name));

	//pInfo->SetStockCode( dwMarket, code );
	//pInfo->SetStockName( name );
	//pInfo->SetType( block.stock_type );

/*
	pInfo->m_fLast		= (float)fabs( block.last_close_price * 0.001 );
	pInfo->OpenPrice		= (float)fabs( block.open_price * 0.001 );
	pInfo->HighestPrice		= (float)fabs( block.high_price * 0.001 );
	pInfo->LowestPrice		= (float)fabs( block.low_price * 0.001 );
	pInfo->ClosePrice		= (float)fabs( block.close_price * 0.001 );
	pInfo->Volume	= (float)fabs( block.total_volume * 100. );
	pInfo->Turnover	= (float)fabs( block.total_value * 1000. );
	pInfo->m_fBuyPrice[0]	= (float)fabs( block.buy_1_price * 0.001 );
	pInfo->m_fBuyVolume[0]	= (float)fabs( block.buy_1_volume * 100. );
	pInfo->m_fBuyPrice[1]	= (float)fabs( block.buy_2_price * 0.001 );
	pInfo->m_fBuyVolume[1]	= (float)fabs( block.buy_2_volume * 100. );
	pInfo->m_fBuyPrice[2]	= (float)fabs( block.buy_3_price * 0.001 );
	pInfo->m_fBuyVolume[2]	= (float)fabs( block.buy_3_volume * 100. );
	pInfo->m_fSellPrice[0]	= (float)fabs( block.sell_1_price * 0.001 );
	pInfo->m_fSellVolume[0]	= (float)fabs( block.sell_1_volume * 100. );
	pInfo->m_fSellPrice[1]	= (float)fabs( block.sell_2_price * 0.001 );
	pInfo->m_fSellVolume[1]	= (float)fabs( block.sell_2_volume * 100. );
	pInfo->m_fSellPrice[2]	= (float)fabs( block.sell_3_price * 0.001 );
	pInfo->m_fSellVolume[2]	= (float)fabs( block.sell_3_volume * 100. );
*/
}

void ConvertQLStockInfo( DWORD dwMarket, QL_Stock_info2_V304 & block, instrument_info *pInfo )
{
	pInfo->clear();

	char	code[sizeof(block.stock_code)+2];
	memset(code,0,sizeof(code));
	strncpy(code,(const char *)block.stock_code,sizeof(block.stock_code));

	char	name[sizeof(block.stock_name)+2];
	memset(name,0,sizeof(name));
	strncpy(name,(const char *)block.stock_name,sizeof(block.stock_name));

	//pInfo->SetStockCode( dwMarket, code );
	//pInfo->SetStockName( name );
	//pInfo->SetType( block.stock_type );

/*
	pInfo->m_fLast		= (float)fabs( block.last_close_price * 0.001 );
	pInfo->OpenPrice		= (float)fabs( block.open_price * 0.001 );
	pInfo->HighestPrice		= (float)fabs( block.high_price * 0.001 );
	pInfo->LowestPrice		= (float)fabs( block.low_price * 0.001 );
	pInfo->ClosePrice		= (float)fabs( block.close_price * 0.001 );
	pInfo->Volume	= (float)fabs( block.total_volume * 100. );
	pInfo->Turnover	= (float)fabs( block.total_value * 1000. );
	pInfo->m_fBuyPrice[0]	= (float)fabs( block.buy_1_price * 0.001 );
	pInfo->m_fBuyVolume[0]	= (float)fabs( block.buy_1_volume * 100. );
	pInfo->m_fBuyPrice[1]	= (float)fabs( block.buy_2_price * 0.001 );
	pInfo->m_fBuyVolume[1]	= (float)fabs( block.buy_2_volume * 100. );
	pInfo->m_fBuyPrice[2]	= (float)fabs( block.buy_3_price * 0.001 );
	pInfo->m_fBuyVolume[2]	= (float)fabs( block.buy_3_volume * 100. );
	pInfo->m_fSellPrice[0]	= (float)fabs( block.sell_1_price * 0.001 );
	pInfo->m_fSellVolume[0]	= (float)fabs( block.sell_1_volume * 100. );
	pInfo->m_fSellPrice[1]	= (float)fabs( block.sell_2_price * 0.001 );
	pInfo->m_fSellVolume[1]	= (float)fabs( block.sell_2_volume * 100. );
	pInfo->m_fSellPrice[2]	= (float)fabs( block.sell_3_price * 0.001 );
	pInfo->m_fSellVolume[2]	= (float)fabs( block.sell_3_volume * 100. );
*/
}

/////////////////////////////////////////////////////////////////////////////////////
// class	CQianlong
/*

struct QL_Info_data datainfo_sha[TYPE_NUM];
struct QL_Info_data datainfo_szn[TYPE_NUM];

unsigned char exepath[80];
unsigned char appdexe[88];

unsigned char ml_sys[80];
unsigned char ml_dat[80];
unsigned char ml_sh_day[80];
unsigned char ml_sz_day[80];
unsigned char ml_sh_min[80];
unsigned char ml_sz_min[80];
unsigned char ml_sh_wek[80];
unsigned char ml_sz_wek[80];
unsigned char ml_sh_mnt[80];
unsigned char ml_sz_mnt[80];

unsigned char sl_sys[80];
unsigned char sl_dat[80];
unsigned char sl_sh_day[80];
unsigned char sl_sz_day[80];
unsigned char sl_sh_min[80];
unsigned char sl_sz_min[80];
unsigned char sl_sh_wek[80];
unsigned char sl_sz_wek[80];
unsigned char sl_sh_mnt[80];
unsigned char sl_sz_mnt[80];

unsigned char hx_sys[80];
unsigned char hx_dat[80];
unsigned char hx_sh_day[80];
unsigned char hx_sz_day[80];
unsigned char hx_sh_min[80];
unsigned char hx_sz_min[80];
unsigned char hx_sh_wek[80];
unsigned char hx_sz_wek[80];
unsigned char hx_sh_mnt[80];
unsigned char hx_sz_mnt[80];

unsigned char dealpath[80];
unsigned char dealdat[80];
unsigned char tmpdir[80];
unsigned char sysdir[80];
unsigned char infdir[80];
unsigned char datdir[80];
unsigned char basdir[80];
unsigned char flag_mlv304;

unsigned long nowdate;
unsigned char datestr[10];
unsigned char flag_dynamic;
unsigned char ml30_flag;
unsigned char slon_flag;
unsigned char hxtw_flag;
*/

char ml_dat[]			= "data";
char ml_sh_info[]		= "data\\shinfo.dat";
char ml_sz_info[]		= "data\\szinfo.dat";
char ml_sh_now[]		= "data\\shnow.dat";
char ml_sz_now[]		= "data\\sznow.dat";
char ml_sh_pyjc[]		= "data\\shpyjc.dat";
char ml_sz_pyjc[]		= "data\\szpyjc.dat";
char ml_sh_trace[]		= "data\\shtrace.dat";
char ml_sz_trace[]		= "data\\sztrace.dat";
char ml_sh_minute[]		= "data\\shminute.dat";
char ml_sz_minute[]		= "data\\szminute.dat";

char ml_data[]			= "data\\";
char ml_sh[]			= "data\\sh\\";
char ml_sz[]			= "data\\sz\\";
char ml_base[]			= "bas\\";
char ml_month[]			= "mon\\";
char ml_week[]			= "wek\\";
char ml_day[]			= "day\\";
char ml_min5[]			= "nmn\\";

char ml_sh_base[]		= "data\\sh\\bas\\";
char ml_sz_base[]		= "data\\sz\\bas\\";
char ml_sh_month[]		= "data\\sh\\mon\\";
char ml_sz_month[]		= "data\\sz\\mon\\";
char ml_sh_week[]		= "data\\sh\\wek\\";
char ml_sz_week[]		= "data\\sz\\wek\\";
char ml_sh_day[]		= "data\\sh\\day\\";
char ml_sz_day[]		= "data\\sz\\day\\";
char ml_sh_min[]		= "data\\sh\\nmn\\";
char ml_sz_min[]		= "data\\sz\\nmn\\";

char ml_ext_base[]		= ".txt";
char ml_ext_month[]		= ".mnt";
char ml_ext_week[]		= ".wek";
char ml_ext_day[]		= ".day";
char ml_ext_min5[]		= ".nmn";

CQianlong::CQianlong( const char * rootpath, bool bOK )
{
	m_bIsOK	= false;
	memset( m_szRootPath, 0, sizeof(m_szRootPath) );
	if( !bOK )
	{
		if( GetAccurateRoot( rootpath, m_szRootPath, sizeof(m_szRootPath)-1 ) )
			m_bIsOK	= true;
		else
			m_bIsOK	= false;
	}
	else
	{
		strncpy( m_szRootPath, rootpath, sizeof(m_szRootPath)-1 );
		m_bIsOK	= true;
	}

	m_nVersion	= versionUnknown;
	if( bOK )
		m_nVersion	= DetectVersion( GetRootPath() );
}

CQianlong::~CQianlong( )
{
}

int	CQianlong::GetMaxNumber( )
{
	assert( m_bIsOK );
	if( ! m_bIsOK )	return 0;

	// load shinfo.dat szinfo.dat

	int	blocksize	= sizeof(struct QL_Stock_info_V302);
	if( version304 == m_nVersion )
		blocksize	= sizeof(struct QL_Stock_info2_V304);

	DWORD	dwCount	= 0;
	string	sFileName	= GetRootPath();
	sFileName	+= ml_sh_now;
	fstream	file;
	file.open( sFileName.c_str(), fstream::in);
	if( file.is_open() )
	{
		DWORD dwFileLen = /*file.tellg()*/0;
		dwCount	+= dwFileLen / blocksize;
		file.close();
	}
	sFileName	= GetRootPath();
	sFileName	+= ml_sz_now;
	file.open( sFileName.c_str(), fstream::app );
	if( file.is_open() )
	{
		DWORD dwFileLen = /*file.GetLength()*/0;
		dwCount	+= dwFileLen / blocksize;
		file.close();
	}
	return	dwCount;
}

int	CQianlong::LoadCodetable( instrument_container & container )
{
	assert( m_bIsOK );
	if( !m_bIsOK )	return 0;

	int maxsize = GetMaxNumber();
	container.resize( maxsize );
	instrument_info * pdata = &container[0];

	char	szShortName[QL_SHORTNAME_LEN+1];
	memset( szShortName, 0, sizeof(szShortName) );
	int	 nCount	= 0;
	string	sFileName	= GetRootPath();
	sFileName	+= ml_sh_now;
	std::fstream	file;
	file.open( sFileName.c_str(), fstream::in);
	if( file.is_open() )
	{
		// pin yin file
		string	sFileNamePyjc	= GetRootPath();
		sFileNamePyjc	+= ml_sh_pyjc;
		std::fstream	filePyjc;
		filePyjc.open( sFileNamePyjc.c_str(), fstream::in);
		
		if( version302 == m_nVersion )
		{
			struct QL_Stock_info_V302	block;
			while( nCount < maxsize 
					&& !file.read( (char*)&block, sizeof(block) ).eof() )
			{
				pdata[nCount].clear();
				ConvertQLStockInfo( instrument::marketSHSE, block, &(pdata[nCount]) );

				// read shortname
				//if( CFile::hFileNull != filePyjc.m_hFile && QL_SHORTNAME_LEN == filePyjc.Read( szShortName, QL_SHORTNAME_LEN ) )
				//	pdata[nCount].SetStockShortName( szShortName );

				nCount	++;
			}
		}
		else if( version304 == m_nVersion )
		{
			struct QL_Stock_info2_V304	block;
			while( nCount < maxsize 
					&& !file.read( (char*)&block, sizeof(block) ).eof() )
			{
				pdata[nCount].clear();
				ConvertQLStockInfo( instrument::marketSHSE, block, &(pdata[nCount]) );

				// read shortname
				//if( CFile::hFileNull != filePyjc.m_hFile && QL_SHORTNAME_LEN == filePyjc.Read( szShortName, QL_SHORTNAME_LEN ) )
				//	pdata[nCount].SetStockShortName( szShortName );

				nCount	++;
			}
		}

// 		if( CFile::hFileNull != filePyjc.m_hFile )
// 			filePyjc.Close();
		file.close();
	}

	sFileName	= GetRootPath();
	sFileName	+= ml_sz_now;
	file.open( sFileName.c_str(), fstream::in);
	if( file.is_open() )
	{
		// pin yin file
		string	sFileNamePyjc	= GetRootPath();
		sFileNamePyjc	+= ml_sz_pyjc;
		fstream	filePyjc;
		filePyjc.open( sFileNamePyjc.c_str(), fstream::in );

		if( version302 == m_nVersion )
		{
			struct QL_Stock_info_V302	block;
			while( nCount < maxsize 
					&& !file.read( (char*)&block, sizeof(block) ).eof() )
			{
				pdata[nCount].clear();
				ConvertQLStockInfo( instrument::marketSZSE, block, &(pdata[nCount]) );

				//// read shortname
				//if( CFile::hFileNull != filePyjc.m_hFile && QL_SHORTNAME_LEN == filePyjc.Read( szShortName, QL_SHORTNAME_LEN ) )
				//	pdata[nCount].SetStockShortName( szShortName );

				nCount	++;
			}
		}
		else if( version304 == m_nVersion )
		{
			struct QL_Stock_info2_V304	block;
			while( nCount < maxsize 
					&& !file.read( (char*)&block, sizeof(block) ) )
			{
				pdata[nCount].clear();
				ConvertQLStockInfo( instrument::marketSZSE, block, &(pdata[nCount]) );

				// read shortname
				//if( CFile::hFileNull != filePyjc.m_hFile && QL_SHORTNAME_LEN == filePyjc.Read( szShortName, QL_SHORTNAME_LEN ) )
				//	pdata[nCount].SetStockShortName( szShortName );

				nCount	++;
			}
		}
		file.close();
	}

	container.resize( nCount );
	return	nCount;
}

int	CQianlong::StoreCodetable( instrument_container & container )
{
	assert( false);
	return 0;
}

int	CQianlong::LoadKDataCache( instrument_container & container, PROGRESS_CALLBACK fnCallback, void *cookie, int nProgStart, int nProgEnd )
{
	assert( m_bIsOK );
	if( !m_bIsOK )	return 0;

	UINT nCacheDays = /*AfxGetProfile().GetCacheDays()*/5;

	// 读取行情缓存
	assert( nProgStart <= nProgEnd );
	int nCount = container.size();
	int	nCacheCount		= 0;
	int	nProgressSegment	= nCount / 25;
	double	dProgressRatio		= ( 0 == nCount ? 1 : (nProgEnd-nProgStart)/nCount );
	//for( int i=0; i<nCount; i++ )
	//{
	//	container.Lock();

	//	CInstrumentInfo	& info = container.ElementAt(i);
	//	
	//	DWORD	dwMarket	= info.GetMarket();
	//	CString	sCode	= info.GetStockCode();

	//	// Get Data From Day K Line
	//	CString	sFileName;
	//	GetFileName( sFileName, CInstrument::dataK, &info, ktypeDay );
	//	CFile	file;
	//	if( file.Open( sFileName, CFile::modeRead | CFile::shareDenyNone ) )
	//	{
	//		DWORD	dwFileLen	= file.GetLength();
	//		struct	QL_Data_day	qlkd;
	//		info.m_kdata.SetSize( 0, nCacheDays+3 );
	//		if( dwFileLen > sizeof(qlkd)*(nCacheDays+1) )
	//			file.Seek( dwFileLen-sizeof(qlkd)*(nCacheDays+1), CFile::begin );
	//		
	//		while( sizeof(qlkd) == file.Read( &qlkd, sizeof(qlkd) ) )
	//		{
	//			KDATA	kd;
	//			convert_QL_Data_day_to_KDATA( dwMarket, sCode, &qlkd, &kd );
	//			info.m_kdata.Add( kd );
	//		}
	//		file.Close();
	//		nCacheCount	++;

	//		if( fnCallback && !(nCacheCount % nProgressSegment) )
	//			fnCallback( PROG_PROGRESS, (int)(nProgStart+nCacheCount*dProgressRatio), NULL, cookie );
	//	}

	//	container.UnLock();
	//}

	return	nCount;
}

int CQianlong::LoadBasetable( instrument_container & container )
{
	assert( false );
	return 0;
}

int CQianlong::StoreBasetable( instrument_container & container )
{
	assert( false );
	return 0;
}

int CQianlong::LoadBaseText( instrument *pstock )
{
	assert( m_bIsOK && pstock && pstock->GetInstrumentInfo().is_valid() );
	if( ! m_bIsOK || ! pstock || !pstock->GetInstrumentInfo().is_valid() )	return 0;

	string	sFileName;
	GetFileName( sFileName, instrument::dataBasetext, &(pstock->GetInstrumentInfo()) );
	
	int	nCount	= 0;
	fstream 	file;
	file.open( sFileName.c_str(), fstream::in );
	if( file.is_open() )
	{
		DWORD dwLen = /*file.GetLength()*/0;
		if( pstock->AllocBaseTextMem( dwLen ) )
		{
			nCount = file.read( pstock->GetBaseTextPtr(), dwLen ).tellg();
		}
		file.close();
	}

	return nCount;
}

int CQianlong::LoadKData( instrument *pstock, int nKType )
{
	assert( m_bIsOK && pstock && pstock->GetInstrumentInfo().is_valid() );
	if( ! m_bIsOK || ! pstock || !pstock->GetInstrumentInfo().is_valid() )	return 0;

	kdata_container	*	pkdata	= NULL;
	//strcpy_s(dwMarket,pstock->GetInstrumentInfo().ExchangeID);
	const char*	sCode	= pstock->GetInstrumentID();

	string	sFileName;
	GetFileName( sFileName, instrument::dataK, &(pstock->GetInstrumentInfo()), nKType );
	
	switch( nKType )
	{
	case ktypeMonth:
		pkdata	= &(pstock->GetKDataMonth());
		break;
	case ktypeWeek:
		pkdata	= &(pstock->GetKDataWeek());
		break;
	case ktypeDay:
		pkdata	= &(pstock->GetKDataDay());
		break;
	case ktypeMin60:
		return	0;
	case ktypeMin30:
		return	0;
	case ktypeMin15:
		return	0;
	case ktypeMin5:
		pkdata	= &(pstock->GetKDataMin5());
		break;
	default:
		return 0;
	}

	if( kdata_container::IsDayOrMin(nKType) )
	{
		fstream	file;
		file.open( sFileName.c_str(), fstream::in|fstream::binary );
		if( pkdata && file.is_open() )
		{
            struct stat info;
            stat(sFileName.c_str(), &info);
			DWORD dwLen = info.st_size;
			struct	QL_Data_day		qlkd;
			int	nSize	= dwLen/sizeof(qlkd);
			
			int	nOldMaindataType = pkdata->GetMaindataType();
			pkdata->Clear();
			pkdata->SetKType( nKType );
			pkdata->SetMaindataType( nOldMaindataType );

			pkdata->resize( 0, nSize+1 );
			file.read( (char*)&qlkd, sizeof(qlkd) );
			while( sizeof(qlkd) == file.gcount()  )
			{
				KDATA	kd;
				convert_QL_Data_day_to_KDATA( pstock->GetInstrumentInfo().ExchangeID, sCode, &qlkd, &kd );
				pkdata->add( kd );
				file.read( (char*)&qlkd, sizeof(qlkd) );
			}

			file.close();
		}
	}
	else
	{
		fstream	file;
		file.open( sFileName.c_str(), fstream::in|fstream::binary ) ;
		if( pkdata && file.is_open())
		{
            struct stat info;
            stat(sFileName.c_str(), &info);
			DWORD dwLen = info.st_size;

			struct	QL_Data_5min		qlkd;
			int	nSize	= dwLen/sizeof(qlkd);
			
			int	nOldMaindataType = pkdata->GetMaindataType();
			pkdata->Clear();
			pkdata->SetKType( nKType );
			pkdata->SetMaindataType( nOldMaindataType );

			pkdata->resize( 0, nSize+1 );
			file.read( (char*)&qlkd, sizeof(qlkd) );
			while( sizeof(qlkd) == file.gcount() )
			{
				KDATA	kd;
				convert_QL_Data_5min_to_KDATA( pstock->GetInstrumentInfo().ExchangeID, sCode, &qlkd, &kd );
				pkdata->add( kd );
				file.read( (char*)&qlkd, sizeof(qlkd) );
			}

			file.close();
		}
	}

	return pkdata->size();
}

int CQianlong::LoadDRData( instrument *pstock )
{
	return 0;
}

int CQianlong::StoreDRData( instrument *pstock )
{
	return 0;
}

int CQianlong::LoadReport( instrument *pstock )
{
	// load shtrace.dat sztrace.dat
	return 0;
}

int CQianlong::LoadMinute( instrument *pstock )
{
	// load shminute.dat szminute.dat
	return 0;
}

int CQianlong::LoadOutline( instrument *pstock )
{
	return 0;
}

int	CQianlong::StoreReport( Tick * pReport, int nCount, bool bBigTrade )
{
	assert( false );
	return 0;
}

int	CQianlong::StoreMinute( MINUTE * pMinute, int nCount )
{
	assert( false );
	return 0;
}

int	CQianlong::StoreOutline( OUTLINE * pOutline, int nCount )
{
	assert( false );
	return 0;
}

int CQianlong::DetectVersion( const char * szRootPath )
{
	int	nRet	= versionUnknown;
	if( ! szRootPath )	return nRet;

	string	sFileName	= szRootPath;
	sFileName	+= ml_sh_now;
	fstream	file;
	file.open( sFileName.c_str(),fstream::in );
	if( file.is_open() )
	{
		int		nErrorCount	= 1;
		int		nTotalCount	= 1;

		struct QL_Stock_info_V302	block;
		while( /*sizeof(block) ==*/ !file.read( (char*)&block, sizeof(block) ) )
		{
			nTotalCount	++;
			if( block.data_id != 0 && block.data_id != 0xFF && block.data_id != 0x94 )
			{
				nErrorCount	++;
			}
		}

		if( (nErrorCount << 5) < nTotalCount )
		{
			nRet	= version302;
		}
		else
		{
			nErrorCount	= 1;
			nTotalCount	= 1;
			struct QL_Stock_info2_V304	block2;
			file.seekg(0);
			while( /*sizeof(block2) ==*/!file.read( (char*)&block2, sizeof(block2) ) )
			{
				nTotalCount	++;
				if( block2.data_id != 0 && block2.data_id != 0xFF )
				{
					nErrorCount	++;
				}
			}
			if( (nErrorCount << 5) < nTotalCount )
				nRet	= version304;
		}
		
		file.close();
	}
	
	return nRet;
}

bool CQianlong::GetAccurateRoot( const char * rootpath, char *accurateroot, int maxlen )
{
// 	if( 0 == rootpath || strlen(rootpath)==0 )
// 		return false;
// 
// 	// get rootpath
// 	CString	strRoot	= rootpath;
// 	int nLen = strRoot.GetLength();
// 	if( strRoot[nLen-1] != '\\' && strRoot[nLen-1] != '/' )
// 		strRoot	+= CHAR_DIRSEP;
// 	nLen = strRoot.GetLength();
// 
// 	//if( 0 != access( strRoot + ml_sh_now, 0 ) )
// 	//	return false;
// 
// 	//if( 0 != access( strRoot + ml_sh, 0 ) )
// 	//	return false;
// 	//if( 0 != access( strRoot + ml_sh_base, 0 ) )
// 	//	return false;
// 	//if( 0 != access( strRoot + ml_sh_day, 0 ) )
// 	//	return false;
// 	//if( 0 != access( strRoot + ml_sh_min, 0 ) )
// 	//	return false;
// 
// 	//strncpy( accurateroot, strRoot, maxlen-1);
// 	accurateroot[maxlen-1]	= '\0';

	return true;
}

int CQianlong::InstallCodetbl( const char * filename, const char *orgname )
{
// 	if( NULL == filename || strlen(filename) == 0
// 		|| NULL == orgname || strlen(orgname) == 0 )
// 		return 0;
// 
// 	CString	sFileName	= GetRootPath();
// 	sFileName	+= ml_dat;
// 	sFileName	+= orgname;
	return 0;
	//return CFile::CopyFile( CString(filename), sFileName, false );
}

int CQianlong::InstallCodetblBlock( const char * filename, const char *orgname )
{
	return 0;
}

int CQianlong::InstallCodetblFxjBlock( const char * filename, const char *orgname )
{
	return 0;
}

int CQianlong::InstallKData( kdata_container &kdata, bool bOverwrite )
{
	if (kdata.size()<=0)
	{
		return 0;
	}
    std::string nOpenFlags="r+b";
	
	

	int	nCount	= 0;
	
	string	sFileName;
	instrument_info	stockinfo;

	char*		dwMarket	= kdata.at(0).szExchange;
	char* sCode	= kdata.at(0).szCode;

	stockinfo.set_id( dwMarket, sCode );
	if( !GetFileName( sFileName, instrument::dataK, &stockinfo, kdata.GetKType() ) )
		return 0;
	if ( 0 != access(sFileName.c_str(), 0))
	{
		FILE* tryCreateFile=fopen(sFileName.c_str(),"a+");
		fclose(tryCreateFile);
	}
	//if( !bOverwrite )	nOpenFlags = "ab+";
	if( kdata_container::IsDayOrMin(kdata.GetKType()) )
	{
		FILE*		fileTo =fopen( sFileName.c_str(), nOpenFlags.c_str());
		if(fileTo)
		{
			for( size_t i=0; i<kdata.size(); i++ )
			{
				struct QL_Data_day	qlkdnew;
				convert_KDATA_to_QL_Data_day( &(kdata.at(i)), &qlkdnew );

				struct QL_Data_day	qlkd;
				memset( &qlkd, 0, sizeof(qlkd) );
				bool	bHas	= false;
				bool	bInsert	= false;
				//fileTo.read( (char*)&qlkd, sizeof(qlkd) );
				int rBytes=fread((char*)&qlkd, sizeof(qlkd),1,fileTo);
				while( sizeof(qlkd) == rBytes*sizeof(qlkd) )
				{
					if( qlkd.day_date == qlkdnew.day_date )
					{
						bHas	= true;
						fseek(fileTo,ftell(fileTo)-sizeof(qlkd),SEEK_SET);
						//fileTo.seekg( (int)fileTo.tellg()-sizeof(qlkd), fstream::beg );
						break;
					}
					if( qlkd.day_date > qlkdnew.day_date )
					{
						bInsert	= true;
						break;
					}
					//fileTo.read( (char*)&qlkd, sizeof(qlkd) );
					rBytes=fread((char*)&qlkd, sizeof(qlkd),1,fileTo);
				}
				if( bHas || !bInsert )
				{
					//fileTo.write( (char*)&qlkdnew, sizeof(qlkdnew) );
					fwrite((char*)&qlkdnew, sizeof(qlkdnew),1,fileTo);
				}
				else if( bInsert )
				{
					long nCur = ftell(fileTo);
					//fileTo.seekp(0,fstream::beg);
					fseek(fileTo,0,SEEK_SET);
					//fileTo.seekp(0,fstream::end);
					fseek(fileTo,0,SEEK_END);
					long nLen =ftell(fileTo);
					//int nLen = fileTo.tellp();
					fseek(fileTo,nCur,SEEK_SET);
					//fileTo.seekp(nCur,fstream::beg);
					
					if( nLen-nCur+1 > 0 )
					{
						char * pbuffer = new char[nLen-nCur+1];
						if( nLen - nCur > 0 )
						{
							//fileTo.read( pbuffer, nLen-nCur );
							fread(pbuffer,sizeof(char),nLen-nCur,fileTo);
						}
						fseek(fileTo,nCur-sizeof(qlkd),SEEK_SET);
						
						//fileTo.write( (char*)&qlkdnew, sizeof(qlkdnew) );
						fwrite( (char*)&qlkdnew, sizeof(qlkdnew),1,fileTo);
						//fileTo.write( (char*)&qlkd, sizeof(qlkd) );
						fwrite( (char*)&qlkd, sizeof(qlkd),1,fileTo);
						if( nLen - nCur > 0 )
						{
							//fileTo.write( pbuffer, nLen-nCur );
							fwrite(pbuffer,sizeof(char),nLen,fileTo);
						}
						delete [] pbuffer;
					}
					//fileTo.seekp( nCur, fstream::beg);
					fseek(fileTo,nCur,SEEK_SET);
				}
				nCount	++;
			}
			//fileTo.close();
			fclose(fileTo);
		}
	}
	else
	{
		//fstream		fileTo( sFileName.c_str(), nOpenFlags );
		FILE* fileTo=fopen(sFileName.c_str(),nOpenFlags.c_str());
		if (fileTo)
		{
				for( size_t i=0; i<kdata.size(); i++ )
				{
					struct QL_Data_5min	qlkdnew;
					convert_KDATA_to_QL_Data_5min( &(kdata.at(i)), &qlkdnew );

					struct QL_Data_5min	qlkd;
					memset( &qlkd, 0, sizeof(qlkd) );
					bool	bHas	= false;
					bool	bInsert	= false;
					int rBytes=fread( (char*)&qlkd, sizeof(qlkd),1,fileTo );
					while( sizeof(qlkd) == /*fileTo.gcount()*/rBytes*sizeof(qlkd) )
					{
						if( qlkd.min_off == qlkdnew.min_off )
						{
							bHas	= true;
							//fileTo.seekg( (int)fileTo.tellg()-sizeof(qlkd), fstream::beg);
							fseek(fileTo,ftell(fileTo)-sizeof(qlkd), SEEK_SET);
							break;
						}
						if( qlkd.min_off > qlkdnew.min_off )
						{
							bInsert	= true;
							break;
						}
						rBytes=fread( (char*)&qlkd, sizeof(qlkd),1,fileTo );
					}
					if( bHas || !bInsert )
					{
						fwrite((char*)&qlkdnew, sizeof(qlkdnew),1,fileTo);
						
					}
					else if( bInsert )
					{
						//int nCur = (int)fileTo.tellp();
						long nCur = ftell(fileTo);
						fseek(fileTo,0,SEEK_SET);
						fseek(fileTo,0,SEEK_END);
						//fileTo.seekp(0,fstream::beg);
						//fileTo.seekp(0,fstream::end);
						long nLen = ftell(fileTo);
						//fileTo.seekp(nCur,fstream::beg);
						fseek(fileTo,nCur,SEEK_SET);
						
						if( (nLen-nCur+1)>0)
						{
							char * pbuffer = new char[nLen-nCur+1];
							if( nLen - nCur > 0 )	
							{
								//fileTo.read( pbuffer, nLen-nCur );
								fread(pbuffer,sizeof(char),nLen-nCur,fileTo);
							}
							fseek(fileTo,nCur-sizeof(qlkd),SEEK_SET);
							fwrite((char*)&qlkdnew, sizeof(qlkdnew),1,fileTo);
							fwrite((char*)&qlkd, sizeof(qlkd),1,fileTo);
							//fileTo.seekp( nCur-sizeof(qlkd), fstream::beg );
							//fileTo.write( (char*)&qlkdnew, sizeof(qlkdnew) );
							//fileTo.write( (char*)&qlkd, sizeof(qlkd) );
							if( nLen - nCur > 0 )
							{
								//cout<<"INSERT: "<<sFileName<<"的 "<<ftell(fileTo)<<" 处写入:"<<ctp_time(qlkdnew.min_off).FormatStd("%H:%M:%S")<<endl;
								fwrite(pbuffer,sizeof(char),nLen-nCur,fileTo);
							}
							delete [] pbuffer;
						}
						fseek(fileTo,nCur,SEEK_SET);
						//fileTo.seekp( nCur, fstream::beg);
					}
					nCount	++;
				}
				//fileTo.close();
				fclose(fileTo);
		}
	}

	return nCount;
}

int CQianlong::InstallKDataTy( const char * stkfile, int nKType,
							PROGRESS_CALLBACK fnCallback, void *cookie )
{
	return 0;
// 	if( NULL == stkfile || strlen(stkfile) == 0 )
// 		return 0;
// 
// 	int	nTotalRecordCount	= 0;
// 	int	nProgCount	= 0;
// 	int	nCount		= 0;
// 
// 	CFile	file;
// 	if( file.Open( CString(stkfile), CFile::modeRead ) )
// 	{
// 		TYDAY_FHEADER	header;
// 		if( sizeof(header) == file.Read(&header,sizeof(header)) )
// 		{
// 			nTotalRecordCount	= header.recordcount;
// 			TYDAY_RECORD	block;
// 			kdata_container			kdata( nKType );
// 			CString		sCurCode;
// 			while( sizeof(block) == file.Read(&block,sizeof(block)) )
// 			{
// 				nProgCount	++;
// 				if( fnCallback && nTotalRecordCount > 0 )
// 					fnCallback( PROG_PROGRESS, DWORD(STKLIB_MAXF_PROGRESS*nProgCount/nTotalRecordCount), NULL, cookie );
// 
// 				KDATA	kdnew;
// 				if( kdata_container::IsDayOrMin(nKType) )
// 					convert_TYDAY_RECORD_to_KDATA( &block, &kdnew );
// 				else
// 					convert_TYDAY_RECORD_MIN_to_KDATA( &block, &kdnew );
// 
// 				//if( sCurCode.IsEmpty() )
// 				//	sCurCode	= kdnew.m_szCode;
// 
// 				//if( 0 == strcmp(kdnew.m_szCode,sCurCode) )
// 				//	kdata.Add( kdnew );
// 				//else
// 				//{
// 				//	nCount += InstallKData( kdata );
// 				//	kdata.RemoveAll();
// 
// 				//	kdata.Add( kdnew );
// 				//	sCurCode	= kdnew.m_szCode;
// 				//}
// 			}
// 			
// 			if( kdata.GetSize() > 0 )
// 			{
// 				nCount += InstallKData( kdata );
// 				kdata.RemoveAll();
// 			}
// 		}
// 		file.Close();
// 	}
// 
// 	if( fnCallback )
// 		fnCallback( PROG_PROGRESS, STKLIB_MAX_PROGRESS, NULL, cookie );
// 
// 	return nCount;
}

int CQianlong::InstallKDataFxj( const char * dadfile, int nKType,
							PROGRESS_CALLBACK fnCallback, void *cookie )
{
	return 0;
// 	if( NULL == dadfile || strlen(dadfile) == 0 )
// 		return 0;
// 
// 	int	nTotalStockCount	= 0;
// 	int	nProgCount	= 0;
// 	int	nCount		= 0;
// 
// 	CFile	file;
// 	if( file.Open( CString(dadfile), CFile::modeRead ) )
// 	{
// 		FXJDAY_FHEADER	header;
// 		if( sizeof(header) == file.Read(&header,sizeof(header)) )
// 		{
// 			nTotalStockCount	= header.m_dwStockCount;
// 			FXJDAY_RECORD	block;
// 			kdata_container			kdata( nKType );
// 			DWORD			dwMarket = CInstrument::marketUnknown;
// 			CString		sCurCode;
// 			while( sizeof(block) == file.Read(&block,sizeof(block)) )
// 			{
// 				if( -1 == block.m_dwMagic )
// 				{
// 					if( kdata.GetSize() > 0 )
// 						nCount += InstallKData( kdata );
// 					kdata.RemoveAll();
// 
// 					if( 'HS' == block.m_wMarket )
// 						dwMarket = CInstrument::marketSHSE;
// 					else if( 'ZS' == block.m_wMarket )
// 						dwMarket = CInstrument::marketSZSE;
// 					else
// 						dwMarket = CInstrument::marketUnknown;
// 
// 					char	code[sizeof(block.m_szCode)+2];
// 					memset( code, 0, sizeof(code) );
// 					strncpy( code, block.m_szCode, min(sizeof(code)-1,sizeof(block.m_szCode)) );
// 					sCurCode	= code;
// 
// 					nProgCount	++;
// 					if( fnCallback && nTotalStockCount > 0 )
// 						fnCallback( PROG_PROGRESS, DWORD(STKLIB_MAXF_PROGRESS*nProgCount/nTotalStockCount), NULL, cookie );
// 				}
// 				else
// 				{
// 					KDATA	kdnew;
// 					if( convert_FXJDAY_RECORD_to_KDATA( dwMarket, sCurCode, nKType, &block, &kdnew ) )
// 						kdata.Add( kdnew );
// 				}
// 			}
// 			
// 			if( kdata.GetSize() > 0 )
// 			{
// 				nCount += InstallKData( kdata );
// 				kdata.RemoveAll();
// 			}
// 		}
// 		file.Close();
// 	}
// 
// 	if( fnCallback )
// 		fnCallback( PROG_PROGRESS, STKLIB_MAX_PROGRESS, NULL, cookie );
// 
// 	return nCount;
}

// int CQianlong::InstallDRData( CDRData & drdata )
// {
// 	return 0;
// }

int CQianlong::InstallDRDataClk( const char * filename, const char *orgname )
{
	
	return 0;
}

int CQianlong::InstallDRDataFxj( const char * fxjfilename )
{
	return 0;
}

int CQianlong::InstallBasetable( const char * filename, const char * orgname )
{
	return 0;
}

int CQianlong::InstallBasetableTdx( const char * filename )
{
	return 0;
}

int CQianlong::InstallBasetableFxj( const char * filename )
{
	return 0;
}

int CQianlong::InstallBaseText( const char * filename, const char *orgname )
{
// 	if( NULL == filename || strlen(filename) == 0
// 		|| NULL == orgname || strlen(orgname) == 0 )
// 		return 0;
// 
// 	string	sCode	= orgname;
// 	int nIndex = sCode.Find( '.' );
// 	if( -1 != nIndex )
// 	{
// 		sCode	= sCode.Left(nIndex);
// 		CInstrumentInfo	stockinfo;
// 		//stockinfo.SetInstrumentID( CInstrument::marketUnknown, sCode );
// 		CString	sFileName;
// 		if( GetFileName( sFileName, CInstrument::dataBasetext, &stockinfo ) )
// 		{
// 			//return CFile::CopyFile( CString(filename), sFileName, false );
// 		}
// 	}
	
	return 0;
}

int CQianlong::InstallBaseText( const char * buffer, int nLen, const char *orgname )
{
// 	if( NULL == buffer || nLen <= 0
// 		|| NULL == orgname || strlen(orgname) == 0 )
// 		return 0;
// 
// 	CString	sCode	= orgname;
// 	int nIndex = sCode.Find( '.' );
// 	if( -1 != nIndex )
// 	{
// 		sCode	= sCode.Left(nIndex);
// 		CInstrumentInfo	stockinfo;
// 		//stockinfo.SetStockCode( CInstrument::marketUnknown, sCode );
// 		CString	sFileName;
// 		CFile		file;
// 		if( GetFileName( sFileName, CInstrument::dataBasetext, &stockinfo )
// 			&& file.Open(sFileName,CFile::modeWrite|CFile::modeCreate) )
// 		{
// 			file.Write( buffer, nLen );
// 			file.Close();
// 			return 1;
// 		}
// 	}
	
	return 0;
}

int CQianlong::InstallNewsText( const char * filename, const char *orgname )
{
	return 0;
}

int CQianlong::InstallNewsText( const char * buffer, int nLen, const char *orgname )
{
	return 0;
}

bool CQianlong::GetFileName( string &sFileName, int nDataType,
						instrument_info * pInfo, int nKType )
{
	if( NULL == pInfo || !pInfo->is_valid() )
		return false;

	// 确定市场类型
	//if( CInstrument::marketUnknown == pInfo->GetMarket() )
	//	pInfo->ResolveTypeAndMarket( );

	if( instrument::dataBasetext == nDataType )
	{
		sFileName	= GetRootPath();
		//if( pInfo->IsShenZhen() )
		//	sFileName	+= ml_sz_base;
		//else
		//	sFileName	+= ml_sh_base;
		//sFileName	+= CString(pInfo->GetStockCode()) + ml_ext_base;
		return true;
	}
	else if( instrument::dataK == nDataType )
	{
		sFileName	= GetRootPath();
		//if( pInfo->IsShenZhen() )
		//	sFileName	+= ml_sz;
		//else
		sFileName	+= ml_sh;
			
		switch( nKType )
		{
		case ktypeMonth:
			sFileName	+= string(ml_month) + pInfo->get_id() + ml_ext_month;
			break;
		case ktypeWeek:
			sFileName	+= string(ml_week) + pInfo->get_id() + ml_ext_week;
			break;
		case ktypeDay:
			sFileName	+= string(ml_day) + pInfo->get_id() + ml_ext_day;
			break;
		case ktypeMin60:
			return	false;
		case ktypeMin30:
			return	false;
		case ktypeMin15:
			return	false;
		case ktypeMin5:
			sFileName	+= string(ml_min5) + pInfo->get_id() + ml_ext_min5;
			break;
		default:
			return false;
		}
		return true;
	}
	else if( instrument::dataDR == nDataType )
	{
		return false;
	}

	return false;
}

int CQianlong::StoreKData( kdata_container & kdata, bool bOverwrite /*= false */ )
{
	return InstallKData(kdata,bOverwrite);
}

int CQianlong::Init(const string& initStr )
{
	return 0;
}
